package com.kp.core.app.razorpay.gatewayService;


import com.google.gson.Gson;
import com.kp.core.app.config.ErrorConfig;
import com.kp.core.app.config.PaymentConfig;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.razorpay.model.*;
import com.kp.core.app.razorpay.utils.ErrorsConstants;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Service
public class RazorPayAPIService {

    private final
    RestTemplate restTemplate;

    private final
    Gson gson;

    private PaymentConfig paymentConfig = ConfigFactory.create(PaymentConfig.class);

    private ErrorConfig errorConfig = ConfigFactory.create(ErrorConfig.class);

    @Autowired
    public RazorPayAPIService(RestTemplate restTemplate, Gson gson) {
        this.restTemplate = restTemplate;
        this.gson = gson;
    }

    public RzpValidateVPAResponseBO makeValidateVPACall(RzpValidateVPARequestBO requestBO, List<ErrorBO> errorBoList, String product) {

        RzpValidateVPAResponseBO responseBO = new RzpValidateVPAResponseBO();

        try {

            HttpHeaders headers = new HttpHeaders();
            headers.set("x-api-key", paymentConfig.paymentExternalApiKey());
            HttpEntity<RzpValidateVPARequestBO> request = new HttpEntity<>(requestBO, headers);

            log.info("ValidateVPA Request | VPA: {}  | Request String: {}", requestBO.getVpa(), gson.toJson(requestBO));

            String baseUrl = paymentConfig.PaymentBaseUrl();

            ResponseEntity response = restTemplate.postForEntity( baseUrl + "v1/payments/validate/vpa", request, String.class);

            log.info("ValidateVPA Response Using restTemplate: VPA: {}  | Response String: {}", requestBO.getVpa(), response.toString());

            if(response.getStatusCode().value()==200 && response.getBody()!=null) {

                responseBO = gson.fromJson(response.getBody().toString(), RzpValidateVPAResponseBO.class);

            }
            else {

                if (response.getStatusCodeValue() == 400 && response.getBody() != null) {

                    RzpErrorBO rzpErrorBO = gson.fromJson(response.getBody().toString(), RzpErrorBO.class);

                    if (rzpErrorBO != null && rzpErrorBO.getError() != null) {

                        String errorDesc = rzpErrorBO.getError().getDescription();

                        if (StringUtils.isNotEmpty(errorDesc) && errorDesc.contains("Invalid VPA"))
                            errorBoList.add(new ErrorBO("", product, "vpa", ErrorsConstants.INVALID_VPA, errorConfig.invalidVPA()));
                    }
                }
            }

            if(errorBoList.size()==0 && response.getStatusCode().value()!=200) {
                errorBoList.add(new ErrorBO("", product, "validate/vpa", ErrorsConstants.VALIDATE_VPA_API_FAIL, errorConfig.validateVPAApiFail()));
            }

            if (responseBO != null) {
                log.info("VPA : {} | ValidateVPA Status : {}", responseBO.getVpa(), responseBO.getSuccess());
            }

        } catch (Exception e) {
            log.error("ValidateVPA - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", product, "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }

        return responseBO;
    }

    public RzpOrderCreationResponseBO makeOrderCreationCall(RzpOrderCreationRequestBO requestBO, List<ErrorBO> errorBoList, String product) {

        RzpOrderCreationResponseBO responseBO = new RzpOrderCreationResponseBO();

        try {

            HttpHeaders headers = new HttpHeaders();
            headers.set("x-api-key", paymentConfig.paymentExternalApiKey());
            HttpEntity<RzpOrderCreationRequestBO> request = new HttpEntity<>(requestBO, headers);

            log.info("OrderCreation Request | PaymentId: {}  | Request String: {}", requestBO.getReceipt(), gson.toJson(requestBO));

            String baseUrl = paymentConfig.PaymentBaseUrl();

            ResponseEntity response = restTemplate.postForEntity(baseUrl + "v1/orders", request, String.class);

            log.info("OrderCreation Response | PaymentId: {}  | Response String: {}", requestBO.getReceipt(), response);

            if(response.getStatusCodeValue()==200 && response.getBody()!=null) {

                responseBO = gson.fromJson(response.getBody().toString(), RzpOrderCreationResponseBO.class);

            }
            else {

                if(response.getStatusCode().value()==400 && response.getBody()!=null) {

                    log.debug("OrderCreation Error Response : " + response.getBody().toString());
                }
                errorBoList.add(new ErrorBO("", product, "orders", ErrorsConstants.ORDER_CREATION_API_FAIL, errorConfig.orderCreationApiFail()));

            }

            if (responseBO != null) {
                log.info("OrderID : {} | Order Creation Status : {}", responseBO.getId(), responseBO.getStatus());
            }

        } catch (Exception e) {
            log.error("OrderCreation - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", product, "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }

        return responseBO;
    }

    public RzpInitiatePaymentResponseBO makeInitiatePaymentCall(RzpInitiatePaymentRequestBO requestBO, List<ErrorBO> errorBoList, String product) {

        RzpInitiatePaymentResponseBO responseBO = new RzpInitiatePaymentResponseBO();

        try {

            HttpHeaders headers = new HttpHeaders();
            headers.set("x-api-key", paymentConfig.paymentExternalApiKey());
            HttpEntity<RzpInitiatePaymentRequestBO> request = new HttpEntity<>(requestBO, headers);

            log.info("InitiatePayment Request | OrderId: {}  | Request String: {}", requestBO.getOrderId(), gson.toJson(requestBO));

            String baseUrl = paymentConfig.PaymentBaseUrl();

            ResponseEntity response = restTemplate.postForEntity(baseUrl + "v1/payments/create/upi", request, String.class);

            log.info("InitiatePayment Response | OrderId: {}  | Response String: {}", requestBO.getOrderId(), response);

            if(response.getStatusCodeValue()==200 && response.getBody()!=null) {

                responseBO = gson.fromJson(response.getBody().toString(), RzpInitiatePaymentResponseBO.class);

            }
            else {

                if(response.getStatusCode().value()==400 && response.getBody()!=null) {

                    log.debug("InitiatePayment Error Response : " + response.getBody().toString());

                    RzpErrorBO rzpErrorBO = gson.fromJson(response.getBody().toString(), RzpErrorBO.class);

                    if (rzpErrorBO != null && rzpErrorBO.getError() != null) {

                        String field = rzpErrorBO.getError().getField();

                        if (StringUtils.isNotEmpty(field) && field.contains("contact"))
                            errorBoList.add(new ErrorBO("", product, "contact", ErrorsConstants.INVALID_CONTACT_NO, errorConfig.invalidContactNo()));
                    }
                }
            }

            if(errorBoList.size()==0 && response.getStatusCode().value()!=200) {
                errorBoList.add(new ErrorBO("", product, "create/upi", ErrorsConstants.PAYMENT_INITIATE_API_FAIL, errorConfig.paymentInitiateApiFail()));
            }

            if (responseBO != null) {
                log.info("InitiatePayment : RazorPayPaymentId : {}", responseBO.getRazorpayPaymentId());
            }

        } catch (Exception e) {
            log.error("InitiatePayment - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", product, "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }

        return responseBO;
    }

    public RzpCustomerEditResponseBO makeCustomerEditCall(String rzpCustomerId, RzpCustomerEditRequestBO requestBO, List<ErrorBO> errorBoList, String product, String paymentMode) {

        RzpCustomerEditResponseBO responseBO = new RzpCustomerEditResponseBO();

        try {

            HttpHeaders headers = new HttpHeaders();
            headers.set("x-api-key", paymentConfig.paymentExternalApiKey());
            HttpEntity<RzpCustomerEditRequestBO> request = new HttpEntity<>(requestBO, headers);

            log.info("CustomerEdit Request | CustomerId: {}  | Request String: {}", rzpCustomerId, gson.toJson(requestBO));

            String baseUrl = paymentConfig.PaymentBaseUrl();

            ResponseEntity response = restTemplate.exchange(baseUrl + "v1/customers/" + rzpCustomerId, HttpMethod.PUT, request, String.class);

            log.info("CustomerEdit Response | CustomerId: {}  | Response String: {}", rzpCustomerId, response);

            if(response.getStatusCodeValue()==200 && response.getBody()!=null) {

                responseBO = gson.fromJson(response.getBody().toString(), RzpCustomerEditResponseBO.class);

            }
            else {

                if(response.getStatusCode().value()==400 && response.getBody()!=null) {

                    log.debug("CustomerEdit Error Response : " + response.getBody().toString());
                }
                errorBoList.add(new ErrorBO("", product, "orders", ErrorsConstants.CUSTOMER_EDIT_API_FAIL, errorConfig.customerEditApiFail()));

            }

            if (responseBO != null) {
                log.info("OrderID : {} | CustomerEdit Status : {}", responseBO.getId(), StringUtils.isNotEmpty(responseBO.getId()));
            }

        } catch (Exception e) {
            log.error("CustomerEdit - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", product, "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }

        return responseBO;
    }

    public RzpNetbankingPaymentStatusResponseBO makeNetBankingInitiatePaymentCall(RzpInitiatePaymentRequestBO requestBO, List<ErrorBO> errorBoList, String product) {

        RzpNetbankingPaymentStatusResponseBO responseBO = new RzpNetbankingPaymentStatusResponseBO();

        try {

            HttpHeaders headers = new HttpHeaders();
            headers.set("x-api-key", paymentConfig.paymentExternalApiKey());
            HttpEntity<RzpInitiatePaymentRequestBO> request = new HttpEntity<>(requestBO, headers);

            log.info("InitiatePayment Request | OrderId: {}  | Request String: {}", requestBO.getOrderId(), gson.toJson(requestBO));
            String baseUrl = paymentConfig.PaymentBaseUrl();

            ResponseEntity response = restTemplate.postForEntity(baseUrl + "v1/payments/create/json", request, String.class);

            log.info("InitiatePayment Response | OrderId: {}  | Response String: {}", requestBO.getOrderId(), response);

            if (response.getStatusCodeValue() == 200 && response.getBody() != null) {

                responseBO = gson.fromJson(response.getBody().toString(), RzpNetbankingPaymentStatusResponseBO.class);

            } else {

                if (response.getStatusCode().value() == 400 && response.getBody() != null) {

                    log.debug("InitiatePayment Error Response : " + response.getBody().toString());

                    RzpErrorBO rzpErrorBO = gson.fromJson(response.getBody().toString(), RzpErrorBO.class);

                    if (rzpErrorBO != null && rzpErrorBO.getError() != null) {

                        String field = rzpErrorBO.getError().getField();

                        // if (StringUtils.isNotEmpty(field) && field.contains("contact"))
                        errorBoList.add(new ErrorBO(requestBO.getNotes().getFi_payment_id(), product, field, rzpErrorBO.getError().getCode(), rzpErrorBO.getError().getDescription()));
                        //}
                    }
                }


                if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
                    errorBoList.add(new ErrorBO("", product, "create/netbanking", ErrorsConstants.PAYMENT_INITIATE_API_FAIL, errorConfig.paymentInitiateApiFail()));
                }

                if (responseBO != null) {
                    log.info("InitiatePayment : RazorPayPaymentId : {}", responseBO.getRazorpayPaymentId());
                }

            }
        }catch (Exception e) {
            log.error("InitiatePayment - Exception : {}", e);
            errorBoList.add(new ErrorBO("", product, "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }

        return responseBO;
    }


}

package com.kp.core.app.razorpay.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RzpVertualAccountCreationResponseBO {

    private String id;
    private String name;
    private String entity;
    private String status;
    private String description;
    private double amount_expected;
    private AccountCreationNotesBO notes;
    private int amount_paid;
    private String customer_id;
    private List<AccountCreationReceiversBO> receivers;
    private String close_by;
    private int closed_at;
    private String created_at;
    private AccountCreationErrorBO error;
    private List<AllowedPayer> allowed_payers;


    @Getter
    @Setter
    public static class AllowedPayer {
        private String type;
        private String id;
        private BankAccount bank_account;

        @Getter
        @Setter
        public static class BankAccount {
            private String ifsc;
            private String account_number;
        }
    }


    @Getter
    @Setter
    public static class AccountCreationNotesBO {
        private String product_type;

    }

    @Getter
    @Setter
    public static class AccountCreationReceiversBO {
        private String id;
        private String entity;
        private String ifsc;
        private String bank_name;
        private String name;
        private String[] notes;
        private String account_number;
        private boolean tpv_enabled;

    }

    @Getter
    @Setter
    public static class AccountCreationErrorBO {

        private String code;
        private String description;
        private String source;
        private String step;
        private String reason;
        private String field;
    }

}

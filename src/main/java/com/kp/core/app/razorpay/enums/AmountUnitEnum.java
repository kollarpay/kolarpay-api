package com.kp.core.app.razorpay.enums;

/**
 * Created by rpuvi on 9/1/21
 */
public enum AmountUnitEnum {
    paise, rupees
}

package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PowerStpUpdateBO {

    private List<String> actions;
    private String stpId;
    private String holdingProfileId;
    private String toSchemeCode;
    private double totalAmount;
    private String monthlyAmount;

}

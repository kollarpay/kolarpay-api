package com.kp.core.app.repository;

import com.kp.core.app.model.UserBO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by rpuvi on 7/9/20.
 */

@Repository
public interface UserRepository extends CrudRepository<UserBO, Long> {

    @Override
    Iterable<UserBO> findAll();

    @Override
    Optional<UserBO> findById(Long id);
}
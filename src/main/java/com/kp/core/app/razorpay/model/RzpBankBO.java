package com.kp.core.app.razorpay.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RzpBankBO {

    private String razorpayBankId;
    private String razorpayBankName;
    private String bz_bank_id;
    private String bz_bank_account_no;
    private String bz_bank_ifsc;

}

package com.kp.core.app.dao;

import com.google.gson.Gson;
import com.kp.core.app.model.MandateBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.Statement;
@Service
@Slf4j
public class MandateDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;
    @Autowired
    Gson gson;

    public void createMandate(MandateBo mandateBo, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert mandates");


        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery = " INSERT INTO mandates (customerid,formtype,frequency,recurringcount,upperlimit,start_at,expiry_by,active,created_user,created_at,updated_user,updated_at,from,remarks,comments) " +
                    " VALUES( ?, ?, ?, ?, ?,?,?,?,?, now(), ?, now(),?, ?, ?)  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, mandateBo.getCustomerId());
                ps.setString(2, mandateBo.getFormType());
                ps.setString(3, mandateBo.getFrequency());
                ps.setString(4, mandateBo.getRecurringCount());
                ps.setString(5, mandateBo.getUpperLimit());
                ps.setString(6, mandateBo.getStart_at());
                ps.setString(7, mandateBo.getExpiry_by());
                ps.setString(8, mandateBo.getActive());
                ps.setInt(9, userId);
                ps.setInt(10, userId);
                ps.setString(11, mandateBo.getFrom());
                ps.setString(12, mandateBo.getRemarks());
                ps.setString(13, mandateBo.getComments());
                return ps;

            }, keyHolder);
            String id;
            if (keyHolder.getKeys().size() > 1) {
                id = keyHolder.getKeys().get("id").toString();
                mandateBo.setId(id + "");
                mandateBo.setSubscriptionId(id+"");

            }

        } catch (Exception e) {
            log.error(logPrefix + "Exception in mandates insertion: ", e);
        }

        log.debug(logPrefix + "mandates id  generate: " + mandateBo.getId());
    }
}

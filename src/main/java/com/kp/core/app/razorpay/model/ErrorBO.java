package com.kp.core.app.razorpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kp.core.app.razorpay.utils.ErrorsConstants;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorBO {

    private String id = "";
    private String productCode = ErrorsConstants.RAZORPAY_PRODUCT_CODE;
    private String productType = "";
    private String code = "";
    private String field = "";
    private Object desc = "";

    public ErrorBO(String field, Object desc) {
        this.field = field;
        this.desc = desc;
    }

    public ErrorBO(String id, String field, Object desc) {
        this.id = id;
        this.field = field;
        this.desc = desc;
    }

    @Builder
    public ErrorBO(String id, String field, String code, Object desc) {
        this.id = id;
        this.code = code;
        this.field = field;
        this.desc = desc;
    }

    public ErrorBO(String id, String productType, String field, String code, Object desc) {
        this.id = id;
        this.productType = productType;
        this.code = code;
        this.field = field;
        this.desc = desc;
    }



}

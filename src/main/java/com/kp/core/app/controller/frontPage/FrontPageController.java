package com.kp.core.app.controller.frontPage;

        import com.kp.core.app.enums.ResponseDataObjectType;
        import com.kp.core.app.model.FrontPageBo;
        import com.kp.core.app.model.FrontPageProductBo;
        import com.kp.core.app.model.GlobalResponseBO;
        import com.kp.core.app.service.apps.FrontPageService;
        import com.kp.core.app.utils.APIGeneralUtil;
        import com.google.gson.Gson;
        import lombok.extern.slf4j.Slf4j;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.MediaType;
        import org.springframework.http.ResponseEntity;
        import org.springframework.util.MultiValueMap;
        import org.springframework.web.bind.annotation.*;




@RestController
@Slf4j
@RequestMapping("/frontpage")
public class FrontPageController {

    @Autowired
    FrontPageService frontPageService;

    @Autowired
    Gson gson;



    @PostMapping(path = "/collection-create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createCollection(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#front page create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "frontPage");

        int userId = 0;
        int bpid = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            bpid = Integer.parseInt(APIGeneralUtil.getHeader(headers, "bpid"));

            log.debug("#UserId : " + userId);

            if(userId > 0 && bpid>0) {

                FrontPageBo frontPageBo = gson.fromJson(payload, FrontPageBo.class);
                globalResponseBO = frontPageService.createFrontPageCollection(frontPageBo, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#create collection widget POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @PostMapping(path = "/product-create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createFrontPageProduct(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#front page create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "frontPage product");

        int userId = 0;
        int bpid = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            bpid = Integer.parseInt(APIGeneralUtil.getHeader(headers, "bpid"));

            log.debug("#UserId : " + userId);

            if(userId > 0 && bpid>0) {

                FrontPageProductBo frontPageProductBo = gson.fromJson(payload, FrontPageProductBo.class);
                globalResponseBO = frontPageService.createFrontPageProducts(frontPageProductBo, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#create collection widget POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> getAllTemplateCollection(@RequestHeader MultiValueMap<String, Object> headers) throws Exception {

        log.debug("#get all publish front page  POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "FrontPage");

        int userId = 0;
        int bpid = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            bpid = Integer.parseInt(APIGeneralUtil.getHeader(headers, "bpid"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = frontPageService.getAllFrontPage(userId,bpid);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#collection create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}
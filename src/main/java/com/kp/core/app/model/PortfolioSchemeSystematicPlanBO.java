package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortfolioSchemeSystematicPlanBO {

    //SIP,VIP,SWP
    private String schemeName;
    private int schemeCode;
    private int amount; //common
    private int tenure; //common
    private int changeAmount;
    private double rating;


    //STP,VTP
    private int fromSchemeCode;
    private int toSchemeCode;
    private String fromSchemeName;
    private  String toSchemeName;
    private String frequency;
    private int allocationAmount;
    private int proposedAllocation;
    private double percentageOfPortfolio;
    private int proposedAmount;
    private double fromSchemeRating;
    private double toSchemeRating;

    //SystematicPlans - SIP
    private String subCategory;

    //pdf
    private String currentAmountFormatted;
    private String proposedAmountFormatted;
    private String currentAllocationFormatted;
    private int ratingFormatted;
    private int toSchemeRatingFormatted;

}

package com.kp.core.app.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.kp.core.app.model.CustomerInfoBO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubscriptionMessage {
    private String message;
    private String status;
    private String subReferenceId;
    private String authLink;
    private String subStatus;
}

package com.kp.core.app.dao;


import com.kp.core.app.model.IFSCBO;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

public class ExcelRead {

   private static final String FILE_NAME = "/Users/selvendiran/Downloads/RBI-BANK-LIST-16Sep-2022.xlsx";
   // private static final String FILE_NAME = "/Users/selvendiran/Downloads/rbi-ifsc-sample.xlsx";

    public static void main(String[] args) {

        try {

            FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            ArrayList<IFSCBO> ifsclist=new ArrayList<>();
            int ii=0;
            while (iterator.hasNext()) {

                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();

                int colIndx=0;
                IFSCBO ifscbo=new IFSCBO();
                //System.out.println("iiiiii==="+ii);
                while (cellIterator.hasNext()) {

                    Cell currentCell = cellIterator.next();
                    String currentCellValue="";
                    if (currentCell.getCellType() == CellType.STRING) {
                        currentCellValue=currentCell.getStringCellValue();
                    } else if (currentCell.getCellType() == CellType.NUMERIC) {
                        currentCellValue= String.valueOf(currentCell.getNumericCellValue());
                       // System.out.println(currentCellValue+"---currentCellValue-- int---");
                    }
                    if(colIndx==0) ifscbo.setBankName(currentCellValue);
                    else if(colIndx==1)ifscbo.setIfscCode(currentCellValue);
                    else if(colIndx==2)ifscbo.setBranchName(currentCellValue);
                    else if(colIndx==3)ifscbo.setAddress(currentCellValue);
                    else if(colIndx==4)ifscbo.setCity(currentCellValue);
                    else if(colIndx==6) ifscbo.setState(currentCellValue);
                    else if(colIndx==8) ifscbo.setPhone(currentCellValue);

                    colIndx++;
                }
                System.out.println("city--"+ifscbo.getCity()+"<< ifscode:"+ifscbo.getIfscCode()+"bankname--"+ifscbo.getBankName()+"branchname--"+ifscbo.getBranchName()+"<< state:"+ifscbo.getState()+"<< address:"+ifscbo.getPhone());
                if(ii!=0)ifsclist.add(ifscbo);
                ii++;

            }
            insertIFSCs(ifsclist);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void insertIFSCs(ArrayList<IFSCBO> ifscbos) {
         final String url = "jdbc:postgresql://kolarpay-test.cenkk33n5cri.ap-south-1.rds.amazonaws.com:5432/kolarpay_test";
         final String user = "kolarpaytest";
         final String password = "wealth123";
        try {
            Connection conn = DriverManager.getConnection(url, user, password);
            String INSERT_IFSC_URL="insert into neftcode_rbi(BANK,ifsccode,branchname,address,city,state,phone) VALUES(?,?,?,?,?,?,?)";
            PreparedStatement statement = conn.prepareStatement(INSERT_IFSC_URL);
            int count = 0;

            for (IFSCBO ifscbo: ifscbos) {
                statement.setString(1, ifscbo.getBankName());
                statement.setString(2, ifscbo.getIfscCode());
                statement.setString(3, ifscbo.getBranchName());
                statement.setString(4, ifscbo.getAddress());
                statement.setString(5, ifscbo.getCity());
                statement.setString(6, ifscbo.getState());
                statement.setString(7, ifscbo.getPhone());

                statement.addBatch();
                count++;
                // execute every 100 rows or less
                if (count % 1000 == 0 || count == ifscbos.size()) {
                    statement.executeBatch();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
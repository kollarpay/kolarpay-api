package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "ifsccodes")
public class Ifsccodes {

    @Id
    private int id;
    private String bankname;
    @TextIndexed(weight = 1)
    private String ifsccode;
    private String micrcode;
    private String branchname;
    @TextIndexed(weight =2)
    private String address;
    @TextIndexed(weight =3)
    private String city;
    private String state;
    private String phone;
    @TextScore
    Float score;
    public Ifsccodes(String bankname, String ifsccode, String micrcode, String branchname, String address, String city, String state, String phone){
        this.bankname=bankname;
        this.ifsccode=ifsccode;
        this.micrcode=micrcode;
        this.branchname=branchname;
        this.address=address;
        this.city=city;
        this.state=state;
        this.phone=phone;

    }

}

package com.kp.core.app.dao;

import com.kp.core.app.model.*;
import com.kp.core.app.utils.BPConstants;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Repository
public class InvestorInfoDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;

    @Autowired
    private TransactionDAO transactionDAO;

    @Autowired
    Gson gson;

    /**
     * get list of holdingProfiles for given userId or holdingProfileId
     * @param userId
     * @param holdingProfileId
     * @param activateStatus
     * @return
     */
    public List<HoldingProfileBO> getHoldingProfileList(int userId, int holdingProfileId, String activateStatus) {
        
        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | #ActiveStatus : " + activateStatus + " | ";
        
        log.debug(logPrefix + "HoldingProfile List fetch");
        
        List<HoldingProfileBO> holdingProfileList =  new ArrayList<>();
        List<Integer> status;

        String appendUser = "", appendActivate = "", appendHpId = "";

        if(userId>0)
            appendUser = "AND IBD.UserID = " + userId;

        if (StringUtils.isNotEmpty(activateStatus) && BPConstants.ALLOW_TRANSACT_DEACTIVE.equalsIgnoreCase(activateStatus))
            appendActivate = " AND IHP.ALLOWTRANSACT= 'D' ";
        else if (StringUtils.isNotEmpty(activateStatus)  && BPConstants.ALLOW_TRANSACT_ACTIVE.equalsIgnoreCase(activateStatus))
            appendActivate = " AND IHP.ALLOWTRANSACT= 'A' ";

        if(holdingProfileId > 0)
            appendHpId = " AND IHP.HoldingProfileID = " + holdingProfileId + " ";

        
        try {
            
            String sqlQuery = "SELECT DISTINCT IBD.INVESTORID,IBD.INVESTORNAME, IBDA.INVESTORNAME AssociateInvestorName, IAI.RELATIONSHIPID, IBD.InvestorStatus, " +
                    " IHP.HOLDINGPROFILEID, IHP.ALLOWTRANSACT, IHP.NEWKYC, IBD.IsNRI, IBD.IsMinor, IAI.ASSOCIATEDINVESTORID, IBDA.IsMinor IsMinor_A, " +
                    " IBD.CONTACTPERSON, ISNULL(IBD.IRType, 'ON') AS IRType,DATEDIFF(year, ibd.dob, GETDATE() ) as age, EKYC.PAN AS EkycPAN, IBD.isBankVerified, " +
                    " CASE " +
                    "    WHEN (IHP.InvestmentLimit = 1 AND IBD.Provisional = 'P') THEN '-111' " +
                    "    ELSE IHP.InvestmentLimit END AS InvestmentLimit, " +
                    " CASE IBD.PAN " +
                    "    WHEN (SELECT PAN FROM InvestorBasicDetail WHERE InvestorID = IAI.PrimaryInvestorID AND (PAN LIKE 'MPAN-%' OR PAN IS NULL)) THEN '-'  " +
                    "    ELSE IBD.PAN END AS PAN," +
                    " CASE IBDA.PAN " +
                    "    WHEN (SELECT PAN FROM InvestorBasicDetail WHERE InvestorID = IAI.AssociatedInvestorID AND (PAN LIKE 'MPAN-%' OR PAN IS NULL)) THEN '-'  " +
                    "    ELSE IBDA.PAN END AS PAN_A," +
                    "    " +
                    " IA.Country, LU.lookUpDescription CountryName, IBD.PoliticalEP, IBD.CountryOfBirth, IBD.FatcaIPAddress, (CONVERT(VARCHAR(25), IBD.FatcaDate, 103)) AS FatcaDate " +
                    " FROM  " +
                    " INVESTORBASICDETAIL IBD " +
                    " INNER JOIN INVESTORADDITIONALINFO IAI ON IBD.INVESTORID = IAI.PRIMARYINVESTORID " + appendUser +
                    " INNER JOIN INVESTORBASICDETAIL IBDA ON IBDA.INVESTORID = IAI.ASSOCIATEDINVESTORID " +
                    " INNER JOIN  INVESTORADDRESS IA ON IA.InvestorID = IBD.InvestorID AND IA.InvestorType = 'MF' AND IA.AddressType = 'H' " +
                    " INNER JOIN INVESTORHOLDINGPROFILE IHP ON  IAI.HOLDINGPROFILEID = IHP.HOLDINGPROFILEID " + appendActivate + appendHpId +
                    " LEFT JOIN LookUp LU ON IA.Country = LU.lookUpId  AND LU.lookUpType = 'country' AND LU.Active = '1' " +
                    " LEFT JOIN EKYCInfo EKYC ON (IBD.IRTYPE IS NULL OR IBD.IRTYPE='ON' OR ibd.IRTYPE='') AND EKYC.Pan = IBD.PAN AND EKYC.InvestorID = IAI.PrimaryInvestorID " +
                    " AND EKYC.status = 'Y' AND EKYC.Flag = 'A' AND EKYC.TransactionID IS NOT NULL " +
                    " ORDER BY IHP.HOLDINGPROFILEID, IAI.RELATIONSHIPID";

            status = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> {

                HoldingProfileBO holdingProfileBO = new HoldingProfileBO();
                List<HoldingProfileBO.InvestorBO> investors = new ArrayList<>();
                String hpId = rs.getString("HOLDINGPROFILEID");
                String nameConcat = "";

                if(StringUtils.isNotEmpty(hpId)) {

                    String allowTransact = StringUtils.isNotEmpty(rs.getString("ALLOWTRANSACT"))? rs.getString("ALLOWTRANSACT").trim() : "";
                    int investmentLimit = rs.getInt("InvestmentLimit");

                    holdingProfileBO.setHoldingProfileId(hpId);

                    //IRTYPE
                    holdingProfileBO.setType(rs.getString("IRType").trim().equals("") ? BPConstants.ONLINE_REGISTRATION : rs.getString("IRType").trim());
                    if(StringUtils.isNotEmpty(rs.getString("EkycPAN")))
                        holdingProfileBO.setType(BPConstants.EKYC_REGISTRATION);

                    //NEW KYC flag
                    if(StringUtils.isEmpty(rs.getString("NEWKYC")))
                        holdingProfileBO.setKyc(-1);
                    else if(rs.getString("NEWKYC").trim().equalsIgnoreCase("Y"))
                        holdingProfileBO.setKyc(1);
                    else if(rs.getString("NEWKYC").trim().equalsIgnoreCase("N"))
                        holdingProfileBO.setKyc(0);

                    //Activated flag
                    holdingProfileBO.setActivated(false);

                    if(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && investmentLimit==-1) {

                        if(holdingProfileBO.getType().equalsIgnoreCase(BPConstants.ONLINE_ENROLLMENT) && StringUtils.isNotEmpty(rs.getString("isBankVerified"))
                                && rs.getString("isBankVerified").trim().equalsIgnoreCase("true"))
                            holdingProfileBO.setActivated(true);

                        else if(holdingProfileBO.getType().equalsIgnoreCase(BPConstants.ONLINE_REGISTRATION))
                            holdingProfileBO.setActivated(true);
                    }

                    holdingProfileBO.setMinor(rs.getInt("IsMinor") == 1);
                    holdingProfileBO.setNri(rs.getInt("IsNRI") == 1);
                    holdingProfileBO.setCountry(rs.getString("CountryName"));
                    holdingProfileBO.setTransactionLimit(investmentLimit);

                    //FATCA flag
                    int investorStatus = rs.getInt("InvestorStatus");
                    boolean validHufFourthLetter = false;
                    if(rs.getInt("InvestorStatus")==8)
                        validHufFourthLetter = hufFourthLetter(rs.getString("PAN"), rs.getInt("InvestorId"));

                    if ((StringUtils.isEmpty(rs.getString("PoliticalEP")) && investorStatus != 8)
                            || (StringUtils.isEmpty(rs.getString("CountryOfBirth")) && investorStatus != 8) || validHufFourthLetter)
                        holdingProfileBO.setFatca(false);
                    else
                        holdingProfileBO.setFatca(true);


                    HoldingProfileBO.InvestorBO investorBO = new HoldingProfileBO.InvestorBO();
                    investorBO.setCustomerId(rs.getString("ASSOCIATEDINVESTORID"));
                    investorBO.setHoldingProfileId(hpId);
                    investorBO.setInvestorName(rs.getString("AssociateInvestorName"));
                    investorBO.setMinor(rs.getInt("IsMinor_A") == 1);

                    String relationShipId = rs.getString("RELATIONSHIPID").trim();

                    //JOIN ACCOUNT MAPPING
                    //If not Primary Investor HodldingProfileName and HoldingProfilePAN to be concatenated
                    if (StringUtils.isNotEmpty(relationShipId) && !relationShipId.equalsIgnoreCase(BPConstants.PRIMARY_RELATIONSHIP_ID)) {

                        if (relationShipId.equalsIgnoreCase(BPConstants.GUARDIAN_RELATIONSHIP_ID))
                            nameConcat = " Rep by U/G ";
                        else
                            nameConcat = " / ";

                        //For each iteration remove the duplicate holdingProfiles from the list
                        HoldingProfileBO holdingProfileBO1 = holdingProfileList.stream().filter(profileBO -> profileBO.getHoldingProfileId().equals(hpId)).findAny().orElse(null);
                        if (holdingProfileBO1 != null) {

                            holdingProfileList.remove(holdingProfileBO1);
                            holdingProfileBO.setHoldingProfileName(holdingProfileBO1.getHoldingProfileName() + nameConcat + rs.getString("AssociateInvestorName"));
                            holdingProfileBO.setHoldingProfilePan(holdingProfileBO1.getHoldingProfilePan() + " / " + rs.getString("PAN_A"));

                            investors = holdingProfileBO1.getInvestors();
                        }
                        else {
                            holdingProfileBO.setHoldingProfileName(rs.getString("INVESTORNAME") + nameConcat + rs.getString("AssociateInvestorName"));
                            holdingProfileBO.setHoldingProfilePan(rs.getString("PAN") + " / " + rs.getString("PAN_A"));
                        }
                        holdingProfileBO.setJoint(true);

                        if(relationShipId.equalsIgnoreCase(BPConstants.GUARDIAN_RELATIONSHIP_ID)) {
                            HoldingProfileBO.InvestorBO primaryInvestorBO = investors.stream().filter(investorBO1 -> investorBO1.getType().equals(BPConstants.PRIMARY_RELATIONSHIP_DESC)).findAny().orElse(null);

                            if(primaryInvestorBO!=null) {

                                investors.remove(primaryInvestorBO);

                                HoldingProfileBO.Guardian guardian = new HoldingProfileBO.Guardian();
                                guardian.setCustomerId(rs.getString("ASSOCIATEDINVESTORID"));
                                guardian.setHoldingProfileId(hpId);
                                guardian.setInvestorName(rs.getString("AssociateInvestorName"));

                                investorBO = primaryInvestorBO;
                                investorBO.setGuardian(guardian);
                            }
                        }
                        //set second/third investor type
                        if(relationShipId.equalsIgnoreCase(BPConstants.SECONADARY_RELATIONSHIP_ID))
                            investorBO.setType(BPConstants.SECONADARY_RELATIONSHIP_DESC);
                        else if(relationShipId.equalsIgnoreCase(BPConstants.TERTIARY_RELATIONSHIP_ID))
                            investorBO.setType(BPConstants.TERTIARY_RELATIONSHIP_DESC);

                    }
                    else if(StringUtils.isNotEmpty(relationShipId) && relationShipId.equalsIgnoreCase(BPConstants.PRIMARY_RELATIONSHIP_ID)){
                        //for Primary Investors simply set the name & PAN
                        holdingProfileBO.setHoldingProfileName(rs.getString("INVESTORNAME"));
                        holdingProfileBO.setHoldingProfilePan(rs.getString("PAN"));
                        holdingProfileBO.setJoint(false);
                        investorBO.setType(BPConstants.PRIMARY_RELATIONSHIP_DESC);
                    }

                    investors.add(investorBO);
                    holdingProfileBO.setInvestors(investors);
                    holdingProfileList.add(holdingProfileBO);
                }

                return 1;
            });

            log.debug(logPrefix + "Result set size : " + status.size());

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }
        
        log.debug("Holding Profile list size : " + holdingProfileList.size());
        
        return holdingProfileList;
        
    }

    public boolean hufFourthLetter(String pan, int investorid) {
        boolean validHUFPan = false;
        String hufChar = "";
        try {
            hufChar = String.valueOf(pan.charAt(3));
            log.info("Pan 4th letter: " + hufChar);
            if (hufChar.equalsIgnoreCase("H")) {
                validHUFPan = getValidHUF(investorid);
            }
        } catch (Exception daoEx) {
            log.error(" # EX : " + daoEx);

        }
        return validHUFPan;
    }

    public boolean getValidHUF(int investorid) {

        boolean isValid = false;

        try {
            String presentQuery =" SELECT COUNT(InvestorId) from investorhuffacta where investorid=? and active='A' ";

            int isPresent = jdbcTemplate.queryForObject(presentQuery, new Object[]{investorid}, Integer.class);

            if(isPresent==0)
                isValid = true;

        } catch (Exception e) {
            log.error("Exception in Valid HUF check : ", e);
        }
        return isValid;
    }

    /**
     * get List of Bank account for given holdingProfileId
     * @param holdingProfileId
     * @param userId
     * @return
     */
    public List<BankInfoBO> getBankList(int holdingProfileId, int investorId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | #InvestorId : " + investorId + " | ";

        log.debug(logPrefix + "Bank List fetch");

        List<BankInfoBO> bankList =  new ArrayList<>();

        try {

            String appendQuery = "";
            if(userId!=0)
                appendQuery = " AND IBD.UserId = " + userId ;
            if(holdingProfileId!=0)
                appendQuery += " AND IAI.HoldingProfileID = " + holdingProfileId;
            if(investorId !=0)
                appendQuery += " AND IBD.InvestorId = " + investorId;

            String sqlQuery = " Select DISTINCT IB.UserBankId as BankId, IB.bankAccountType, IB.accountNumber, IB.IFSCCode, IB.BANKCATEGORY, IB.Active, " +
                              " IBD.InvestorId, IBD.InvestorName, IB.ModeOfHolding, WM.ReferenceId AS TPSLBankId, SI.SI_BankCode AS SI_TPSLBankId, " +
                              " IBD.InvestmentLimit, IBD.AllowTransact, IBD.isBankVerified, IBD.IrType, " +
                              " CASE   " +
                              "     WHEN IB.banklookupid = '999' THEN IB.bankname   " +
                              "     ELSE (SELECT lookupdescription FROM lookup WHERE lookupid = IB.banklookupid AND lookuptype = 'BANKLIST') " +
                              " END AS bankname  " +
                              " FROM InvestorAdditionalInfo IAI  " +
                              " JOIN InvestorBankInfo IB ON IB.Investor_ID = IAI.AssociatedInvestorID AND IB.InvestorType = 'MF'  " +
                              " JOIN InvestorBasicDetail IBD ON IBD.InvestorId = IB.Investor_ID " +
                              " LEFT JOIN WEALTHMAP WM ON IB.BankLookUpId = WM.MapId AND WM.LOOKUPTYPE = 'PAYBANK' " +
                              " LEFT JOIN LookUp LU ON LU.LookUpId = WM.ReferenceId AND LU.LOOKUPTYPE = 'NETBANKING' AND LU.Active>0 " +
                              " LEFT JOIN SI_ALLOWED_BANKS SI ON SI.BankLookUpId = IB.BankLookUpId AND SI.Active='A' AND VENDOR = 'TPSL_ENACH' " +
                              " WHERE IB.BANKLOOKUPID<>'' AND IB.BANKLOOKUPID IS NOT NULL " + appendQuery;

            bankList = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> {


                BankInfoBO bankInfoBO = setBankInfoBO(rs);
                //bankInfoBO.setTpslBankId(rs.getString("TPSLBankId"));
                return bankInfoBO;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        log.debug("Bank list size : " + bankList.size());

        return bankList;

    }

    public BankInfoBO getBankInfoUsingUserBankId(int userBankId, int investorId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #UserBankId : " + userBankId + " | #InvestorId : " + investorId + " | ";

        log.debug(logPrefix + "Bank Info fetch");

        BankInfoBO bankInfoBO = new BankInfoBO();

        try {

            String sqlQuery = " Select DISTINCT IB.UserBankId as BankId, IB.bankAccountType, IB.accountNumber, IB.IFSCCode, IB.BANKCATEGORY, IB.Active, " +
                    " IBD.InvestorId, IBD.InvestorName, IB.ModeOfHolding, WM.ReferenceId AS TPSLBankId, IB.MicrCode, SI.SI_BankCode AS SI_TPSLBankId, IB.banklookupid, " +
                    " IBD.InvestmentLimit, IBD.AllowTransact, IBD.isBankVerified, IBD.IrType, " +
                    " CASE   " +
                    "     WHEN IB.banklookupid = '999' THEN IB.bankname   " +
                    "     ELSE (SELECT lookupdescription FROM lookup WHERE lookupid = IB.banklookupid AND lookuptype = 'BANKLIST') " +
                    " END AS bankname  " +
                    " FROM InvestorBankInfo IB " +
                    " JOIN InvestorBasicDetail IBD ON IBD.InvestorId = IB.Investor_ID AND IB.InvestorType = 'MF' " +
                    " LEFT JOIN WEALTHMAP WM ON IB.BankLookUpId = WM.MapId AND WM.LOOKUPTYPE = 'PAYBANK' " +
                    " LEFT JOIN LookUp LU ON LU.LookUpId = WM.ReferenceId AND LU.LOOKUPTYPE = 'NETBANKING' AND LU.Active>0 " +
                    " LEFT JOIN SI_ALLOWED_BANKS SI ON SI.BankLookUpId = IB.BankLookUpId AND SI.Active='A' AND VENDOR = 'TPSL_ENACH' " +
                    " WHERE IB.BANKLOOKUPID<>'' AND IB.BANKLOOKUPID IS NOT NULL AND IB.UserBankId = ? AND IB.Investor_ID = ? " ;

            bankInfoBO = jdbcTemplate.queryForObject(sqlQuery, new Object[]{userBankId, investorId}, (rs, rowNumber) -> {

                BankInfoBO bankInfo = setBankInfoBO(rs);
                //bankInfo.setTpslBankId(rs.getString("TPSLBankId"));
               // bankInfo.setTpslSIBankId(rs.getString("SI_TPSLBankId"));
                bankInfo.setMicr(rs.getString("MicrCode"));
                bankInfo.setBankLookUpId(rs.getString("banklookupid"));
                return bankInfo;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        return bankInfoBO;

    }

    public List<FolioBankBO> getFolioBankList(int holdingProfileId, int investorId, String schemeCond, String amcCond, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | #InvestorId : " + investorId;

        log.debug(logPrefix + "Folio Bank List fetch");

        List<FolioBankBO> folioBankList =  new ArrayList<>();
        String appendCond = "";

        try {

            if(holdingProfileId>0)
                appendCond += " AND FBI.HoldingProfileID = "+ holdingProfileId;
            if(investorId>0)
                appendCond += " AND IB.Investor_ID = " + investorId;
            if(StringUtils.isNotEmpty(schemeCond)) {
                appendCond += " AND PI.SchemeCode IN " + schemeCond;
            }
            if(StringUtils.isNotEmpty(amcCond)) {
                appendCond += " AND ASD.Amc_Code IN " + amcCond;
            }

            String sqlQuery = " SELECT DISTINCT FBI.FolioNumber, PI.SchemeCode, ASD.Amc_Code, IB.UserBankId as BankId, IB.bankAccountType, IB.accountNumber, IB.IFSCCode, IB.BANKCATEGORY, IB.Active, " +
                              " IBD.InvestorId, IBD.InvestorName, IB.ModeOfHolding, SI.SI_BankCode AS SI_TPSLBankId, IBD.InvestmentLimit, IBD.AllowTransact, IBD.isBankVerified, IBD.IrType, " +
                              " CASE " +
                              "     WHEN IB.banklookupid = '999' THEN IB.bankname  " +
                              "     ELSE (SELECT lookupdescription FROM lookup WHERE lookupid = IB.banklookupid AND lookuptype = 'BANKLIST') " +
                              " END AS bankname   from dbo.FOLIO_BANK_INFO FBI" +
                              " JOIN InvestorAdditionalInfo IAI on IAI.HoldingProfileID = FBI.HoldingProfileID " +
                              " JOIN InvestorBasicDetail IBD ON IBD.InvestorId = IAI.AssociatedInvestorId " +
                              " JOIN InvestorBankInfo IB on IB.Investor_ID = IBD.InvestorId and FBI.UserBankId = IB.userBankId AND IB.InvestorType='MF' " +
                              " JOIN PositionsInfo PI ON PI.FolioNumber=FBI.FolioNumber" +
                              " JOIN AFT_Scheme_Details ASD ON ASD.SchemeCode = PI.SchemeCode" +
                              " JOIN InvestorHoldingProfile IHP ON IHP.HoldingProfileId=FBI.HoldingProfileID " +
                              " LEFT JOIN SI_ALLOWED_BANKS SI ON SI.BankLookUpId = IB.BankLookUpId AND SI.Active='A' AND VENDOR = 'TPSL_ENACH' " +
                              " WHERE IHP.UserId=? " + appendCond;

            folioBankList = jdbcTemplate.query(sqlQuery, new Object[]{userId}, (rs, rowNumber) -> {

                FolioBankBO folioBankBO = new FolioBankBO();

                BankInfoBO bankInfoBO = setBankInfoBO(rs);

                folioBankBO.setFolio(rs.getString("FolioNumber"));
                folioBankBO.setAmcCode(rs.getString("Amc_Code"));
                folioBankBO.setSchemeCode(rs.getString("SchemeCode"));
                folioBankBO.setBank(bankInfoBO);

                return folioBankBO;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        log.debug("FolioBank list size : " + folioBankList.size());

        return folioBankList;

    }

    public BankInfoBO setBankInfoBO(ResultSet rs) {

        BankInfoBO bankInfoBO = new BankInfoBO();
        List<String> mandateTypes = new ArrayList<>();
        try {

            String type = rs.getString("BANKCATEGORY");
            String accType = rs.getString("bankAccountType");
            String modeOfHolding = rs.getString("ModeOfHolding");

            bankInfoBO.setCustomerId(rs.getString("InvestorId"));
            bankInfoBO.setAccountHolderName(rs.getString("InvestorName"));
            bankInfoBO.setBankId(StringUtils.isNotEmpty(rs.getString("BankId")) ? rs.getString("BankId").trim() : "");
            bankInfoBO.setBankName(StringUtils.isNotEmpty(rs.getString("bankname")) ? rs.getString("bankname").trim() : "");
            bankInfoBO.setIfsc(StringUtils.isNotEmpty(rs.getString("IFSCCode")) ? rs.getString("IFSCCode").trim() : "");
            bankInfoBO.setAccountNo(StringUtils.isNotEmpty(rs.getString("accountNumber")) ? rs.getString("accountNumber") : "");

            if(StringUtils.isNotEmpty(modeOfHolding)) {
                switch (modeOfHolding.trim()) {
                    case "1":
                        bankInfoBO.setAccountType(BPConstants.MODE_OF_HOLDING_SINGLE);
                        break;
                    case "2":
                        bankInfoBO.setAccountType(BPConstants.MODE_OF_HOLDING_JOINT);
                        break;
                    case "3":
                        bankInfoBO.setAccountType(BPConstants.MODE_OF_HOLDING_ANYONE_SURVIVOR);
                        break;
                }
            }

            if(StringUtils.isNotEmpty(accType)) {
                switch (accType.trim()) {
                    case "1":
                        bankInfoBO.setBankAccountType(BPConstants.ACC_TYPE_CURRENT);
                        break;
                    case "2":
                        bankInfoBO.setBankAccountType(BPConstants.ACC_TYPE_SAVINGS);
                        break;
                    case "3":
                        bankInfoBO.setBankAccountType(BPConstants.ACC_TYPE_NRO);
                        break;
                    case "4":
                        bankInfoBO.setBankAccountType(BPConstants.ACC_TYPE_NRE);
                        break;
                }
            }

            if(StringUtils.isNotEmpty(type)) {

                type = type.trim();
                if(type.equalsIgnoreCase("P"))
                    bankInfoBO.setType(BPConstants.BANK_CATEGORY_PRIMARY);
                else if(type.equalsIgnoreCase("S"))
                    bankInfoBO.setType(BPConstants.BANK_CATEGORY_SECONDARY);
                else bankInfoBO.setType(BPConstants.BANK_CATEGORY_OTHERS);

                boolean investorActive = false;
                String allowTransact = rs.getString("AllowTransact");
                String irType = StringUtils.isEmpty(rs.getString("IrType")) ? BPConstants.ONLINE_REGISTRATION : rs.getString("IrType");

                if(StringUtils.isNotEmpty(allowTransact) && allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && rs.getInt("InvestmentLimit")==-1) {

                    if(irType.equalsIgnoreCase(BPConstants.ONLINE_ENROLLMENT) && StringUtils.isNotEmpty(rs.getString("isBankVerified"))
                            && rs.getString("isBankVerified").trim().equalsIgnoreCase("true"))
                       investorActive = true;

                    else if(irType.equalsIgnoreCase(BPConstants.ONLINE_REGISTRATION))
                        investorActive = true;
                }
                if ((type.equalsIgnoreCase("P") && investorActive) || (type.equalsIgnoreCase("S") && rs.getInt("Active") == 1))
                    bankInfoBO.setActivated(true);
                else
                    bankInfoBO.setActivated(false);
            }
            else
                bankInfoBO.setType(BPConstants.BANK_CATEGORY_OTHERS);

            mandateTypes.add(BPConstants.MANDATE_TYPE_NACH);
            if(StringUtils.isNotEmpty(rs.getString("SI_TPSLBankId")))
                mandateTypes.add(BPConstants.MANDATE_TYPE_ENACH);

            bankInfoBO.setMandateType(mandateTypes);

        } catch (Exception e) {
            log.error("Exception in setting BankInfoBO : ", e);
        }

        return bankInfoBO;
    }

    public List<InvestorGoalBO> getGoalList(String goalType, int userId) {

        String logPrefix = "#UserId : " + userId + " | #GoalType : " + goalType;

        log.debug(logPrefix + "Goal List Fetch");
        List<InvestorGoalBO> goalList = new ArrayList<>();
        try {

            String appendGoalType = "";
            if(StringUtils.isNotEmpty(goalType))
                appendGoalType = " AND IGD.GoalType = '" + goalType +"'";

            String sqlQuery = " SELECT PI.PortfolioID, PI.PortfolioName, IGD.GoalType, IGD.Amount, IGD.Target, IGD.AdjustedTarget, IGD.Inflation, IGD.TargetAmount, " +
                    " RPM.RiskType, IRP.Equity, IRP.Debt, IRP.Gold, IRP.Questionnaire " +
                    " FROM PortfolioInfo PI " +
                    " LEFT JOIN InvestorGoalDetails IGD ON IGD.PortfolioId = PI.PortfolioID AND IGD.Active=1 " +
                    " LEFT JOIN InvestorRiskProfile IRP ON IRP.GoalId = IGD.GoalId AND IRP.Active=1 " +
                    " LEFT JOIN RiskProfileMaster RPM ON RPM.RiskProfileId = IRP.RiskProfileId AND RPM.Active=1 " +
                    " WHERE PI.UserID = ? AND PI.Active = 1 " + appendGoalType ;

            goalList = jdbcTemplate.query(sqlQuery, new Object[]{userId}, (rs, rowNumber) -> {
                InvestorGoalBO investorGoalBO = new InvestorGoalBO();
                investorGoalBO.setGoalId(rs.getInt("PortfolioID"));
                investorGoalBO.setGoalName(StringUtils.isNotEmpty(rs.getString("PortfolioName")) ? rs.getString("PortfolioName") : "");
                investorGoalBO.setGoalType(StringUtils.isNotEmpty(rs.getString("GoalType"))? rs.getString("GoalType") : "");
                investorGoalBO.setAmount(rs.getDouble("Amount"));
                investorGoalBO.setTarget(StringUtils.isNotEmpty(rs.getString("Target")) ? rs.getString("Target") : "");
                investorGoalBO.setRiskType(StringUtils.isNotEmpty(rs.getString("RiskType")) ? rs.getString("RiskType") : "");
                investorGoalBO.setQuestionnaire(rs.getInt("Questionnaire")==1);

                InvestorGoalBO.AdjustedBO adjustedBO = new InvestorGoalBO.AdjustedBO();
                adjustedBO.setTarget(StringUtils.isNotEmpty(rs.getString("AdjustedTarget")) ? rs.getString("Target") : "");
                adjustedBO.setAmount(rs.getDouble("TargetAmount"));
                adjustedBO.setInflation(rs.getDouble("Inflation"));
                investorGoalBO.setAdjusted(adjustedBO);

                if(StringUtils.isNotEmpty(investorGoalBO.getRiskType()) && investorGoalBO.getRiskType().equalsIgnoreCase("custom")) {
                    InvestorGoalBO.CustomAssetBO customAssetBO = new InvestorGoalBO.CustomAssetBO();
                    customAssetBO.setEquity(rs.getDouble("Equity"));
                    customAssetBO.setDebt(rs.getDouble("Debt"));
                    customAssetBO.setGold(rs.getDouble("Gold"));
                    investorGoalBO.setCustomAssetAllocation(customAssetBO);
                }
                return investorGoalBO;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching goal list: ", e);
        }

        log.debug(logPrefix + "Goal List Size : " + goalList.size());
        return goalList;
    }

    public int createPortfolio(String portfolioName, String portfolioType, int userId) {

        String logPrefix = "#UserId : " + userId + " | #PortfolioName : " + portfolioName + " | #PortfolioType : " + portfolioType + " | ";

        log.debug(logPrefix + "Create Portfolio");

        int portfolioId = 0;

        try {
            int id = autoNumberGenerator.getAutoNumberGenerated("PortfolioID");

            String sqlQuery = " INSERT INTO PortfolioInfo(PortfolioId, UserId, PortfolioName, PortfolioType, Active, CreatedUser, CreatedDate, ModifiedUser, ModifiedDate) " +
                              " VALUES (?, ?, ?, ?, 1, ?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()))";

            int status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) preparedStatement -> {

                preparedStatement.setInt(1, id);
                preparedStatement.setInt(2, userId);
                preparedStatement.setString(3, portfolioName);
                preparedStatement.setString(4, portfolioType);
                preparedStatement.setInt(5, userId);
                preparedStatement.setInt(6, userId);

            });

            if(id!=0 && status>0)
                portfolioId = id;

        } catch (Exception e) {
            log.error(logPrefix + "Exception in Portfolio creation: ", e);
        }

        log.debug(logPrefix + "PortfolioId : " + portfolioId);
        return portfolioId;
    }


    public int deactivatePortfolio(int portfolioId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #PortfolioId : " + portfolioId + " | ";

        log.debug(logPrefix + "Deactivate Portfolio");
        int status = 0;

        try {

            String sqlQuery = " UPDATE PortfolioInfo SET Active=0, ModifiedUser=?, ModifiedDate=DATEADD(MI, 330, GETDATE()) WHERE PortfolioId=? AND UserId=? ";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) preparedStatement -> {
                preparedStatement.setInt(1, userId);
                preparedStatement.setInt(2, portfolioId);
                preparedStatement.setInt(3, userId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        log.debug(logPrefix + "Deactivate Portfolio status : " + status);
        return status;
    }

    public int createGoal(InvestorGoalBO investorGoalBO, InvestorGoalResponseBO responseBO, int portfolioId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #PortfolioId : " + portfolioId + " | ";

        log.debug(logPrefix + "Create Goal");

        int goalId = 0;

        try {
            int id = autoNumberGenerator.getAutoNumberGenerated("MFGoalId");

            String sqlQuery = " INSERT INTO InvestorGoalDetails(GoalId, PortfolioId, UserId, GoalName, GoalType, Amount, Target, AdjustedTarget, " +
                    " Inflation, TargetAmount, ProjectedAmount, CalculatedSIP, CalculatedOTI, CAGR, RiskType, Active, CreatedUser, CreatedDate, ModifiedUser, ModifiedDate) " +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, ?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()))";

            int status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) preparedStatement -> {

                preparedStatement.setInt(1, id);
                preparedStatement.setInt(2, portfolioId);
                preparedStatement.setInt(3, userId);
                preparedStatement.setString(4, investorGoalBO.getGoalName());
                preparedStatement.setString(5, investorGoalBO.getGoalType());
                preparedStatement.setDouble(6, investorGoalBO.getAmount());
                preparedStatement.setString(7, investorGoalBO.getTarget());
                preparedStatement.setString(8, investorGoalBO.getAdjusted().getTarget());
                preparedStatement.setDouble(9, investorGoalBO.getAdjusted().getInflation());
                preparedStatement.setDouble(10, investorGoalBO.getAdjusted().getAmount());
                preparedStatement.setDouble(11, responseBO.getProjectedAmount());
                preparedStatement.setDouble(12, responseBO.getCalculatedSipAmount());
                preparedStatement.setDouble(13, responseBO.getCalculatedOtiAmount());
                preparedStatement.setFloat(14, 0.0f);//Double.parseDouble(responseBO.getCagr()));
                preparedStatement.setString(15, investorGoalBO.getRiskType());
                preparedStatement.setInt(16, userId);
                preparedStatement.setInt(17, userId);

            });

            if(id!=0 && status>0)
                goalId = id;

        } catch (Exception e) {
            log.error(logPrefix + "Exception in goal creation: ", e);
        }

        log.debug(logPrefix + "GoalId : " + goalId);
        return goalId;
    }

    public int createRiskProfile(InvestorRiskProfileBO riskProfileBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Create RiskProfile");
        int status = 0;

        try {

            String sqlQuery = " INSERT INTO InvestorRiskProfile(UserId, GoalId, RiskProfileId, Equity, Debt, Gold, Questionnaire, Active, CreatedUser, CreatedDate, ModifiedUser, ModifiedDate) " +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, 1, ?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()))";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) preparedStatement -> {

                preparedStatement.setInt(1, userId);
                preparedStatement.setInt(2, riskProfileBO.getGoalId());
                preparedStatement.setInt(3, riskProfileBO.getRiskProfileId());
                preparedStatement.setDouble(4, riskProfileBO.getEquity());
                preparedStatement.setDouble(5, riskProfileBO.getDebt());
                preparedStatement.setDouble(6, riskProfileBO.getGold());
                preparedStatement.setInt(7, riskProfileBO.getQuestionnaire() ? 1 : 0);
                preparedStatement.setInt(8, userId);
                preparedStatement.setInt(9, userId);

            });
        } catch (Exception e) {
            log.error(logPrefix + "Exception in RiskProfile creation: ", e);
        }

        log.debug(logPrefix + "Status : " + status);
        return status;
    }

    public int insertCartDetailsMF(int cartId, int holdingProfileId, String holdingProfileName, String holdingProfilePan, SchemeCartBO.Schemes schemeBO, String transType, String mediumThru, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId +  " | #CartId : " + cartId + " | #TransType : " + transType + " | ";

        int status = 0;

        try {
            if(cartId==0)
                cartId = autoNumberGenerator.getAutoNumberGenerated("MFCartId");

            String sqlQuery = " IF EXISTS (SELECT CartId FROM InvestorCartDetails WHERE CartId=? AND ReferenceId=?) "
                            + " DELETE InvestorCartDetails WHERE CartId=? "
                            + " INSERT INTO InvestorCartDetails(CartId, ReferenceId, HoldingProfileId, HoldingProfileName, HoldingProfilePan, InvestmentType, InitialInvestmentFlag, TransType,"
                            + " PortfolioId, SchemeCode, FolioNumber, Amount, BankId, InvOption, DividendOption, SipType, SipDetails, Active, MediumThru, CreatedUser, CreatedDate, ModifiedUser, ModifiedDate) "
                            + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,?,DATEADD(MI, 330, GETDATE()),?,DATEADD(MI, 330, GETDATE()))";

            int id = cartId;

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {

                String sipDetails = null;
                SchemeCartBO.SipDetail sipDetail = new SchemeCartBO.SipDetail();

                if(StringUtils.isNotEmpty(schemeBO.getSipType())) {
                    if (schemeBO.getSipType().equalsIgnoreCase(BPConstants.REGULAR_SIP_TYPE))
                        sipDetail = schemeBO.getRegular();
                    else if (schemeBO.getSipType().equalsIgnoreCase(BPConstants.FLEXI_SIP_TYPE))
                        sipDetail = schemeBO.getFlexi();
                    else if (schemeBO.getSipType().equalsIgnoreCase(BPConstants.STEPUP_SIP_TYPE))
                        sipDetail = schemeBO.getStepup();
                    else if (schemeBO.getSipType().equalsIgnoreCase(BPConstants.ALERT_SIP_TYPE))
                        sipDetail = schemeBO.getAlert();

                    if (sipDetail != null) {
                        sipDetail.setSipDate(schemeBO.getSipDate());
                        sipDetails = sipDetail.toString();
                    }
                }

                ps.setInt(1, id);
                ps.setString(2, schemeBO.getId());
                ps.setInt(3, id);
                ps.setInt(4, id);
                ps.setString(5, schemeBO.getId());
                ps.setInt(6, holdingProfileId);
                ps.setString(7, holdingProfileName);
                ps.setString(8, holdingProfilePan);
                ps.setString(9, StringUtils.isEmpty(schemeBO.getSipType()) ? "OTI" : "SIP");
                ps.setInt(10, schemeBO.getPayment() ? 1 : 0);
                ps.setString(11, transType);
                ps.setInt(12, StringUtils.isNotEmpty(schemeBO.getGoalId()) ? Integer.parseInt(schemeBO.getGoalId()) : 0);
                ps.setInt(13, Integer.parseInt(schemeBO.getSchemeCode()));
                ps.setString(14, schemeBO.getFolio());
                ps.setInt(15, schemeBO.getAmount());
                ps.setInt(16, StringUtils.isNotEmpty(schemeBO.getBankId()) ? Integer.parseInt(schemeBO.getBankId()) : 0);
                ps.setString(17, schemeBO.getOption());
                ps.setString(18, schemeBO.getDividendOption());
                ps.setString(19, schemeBO.getSipType());
                ps.setString(20, sipDetails);
                ps.setString(21, mediumThru);
                ps.setInt(22, userId);
                ps.setInt(23, userId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        if(status!=0)
            status = cartId;
        return status;
    }

    public int insertCartDetailsEMandate(int cartId, int holdingProfileId, CreateMandataBO createMandataBO, String mediumThru, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert InvestorCartDetails - EMandate");
        int status = 0;

        try {

            if(cartId==0)
                cartId = autoNumberGenerator.getAutoNumberGenerated("MFCartId");

            String sqlQuery = " IF EXISTS (SELECT CartId FROM InvestorCartDetails WHERE CartId=?) " +
                              " DELETE InvestorCartDetails WHERE CartId=? " +
                              " INSERT INTO InvestorCartDetails(CartId, HoldingProfileId, InvestmentType, InvestorId, EMandateInfo, " +
                              " Active, MediumThru, CreatedUser, CreatedDate, ModifiedUser, ModifiedDate) " +
                              " VALUES (?, ?, ?, ?, ?, 1, ?, ?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()))";

            int id = cartId;

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) preparedStatement -> {

                preparedStatement.setInt(1, id);
                preparedStatement.setInt(2, id);
                preparedStatement.setInt(3, id);
                preparedStatement.setInt(4, holdingProfileId);
                preparedStatement.setString(5, "ENACH");
                preparedStatement.setInt(6, StringUtils.isNotEmpty(createMandataBO.getCustomerId()) ? Integer.parseInt(createMandataBO.getCustomerId()) : 0);
                preparedStatement.setString(7, createMandataBO.toString());
                preparedStatement.setString(8, mediumThru);
                preparedStatement.setInt(9, userId);
                preparedStatement.setInt(10, userId);

            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorCartDetails - EMandate insertion: ", e);
        }

        if(status==0)
            cartId = 0;

        log.debug(logPrefix + "Status : " + status);
        return cartId;
    }

    public int insertCartAppInfo(int cartId, AppInfoBO appInfoBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert InvestorCartAppInfo");
        int status = 0;

        try {

            String sqlQuery = " IF EXISTS (SELECT CartId FROM InvestorCartAppInfo WHERE CartId=?) " +
                    " DELETE InvestorCartAppInfo WHERE CartId=? " +
                    " INSERT INTO InvestorCartAppInfo(CartId, AppInfo, Active, CreatedUser, CreatedDate, ModifiedUser, ModifiedDate) " +
                    " VALUES (?, ?, 1, ?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()))";

            int id = cartId;

            status = jdbcTemplate.update(sqlQuery, preparedStatement -> {

                preparedStatement.setInt(1, id);
                preparedStatement.setInt(2, id);
                preparedStatement.setInt(3, id);
                preparedStatement.setString(4, appInfoBO.toString());
                preparedStatement.setInt(5, userId);
                preparedStatement.setInt(6, userId);

            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorCartDetails - EMandate insertion: ", e);
        }

        if(status==0)
            cartId = 0;

        log.debug(logPrefix + "Status : " + status);
        return cartId;
    }


    public SchemeCartBO fetchInvestorCart(int cartId, int userId, boolean initialInvestOnly) {

        String logPrefix = "#UserId : " + userId + " | #CartId : " + cartId + " | ";

        log.debug(logPrefix + "Cart scheme List Fetch");
        SchemeCartBO schemeCartBO = new SchemeCartBO();
        List<SchemeCartBO.Schemes> otiSchemeList = new ArrayList<>();
        List<SchemeCartBO.Schemes> sipSchemeList = new ArrayList<>();
        List<Integer> resultSet = new ArrayList<>();
        AtomicInteger sipAmount= new AtomicInteger();
        AtomicReference<String> createdDate = new AtomicReference<>();
        AtomicReference<String> modifiedDate = new AtomicReference<>();
        try {

            String sqlQuery = " SELECT DISTINCT ICD.CartId, ICD.ReferenceId, ICD.HoldingProfileId, ICD.HoldingProfileName, ICD.HoldingProfilePan, ICD.PortfolioId, ICD.InitialInvestmentFlag, " +
                    " ICD.FolioNumber, ICD.SchemeCode, ASD.S_Name, ICD.InvestmentType, ICD.Amount, ICD.BankId, ICD.InvOption, ICD.DividendOption, ICD.SipType, ICD.SipDetails, ICD.MediumThru, " +
                    " ICD.UserTransRefId, ICD.PaymentId, IHP.UserId, IGD.GoalType, IT.TransactionStatus, ITT.PaidThru, CONVERT(varchar(10),IT.CreatedDate,23) AS TransactionDate, " +
                    " ICD.ConsumerCode, ICD.EMandateInfo, ICD.InvestorId, CONVERT(varchar(25),ICD.CreatedDate,127) AS CreatedDate, CONVERT(varchar(25),ICD.ModifiedDate,127) AS ModifiedDate " +
                    " FROM InvestorCartDetails ICD " +
                    " JOIN InvestorHoldingProfile IHP ON IHP.HoldingProfileId = ICD.HoldingProfileId " +
                    " LEFT JOIN AFT_SCHEME_DETAILS ASD ON ASD.SchemeCode = ICD.SchemeCode" +
                    " LEFT JOIN InvestorGoalDetails IGD ON IGD.PortfolioId = ICD.PortfolioId AND IGD.Active = 1 " +
                    " LEFT JOIN InvestorTransaction IT ON IT.PaymentID = ICD.PaymentId " +
                    " LEFT JOIN InvestorTransactionTrack ITT ON ITT.UserTransRefID = ICD.UserTransRefId " +
                    " WHERE ICD.CartId = ? AND ICD.Active=1 ";

            resultSet = jdbcTemplate.query(sqlQuery, new Object[]{cartId}, (rs, rowNumber) -> {

                String investmentType = rs.getString("InvestmentType");

                schemeCartBO.setHoldingProfileId(String.valueOf(rs.getInt("HoldingProfileId")));
                schemeCartBO.setUserId(rs.getInt("UserId"));

                if(investmentType.equals("SIP") || investmentType.equals("OTI")) {
                    schemeCartBO.setHoldingProfileName(rs.getString("HoldingProfileName"));
                    schemeCartBO.setHoldingProfilePan(rs.getString("HoldingProfilePan"));
                    schemeCartBO.setPaymentId(rs.getString("PaymentId"));
                    schemeCartBO.setPaymentMode(StringUtils.isNotEmpty(rs.getString("PaidThru")) ? rs.getString("PaidThru").trim().toLowerCase() : null);
                    schemeCartBO.setTransactionDate(rs.getString("TransactionDate"));

                    String paidStatus = rs.getString("TransactionStatus");
                    if(StringUtils.isNotEmpty(paidStatus)) {

                        switch (paidStatus.trim()) {
                            case "TC":
                                schemeCartBO.setPaidStatus(BPConstants.PAID_STATUS_TC);
                                break;
                            case "TS":
                                schemeCartBO.setPaidStatus(BPConstants.PAID_STATUS_TS);
                                break;
                            case "PS":
                                schemeCartBO.setPaidStatus(BPConstants.PAID_STATUS_PS);
                                break;
                            case "PF":
                                schemeCartBO.setPaidStatus(BPConstants.PAID_STATUS_PF);
                                break;
                            case "PW":
                                schemeCartBO.setPaidStatus(BPConstants.PAID_STATUS_PW);
                                break;
                        }
                    }

                    if(!initialInvestOnly || rs.getInt("InitialInvestmentFlag") == 1) {
                        SchemeCartBO.Schemes scheme = new SchemeCartBO.Schemes();
                        scheme.setId(rs.getString("ReferenceId"));
                        scheme.setPayment(rs.getInt("InitialInvestmentFlag") == 1);
                        scheme.setSchemeName(rs.getString("S_Name"));
                        scheme.setSchemeCode(rs.getString("SchemeCode"));
                        scheme.setFolio(rs.getString("FolioNumber"));
                        scheme.setBankId(rs.getInt("BankId") == 0 ? null : String.valueOf(rs.getInt("BankId")));
                        scheme.setGoalId(String.valueOf(rs.getInt("PortfolioId")));
                        scheme.setGoalType(rs.getString("GoalType"));
                        scheme.setOption(rs.getString("InvOption"));
                        scheme.setDividendOption(rs.getString("DividendOption"));
                        scheme.setUserTransRefId(rs.getInt("UserTransRefId"));

                        if (rs.getString("InvestmentType").equals("SIP")) {
                            String sipType = rs.getString("SipType");
                            String sipDetails = rs.getString("SipDetails");

                            scheme.setSipType(sipType);

                            SchemeCartBO.SipDetail sipDetail = gson.fromJson(sipDetails, SchemeCartBO.SipDetail.class);

                            scheme.setAmount(sipDetail.getAmount());

                            sipAmount.addAndGet(sipDetail.getAmount());

                            if (sipType.equalsIgnoreCase(BPConstants.REGULAR_SIP_TYPE))
                                scheme.setRegular(sipDetail);

                            else if (sipType.equalsIgnoreCase(BPConstants.FLEXI_SIP_TYPE))
                                scheme.setFlexi(sipDetail);

                            else if (sipType.equalsIgnoreCase(BPConstants.ALERT_SIP_TYPE))
                                scheme.setAlert(sipDetail);

                            else if (sipType.equalsIgnoreCase(BPConstants.STEPUP_SIP_TYPE))
                                scheme.setStepup(sipDetail);

                            scheme.setSipDate(sipDetail.getSipDate());
                            scheme.setNextInstallmentOn(transactionDAO.getNextEcsDateFromUserTransRefId(scheme.getUserTransRefId(), scheme.getSchemeCode()));

                            sipSchemeList.add(scheme);

                        } else if (rs.getString("InvestmentType").equals("OTI")) {
                            scheme.setAmount(rs.getInt("Amount"));
                            otiSchemeList.add(scheme);
                        }
                    }
                }

                else if(investmentType.equals("ENACH")) {
                    CreateMandataBO mandataBO = null;
                    if (StringUtils.isNotEmpty(rs.getString("EMandateInfo"))) {
                        mandataBO = gson.fromJson(rs.getString("EMandateInfo"), CreateMandataBO.class);
                        mandataBO.setConsumerCode(rs.getString("ConsumerCode"));
                        mandataBO.setCustomerId(rs.getString("InvestorId"));
                    }
                    schemeCartBO.setEmandate(mandataBO);
                }

                createdDate.set(rs.getString("CreatedDate"));
                modifiedDate.set(rs.getString("ModifiedDate"));

                return 1;
            });

            if(otiSchemeList.size()>0 || sipSchemeList.size()>0) {
                SchemeCartBO.MFDataBO mfDataBO = new SchemeCartBO.MFDataBO();

                if (otiSchemeList.size() > 0) {
                    SchemeCartBO.Investment oti = new SchemeCartBO.Investment();
                    oti.setSchemes(otiSchemeList);
                    oti.setTotalAmount(otiSchemeList.stream().mapToInt(schemes -> schemes.getAmount()).sum());
                    mfDataBO.setOti(oti);
                }
                if (sipSchemeList.size() > 0) {
                    SchemeCartBO.Investment sip = new SchemeCartBO.Investment();
                    sip.setSchemes(sipSchemeList);
                    sip.setTotalAmount(sipAmount.get());
                    mfDataBO.setSip(sip);
                }

                schemeCartBO.setMf(mfDataBO);
            }
            else if(schemeCartBO.getEmandate()==null)
                cartId = 0;

            schemeCartBO.setAddedOn(createdDate.get());
            schemeCartBO.setUpdatedOn(modifiedDate.get());



        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching cart scheme list: ", e);
        }

        schemeCartBO.setId(String.valueOf(cartId));

        log.debug(logPrefix + "Cart Size : " + resultSet.size());
        return schemeCartBO;
    }

    public AppInfoBO fetchCartAppInfo(int cartId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #CartId : " + cartId + " | ";

        log.debug(logPrefix + " InvestorCartAppInfo Fetch");

        AppInfoBO appInfoBO = new AppInfoBO();

        try {
            String sqlQuery = " SELECT AppInfo FROM InvestorCartAppInfo WHERE CartId = ? AND Active = 1 ";

            appInfoBO = jdbcTemplate.queryForObject(sqlQuery, new Object[]{cartId}, (rs, rowNumber) -> {
                AppInfoBO appInfo = new AppInfoBO();

                String appInfoDetails = rs.getString("AppInfo");
                if(StringUtils.isNotEmpty(appInfoDetails))
                    appInfo = gson.fromJson(appInfoDetails, AppInfoBO.class);
                return appInfo;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching investor list: ", e);
        }

        return appInfoBO;
    }

    public int deactivateCart(int cartId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #CartId : " + cartId + " | ";

        int status = 0;

        try {

            String sqlQuery = " UPDATE InvestorCartDetails SET Active = 0, ModifiedDate = DATEADD(MI, 330, GETDATE()) WHERE CartId=? and ModifiedUser = ?";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {
                ps.setInt(1, cartId);
                ps.setInt(2, userId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in cart deactivate: ", e);
        }
        return status;
    }

    public boolean isCartAvailable(int cartId) {

        boolean isCartAvailable = false;

        try {
            String presentQuery =" SELECT COUNT(CartId) FROM InvestorCartDetails WHERE CartId=? AND Active=1 ";
            int isPresent = jdbcTemplate.queryForObject(presentQuery, new Object[]{cartId}, Integer.class);

            if(isPresent!=0)
                isCartAvailable = true;

        } catch (Exception e) {
            log.error("Exception in cart availability check : ", e);
        }
        return isCartAvailable;
    }

    public boolean isCartUpdateRetricted(int cartId) {

        boolean restrict = false;

        try {
            String presentQuery =" SELECT COUNT(CartId) FROM InvestorCartDetails " +
                                 " WHERE CartId=? AND Active=1 AND ((PaymentId IS NOT NULL AND PaymentId<>0) OR (ConsumerCode IS NOT NULL AND ConsumerCode <> ''))";

            int isPresent = jdbcTemplate.queryForObject(presentQuery, new Object[]{cartId}, Integer.class);

            if(isPresent!=0)
                restrict = true;

        } catch (Exception e) {
            log.error("Exception in cart availability check : ", e);
        }
        return restrict;
    }

    public HashMap<String, String> getInvestorDetails(int holdingProfileId, String bankId) {

        HashMap<String, String> invDetails = new HashMap<>();

        try {

            String sqlQuery = " SELECT DISTINCT IBD.ISNRI,IBI.BANKACCOUNTTYPE,IBD.ISMINOR, IBD.IRTYPE, IBD.NATIONALITY, IA.COUNTRY " +
                    " FROM INVESTORADDITIONALINFO IAI " +
                    " JOIN INVESTORBANKINFO IBI ON IBI.INVESTOR_ID = IAI.ASSOCIATEDINVESTORID AND IAI.RELATIONSHIPID = 'F' AND IBI.INVESTORTYPE='MF' " +
                    " JOIN INVESTORBASICDETAIL IBD ON IBI.INVESTOR_ID = IBD.INVESTORID " +
                    " JOIN INVESTORHOLDINGPROFILE IHP ON IAI.HOLDINGPROFILEID = IHP.HOLDINGPROFILEID " +
                    " LEFT JOIN InvestorAddress IA ON IA.InvestorId = IBD.INVESTORID AND IA.INVESTORTYPE = 'MF' AND IA.ADDRESSTYPE IN ('H', 'C') " +
                    " WHERE IBI.UserBankId = ? AND IHP.HOLDINGPROFILEID = ? AND IBI.Active=1 ";
            invDetails = jdbcTemplate.queryForObject(sqlQuery, new Object[]{bankId, holdingProfileId}, (rs, rowNumber) -> {

                HashMap<String, String> result = new HashMap<>();
                result.put("isNri", rs.getString("ISNRI").trim());
                result.put("bankAccType", rs.getString("BANKACCOUNTTYPE").trim());
                result.put("isMinor", rs.getString("ISMINOR").trim());
                result.put("irType", rs.getString("IRTYPE").trim());
                result.put("nationality", rs.getString("NATIONALITY").trim());
                result.put("country", rs.getString("COUNTRY").trim());

                return result;
            });
        } catch (Exception e) {
            log.error("Exception in getting some investor details : ", e);
        }

        return invDetails;
    }

    public UserBO getInvestorDetail(int investorId, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + " Investor details");

        UserBO userInfo = new UserBO();

        try {
            String sqlQuery = " SELECT DISTINCT IBD.InvestorId, IBD.InvestorName, IBD.IrType, UI.Email, IA.Mobile " +
                    " FROM UserInfo UI " +
                    " JOIN InvestorBasicDetail IBD on IBD.UserID = UI.userId and IBD.AllowTransact = 'A' and IBD.InvestmentLimit = -1 " +
                    " join InvestorAddress IA on IA.InvestorID = IBD.InvestorID and IA.MailingAddress = IA.AddressType and IA.InvestorType = 'MF'" +
                    " WHERE IBD.UserID = ? AND IBD.InvestorId = ? ";

            userInfo = jdbcTemplate.queryForObject(sqlQuery, new Object[]{userId, investorId}, (rs, rowNumber) -> {
                UserBO userBo = new UserBO();
                userBo.setId(rs.getInt("InvestorId"));
                userBo.setName(rs.getString("InvestorName"));
                userBo.setEmail(rs.getString("Email"));
                userBo.setMobile(rs.getString("Mobile"));
                userBo.setIrType(rs.getString("IrType"));
                return userBo;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching investor list: ", e);
        }

        return userInfo;
    }

    public int getPrimaryInvestorId(int holdingprofileId) {

        String logPrefix = "#HoldingProfileId : " + holdingprofileId + " | ";

        log.debug(logPrefix + "Primary InvestorId");

        int primaryInvestorId = 0;

        try {
            String sqlQuery = " SELECT IAI.PrimaryInvestorId FROM InvestorAdditionalInfo IAI " +
                              " JOIN InvestorHoldingProfile IHP ON IHP.HoldingProfileId = IAI.HoldingProfileId AND IAI.RelationShipId = 'F'" +
                              " WHERE IHP.HoldingprofileId = ? ";

            primaryInvestorId = jdbcTemplate.queryForObject(sqlQuery, new Object[]{holdingprofileId}, Integer.class);

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching primary investorId: ", e);
        }

        return primaryInvestorId;
    }

    public UserInfoBO getUserInfo(int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "User details fetch");

        UserInfoBO userInfoBO = new UserInfoBO();
        List<CustomerInfoBO> investorList =  new ArrayList<>();
        AtomicInteger i= new AtomicInteger();

        try {


            String sqlQuery = " SELECT DISTINCT UI.UserId, UI.Name, UI.Email, UI.MobileNumber, UI.bpid, UI.UserType, cbd.surname, cbd.DOB, " +
                              " cbd.customerId, cbd.PAN,cbd.allow_trans, cbd.customerLimit, cbd.IsMinor, cbd.Gender" +
                              " FROM USERINFO UI " +
                              " INNER JOIN customerbasicdetail cbd ON cbd.userid = UI.userId " +
                             // " LEFT JOIN LOOKUP L ON L.LookUpId = IBD.Nationality AND L.LookUpType = 'Nationality' " +
                             // " LEFT JOIN InvestorRiskProfile IRP ON IRP.UserId = UI.UserId AND IRP.Active=1 " +
                            //  " LEFT JOIN UserAuthToken UA ON UA.UserId = UI.UserId AND UA.Flag='A' " +
                              " WHERE UI.Flag='A' AND UI.UserId=? ";

            investorList = jdbcTemplate.query(sqlQuery, new Object[]{userId}, (rs, rowNumber) -> {

                if(i.get() ==0) {

                    i.getAndIncrement();

                    userInfoBO.setUserId(userId);
                    userInfoBO.setEmailId(rs.getString("Email"));
                    userInfoBO.setMobile(rs.getString("MobileNumber"));
                    userInfoBO.setName(rs.getString("Name"));
                    userInfoBO.setPId(rs.getInt("bpid"));
                    userInfoBO.setToken(rs.getString("UserCode"));
                    userInfoBO.setManagementUserId(userId);
                    if (rs.getString("UserType").equals("RU"))
                        userInfoBO.setManagementUserRoles("user");
                    else if (rs.getString("UserType").equals("AD"))
                        userInfoBO.setManagementUserRoles("admin");
                    else
                        userInfoBO.setManagementUserRoles("partner");
                    userInfoBO.setUserType(new IdDesc(rs.getString("UserType").trim(), ""));
                }

                String allowTransact = StringUtils.isNotEmpty(rs.getString("allow_trans"))? rs.getString("ALLOWTRANSACT").trim() : "";
                int customerLimit = rs.getInt("customerLimit");

                CustomerInfoBO customerInfoBO = new CustomerInfoBO();
                customerInfoBO.setName(rs.getString("surname"));
                customerInfoBO.setCustomerId(rs.getString("customerId"));
                customerInfoBO.setPan(rs.getString("PAN"));
                customerInfoBO.setEmail(rs.getString("Email"));
                customerInfoBO.setMobile(rs.getString("MobileNumber"));
                customerInfoBO.setActivated(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && customerLimit!=49999);
                customerInfoBO.setDateOfBirth(rs.getString("DOB"));
                customerInfoBO.setGender((StringUtils.isNotEmpty(rs.getString("Gender")) && rs.getString("Gender").equals("1M")) ? "Male" : "Female");
                //customerInfoBO.setBanks(getBankList(0, customerInfoBO.getCustomerId(), userId));
                return customerInfoBO;
            });

            userInfoBO.setCustomers(investorList);

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        log.debug("Investor list size : " + investorList.size());

        return userInfoBO;
    }

    public boolean isAadhaarInvestor(int holdingProfileID) {

        String logPrefix = "#HoldingProfileId : " + holdingProfileID + " | ";
        log.debug(logPrefix + " check is Aadhaar Investor");
        String status;
        boolean isAadhaarInvestor = false;

        try {
            String presentQuery =" SELECT ekyc.status from InvestorAdditionalInfo iai " +
                    " join InvestorBasicDetail ibd on iai.PrimaryInvestorID = ibd.InvestorID " +
                    " join EKYCInfo ekyc on ibd.InvestorID = ekyc.InvestorID and ibd.UserID = ekyc.UserId " +
                    " and ibd.PAN = ekyc.Pan and (isnull(ibd.irType,'') = '' or ibd.irType='ON') " +
                    " and ekyc.Flag = 'A' and isnull(ekyc.TransactionID,'') <> '' and ekyc.status = 'y' " +
                    " where iai.HoldingProfileID = ? ";

            status = jdbcTemplate.queryForObject(presentQuery, new Object[]{holdingProfileID}, String.class);

            if(StringUtils.isNotEmpty(status) && status.trim().equalsIgnoreCase("y"))
                isAadhaarInvestor = true;


        } catch (Exception e) {
            log.error(logPrefix + "check is Aadhaar Investor Exception : ", e);
        }
        log.debug(logPrefix + "Is Aadhaar Investor : " + isAadhaarInvestor);
        return isAadhaarInvestor;
    }

    public boolean isFirstInvestmentForON(int holdingProfileID) {

        String logPrefix = "#HoldingProfileId : " + holdingProfileID + " | ";
        log.debug(logPrefix + "check is First Investment");
        int status;
        boolean isFirstInvestment = false;

        try {
            String presentQuery =" SELECT COUNT(DISTINCT P.PositionID) AS rescount " +
                    " FROM InvestorAdditionalInfo IAI " +
                    " INNER JOIN InvestorAdditionalInfo IAI2 ON IAI.PrimaryInvestorId = IAI2.AssociatedInvestorID AND IAI2.RelationshipID='F' " +
                    " INNER JOIN InvestorAdditionalInfo IAI3 ON IAI2.AssociatedInvestorID = IAI3.PrimaryInvestorId " +
                    " INNER JOIN PositionsInfo P ON P.HOLDINGPROFILEID = IAI3.HOLDINGPROFILEID " +
                    " WHERE P.ACTIVE='A' AND IAI.HoldingProfileID = ? ";

            status = jdbcTemplate.queryForObject(presentQuery, new Object[]{holdingProfileID}, Integer.class);

            if(status==0)
                isFirstInvestment = true;


        } catch (Exception e) {
            log.error(logPrefix + "check is First Investment Exception : ", e);
        }
        log.debug(logPrefix + "Is First Investment : " + isFirstInvestment);
        return isFirstInvestment;
    }

    public JSONObject getAMCInvestmentDetails(String holdingProfileId, String schemeId) {

        String logPrefix = "#HoldingProfileId : " + holdingProfileId + " | #SchemeId : " + schemeId + " | ";

        log.debug(logPrefix + "AMC Investment details fetch");

        JSONObject details = new JSONObject();

        try {


            String sqlQuery = " EXEC WF_Financial_AMC ?,? ";

            details = jdbcTemplate.queryForObject(sqlQuery, new Object[]{holdingProfileId, schemeId}, (rs, rowNumber) -> {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("amcCode", rs.getString("AMC"));
                jsonObject.put("amcName", rs.getString("AMC_CODE"));
                jsonObject.put("amcAmount", rs.getDouble("Amount"));
                return jsonObject;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        return details;
    }

    public InvestorRiskProfileBO getRiskProfile(String riskProfileType, int userId) {

        String logPrefix = "#UserId : " + userId + " | #RiskProfile : " + riskProfileType + " | ";

        log.debug(logPrefix + "RiskProfile details");

        InvestorRiskProfileBO investorRiskProfileBO = new InvestorRiskProfileBO();

        try {
            String sqlQuery = " SELECT RiskProfileId, RiskType, Equity, Debt, Gold FROM RiskProfileMaster WHERE RiskType=? AND Active=1 ";

            investorRiskProfileBO = jdbcTemplate.queryForObject(sqlQuery, new Object[]{riskProfileType}, (rs, rowNumber) -> {

                InvestorRiskProfileBO riskProfileBO = new InvestorRiskProfileBO();
                riskProfileBO.setRiskProfileId(rs.getInt("RiskProfileId"));
                riskProfileBO.setRiskProfileType(rs.getString("RiskType"));
                if(!riskProfileType.equalsIgnoreCase("custom")) {
                    riskProfileBO.setEquity(rs.getDouble("Equity"));
                    riskProfileBO.setDebt(rs.getDouble("Debt"));
                    riskProfileBO.setGold(rs.getDouble("Gold"));
                }

                return riskProfileBO;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching riskProfile master details: ", e);
        }

        return investorRiskProfileBO;
    }

    public boolean isExistingGoal(String goalName, String goalType, int userId) {

        String logPrefix = "#UserId : " + userId + " | #GoalName : " + goalName + " | #GoalType : " + goalType + " | ";
        log.debug(logPrefix + " check is existing goal");
        int status;
        boolean isExistingGoal = false;

        try {
            String presentQuery =" SELECT COUNT(GoalName) FROM InvestorGoalDetails WHERE GoalName = ? AND GoalType = ? AND UserId = ? AND Active=1 ";

            status = jdbcTemplate.queryForObject(presentQuery, new Object[]{goalName, goalType, userId}, Integer.class);

            if(status>0)
                isExistingGoal = true;


        } catch (Exception e) {
            log.error(logPrefix + "Is Existing goal check Exception : ", e);
        }
        log.debug(logPrefix + "Is Existing goal : " + isExistingGoal);
        return isExistingGoal;
    }

    public boolean isValidGoalType(String goalType, int userId) {

        String logPrefix = "#UserId : " + userId + " | #GoalType : " + goalType + " | ";
        log.debug(logPrefix + " check is valid goal type");
        int status;
        boolean isValid = false;

        try {
            String presentQuery =" SELECT COUNT(Goal_SName) FROM GoalMaster WHERE Goal_SName = ? AND Active=1 ";

            status = jdbcTemplate.queryForObject(presentQuery, new Object[]{goalType}, Integer.class);

            if(status>0)
                isValid = true;


        } catch (Exception e) {
            log.error(logPrefix + "Is valid goal type check Exception : ", e);
        }
        log.debug(logPrefix + "Is valid goal type : " + isValid);
        return isValid;
    }

    public int getHpIdFromInvestorId(int investorId) {

        String logPrefix = "#InvestorId : " + investorId + " | ";
        log.debug(logPrefix + "find HoldingProfileId");
        int holdingProfileId = 0;

        try {
            String presentQuery =" SELECT HoldingProfileId FROM InvestorAdditionalInfo WHERE PrimaryInvestorId = ? AND RelationshipId = 'F' ";

            holdingProfileId = jdbcTemplate.queryForObject(presentQuery, new Object[]{investorId}, Integer.class);


        } catch (Exception e) {
            log.error(logPrefix + "HoldingProfileId fetch Exception : ", e);
        }
        log.debug(logPrefix + "#HoldingProfileId : " + holdingProfileId);
        return holdingProfileId;
    }



}

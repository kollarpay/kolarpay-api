package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/brospay/config/properties/BusinessErrorCodes.properties")
@Config.HotReload(type = Config.HotReloadType.ASYNC, unit = TimeUnit.MINUTES, value = 10)

public interface MFErrorsConfig extends Reloadable {

    //GENERAL ERROR CODES
    @Config.Key("11001")
    String getMandatoryFieldNotEmpty();

    @Config.Key("11002")
    String getFieldLengthMismatch();

    @Config.Key("11003")
    String getDatatypeMismatch();

    @Config.Key("11004")
    String getInvalidData();

    @Config.Key("11005")
    String getNoDataFound();

    @Config.Key("11006")
    String getMinimumMaximum();

    @Config.Key("11007")
    String getPisitiveNumbers();

    @Config.Key("11008")
    String getInvalidDateFormat();

    @Config.Key("11009")
    String getMandatoryFieldTagNotFound();


    @Config.Key("11010")
    String getInvalidJsonTag();


    @Config.Key("11031")
    String getGoalUnique();

    @Config.Key("11032")
    String getGoalTypeValue();

    @Config.Key("11033")
    String getGoalAmountMinmax();

    @Config.Key("11034")
    String getGoalTargetVal();

    @Config.Key("11035")
    String getGoalInflationMinmax();

    @Config.Key("11036")
    String getGoalCustomAllocation();

    //CART CREATION - General
    @Config.Key("11037")
    String getGoalTargetDateFormatInvalid();

    @Config.Key("11038")
    String getCartProductTypeValidation();

    @Config.Key("11039")
    String getCartInvestmentTypeValidation();

    @Config.Key("11040")
    String getGoalOtiTotalMismatch();

    @Config.Key("11041")
    String getSchemeMinMismatch();

    @Config.Key("11042")
    String getInvestmentOptionValidate();

    @Config.Key("11043")
    String getDividendOptionValidate();

    @Config.Key("11044")
    String getGoalSipTotalMismatch();

    @Config.Key("11045")
    String getGoalRiskProfileMissing();

    @Config.Key("11046")
    String getFrequencyValidate();

    //CART CREATION - Business
    @Config.Key("13001")
    String getCartNriRestriction();

    @Config.Key("13002")
    String getCartNriMultipleAmcRestriction();

    @Config.Key("13003")
    String getCartNriNotAllowedScheme();

    @Config.Key("13004")
    String getCartMinorScheme();

    @Config.Key("13005")
    String getCartLqSchemeNotAllowed();

    @Config.Key("13006")
    String getCartNotAvailablePur();

    @Config.Key("13007")
    String getCartOeRestrictedAmc();

    @Config.Key("13008")
    String getCartLumpsumNotSupport();

    @Config.Key("13009")
    String getCartMinExceedEkyc();

    @Config.Key("13010")
    String getCartSchemeMinExceedPerday();

    @Config.Key("13011")
    String getCartAadharInvestorLimit();

    @Config.Key("13012")
    @DefaultValue("Mandate Limit exceeded")
    String getCartMandateAvailableLimit();



    @Config.Key("15001")
    String getDbError();

    @Config.Key("11999")
    String getSystemError();


}

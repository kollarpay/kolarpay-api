package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HoldingProfileBO {

    private String holdingProfileId;
    private String holdingProfileName;
    private String holdingProfilePan;
    private int kyc;
    private Boolean activated;
    private Boolean joint;
    private Boolean nri;
    private Boolean minor;
    private String country;
    private int transactionLimit;
    private Boolean fatca;
    private String type;
    private List<BankInfoBO> banks = new ArrayList<>();
    private List<InvestorBO> investors = new ArrayList<>();

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class InvestorBO {

        private String customerId;
        private String holdingProfileId;
        private String investorName;
        private String type;
        private Boolean minor;
        private Boolean poa = false;
        private Guardian guardian;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Guardian {

        private String customerId;
        private String holdingProfileId;
        private String investorName;
    }

}

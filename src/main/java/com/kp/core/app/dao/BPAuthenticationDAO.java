package com.kp.core.app.dao;

import com.kp.core.app.model.AccessJwtToken;
import com.kp.core.app.model.JwtToken;
import com.kp.core.app.model.UserDetailsBO;
import com.kp.core.app.utils.EncryptionDecryption;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Repository
public class BPAuthenticationDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    Gson gson;

    @Autowired
    private EncryptionDecryption encryptionDecryption;

    public UserDetailsBO getValidateUserSignIn(String userEmailId, String userPassword) {

        String logPrefix = "#emailId : " + userEmailId + " | ";

        UserDetailsBO userBo;
        AtomicReference<String> resultEmailId = new AtomicReference<>();
        AtomicReference<String> resultPassword = new AtomicReference<>();
        String encryptUserPassword;

        String sqlQuery = "\n" +
                " SELECT ui.userId,ui.first_name,ui.pid,ui.email,ui.password_hash,ui.mobileNumber,ur.role_id,r.role_name,ui.verified,ui.lockFlag \n" +
                " FROM \n" +
                " UserInfo ui \n" +
                " join user_roles ur on ui.userid=ur.user_id \n" +
                " join roles r on r.role_id=ur.role_id \n" +
                " where ui.email = ?";

        try {
            userBo = jdbcTemplate.queryForObject(sqlQuery, new Object[]{userEmailId}, (rs, rowNumber) -> {

                UserDetailsBO userDetailsBO = new UserDetailsBO();
                userDetailsBO.setUserId(rs.getInt("userId"));
                userDetailsBO.setUserName(rs.getString("first_name").trim());
                userDetailsBO.setMobileNumber(rs.getString("mobileNumber"));
                userDetailsBO.setPId(rs.getInt("pid"));
                userDetailsBO.setUserType(rs.getString("role_name"));
                userDetailsBO.setRole(rs.getString("role_name"));
                userDetailsBO.setRoleId(rs.getInt("role_id"));
                userDetailsBO.setLockFlag(rs.getString("lockFlag"));
                userDetailsBO.setVerified(rs.getString("verified"));
                userDetailsBO.setEmail(rs.getString("email").trim());

                resultEmailId.set(rs.getString("email").trim());
                resultPassword.set(rs.getString("password_hash").trim());

                return userDetailsBO;
            });

            if (StringUtils.isNotEmpty(resultPassword.get()) && StringUtils.isNotEmpty(resultEmailId.get())) {

                encryptUserPassword = encryptionDecryption.Encryption(userPassword);

                if (resultEmailId.get().equalsIgnoreCase(userEmailId) && (resultPassword.get().equals(encryptUserPassword)))
                    return userBo;
            }

        } catch (Exception errEx) {
            log.error(logPrefix + "Exception in Sign in user validation : " + errEx);
        }

        return null;
    }
    public UserDetailsBO getUserByMobileNo(String mobileNumber) {

        String logPrefix = "#mobileno : " + mobileNumber + " | ";

        UserDetailsBO userBo;
        AtomicReference<String> resultMobileNo = new AtomicReference<>();
        AtomicReference<String> resultPassword = new AtomicReference<>();
        AtomicReference<String> userName = new AtomicReference<>();
        String encryptUserPassword;

        String sqlQuery="\n" +
                " SELECT ui.userId,ui.first_name,ui.pid,ui.email,ui.password_hash,ui.mobileNumber,ur.role_id,r.role_name ,ui.verified,ui.lockFlag \n" +
                " FROM \n" +
                " UserInfo ui \n" +
                " join user_roles ur on ui.userid=ur.user_id \n" +
                " join roles r on r.role_id=ur.role_id \n" +
                " where ui.mobileNumber =?";
        try {
            userBo = jdbcTemplate.queryForObject(sqlQuery, new Object[]{mobileNumber}, (rs, rowNumber) -> {

                UserDetailsBO userDetailsBO = new UserDetailsBO();
                userDetailsBO.setUserId(rs.getInt("userid"));
                userDetailsBO.setUserName(rs.getString("mobileNumber").trim());
                userDetailsBO.setMobileNumber(rs.getString("mobileNumber"));
                userDetailsBO.setPId(rs.getInt("pid"));
                userDetailsBO.setUserType(rs.getString("role_name"));
                userDetailsBO.setRole(rs.getString("role_name"));
                userDetailsBO.setRoleId(rs.getInt("role_id"));
                userDetailsBO.setLockFlag(rs.getString("lockFlag"));
                userDetailsBO.setVerified(rs.getString("verified"));
                userDetailsBO.setEmail(rs.getString("email").trim());
                resultMobileNo.set(rs.getString("mobileNumber").trim());
                resultPassword.set(rs.getString("password_hash").trim());
                userName.set(rs.getString("mobileNumber"));
                return userDetailsBO;
            });

            if (StringUtils.isNotEmpty(resultMobileNo.get())) {
                userBo.setUserName(userName.toString());
            }



        } catch (Exception errEx) {
            log.error(logPrefix + "Exception in mSign in user validation : " + errEx);
        }

        return null;
    }

    public UserDetailsBO getmobileNoValidateUserSignIn(String mobileNumber, String userPassword) {

        String logPrefix = "#mobileno : " + mobileNumber + " | ";

        UserDetailsBO userBo;
        AtomicReference<String> resultMobileNo = new AtomicReference<>();
        AtomicReference<String> resultPassword = new AtomicReference<>();
        AtomicReference<String> userName = new AtomicReference<>();
        String encryptUserPassword;

        String sqlQuery="\n" +
                " SELECT ui.userId,ui.first_name,ui.pid,ui.email,ui.password_hash,ui.mobileNumber,ur.role_id,r.role_name ,ui.verified,ui.lockFlag \n" +
                " FROM \n" +
                " UserInfo ui \n" +
                " join user_roles ur on ui.userid=ur.user_id \n" +
                " join roles r on r.role_id=ur.role_id \n" +
                " where ui.mobileNumber =?";
        try {
            userBo = jdbcTemplate.queryForObject(sqlQuery, new Object[]{mobileNumber}, (rs, rowNumber) -> {

                UserDetailsBO userDetailsBO = new UserDetailsBO();
                userDetailsBO.setUserId(rs.getInt("userid"));
                userDetailsBO.setUserName(rs.getString("mobileNumber").trim());
                userDetailsBO.setMobileNumber(rs.getString("mobileNumber"));
                userDetailsBO.setPId(rs.getInt("pid"));
                userDetailsBO.setUserType(rs.getString("role_name"));
                userDetailsBO.setRole(rs.getString("role_name"));
                userDetailsBO.setRoleId(rs.getInt("role_id"));
                userDetailsBO.setLockFlag(rs.getString("lockFlag"));
                userDetailsBO.setVerified(rs.getString("verified"));
                userDetailsBO.setEmail(rs.getString("email").trim());
                resultMobileNo.set(rs.getString("mobileNumber").trim());
                resultPassword.set(rs.getString("password_hash").trim());
                userName.set(rs.getString("mobileNumber"));
                return userDetailsBO;
            });

            if (StringUtils.isNotEmpty(resultMobileNo.get())) {
                userBo.setUserName(userName.toString());
            }
            if (StringUtils.isNotEmpty(resultPassword.get()) && StringUtils.isNotEmpty(resultMobileNo.get())) {

                encryptUserPassword = encryptionDecryption.Encryption(userPassword);

                if (resultMobileNo.get().equalsIgnoreCase(mobileNumber) && (resultPassword.get().equals(encryptUserPassword)))
                    return userBo;
                else return null;
            }


        } catch (Exception errEx) {
            log.error(logPrefix + "Exception in mSign in user validation : " + errEx);
        }

        return null;
    }
    public UserDetailsBO getUserFromUserName(String userName) {

        String logPrefix = "#userName : " + userName + " | ";

        UserDetailsBO userBo;


        String sqlQuery="\n" +
                " SELECT ui.userId as userid,ui.first_name,ui.pid,ui.email,ui.mobileNumber,ur.role_id,r.role_name  \n" +
                " FROM \n" +
                " UserInfo ui \n" +
                " join user_roles ur on ui.userid=ur.user_id \n" +
                " join roles r on r.role_id=ur.role_id \n" +
                " where ui.userid ="+userName;
        log.info("sqlQuery---"+sqlQuery);
        try {
            userBo = jdbcTemplate.queryForObject(sqlQuery, (rs, rowNumber) -> {

                UserDetailsBO userDetailsBO = new UserDetailsBO();
                userDetailsBO.setUserId(rs.getInt("userid"));
                userDetailsBO.setUserName(rs.getString("mobileNumber").trim());
                userDetailsBO.setMobileNumber(rs.getString("mobileNumber").trim());
                userDetailsBO.setPId(rs.getInt("pid"));
                userDetailsBO.setUserType(rs.getString("role_name"));
                userDetailsBO.setRole(rs.getString("role_name"));
                userDetailsBO.setRoleId(rs.getInt("role_id"));
                userDetailsBO.setEmail(rs.getString("email").trim());
                return userDetailsBO;
            });
          return userBo;




        } catch (Exception errEx) {
            log.error(logPrefix + "Exception in mSign in user validation : " + errEx);
        }

        return null;
    }

    public String fetchUserIdByRefreshToken(String refreshToken) {

        String userId = null;

        try {
            String presentQuery =" SELECT UI.UserId" +
                    " FROM " +
                    "User_tokens UT " +
                    " LEFT JOIN UserInfo UI ON UT.userId = UI.userId  " +
                    " where UT.refresh_token=? and UI.flag='A' ";

            userId = jdbcTemplate.queryForObject(presentQuery, new Object[]{refreshToken}, String.class);

        } catch (Exception e) {
            log.error("Exception in UserId fetch : ", e);
        }
        return userId;
    }

    public void insertUserToken(UserDetailsBO userBo, AccessJwtToken accessJwtToken, JwtToken refreshToken) {

        log.debug("Insert User_tokens");

        try {

            String sqlQuery = " INSERT INTO User_tokens (userid, access_token, refresh_token, jti, sub, " +
                    " scope, issued_at, expire_at, created_by, updated_by) " +
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)  ";

            int status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {

                ps.setInt(1, userBo.getUserId());
                ps.setString(2, accessJwtToken.getToken());
                ps.setString(3, refreshToken.getToken());
                ps.setString(4, accessJwtToken.getClaims().getId());
                ps.setString(5, accessJwtToken.getClaims().getSubject());
                ps.setString(6, accessJwtToken.getClaims().get("scope", String.class));
                ps.setString(7, String.valueOf(accessJwtToken.getClaims().getIssuedAt().getTime()));
                ps.setString(8, String.valueOf(accessJwtToken.getClaims().getExpiration().getTime()));
                ps.setInt(9, userBo.getUserId());
                ps.setInt(10, userBo.getUserId());

            });

            log.debug("Insert User_tokens Status : " + status);
        } catch (Exception e) {
            log.error("Exception in User_tokens insertion: ", e);
        }
    }
}

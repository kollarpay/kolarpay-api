package com.kp.core.app.rz.services;

import com.kp.core.app.dao.CustomerDao;
import com.kp.core.app.dao.UserDao;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.CustomerInfoBO;
import com.kp.core.app.model.CustomerRequest;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.utils.BPConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class PaymentCustomerServices {
    @Autowired
    private GeneralUtil generalUtil;


    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private UserDao userDao;

    public GlobalResponseBO createCustomer(CustomerRequest customerRequest, List<ErrorBO> errorBoList, boolean insert, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        String customerId="";
        String stat = "";
        CustomerInfoBO customerInfoBO=new CustomerInfoBO();

        try {

            if (StringUtils.isEmpty(customerRequest.getCustomerId()))
                errorBoList.add(new ErrorBO("CustomerId", "customerId shouldn't be empty"));

//            if (StringUtils.isEmpty(customerRequest.getFirstName()))
//                errorBoList.add(new ErrorBO("FirstName", "FirstName shouldn't be empty"));
//            if (StringUtils.isEmpty(customerRequest.getSurName()))
//                errorBoList.add(new ErrorBO("SurName", "SurName shouldn't be empty"));
//
//            if (StringUtils.isEmpty(customerRequest.getEmailId()))
//                errorBoList.add(new ErrorBO("emailId", "EmailId shouldn't be empty"));
//            if(!StringUtils.isEmpty(customerRequest.getEmailId()) && !customerRequest.getEmailId().matches(BPConstants.EMAIL_REGEX))
//                errorBoList.add(new ErrorBO("emailId", "Enter valid EmailId"));
//
//            if (StringUtils.isEmpty(customerRequest.getMobileNumber()))
//                errorBoList.add(new ErrorBO("mobileNumber", "MobileNumber shouldn't be empty"));
//            if (!StringUtils.isEmpty(customerRequest.getMobileNumber()) && customerRequest.getMobileNumber().trim().length()<10)
//                errorBoList.add(new ErrorBO("mobileNumber", "Enter valid MobileNumber"));
//            if (StringUtils.isEmpty(customerRequest.getDob()))
//                errorBoList.add(new ErrorBO("dateOfBirth", "dateOfBirth shouldn't be empty"));
//            if (StringUtils.isEmpty(customerRequest.getPassword()))
//                errorBoList.add(new ErrorBO("Password", "Password shouldn't be empty"));
//            if (!StringUtils.isEmpty(customerRequest.getPassword()) && customerRequest.getPassword().trim().length()<8)
//                errorBoList.add(new ErrorBO("Password", "Enter valid Password"));
            if(errorBoList.isEmpty()) {
                customerInfoBO= customerDao.getCustomerInfo(customerRequest.getEmailId(),customerRequest.getMobileNumber());
                if(customerInfoBO==null) {
                    customerId = customerDao.saveCustomer(customerRequest);
                    if (StringUtils.isEmpty(customerId))
                        errorBoList.add(new ErrorBO("failed", "customer creation failed"));
                }

            }

            if(!StringUtils.isEmpty(customerId)) {
                customerInfoBO=new CustomerInfoBO();
                customerInfoBO.setUserId(customerRequest.getUserId());
                customerInfoBO.setCustomerId(customerId);
                customerInfoBO.setName(customerRequest.getName());
                customerInfoBO.setEmail(customerRequest.getEmailId());
                customerInfoBO.setMobile(customerRequest.getMobileNumber());
                customerInfoBO.setType(customerRequest.getType());
            }

        } catch (Exception e) {
            log.error("Exception in customer basic detail table insertions : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, customerInfoBO, globalResponseBo);

        return globalResponseBo;

    }
}

package com.kp.core.app.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerBankInfo {

    private String id;
    private String referenceId;
    private String customerId;
    private String customerEmailId;
    private String customerMobileNumber;
    private String accountName;
    private String userBankId;
    private String bankName;
    private String svg;
    private String bankCity;
    private String branchName;
    private String branchAddress;
    private String accountNumber;
    private String accountType;
    private String micrCode;
    private String ifscCode;
    private String neftCode;
    private String banklookupId;
    private String debtorName;
    private String debtorAccountNo;
    private String debtorBankName;
    private String debtorBankCode;
    private String bankState;
    private String phone;
    private String remarks;
    private String active;
    private String nameMatch;
    private Double nameMatchDouble;
    private String customertype;
    private String bankCategory;
    private String bvRefid;
    private int verified;
    private String createdFrom;
    private String vpa;
    private String accountExists;
}

package com.kp.core.app.phonpe.controller;

import com.google.gson.Gson;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.phonpe.model.PaymentEncodeResponse;
import com.kp.core.app.phonpe.services.PaymentStatusService;
import com.kp.core.app.utils.APIGeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;


@RestController
@Slf4j
@RequestMapping("/payment-status")
public class PaymentStatusController {

    @Autowired
    Gson gson;

    @Autowired
    PaymentStatusService paymentStatusService;

    @PostMapping(path = "/check", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> checkStatus(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody PaymentEncodeResponse paymentStatusResponse) throws Exception {
        log.info("#paymentStatus get API Request{}", paymentStatusResponse.getResponse());
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "paymentStatus");
        try {
            String x_verify = APIGeneralUtil.getHeader(headers, "x-verify");
            log.info("x_verify----{}", x_verify);

            globalResponseBO= paymentStatusService.updateTransactionStatus(paymentStatusResponse,999);


        } catch (Exception e) {
        }
        log.info("#paymentStatus get API Response : {}", globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    }


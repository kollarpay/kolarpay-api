package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BankInfoBO {

    private String customerId;
    private String accountHolderName;
    private String bankId;
    private String bankName;
    private String type;
    private String ifsc;
    private String micr;
    private String neft;
    private String branchCity;
    private String branchAddress;
    private String state;
    private String verified;
    private String accountNo;
    private String accountType;
    private String bankAccountType;
    private Boolean activated = false;
    private List<String> mandateType;
    private String bankLookUpId;
    private String branchName;
    private String neftCode;

}

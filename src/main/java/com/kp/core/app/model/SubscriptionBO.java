package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kp.core.app.model.request.SubscriptionMessage;
import com.kp.core.app.phonpe.model.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionBO {

    @Expose
    private String id;
    @SerializedName("subscriptionid")
    @Expose
    private String subscriptionId;

    @SerializedName("customerid")
    @Expose
    private String customerId;

    @SerializedName("mobilenumber")
    @Expose
    private String MobileNumber;

    @SerializedName("planid")
    @Expose
    private String planId;

    @SerializedName("type")
    @Expose
    private String subscriptionType;

    @SerializedName("startdate")
    @Expose
    private String startDate;

    @SerializedName("enddate")
    @Expose
    private String endDate;

    @SerializedName("ecsdate")
    @Expose
    private int ecsDate;

    @SerializedName("amount")
    @Expose
    private double amount;

    @SerializedName("ecsamount")
    @Expose
    private double ecsamount;

    @SerializedName("consumercode")
    @Expose
    private String consumerCode;

    @SerializedName("folio")
    @Expose
    private String folio;

    @SerializedName("noofinstallment")
    @Expose
    @Builder.Default
    private int noofinstallment=11;

    @SerializedName("frequency")
    @Expose
    @Builder.Default
    private String frequency="MONTHLY";

    @SerializedName("portfolioid")
    @Expose
    private int portfolioId;

    @SerializedName("active")
    @Expose
    private int active;

    @SerializedName("formtype")
    @Expose
    private String formType;

    @Expose
    private IdDesc formTypeIdDesc;

    @SerializedName("affecton")
    @Expose
    private String affectOn;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("remarks")
    @Expose
    private String remarks;

    @SerializedName("initialpayment")
    @Expose
    private String initialPayment;

    @SerializedName("subscriptionrefid")
    @Expose
    private String subscriptionRefId;

    @SerializedName("orderid")
    @Expose
    private String orderId;

    @SerializedName("maxamount")
    @Expose
    private double maxAmount;

    @SerializedName("flexiamount")
    @Expose
    private double flexiAmount;

    @SerializedName("stepupfrequency")
    @Expose
    private String stepUpFrequency;

    @SerializedName("stepupamount")
    @Expose
    private double stepUpAmount;

    @SerializedName("datechange")
    @Expose
    private String dateChange;

    @SerializedName("reason")
    @Expose
    private String reason;


    @SerializedName("perpetual")
    @Expose
    private int perpetual;

    @SerializedName("medium")
    @Expose
    private String medium;

    @SerializedName("created_user")
    @Expose
    private String created_user;

    @SerializedName("updated_user")
    @Expose
    private String updated_user;

    @SerializedName("ipaddress")
    @Expose
    private String ipAddress;
    @SerializedName("authRequestId")
    @Expose
    public String authRequestId;

    @SerializedName("expires_at")
    @Expose
    private String expiresAt;

    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @Expose
    private String customerName;
    @Expose
    private String customerEmail;
    @Expose
    private String customerPhone;
    @Expose
    private double authAmount;

    @SerializedName("returnUrl")
    @Expose
    private String returnUrl;

    @SerializedName("subscriptionNote")
    @Expose
    private String subscriptionNote;

    @SerializedName("paymentOption")
    @Expose
    private String paymentOption;

    @SerializedName("card_number")
    @Expose
    private String cardNumber;

    @SerializedName("card_expiryMonth")
    @Expose
    private String cardExpiryMonth;

    @SerializedName("card_expiryYear")
    @Expose
    private String cardExpiryYear;

    @SerializedName("card_cvv")
    @Expose
    private String cardCvv;
    @SerializedName("card_holder")
    @Expose
    private String cardHolder;

    @SerializedName("emandate_accountHolder")
    @Expose
    private String emandateAccountHolder;

    @SerializedName("emandate_accountNumber")
    @Expose
    private String emandateAccountNumber;

    @SerializedName("emandate_bankId")
    @Expose
    private String emandateBankId;

    @SerializedName("emandate_authMode")
    @Expose
    private String emandateAuthMode;

    @SerializedName("emandate_accountType")
    @Expose
    private String emandateAccountType;

    @SerializedName("paymentInstrument")
    @Expose
    private PaymentInstrument paymentInstrument;

    @SerializedName("deviceContext")
    @Expose
    private DeviceContext deviceContext;

    @Expose
    private SubscriptionMessage subscriptionMessage;
    @Expose
    private SubscriptionCreateResponse subscriptionCreateResponse;

    @Expose
    private SubscriptionAuthRequest subscriptionAuthRequest;
    @Expose
    private SubscriptionAuthResponse subscriptionAuthResponse;
}




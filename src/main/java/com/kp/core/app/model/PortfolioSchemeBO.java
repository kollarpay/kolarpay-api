package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortfolioSchemeBO {

    private String schemeCode;
    private String name;
    private String category;
    private String subCategory;
    private String amcCode;
    private String amc;
    private double rating;
    private int amount;
    private int proposedAmount;
    private double currentAllocation;
    private double proposedAllocation;
    private int minimumInvestment;
    private boolean rated;
    private SystematicPlanBO systematicPlans;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SystematicPlanBO {

        private PortfolioSchemeSystematicPlanBO sip;
        private List<PortfolioSchemeSystematicPlanBO> stp;
        private List<PortfolioSchemeSystematicPlanBO> vtp;
        private PortfolioSchemeSystematicPlanBO vip;
        private PortfolioSchemeSystematicPlanBO swp;

    }

}

package com.kp.core.app.rz.controller;

import com.google.gson.Gson;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.CustomerRequest;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.rz.services.PaymentCustomerServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/payment-customer")
public class PaymentCustomerController {

    @Autowired
    Gson gson;
    @Autowired
    PaymentCustomerServices paymentCustomerServices;

    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createCustomer(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#create payment customer POST API Request : " + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "createPaymentCustomer");
        List<ErrorBO> errorBoList = new ArrayList<>();
        int userId = 0;
        try {

            CustomerRequest customerRequest = gson.fromJson(payload, CustomerRequest.class);
            globalResponseBO = paymentCustomerServices.createCustomer(customerRequest,errorBoList,true,"createPaymentCustomer");


        } catch (Exception e) {
            log.error("#AdvUserid : {}| Exception : ", userId, e);
        }
        log.debug("#User create POST API Response : {}", globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

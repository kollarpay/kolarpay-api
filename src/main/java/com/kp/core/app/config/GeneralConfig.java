package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/brospay/config/properties/general.properties")
@Config.HotReload(type = Config.HotReloadType.ASYNC, unit = TimeUnit.MINUTES, value = 10)

public interface GeneralConfig extends Reloadable {

	@Config.Key("SIP.SHOWUP")
	@Config.DefaultValue("0")
	String sipShowUpSchemes();

	@Config.Key("advisory.pdf.path")
	@Config.DefaultValue("/data/logs/brospay/pdf/advisoryportfolio/<<USERID>>/")
	String advisoryPdfPath();

	@Config.Key("chart.url")
	@Config.DefaultValue("https://build.fundsindia.com/charts")
	String chartGenerationUTL();

	@Config.Key("ES.HOST")
	@Config.DefaultValue("search-fideves-5ztisvoz4bxlypv7n4vamjtvaq.ap-south-1.es.amazonaws.com")
	String esHost();

	@Config.Key("ES.PORT")
	@Config.DefaultValue("443")
	String esPort();

	@Config.Key("ES.USERNAME")
	@Config.DefaultValue("fideves")
	String esUserName();

	@Config.Key("ES.PASSWORD")
	@Config.DefaultValue("Fideves@123")
	String esPassword();

	@Config.Key("ES.SCHEME.INDEX")
	@Config.DefaultValue("scheme_explorer_1")
	String esSchemeIndex();

	@Config.Key("ES.SCHEME.INSERT.CRON.FLAG")
	@Config.DefaultValue("false")
	String esSchemeInsertCronFlag();

	@Config.Key("SIP.DATE.RESTRICTED.SCHEME")
	@Config.DefaultValue("9767,9768")
	String sipDateRectrictedScheme();

	@Config.Key("SIP.ALLOWED.DATE")
	@Config.DefaultValue("1,5,10,15,20,25")
	String sipAllowedDate();

	@Config.Key("TPSL.ENACH.FF_ELIGIBLE.DAYS")
	@Config.DefaultValue("2")
	String tpslEnachFFEligibleDays();

	@Config.Key("TPSL.ENACH.FREQUENCY")
	@Config.DefaultValue("ADHO")
	String tpslEnachFrequency();

	@Config.Key("NLinitTime")
	@Config.DefaultValue("10:59:59")
	String NLInitialTime();

	@Config.Key("NLfinalTime")
	@Config.DefaultValue("14:00:01")
	String NLFinalTime();

	@Config.Key("RelianceAMCCODE")
	@Config.DefaultValue("")
	String relianceAmcCode();

	@Config.Key("TMSGinitTime")
	@Config.DefaultValue("23:59:59")
	String transMsgInitTime();

	@Config.Key("TMSGfinalTime")
	@Config.DefaultValue("14:00:01")
	String transMsgFinalTime();

	@Config.Key("paymentdemouserFlag")
	@Config.DefaultValue("ALL")
	String paymentDemoUserFlag();

	@Config.Key("paymentdemouser")
	@Config.DefaultValue("")
	String paymentDemoUser();

	@Config.Key("selectedBank")
	@Config.DefaultValue("")
	String selectedBankId();

	@Config.Key("FI.CORE.INTERNAL.API.KEY")
	@Config.DefaultValue("uYrwx0PMnD9cmzwQL1woe3Mgan8Wvr6U7dRTSO5o")
	String internalAPIKey();

	@Config.Key("DISPLAY_TOTAL_REDEMPTION_VALUE_IN_DASHBOARD")
	@Config.DefaultValue("157,182,204")
	String displayTotalRedemptionPlId();

	@Config.Key("RESTRICT_CHANGE_AMOUNT")
	@Config.DefaultValue("")
	String changeAmountRestrictedSchemes();

	@Key("RZP.EXTERNAL.API.KEY")
	@DefaultValue("160428d94d6a847505ad337b6f824061")
	String RZPExternalApiKey();
	@Key("RZP.EXTERNAL.API.VALUE")
	@DefaultValue("7f5f56c25598027616269f76067b61793395ab7f")
	String RZPExternalApiSecret();
	@Key("RZP.EXTERNAL.API.VERSION")
	@DefaultValue("2022-01-01")
	String RZPExternalApiVersion();


	@Key("RZP.EXTERNAL.BASE.URL")
	@DefaultValue("https://api.razorpay.com")
	String rzpBaseUrl();

	@Key("RZP.EXTERNAL.BASE.URL")
	@DefaultValue("https://api.razorpay.com")
	String rzpGOUpiBaseUrl();
	@Key("RZP.EXTERNAL.VERSION.URL")
	@DefaultValue("/v1")
	String rzpVersion();

	@Key("RZP.EXTERNAL.CONTENT.TYPE")
	@DefaultValue("application/json")
	String getContentTypeJson();

	@Key("UPI.MID.ENABLE")
	@DefaultValue("false")
	boolean upiMidEnable();

	@Key("MONGO.DB.URL")
	@DefaultValue("mongodb+srv://brospay:nTRPjrKc9bieShNFUrB90Y83iHiPMzAPV8pH1bORVvzthLzLxRXdy1bAlBV64274@brospay.aqtqo5l.mongodb.net/?retryWrites=true&w=majority&appName=brospay")
	String mongoDbUrl();

	@Key("MONGO.DB.DATABASE")
	@DefaultValue("brospayDB")
	String mongoDatabaseName();


}

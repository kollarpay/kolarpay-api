package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SchemeCartBO {

    private String id;
    private String holdingProfileId;
    private String holdingProfileName;
    private String holdingProfilePan;
    private String customerId;
    private String investorName;
    private String paymentId;
    private String paymentMode;
    private String paidStatus;
    private String transactionDate;
    private  MFDataBO mf;
    private CreateMandataBO emandate;
    private String addedOn;
    private String updatedOn;
    private AppInfoBO appInfo;
    private int userId;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MFDataBO {

        private Investment oti;
        private Investment sip;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Investment {

        private int totalAmount;
        private String investmentType;
        private List<Schemes> schemes;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Schemes {

        private String id;
        private Boolean payment;
        private String schemeName;
        private String schemeCode;
        private String folio;
        private String goalId;
        private String goalType;
        private String bankId;
        private int amount;
        private int sipDate;
        private String nextInstallmentOn;
        private String option;
        private String dividendOption;
        private String sipType;
        private SipDetail regular;
        private SipDetail flexi;
        private SipDetail stepup;
        private SipDetail alert;
        private int userTransRefId;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SipDetail {

        private String consumerCode;
        private int tenure;
        private int sipDate;
        private int amount;
        private String frequency;
        private int maximumAmount;
        private String stepupfrequeny;
        private int stepupAmount;
        private int finalAmount;
        private String endDate;
        private String startDate;

        @Override
        public String toString() {

            String sipdetails = "{\"amount\" : " + amount ;

            if(frequency!=null)
                sipdetails += ",\"frequency\" : \"" + frequency + "\"";
            if(stepupfrequeny!=null)
                sipdetails += ",\"stepupfrequeny\" : \"" + stepupfrequeny + "\"";
            if(stepupAmount!=0)
                sipdetails += ",\"stepupAmount\" : " + stepupAmount;
            if(finalAmount!=0)
                sipdetails += ",\"finalAmount\" : " + finalAmount;
            if(sipDate!=0)
                sipdetails += ",\"sipDate\" : " + sipDate;
            if(tenure!=0)
                sipdetails += ",\"tenure\" : " + tenure;
            if(maximumAmount!=0)
                sipdetails += ",\"maximumAmount\" : " + maximumAmount;
            if(consumerCode!=null)
                sipdetails += ",\"consumerCode\" : \"" + consumerCode + "\"";
            if(endDate!=null)
                sipdetails += ",\"endDate\" : \"" + endDate + "\"";
            if(startDate!=null)
                sipdetails += ",\"startDate\" : \"" + startDate + "\"";

            sipdetails += "}";

            return sipdetails;
        }
    }
}

package com.kp.core.app.Mail;

import com.kp.core.app.config.EmailConfig;
import com.kp.core.app.dao.UserDao;
import com.kp.core.app.model.PrivateLabelBO;
import com.kp.core.app.model.UserBO;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
public class MailerBL {

    @Autowired
    UserDao userDao;



    @Autowired
    Mailer mailer;

    EmailConfig configuration = ConfigFactory.create(EmailConfig.class);

    /**
     * Trigger Email when PowerSTP setup
     *
     * @param userId
     * @throws Exception
     */
    public void welcomeEmail(int userId) throws Exception {

        String logPrefix = "welcome Mail | UserId: " + userId + " | ";

        HashMap<String, String> emailHashMap = new HashMap<String, String>();
        List<UserBO> userList = new ArrayList<>();
        boolean mailStatus = false;
        String toMail = "";

        try {
            userList = userDao.getUserMailId(userId);
            for (UserBO userBO : userList) {
                toMail = userBO.getEmail();
            }
            log.debug(logPrefix + "toMailId : " + toMail);

            emailHashMap.put("-templateId-", configuration.getWelcomeEmailTemplateId());

            PrivateLabelBO privateLabelBO = userDao.getPrivateLabelBOByUserId(userId);

            mailStatus = mailer.sendMail(privateLabelBO, toMail, emailHashMap, "", "");
            log.debug(logPrefix + ", ToMail" + toMail + ", Mail Status : " + mailStatus);

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
    }

    /**
     * Trigger Email to user when PowerSTP has updates
     * Change MonthlyAmt, Change TotalAmt, Change ToScheme
     * @param userId
     * @return
     * @throws Exception
     */
    public boolean powerStpUpdateMail(int userId) throws Exception {

        String logPrefix = "PowerSTP Update Mail | UserId: " + userId + " | ";
        log.debug(logPrefix);

        HashMap<String, String> emailHashMap = new HashMap<String, String>();
        List<UserBO> userList = new ArrayList<>();
        boolean mailStatus = false;
        String toMail = "";

        try {
            userList = userDao.getUserMailId(userId);
            for (UserBO userBO : userList) {
                toMail = userBO.getEmail();
            }
            log.debug(logPrefix + "toMailId : " + toMail);

            //Add email hash map

            PrivateLabelBO privateLabelBO = userDao.getPrivateLabelBOByUserId(userId);

            mailStatus = mailer.sendMail(privateLabelBO, toMail, emailHashMap, "", "");
            log.debug(logPrefix + ", ToMail" + toMail + ", Mail Status : " + mailStatus);

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        return mailStatus;
    }

    /**
     * Trigger Email to user when PowerSTP deleted
     * @param userId
     * @return
     * @throws Exception
     */
    public boolean powerStpDeleteMail(int userId) throws Exception {

        String logPrefix = "PowerSTP Delete Mail | UserId: " + userId + " | ";
        log.debug(logPrefix);

        HashMap<String, String> emailHashMap = new HashMap<String, String>();
        List<UserBO> userList = new ArrayList<>();
        boolean mailStatus = false;
        String toMail = "";

        try {
            userList = userDao.getUserMailId(userId);
            for (UserBO userBO : userList) {
                toMail = userBO.getEmail();
            }
            log.debug(logPrefix + "toMailId : " + toMail);

            //Add email hash map

            PrivateLabelBO privateLabelBO = userDao.getPrivateLabelBOByUserId(userId);

            mailStatus = mailer.sendMail(privateLabelBO, toMail, emailHashMap, "", "");
            log.debug(logPrefix + ", ToMail" + toMail + ", Mail Status : " + mailStatus);

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        return mailStatus;
    }
}

package com.kp.core.app.mapper;

import com.kp.core.app.model.CustomerInfoBO;
import com.kp.core.app.utils.BPConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerMapper implements ResultSetExtractor<List<CustomerInfoBO>> {

    @Override
    public List<CustomerInfoBO> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<CustomerInfoBO> customerInfoBOList = new ArrayList<>();
       while (rs.next()) {
           CustomerInfoBO customerInfoBO = new CustomerInfoBO();
           String allowTransact = StringUtils.isNotEmpty(rs.getString("allow_trans"))? rs.getString("allow_trans").trim() : "";
           int customerLimit = rs.getInt("customerlimit");
           int userid = rs.getInt("userid");
           customerInfoBO.setUserId(userid);
           String name=rs.getString("name");
           if(StringUtils.isNotEmpty(name)){
               name=name.trim();
           }
           customerInfoBO.setName(name);
           customerInfoBO.setCustomerId(rs.getString("customerid"));
           customerInfoBO.setEmail(rs.getString("email"));
           customerInfoBO.setMobile(rs.getString("mobilenumber"));
           customerInfoBO.setActivated(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && customerLimit!=49999);
           customerInfoBO.setDateOfBirth(rs.getString("dob"));
           customerInfoBOList.add(customerInfoBO);
       }
       return customerInfoBOList;
    }
}

package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SIPEditBO {

    private String holdingProfileId;
    private String referenceId;
    private String schemeCode;
    private String actionType;
    private int change_flexi_amount;
    private int change_amount;
    private String change_scheme;
    private String remarks;
}

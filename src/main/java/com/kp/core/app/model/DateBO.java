package com.kp.core.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class DateBO implements Serializable {

    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    private int dayValue = 0;
    private int monthValue = 0;
    private int yearValue = 0;

    @Expose
    @SerializedName("date")
    private String dateValue = "";
    private String dateReturnValue = "";
    private String dbDateTime = "";
    private String dbDay = "";

    public DateBO(String dateValue) {
        setDate(dateValue);
    }

    public DateBO() {
        this.dateValue = new Date().toString();
    }

    public String getDay() throws Exception {
        try {
            Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
            DateFormat Tempformatter = new SimpleDateFormat("dd");
            return Tempformatter.format(date);
        } catch (Exception e) {
            log.error("Exception in GetDay() in DateBO " + e);
            return "";
        }
    }

    public void setDay(int dayValue) {
        this.dayValue = dayValue;
        setDate(this.dayValue + "/" + this.monthValue + "/" + this.yearValue);
    }

    public String getMonthNumber() {
        try {
            Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
            DateFormat Tempformatter = new SimpleDateFormat("MM");
            return Tempformatter.format(date);
        } catch (Exception e) {
            log.error("Exception in getMonthNumber() in DateBO " + e);
            return "";
        }
    }

    public String getMonth() {
        try {
            Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
            DateFormat Tempformatter = new SimpleDateFormat("MMM");
            return Tempformatter.format(date);
        } catch (Exception e) {
            log.error("Exception in GetMonth() in DateBO " + e);
            return "";
        }
    }

    public void setMonth(int monthValue) {
        this.monthValue = monthValue;
        setDate(this.dayValue + "/" + this.monthValue + "/" + this.yearValue);
    }

    public String getYear() {
        try {
            Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
            DateFormat Tempformatter = new SimpleDateFormat("yyyy");
            return Tempformatter.format(date);
        } catch (Exception e) {
            log.error("Exception in GetYear() in DateBO " + e);
            return "";
        }
    }

    public void setYear(int yearValue) {
        this.yearValue = yearValue;
        setDate(this.dayValue + "/" + this.monthValue + "/" + this.yearValue);
    }

    public String getDate() {
        return this.dateValue;
    }

    public void setDate(String dateValue) {
        dateReturnValue = "";
        if (dateValue == null) this.dateValue = dateReturnValue;
        else if (!dateValue.equals("")) {
            DateFormat viewFormat = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat DBFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date = (Date) viewFormat.parse(dateValue);
                dateReturnValue = viewFormat.format(date);
            } catch (Exception Exp) {
                try {
                    Date date = (Date) DBFormat.parse(dateValue);
                    dateReturnValue = viewFormat.format(date);
                } catch (Exception e) {
                    log.error(" Exception in DateBO.setDate " + e);
                }
            }
        }
        this.dateValue = dateReturnValue;

        try {
            Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
            DateFormat Tempformatter = new SimpleDateFormat("dd");
            this.dayValue = Integer.valueOf(Tempformatter.format(date)).intValue();
        } catch (Exception e) {//log.error("Exception in Set 'this.dayValue' in DateBO "+e);
        }
        try {
            Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
            DateFormat Tempformatter = new SimpleDateFormat("MM");
            this.monthValue = Integer.valueOf(Tempformatter.format(date)).intValue();
        } catch (Exception e) {//log.error("Exception in Set 'this.monthValue' in DateBO "+e);
        }
        try {
            Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
            DateFormat Tempformatter = new SimpleDateFormat("yyyy");
            this.yearValue = Integer.valueOf(Tempformatter.format(date)).intValue();
        } catch (Exception e) {//log.error("Exception in Set 'this.yearValue' in DateBO "+e);
        }

    }

    public String getDateFirstView() {
        dateReturnValue = "";
        try {
            if (this.dateValue != null && !"".equals(this.dateValue)) {
                Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
                DateFormat Tempformatter = new SimpleDateFormat("dd/MM/yyyy");
                dateReturnValue = Tempformatter.format(date);
            }
        } catch (Exception Exp) {
            log.error(" Exception in DateBO.getDateFirstView " + Exp.getMessage());
        }
        return dateReturnValue;
    }


    public String getDateTimeFirstView() {
        dateReturnValue = "";
        try {
            if (this.dateValue != null && !"".equals(this.dateValue)) {
                Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
                DateFormat Tempformatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                dateReturnValue = Tempformatter.format(date);
            }
        } catch (Exception Exp) {
            log.error(" Exception in DateBO.getDateFirstView " + Exp.getMessage());
        }
        return dateReturnValue;
    }

    public String getMonthFirstView() {
        dateReturnValue = "";
        try {
            if (this.dateValue != null && !"".equals(this.dateValue)) {
                Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
                DateFormat Tempformatter = new SimpleDateFormat("MM/dd/yyyy");
                dateReturnValue = Tempformatter.format(date);
            }
        } catch (Exception Exp) {
            log.error(" Exception in DateBO.getMonthFirstView " + Exp);
        }
        return dateReturnValue;
    }

    public String getFulMonthView() {
        try {
            Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
            DateFormat Tempformatter = new SimpleDateFormat("dd/MMM/yyyy");
            dateReturnValue = Tempformatter.format(date);
        } catch (Exception Exp) {
            dateReturnValue = "Formatting Error";
//			log.error(" Exception in DateBO.getFulMonthView "+Exp);
        }
        return dateReturnValue;
    }

    //------------ Get Db format ----------------
    public String getDBFormat() {
        return getMonthFirstView();
    }

    public String getDefaultDBFormat() {
        dateReturnValue = "";
        try {
            if (this.dateValue != null && !"".equals(this.dateValue)) {
                Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
                DateFormat tempformatter = new SimpleDateFormat("yyyy-MM-dd");
                dateReturnValue = tempformatter.format(date);
            }
        } catch (Exception Exp) {
//			dateReturnValue = "Formatting Error";
//			log.error(" Exception in DateBO.getFulMonthView "+Exp);
        }
        return dateReturnValue;
    }

    //------------ Get date in Required format -----------
    public String getRequiredDateFormat(String strRequiredFormat) {

        dateReturnValue = "";
        try {
            Date date = (Date) formatter.parse(String.valueOf(this.dateValue));
            DateFormat Tempformatter = new SimpleDateFormat(strRequiredFormat);
            dateReturnValue = Tempformatter.format(date);
        } catch (Exception Exp) {
            dateReturnValue = "Invalid Format";
            log.error(" Exception in DateBO.getRequiredDateFormat. Exception : " + Exp);
        }
        return dateReturnValue;
    }

    public String getDbDay() {
        return dbDay;
    }

    public void setDbDay(String dbDay) {
        this.dbDay = dbDay;
    }

    public String getDbDateTime() {
        return dbDateTime;
    }

    public void setDbDateTime(String dbDateTime) {
        this.dbDateTime = dbDateTime;
    }

    /**
     * Get Date & Time in view format (dd/MM/yyyy hh:mm:ss:S) from Database format (yyyy-MM-dd hh:mm:ss.S)
     *
     * @param dateTime_DBFormat - String of Date & Time in Database format (yyyy-MM-dd hh:mm:ss.S)
     * @return String of Date & Time in view format (dd/MM/yyyy hh:mm:ss:S)
     */

    public String getDateTime_ViewFormat(String dateTime_DBFormat) {
        String returnString = "";

        if (StringUtils.isEmpty(dateTime_DBFormat))
            return returnString;

        try {
            DateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
            DateFormat viewFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss:S");

            Date date = (Date) dbFormat.parse(dateTime_DBFormat);
            returnString = viewFormat.format(date);

        } catch (Exception uEx) {
            log.error("U.EX - DATE TIME FORMAT. $ I/P :" + dateTime_DBFormat + " # EX: " + uEx);
        }
        return returnString;
    }

    /**
     * Convert the given date to required format
     * @param date
     * @param date_CurrentFormat
     * @param date_RequiredFormat
     * @return
     */
    public static String convertDate(String date, String date_CurrentFormat, String date_RequiredFormat) {

        String returnValue = "";

        if (StringUtils.isEmpty(date)
                || StringUtils.isEmpty(date_CurrentFormat)
                || StringUtils.isEmpty(date_RequiredFormat))
            return returnValue;

        try {
            DateFormat currentFormat = new SimpleDateFormat(date_CurrentFormat);
            DateFormat requiredFormat = new SimpleDateFormat(date_RequiredFormat);

            Date date_Date = (Date) currentFormat.parse(date);

            returnValue = requiredFormat.format(date_Date);
        } catch (Exception Exp) {
            log.error("Exception : " + Exp);
        }
        return returnValue;
    }
}

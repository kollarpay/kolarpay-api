package com.kp.core.app.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ErrorBO {

    private String id;
    private String code;
    private String field = "";
    private Object desc;
    private String productType;

    public ErrorBO(String field, Object desc) {
        this.field = field;
        this.desc = desc;
    }

    public ErrorBO(String id, String field, Object desc) {
        this.id = id;
        this.field = field;
        this.desc = desc;
    }
    public ErrorBO(String id, String field, String code, Object desc) {
        this.id = id;
        this.field = field;
        this.code = code;
        this.desc = desc;

    }

    public ErrorBO() {
    }

    public ErrorBO(String id, String productType, String field, String code, Object desc) {
        this.id = id;
        this.productType = productType;
        this.code = code;
        this.field = field;
        this.desc = desc;
    }
}

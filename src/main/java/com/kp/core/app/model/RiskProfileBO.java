package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RiskProfileBO {

    private int riskProfileId;
    private String riskProfile;
    private double minValue;
    private double maxValue;
    private double equity;
    private double gold;
    private double debt;
}

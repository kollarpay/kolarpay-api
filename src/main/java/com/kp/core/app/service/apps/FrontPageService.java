package com.kp.core.app.service.apps;

import com.kp.core.app.dao.FrontPageDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.FrontPageBo;
import com.kp.core.app.model.FrontPageProductBo;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
public class FrontPageService {

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private FrontPageDAO frontPageDAO;




    public GlobalResponseBO createFrontPageCollection(FrontPageBo frontPageBo, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create createFrontPage");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "create FrontPage Collection");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            isValidFrontPageCollection(frontPageBo, errorBoList);


            if(errorBoList.size()==0 ) {
                frontPageDAO.insertFrontPageCollection(frontPageBo,userId);
            }


        } catch (Exception e) {
            log.error(logPrefix + "createFrontPage API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, frontPageBo, globalResponseBo);

        log.debug(logPrefix + "Create Widget API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO createFrontPageProducts(FrontPageProductBo frontPageProductBo, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create createFrontPage");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "create FrontPage product");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            isValidFrontPageProduct(frontPageProductBo, errorBoList);


            if(errorBoList.size()==0 ) {
                frontPageDAO.insertFrontPageProducts(frontPageProductBo,userId);
            }


        } catch (Exception e) {
            log.error(logPrefix + "createFrontPage API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, frontPageProductBo, globalResponseBo);

        log.debug(logPrefix + "Create Widget API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO getAllFrontPage(int userId,int bpid) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "get all published frontPage");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "AllfrontPage");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        List<FrontPageBo> frontPageBoList=new ArrayList<>();
        try {


            if(errorBoList.size()==0 ) {
                frontPageBoList=frontPageDAO.getFrontPageCollectionList(userId,bpid);
            }


        } catch (Exception e) {
            log.error(logPrefix + "get dashboardTemplate API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, frontPageBoList, globalResponseBo);

        log.debug(logPrefix + "frontPage API Status : " + stat);
        return  globalResponseBo;

    }
    public void isValidFrontPageCollection(FrontPageBo frontPageBo, List<ErrorBO> errorBoList) {

        try {


            if(frontPageBo.getBpid()<100)
                errorBoList.add(new ErrorBO("bpid", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getTitle()))
                errorBoList.add(new ErrorBO("title", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getCollectionName()))
                errorBoList.add(new ErrorBO("collection Name", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getCollectionType()))
                errorBoList.add(new ErrorBO("collection Type", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getShape()))
                errorBoList.add(new ErrorBO("Shape", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getBackground()))
                errorBoList.add(new ErrorBO("background", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getPublishedScope()))
                errorBoList.add(new ErrorBO("publish scope", "Invalid input"));
            if(frontPageBo.getMaxItems()==0)
                errorBoList.add(new ErrorBO("max items", "Invalid input"));
            if(frontPageBo.getPosition()<1)
                errorBoList.add(new ErrorBO("position", "Invalid input"));

        } catch (Exception e) {
            log.error("Exception in fronPage Collection creation - validate : ", e);
        }
    }
    public void isValidFrontPageProduct(FrontPageProductBo frontPageBo, List<ErrorBO> errorBoList) {

        try {


            if(frontPageBo.getBpid()<100)
                errorBoList.add(new ErrorBO("bpid", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getTitle()))
                errorBoList.add(new ErrorBO("title", "Invalid input"));
            if(frontPageBo.getTags().length==0)
                errorBoList.add(new ErrorBO("tags", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getProductName()))
                errorBoList.add(new ErrorBO("collection name", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getProductType()))
                errorBoList.add(new ErrorBO("collection type", "Invalid input"));
            if(StringUtils.isEmpty(frontPageBo.getPublishedScope()))
                errorBoList.add(new ErrorBO("publish scope", "Invalid input"));
            if(frontPageBo.getPosition()<1)
                errorBoList.add(new ErrorBO("position", "Invalid input"));

        } catch (Exception e) {
            log.error("Exception in fronPage product creation - validate : ", e);
        }
    }
}


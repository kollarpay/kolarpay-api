package com.kp.core.app.controller.Utilities;

import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.AuthRequest;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.service.Utilities.BPAuthenticationService;
import com.kp.core.app.utils.GeneralUtil;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/auth")
public class BPAuthenticationController {

    @Autowired
    GeneralUtil generalUtil;

    @Autowired
    Gson gson;

    @Autowired
    BPAuthenticationService authenticationService;

    @PostMapping(path = "/signin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> userLogin(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#login POST API Request : " + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "Login");

        int userId = 0;
        try {

            AuthRequest authRequest = gson.fromJson(payload, AuthRequest.class);
            globalResponseBO = authenticationService.loginCredentials(authRequest, null);



        } catch (Exception e) {
            log.error("#AdvUserId : " + userId + "| Exception : ", e);
        }
        log.debug("#mandates create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @PostMapping(path = "/msignin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> muserLogin(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#mlogin POST API Request : " + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "mLogin");

        try {

            AuthRequest authRequest = gson.fromJson(payload, AuthRequest.class);
            globalResponseBO = authenticationService.loginCredentials(authRequest, null);



        } catch (Exception e) {
            log.error("#msignin | Exception : ", e);
        }
        log.debug("#msignin POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @PostMapping(path = "/impersonate", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> impersonate(@RequestHeader("Authorization") String token, @RequestParam String client_id) throws Exception {

        log.debug("#impersonate POST API Request : client_id |" + client_id);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "impersonate");

        AuthRequest authRequest = new AuthRequest();
        try {
            String newToken=token.replace("Bearer ", "");
            authRequest.setToken(newToken);
            authRequest.setGrantType("impersonate");
            globalResponseBO = authenticationService.loginCredentials(authRequest, client_id);



        } catch (Exception e) {
            log.error("#client_id : " + client_id + "| Exception : ", e);
        }
        log.debug("#impersonate create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

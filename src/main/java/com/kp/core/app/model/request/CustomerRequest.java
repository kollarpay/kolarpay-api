package com.kp.core.app.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRequest {
    private String id;
    private String name;
    private String contact;
    private String email;
    private String fail_existing;
    private String gstin;
    private HashMap<String,String> notes;

}

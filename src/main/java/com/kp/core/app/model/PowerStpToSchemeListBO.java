package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PowerStpToSchemeListBO {

    private String schemeName;
    private String schemeCode;
    private int ratings;
    private double currentNav;
    private String currentNavFormatted;
    private String navAsOn;
    private String category;
    private String expenseRatio;
    private int minimumInvestment;
    private String minimumInvestmentFormatted;
    private String launchDate;

}

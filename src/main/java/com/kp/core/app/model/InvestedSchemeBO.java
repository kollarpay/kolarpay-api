package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvestedSchemeBO {

    private String schemeName;
    private String schemeCode;
    private String folio;
    private String goalName;
    private String goalId;
    private double units;
    private String unitsFormatted;
    private BigDecimal currentAmount;
    private String currentAmountFormatted;
    private BigDecimal investedAmount;
    private String investedAmountFormatted;
    private boolean invest;
    private String bankId;
    private String option;
    private String dividendOption;
}

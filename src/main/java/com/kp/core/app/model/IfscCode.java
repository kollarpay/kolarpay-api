package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

@AllArgsConstructor
public class IfscCode {

    public String bank;
    public String ifsccode;
    public String micrcode;
    public String branchname;
    public String address;
    public String city;
    public String state;

}

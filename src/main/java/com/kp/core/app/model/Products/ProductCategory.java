package com.kp.core.app.model.Products;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kp.core.app.model.IdDesc;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductCategory {
    public String categoryId;
    public String categoryName;
    public String description;
    public String imgUrl;
    public List<IdDesc> categoryList;



}



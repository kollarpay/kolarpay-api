package com.kp.core.app.razorpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RzpVertualAccountCreationRequestBO {

    private String description;
    private String customer_id;
    private String close_by;
    private AccountCreationReceivers receivers;
    private List<AccountCreationAllowedPayers> allowed_payers;
    private AccountCreationNotesBO notes;


    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AccountCreationReceivers {
        private String types[];
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AccountCreationAllowedPayers {
        private String type;
        private AccountCreationBankAccount bank_account;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AccountCreationBankAccount {
        private String ifsc;
        private String account_number;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AccountCreationNotesBO {
        private String product;
        private String clientId;

    }

}

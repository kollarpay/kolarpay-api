package com.kp.core.app.service.Payment;

import com.google.gson.Gson;
import com.kp.core.app.config.GeneralConfig;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.razorpay.config.ErrorConfig;
import com.kp.core.app.config.PaymentConfig;
import com.kp.core.app.dao.CustomerDao;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.model.request.*;
import com.kp.core.app.model.request.CustomerRequest;
import com.kp.core.app.razorpay.dao.RzpCustomerDao;
import com.kp.core.app.razorpay.gatewayService.RazorPayAPIService;
import com.kp.core.app.razorpay.model.*;
import com.kp.core.app.service.Utilities.GeneralInfoService;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.BPConstants;
import com.kp.core.app.utils.ErrorsConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@Service
public class PaymentServices {

    private final RestTemplate restTemplate;
    private final GeneralUtil generalUtil;

    private final
    Gson gson;

    private PaymentConfig paymentConfig = ConfigFactory.create(PaymentConfig.class);
    private GeneralConfig generalConfig = ConfigFactory.create(GeneralConfig.class);


    private ErrorConfig  errorConfig = ConfigFactory.create(ErrorConfig.class);

    private final RzpCustomerDao rzpCustomerDao;

    @Autowired
    RazorPayAPIService razorPayAPIService;



    @Autowired
    public PaymentServices(RestTemplate restTemplate, Gson gson, GeneralUtil generalUtil, CustomerDao customerDao, RzpCustomerDao rzpCustomerDao) {
        this.restTemplate = restTemplate;
        this.gson = gson;
        this.generalUtil = generalUtil;
        this.rzpCustomerDao = rzpCustomerDao;
    }

    public GlobalResponseBO customerAccount(PaymentInitiateRequestBO paymentInitiateRequestBO,String channelId) {
        String logPrefix = "#channelId : " + channelId + " | ";
        log.debug(logPrefix + "Razorpay Create customer");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createRazorpayCustomer");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        String rzpCustomerId="";
        String emailId="";
        CustomerRequest customerRequest=new CustomerRequest();
        RzpCustomerCreationRequestBO rzpCustomerCreationRequestBO= new RzpCustomerCreationRequestBO();
        RzpCustomerCreationResponseBO rzpCustomerCreationResponseBO = new RzpCustomerCreationResponseBO();
        CustomerInfoBO customerInfoBO = null;
        HttpHeaders headers = new HttpHeaders();
        Map<String,String> rzpVirtualAccountIdMap=new HashMap<>();

        try {
            if (paymentInitiateRequestBO.getMode().trim().equalsIgnoreCase(BPConstants.PAYMENT_MODE_BANK_TRANSFER) && paymentInitiateRequestBO.getSubMode().trim().equalsIgnoreCase(BPConstants.PAYMENT_SUB_MODE_NEFT)) {
                if (StringUtils.isEmpty(paymentInitiateRequestBO.getCustomerId()) || paymentInitiateRequestBO.getCustomerId().equals("")) {
                    errorBoList.add(new ErrorBO(paymentInitiateRequestBO.getCustomerId(), paymentInitiateRequestBO.getProductType(), "customerId", ErrorsConstants.REQUIRED_DATA_NOT_FOUND, errorConfig.requiredDataNotFound()));
                } else {
                    headers = GeneralInfoService.LoadPaymentHeader(headers);
                    String referenceId = GeneralUtil.getReferenceIdByCustomerID(paymentInitiateRequestBO.getCustomerId());
                    customerInfoBO = rzpCustomerDao.getCustomerByCustomerId(paymentInitiateRequestBO.getCustomerId());
                    Map<String, String> rzpCustIdEmail = rzpCustomerDao.getRzCustomer(referenceId, paymentInitiateRequestBO.getProductType(), paymentInitiateRequestBO.getSubMode());
                    if (rzpCustIdEmail != null && !StringUtils.isEmpty(rzpCustIdEmail.get("rzpCustomerId"))) {
                        rzpCustomerId = rzpCustIdEmail.get("rzpCustomerId");
                        emailId = rzpCustIdEmail.get("emailId");
                        if (!StringUtils.isEmpty(rzpCustomerId) && !customerInfoBO.getEmail().equalsIgnoreCase(emailId)) {
                            RzpCustomerEditRequestBO editRequestBO = new RzpCustomerEditRequestBO();
                            editRequestBO.setEmail(customerInfoBO.getEmail());
                            RzpCustomerEditResponseBO editResponseBO = razorPayAPIService.makeCustomerEditCall(rzpCustomerId, editRequestBO, errorBoList, paymentInitiateRequestBO.getProductType(), "NEFT");
                            if (editResponseBO != null && StringUtils.isNotEmpty(editResponseBO.getEmail()) && editResponseBO.getEmail().equalsIgnoreCase(customerInfoBO.getEmail())) {
                                rzpCustomerDao.updateBPEmail(rzpCustomerId, customerInfoBO.getEmail(), paymentInitiateRequestBO.getUserId());
                            } else {
                                rzpCustomerId = "";
                                errorBoList.add(new ErrorBO(customerInfoBO.getCustomerId(), paymentInitiateRequestBO.getProductType(), "razorpay_customer_mapping", ErrorsConstants.CUSTOMER_EMAIL_UPDATE_FAILED, errorConfig.customerEmailUpdateFailed()));
                            }

                        }
                    }
                    if (errorBoList.size() == 0) {
                        if (StringUtils.isEmpty(rzpCustomerId)) {
                            rzpCustomerCreationResponseBO = customerCreationAPICall(rzpCustomerCreationRequestBO, customerInfoBO, referenceId, rzpCustomerId, customerInfoBO.getCustomerId(), errorBoList, paymentInitiateRequestBO.getProductType(), "NEFT");
                            if (errorBoList.size() == 0)
                                rzpCustomerId = rzpCustomerCreationResponseBO.getId();
                        } else {
                            //Setting values for customer creation
                            rzpCustomerCreationRequestBO.setName(customerInfoBO.getName());
                            rzpCustomerCreationRequestBO.setEmail(customerInfoBO.getEmail());

                            //getting the values from DB
                            rzpCustomerCreationResponseBO = rzpCustomerDao.getRzpCustomerMappingResponse(referenceId, customerInfoBO.getEmail(), paymentInitiateRequestBO.getProductType(), "NEFT");
                            if ((rzpCustomerCreationResponseBO == null) || (StringUtils.isEmpty(rzpCustomerCreationResponseBO.getId())))
                                errorBoList.add(new ErrorBO(referenceId, paymentInitiateRequestBO.getProductType(), "razorpay_customer_mapping", ErrorsConstants.CUSTOM_ACCOUNT_DOES_NOT_EXIST, errorConfig.customerAccountDoesNotExist()));

                        }
                        if (StringUtils.isNotEmpty(rzpCustomerId))
                            rzpVirtualAccountIdMap = rzpCustomerDao.getRzpVirtualAccountId(rzpCustomerId, paymentInitiateRequestBO.getProductType());
                        if (rzpVirtualAccountIdMap.size() > 0 && errorBoList.size() == 0) {

                            for (Map.Entry<String, String> rzpVirtualAccountId : rzpVirtualAccountIdMap.entrySet()) {


                                List<RzpBankBO> rzpBankBOList = new ArrayList<>();
                                rzpBankBOList = rzpCustomerDao.getVirtualBankDetails(rzpVirtualAccountId.getKey());

                                //available list
                                List<BankInfoBO> customerBankFullList = customerInfoBO.getBanks();
                                List<BankInfoBO> customerBanks = customerBankFullList.stream().filter(BankInfoBO::getActivated).collect(Collectors.toList());


                            }


                        }
//                        else if (errorBoList.size() == 0) {
//                            // customer account
//                            createCustomeAccount(referenceId, holdingProfileBO, rzpBankBOHashMap, rzpCustomerCreationResponseBO, requestBO, errorBoList);
//                        } else
//                            errorBoList.add(new ErrorBO(referenceId, requestBO.getProductType(), "razorpay_customer_mapping", ErrorsConstants.REQUIRED_DATA_NOT_FOUND, errorConfig.requiredDataNotFound()));


                    }

                    customerRequest = GeneralInfoService.getPaymentCustomerRequest(customerInfoBO);
                    HttpEntity<CustomerRequest> request = new HttpEntity<>(customerRequest, headers);
                    Map<String, String> params = new HashMap<String, String>();
                    log.info("Validate customer Email Request {}  | Request String: {}", customerRequest.getEmail(), gson.toJson(customerRequest));
                    String paymentAuthURL = paymentConfig.PaymentBaseUrl() + paymentConfig.getSlash() + paymentConfig.PaymentVersion() + paymentConfig.getSlash() + paymentConfig.paymentCustomerCreateUrl();
                    log.info("paymentAuthURL--" + paymentAuthURL);
                    ResponseEntity response = restTemplate.postForEntity(paymentAuthURL, request, String.class);


                    if (response.getStatusCode().value() == 200 && response.getBody() != null) {

                        customerRequest = gson.fromJson(response.getBody().toString(), CustomerRequest.class);

                    } else {

                        if (response.getStatusCodeValue() == 401 && response.getBody() != null) {

                            errorBoList.add(new ErrorBO("", "Invalid clientId and clientSecret combination", ErrorsConstants.VALIDATE_PAYMENT_AUTH_TOKEN, "errorConfig.validatePaymentAuthToken()"));

                        }
                    }

                    if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
                        errorBoList.add(new ErrorBO("", "authorize", ErrorsConstants.VALIDATE_PAYMENT_AUTH_TOKEN, "errorConfig.validatePaymentAuthToken()"));
                    }

                    if (customerRequest != null && customerRequest.getId() != "") {
                        log.info("customer id " + customerRequest.getId());
                    }

                }
//                else{
//                    errorBoList.add(new ErrorBO(paymentInitiateRequestBO.getPaymentId(), paymentInitiateRequestBO.getProductType(), "mode/submmode", ErrorsConstants.REQUIRED_DATA_NOT_FOUND, errorConfig.requiredDataNotFound()));
//
//                }
            }
        }catch (Exception e) {
            log.error("Validate customer create - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, customerRequest, globalResponseBo);

        return globalResponseBo;
    }

    public RzpCustomerCreationResponseBO customerCreationAPICall(RzpCustomerCreationRequestBO rzpCustomerCreationRequestBO, CustomerInfoBO customerInfoBO, String referenceId,
                                                                 String rzpCustomerId, String investorId, List<ErrorBO> errorBoList, String product, String paymentMode) throws Exception {

        RzpCustomerCreationResponseBO rzpCustomerCreationResponseBO = new RzpCustomerCreationResponseBO();

        //Setting values for customer creation
        rzpCustomerCreationRequestBO.setName(customerInfoBO.getName());
        rzpCustomerCreationRequestBO.setContact(referenceId);
        rzpCustomerCreationRequestBO.setEmail(customerInfoBO.getEmail());
        rzpCustomerCreationRequestBO.setFail_existing("0");

        RzpCustomerCreationRequestBO.CustomerCreationNotesBO customerCreationNotesBO = new RzpCustomerCreationRequestBO.CustomerCreationNotesBO();
        customerCreationNotesBO.setLabelId(customerInfoBO.getBpid()+"");
        customerCreationNotesBO.setMobile(customerInfoBO.getMobile());
        rzpCustomerCreationRequestBO.setNotes(customerCreationNotesBO);

        int dbStatus = 0;
        if (rzpCustomerId != null)//DB insert before customer creation
            dbStatus = rzpCustomerDao.insertRazorPayCustomerMapping(rzpCustomerCreationRequestBO, investorId, product, paymentMode, customerInfoBO.getUserId());
        if (dbStatus > 0 || StringUtils.isEmpty(rzpCustomerId)) {

            //customer creation external api
            rzpCustomerCreationResponseBO = custmerCreation(referenceId, rzpCustomerCreationRequestBO, product, paymentMode,true, false, errorBoList);
            if ((rzpCustomerCreationResponseBO == null) || (StringUtils.isEmpty(rzpCustomerCreationResponseBO.getId())))
                errorBoList.add(new ErrorBO(referenceId, product, "CustomerId", ErrorsConstants.CUSTOM_ACCOUNT_SERVICE, errorConfig.customAccountService()));
            else {
                //DB update for customer mapping
                dbStatus = rzpCustomerDao.updateRazorPayCustomerMapping(referenceId, rzpCustomerCreationRequestBO.getEmail(), product, paymentMode, rzpCustomerCreationResponseBO, customerInfoBO.getUserId());
                if (dbStatus == 0)
                    errorBoList.add(new ErrorBO(referenceId, product, "razorpay_customer_mapping", ErrorsConstants.DB_UPDATE_FAIL, errorConfig.dbUpdateFail()));
            }

        } else
            errorBoList.add(new ErrorBO(referenceId, product, "razorpay_customer_mapping", ErrorsConstants.DB_INSERT_FAIL, errorConfig.dbInsertionFail()));

        return rzpCustomerCreationResponseBO;

    }
    public RzpCustomerCreationResponseBO custmerCreation(String referenceId, RzpCustomerCreationRequestBO requestBO, String product, String paymentMode, boolean retry, boolean isIndividual, List<ErrorBO> errorBoList) {

        RzpCustomerCreationResponseBO responseBO = new RzpCustomerCreationResponseBO();

        try {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<RzpCustomerCreationRequestBO> request = new HttpEntity<>(requestBO, headers);

            log.info("Customer creation | Request: {}", gson.toJson(requestBO));

            String customerCreateAPIURL = paymentConfig.PaymentBaseUrl() + paymentConfig.getSlash() + paymentConfig.PaymentVersion() + paymentConfig.getSlash() + paymentConfig.paymentCustomerCreateUrl();

            ResponseEntity response = restTemplate.postForEntity(customerCreateAPIURL, request, String.class);

            log.info("Customer creation | Request: {} | Response: {} ", gson.toJson(requestBO), response);

            if(response.getStatusCode().value() == 200 && response.getBody() != null) {

                responseBO = gson.fromJson(response.getBody().toString(), RzpCustomerCreationResponseBO.class);
            }

            else if (response.getStatusCode().value() == 400 && response.getBody() != null) {

                log.debug("InitiatePayment Error Response : " + response.getBody().toString());

                RzpErrorBO rzpErrorBO = gson.fromJson(response.getBody().toString(), RzpErrorBO.class);

                if(rzpErrorBO!=null && rzpErrorBO.getError()!=null && StringUtils.isNotEmpty(rzpErrorBO.getError().getDescription())
                        && rzpErrorBO.getError().getDescription().contains("Contact number contains invalid country code")) {

                    if(isIndividual && retry) {
                        requestBO.setContact(requestBO.getNotes().getMobile());

                        responseBO = custmerCreation(referenceId, requestBO, product, paymentMode,false, true, errorBoList);
                    }
                    else {
                        errorBoList.add(new ErrorBO(referenceId, product, "CustomerId", ErrorsConstants.CUSTOMERID_INVALID_CONTACT, errorConfig.customerIdInvalidContact()));
                    }
                }
            }

        } catch (Exception e) {
            log.error("Customer creation | Request: {} | Exception : {}", gson.toJson(requestBO), e);
        }

        return responseBO;
    }
    public PaymentAuthTokenBO GeneratePaymentAuthToken(PaymentAuthTokenBO paymentAuthTokenBO, List<ErrorBO> errorBoList) {

        try {
            HttpHeaders headers = new HttpHeaders();
            //headers=GeneralInfoService.LoadPayOutHeader(headers);
            HttpEntity<PaymentAuthTokenBO> request = new HttpEntity<>(paymentAuthTokenBO, headers);
            Map<String, String> params = new HashMap<String, String>();
            log.info("Validate customer Email Request {}  | Request String: {}", paymentAuthTokenBO.getStatus(), gson.toJson(paymentAuthTokenBO));
            String paymentAuthURL=paymentConfig.capPayoutUrl()+paymentConfig.getSlash()+paymentConfig.capPayoutVersion()+paymentConfig.getSlash()+paymentConfig.capPayoutAuth();
            ResponseEntity response = restTemplate.postForEntity(paymentAuthURL,request, String.class);


            if (response.getStatusCode().value() == 200 && response.getBody() != null) {

                paymentAuthTokenBO = gson.fromJson(response.getBody().toString(), PaymentAuthTokenBO.class);

            } else {

                if (response.getStatusCodeValue() == 401 && response.getBody() != null) {

                    errorBoList.add(new ErrorBO("", "Invalid clientId and clientSecret combination", ErrorsConstants.VALIDATE_PAYMENT_AUTH_TOKEN, errorConfig.validateVPAApiFail()));

                }
            }

            if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
                errorBoList.add(new ErrorBO("", "authorize", ErrorsConstants.VALIDATE_PAYMENT_AUTH_TOKEN, errorConfig.validateVPAApiFail()));
            }

            if (paymentAuthTokenBO != null && paymentAuthTokenBO.getData().token!="") {
                log.info("auth token"+paymentAuthTokenBO.getData().token);
            }

        } catch (Exception e) {
            log.error("Validate auth Token - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }

        return paymentAuthTokenBO;
    }
    public BankVerifyRequestBO getBankVerification(PaymentAuthTokenBO paymentAuthTokenBO, CustomerBankInfo customerBankInfo, HttpHeaders headers, List<ErrorBO> errorBoList){
        String bvRefId = "";
        Map<String, String> params = new HashMap<String, String>();
        PaymentRequestBO paymentRequestBO=new PaymentRequestBO();
        BankVerifyRequestBO bankVerifyRequestBO=new BankVerifyRequestBO();
        String BearerToken="Bearer "+paymentAuthTokenBO.getData().token;
        log.info("BearerToken-----"+BearerToken);
        headers.put("Authorization", Collections.singletonList(BearerToken));
        params=paymentRequestBO.bankVerifyRequestBO(params, customerBankInfo);
        HttpEntity<BankVerifyRequestBO> request = new HttpEntity<>(bankVerifyRequestBO, headers);
        log.info("Validate customerBanking Request {}  | Request String: {}", bankVerifyRequestBO, gson.toJson(bankVerifyRequestBO));
        String BankVerifyURL = paymentConfig.capPayoutUrl() + paymentConfig.getSlash() + paymentConfig.capPayoutVerifyBankVersion() + paymentConfig.getSlash() + paymentConfig.capPayoutVerifyBank();
        log.info("BankVerifyURL==="+BankVerifyURL);
        ResponseEntity response = restTemplate.exchange(BankVerifyURL, HttpMethod.GET, request, String.class,params);


        if (response.getStatusCode().value() == 200 && response.getBody() != null) {
            String bankVerifyResponse=response.getBody().toString();
            bankVerifyResponse="{\"status\":\"SUCCESS\",\"subCode\":\"200\",\"message\":\"Validation request accepted successfully\",\"data\":{\"bvRefId\":\"1097292\"}}";
            bankVerifyRequestBO = gson.fromJson(bankVerifyResponse, BankVerifyRequestBO.class);
            if(bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.SUCCESS_STRING)){
                bvRefId=bankVerifyRequestBO.getData().getBvRefId();
                log.info("bvRefId----"+bvRefId);
            }
            else if(bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.ERROR_STRING) || bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.ERROR_STRING)){
                errorBoList.add(new ErrorBO("", bankVerifyRequestBO.getMessage(), bankVerifyRequestBO.getSubCode(), "errorConfig.validateCFBankFail()"));


            }


        } else {

            if (response.getStatusCodeValue() == 400 && response.getBody() != null) {
                if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
                    String statusCode=response.getStatusCode().toString();
                    errorBoList.add(new ErrorBO("", statusCode, ErrorsConstants.CF_BANK_INFO_FETCH_ERROR, "errorConfig.validateCFBankFail()"));
                }

            }
        }

        if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
            errorBoList.add(new ErrorBO("", "bank verification failed", ErrorsConstants.CF_BANK_INFO_FETCH_ERROR, "errorConfig.validateCFBankFail()"));
        }

        return bankVerifyRequestBO;
    }
    public BankVerifyRequestBO getBankVerifyStatus(BankVerifyRequestBO bankVerifyRequestBO, HttpHeaders headers, List<ErrorBO> errorBoList) {
        Map<String, String> params = new HashMap<String, String>();
        String BvRefId=bankVerifyRequestBO.getData().getBvRefId();
        params.put("bvRefId",BvRefId);
        HttpEntity<BankVerifyRequestBO> request = new HttpEntity<>(bankVerifyRequestBO, headers);
        log.info("Validate customerBanking Request {}  | Request String: {}", bankVerifyRequestBO, gson.toJson(bankVerifyRequestBO));
        String BankVerifyURL = paymentConfig.capPayoutUrl() + paymentConfig.getSlash() + paymentConfig.capPayoutVerifyBankVersion() + paymentConfig.getSlash() + paymentConfig.capPayoutVerifyBankStatus();
        log.info("BankVerifyURL==="+BankVerifyURL);
        ResponseEntity response = restTemplate.exchange(BankVerifyURL, HttpMethod.GET, request, String.class,params);
        int intStatusCode=response.getStatusCode().value();
        intStatusCode=200;
        if (intStatusCode == 200 && response.getBody() != null) {
            String bankVerifyStatusResponse=response.getBody().toString();
            bankVerifyStatusResponse="{\"status\":\"SUCCESS\",\"subCode\":\"200\",\"message\":\"Bank Account details verified successfully.\",\"data\":{\"bvRefId\":\"123456\",\"nameAtBank\":\"John Doe\",\"accountExists\":\"YES\",\"amountDeposited\":\"1.60\",\"bankAccount\":\"026291800001191\",\"ifsc\":\"YESB0000262\",\"utr\":\"012341234123\",\"nameMatchScore\":\"1\",\"nameMatchResult\":\"DIRECT_MATCH\"}}";
            bankVerifyRequestBO = gson.fromJson(bankVerifyStatusResponse, BankVerifyRequestBO.class);
            if(bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.SUCCESS_STRING) && bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.VALID_STRING)){
             return bankVerifyRequestBO;
            }
            if(bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.SUCCESS_STRING) && bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.INVALID_STRING)){
                errorBoList.add(new ErrorBO("", bankVerifyRequestBO.getMessage(), ErrorsConstants.CF_BANK_INFO_INVALID_ERROR, "errorConfig.validateCFBankVerifyStatusFail()"));

            }
            else if(bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.ERROR_STRING) || bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.ERROR_STRING)){
                errorBoList.add(new ErrorBO("", bankVerifyRequestBO.getAccountStatus(), bankVerifyRequestBO.getSubCode(), bankVerifyRequestBO.getAccountStatusCode()));

            }


        } else {

            if (response.getStatusCodeValue() == 400 && response.getBody() != null) {
                if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
                    String statusCode=response.getStatusCode().toString();
                    errorBoList.add(new ErrorBO("", statusCode, ErrorsConstants.CF_BANK_INFO_INVALID_ERROR, "errorConfig.validateCFBankVerifyStatusFail()"));
                }

            }
        }

        if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
            errorBoList.add(new ErrorBO("", "bank verification status failed", ErrorsConstants.CF_BANK_INFO_FETCH_ERROR, "errorConfig.validateCFBankVerifyStatusFail()"));
        }

        return bankVerifyRequestBO;
    }
        public RzpOrderCreationResponseBO makeOrderCreationCall(OrderBO orderBO, List<ErrorBO> errorBoList) {

            RzpOrderCreationResponseBO orderCreationResponseBO=new RzpOrderCreationResponseBO();
            RzpOrderCreationRequestBO orderCreationRequestBO=new RzpOrderCreationRequestBO();

            try {
            HttpHeaders headers = new HttpHeaders();
            headers=GeneralInfoService.LoadPaymentHeader(headers);
            PaymentRequestBO paymentRequestBO=new PaymentRequestBO();
            paymentRequestBO.razorpayCreateOrderBO(orderCreationRequestBO,orderBO);
            HttpEntity<RzpOrderCreationRequestBO> request = new HttpEntity<>(orderCreationRequestBO, headers);
            String orderURL = paymentConfig.PaymentBaseUrl()+paymentConfig.PaymentVersion()+ "/orders";
            log.info("orderURL---"+orderURL);
            ResponseEntity response = restTemplate.postForEntity(orderURL, request, String.class);

            if (response.getStatusCode().value() == 200 && response.getBody() != null) {
                orderCreationResponseBO = gson.fromJson(response.getBody().toString(), RzpOrderCreationResponseBO.class);
                log.info("OrderCreation Response | PaymentId: {}  | Response String: {}", orderCreationResponseBO.getReceipt(), response);

            } else {

                if (response.getStatusCodeValue() == 400 && response.getBody() != null) {


                }
            }

            if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
                errorBoList.add(new ErrorBO("", "validate/order", ErrorsConstants.VALIDATE_ORDER_TOKEN, errorConfig.validateVPAApiFail()));
            }

            if (orderCreationResponseBO != null && orderCreationResponseBO.getId()!="") {
                log.info("orderid---"+orderCreationResponseBO.getId());
                orderBO.setPaymentOrderID(orderCreationResponseBO.getId());
            }

        } catch (Exception e) {
            log.error("Validate Order - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }

        return orderCreationResponseBO;
    }
    public PlanBO createCFPlan(PlanBO planBO, List<ErrorBO> errorBoList) {


        try {
            Message message=new Message();
            HttpHeaders headers = new HttpHeaders();
            headers=GeneralInfoService.LoadPaymentHeader(headers);
            PlanRequestBO planRequestBO=new PlanRequestBO();
            PaymentRequestBO paymentRequestBO=new PaymentRequestBO();
            paymentRequestBO.planRequestBO(planBO,planRequestBO);
            HttpEntity<PlanRequestBO> request = new HttpEntity<>(planRequestBO, headers);
            log.info("Validate Plan Request {}  | Request String: {}", planRequestBO, gson.toJson(planRequestBO));
            String baseCFURL=paymentConfig.PaymentBaseUrl()+paymentConfig.paymentExternalApiVersion();
            ResponseEntity response = restTemplate.postForEntity(baseCFURL+"/subscription-plans", request, String.class);


            if (response.getStatusCode().value() == 200 && response.getBody() != null) {

                message = gson.fromJson(response.getBody().toString(), Message.class);
                planBO.setMessage(message);

            } else {

                if (response.getStatusCodeValue() == 400 && response.getBody() != null) {


                }
            }

            if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
                errorBoList.add(new ErrorBO("", "validate/order-token", ErrorsConstants.VALIDATE_ORDER_TOKEN, errorConfig.validateVPAApiFail()));
            }

            if (message != null && message.getStatus()!="") {
                log.info("message--"+message.getMessage()+"::status"+message.getStatus());
            }

        } catch (Exception e) {
            log.error("Validate Plan - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }

        return planBO;
    }
    public SubscriptionBO createCFSubscription(SubscriptionBO subscriptionBO, List<ErrorBO> errorBoList) {


        try {
            SubscriptionMessage subscriptionMessage=new SubscriptionMessage();
            HttpHeaders headers = new HttpHeaders();
            headers=GeneralInfoService.LoadPaymentHeader(headers);
            SubscriptionRequestBO subscriptionRequestBO=new SubscriptionRequestBO();
            PaymentRequestBO paymentRequestBO=new PaymentRequestBO();
            paymentRequestBO.subscriptionRequestBO(subscriptionBO,subscriptionRequestBO);
            HttpEntity<SubscriptionRequestBO> request = new HttpEntity<>(subscriptionRequestBO, headers);
            log.info("Validate subscription Request {}  | Request String: {}", subscriptionRequestBO, gson.toJson(paymentRequestBO));
            String baseCFURL=paymentConfig.PaymentBaseUrl()+paymentConfig.PaymentVersion();
            ResponseEntity response = restTemplate.postForEntity(baseCFURL+"/subscriptions", request, String.class);


            if (response.getStatusCode().value() == 200 && response.getBody() != null) {

                subscriptionMessage = gson.fromJson(response.getBody().toString(), SubscriptionMessage.class);
                subscriptionBO.setSubscriptionMessage(subscriptionMessage);

            } else {

                if (response.getStatusCodeValue() == 400 && response.getBody() != null) {


                }
            }

            if (errorBoList.size() == 0 && response.getStatusCode().value() != 200) {
                errorBoList.add(new ErrorBO("", "validate/order-token", ErrorsConstants.VALIDATE_ORDER_TOKEN, errorConfig.validateVPAApiFail()));
            }

            if (subscriptionMessage != null && subscriptionMessage.getStatus()!="") {
                log.info("subscription Message--"+subscriptionMessage.getMessage()+"::status"+subscriptionMessage.getStatus());
            }

        } catch (Exception e) {
            log.error("Validate Plan - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
        }

        return subscriptionBO;
    }
    public void isValidCustomer(CustomerInfoBO customerInfoBO, List<ErrorBO> errorBoList) {

        try {
            if(StringUtils.isEmpty(customerInfoBO.getCustomerId()))
                errorBoList.add(new ErrorBO("customer Id", "Invalid input"));
       ;


        } catch (Exception e) {
            log.error("Exception in customer creation - validate : ", e);
        }
    }

}
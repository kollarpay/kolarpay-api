package com.kp.core.app.controller.user;

import com.google.gson.Gson;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.OTPBO;
import com.kp.core.app.model.UserRequest;
import com.kp.core.app.service.Users.UserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/users")
public class UserController {

    @Autowired
    Gson gson;

    @Autowired
    UserInfoService userInfoService;


    @PostMapping(path = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> register(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#update email id POST API Request :" + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "updateEmail");
        List<ErrorBO> errorBoList = new ArrayList<>();
        int userId = 0;
        try {

            UserRequest userRequest = gson.fromJson(payload, UserRequest.class);
            globalResponseBO = userInfoService.updateUserEmailIdDetails(userRequest,errorBoList,true,"updateEmail");

        } catch (Exception e) {
            log.error("#User ID : " + userId + "| Exception : ", e);
        }
        log.debug("#User Mobile no update POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @PostMapping(path = "/mregister", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> saveUserByMobileNo(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#create user using mobile POST API Request :::" + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "createUser");
        List<ErrorBO> errorBoList = new ArrayList<>();
        int userId = 0;
        try {

            UserRequest userRequest = gson.fromJson(payload, UserRequest.class);
            globalResponseBO = userInfoService.registerMobile(userRequest,errorBoList,true,"createUser");

        } catch (Exception e) {
            log.error("#userinfo : " + userId + "| Exception : ", e);
        }
        log.debug("#User create using MobileNo POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @PostMapping(path = "/otpverify", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> otpVerify(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#create user POST API Request :" + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "OTPVERIFY");
        List<ErrorBO> errorBoList = new ArrayList<>();
        try {

            OTPBO otpbo = gson.fromJson(payload, OTPBO.class);
            globalResponseBO = userInfoService.verifyOTP(otpbo,errorBoList,"OTPVERIFY");

        } catch (Exception e) {
            log.error("#otpverify :  Exception : ", e);
        }
        log.debug("#otpverify POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);

    }
    @PostMapping(path = "/update-email", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> updateEmailId(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#update email id POST API Request :" + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "updateEmail");
        List<ErrorBO> errorBoList = new ArrayList<>();
        int userId = 0;
        try {

            UserRequest userRequest = gson.fromJson(payload, UserRequest.class);
            globalResponseBO = userInfoService.updateUserEmailIdDetails(userRequest,errorBoList,true,"updateEmail");

        } catch (Exception e) {
            log.error("#User ID : " + userId + "| Exception : ", e);
        }
        log.debug("#User email id update POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }


    @PostMapping(path = "/password", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> updatePassword(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#update password POST API Request :" + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "updatePassword");
        List<ErrorBO> errorBoList = new ArrayList<>();
        int userId = 0;
        try {
            UserRequest userRequest = gson.fromJson(payload, UserRequest.class);
            globalResponseBO = userInfoService.updatePassword(userRequest,errorBoList,true,"updatePassword");

        } catch (Exception e) {
            log.error("#User ID : " + userId + "| Exception : ", e);
        }
        log.debug("#User password update POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/info", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> ProductByTag(@RequestParam("type") String type,@RequestParam("str") String SearchStr,@RequestHeader MultiValueMap<String, Object> headers, @RequestBody(required = false) String payload) throws Exception {

        log.debug("# GET UserInfo by type API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "getCustomerInfoBySearchType");

        int userId = 0;
        int intType = 0;
        List<ErrorBO> errorBoList = new ArrayList<>();
        try {
            log.debug("#UserId : " + userId);
            if(!StringUtils.isEmpty(type)) intType=Integer.parseInt(type);

            if(intType > 0) {
                globalResponseBO = userInfoService.getUserInfo(intType,SearchStr,errorBoList,"getUserInfo");
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#getUserInfoBySearchType API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @PostMapping(path = "/signup", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> signup(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#signup POST API Request :" + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "signup");
        List<ErrorBO> errorBoList = new ArrayList<>();
        int userId = 0;
        try {

            UserRequest userRequest = gson.fromJson(payload, UserRequest.class);
            globalResponseBO = userInfoService.signup(userRequest,errorBoList,true,"signup");

        } catch (Exception e) {
            log.error("#User ID : " + userId + "| Exception : ", e);
        }
        log.debug("#User signup POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }


}

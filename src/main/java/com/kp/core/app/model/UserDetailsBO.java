package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDetailsBO {

    private int userId = 0;
    private String mobileNumber = "";
    private int pId;
    private String userName = "";
    private String partnerId = "";
    private boolean isAdmin = false;
    private String requestIP = "";
    private String smsFlag;
    private String lastLogin;
    private PrivateLabelBO plBo;
    private String jTag;
    private String from;
    private String email;
    private String network = "";
    private String password;
    private String clientId;
    private String role;
    private int roleId;
    int custmerCount;
    private String lockFlag = "";
    private int lockCount = 0;
    private boolean locked = false;
    private String userCode;
    private String userReferralCode;
    private String url;
    private String expiresOn;
    private boolean isValidSession = false;
    private boolean sendDebugReport = false;
    private String userType;
    private String verified = "true";
    private String referralText = "";
    private String adminErrorMsg = "";
}

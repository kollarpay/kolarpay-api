package com.kp.core.app.utils;

import com.kp.core.app.config.CacheConfiguration;
import com.kp.core.app.config.GeneralConfig;
import com.kp.core.app.config.MessagesConfig;
import com.kp.core.app.config.SmsConfig;
import com.kp.core.app.dao.GeneralInfoDAO;
import com.kp.core.app.dao.SchemeSearchDAO;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import okhttp3.*;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

import jakarta.servlet.http.HttpServletRequest;
import java.io.*;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@Slf4j
@Component
public class GeneralUtil {

    @Autowired
    private SchemeSearchDAO schemeSearchDAO;

    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    private GeneralConfig generalConfig = ConfigFactory.create(GeneralConfig.class);
    private SmsConfig smsConfig = ConfigFactory.create(SmsConfig.class);
    /**
     * sets provided inputs to appropriate parameters of GlobalResponseBO
     * @param stat
     * @param errorBoList
     * @param reponseData
     * @param globalResponseBO
     * @return
     */
    private static String emailRegexPattern = "^(.+)@(\\S+)$";

    public GlobalResponseBO getGlobalResponseBO(String stat, List<ErrorBO> errorBoList, Object reponseData, GlobalResponseBO globalResponseBO) {

        try {
            if(stat.equalsIgnoreCase(BPConstants.SUCCESS) || stat.equalsIgnoreCase("")) {
                globalResponseBO.setData(reponseData!=null ? reponseData : globalResponseBO.getData() );
                globalResponseBO.setSuccess(true);
                globalResponseBO.setDesc(BPConstants.SUCCESS);
                globalResponseBO.setCode(APIConstants.SUCCESS_INT);
            } else {
                if(errorBoList != null && !errorBoList.isEmpty())
                    globalResponseBO.setErrors(errorBoList);

                globalResponseBO.setSuccess(false);
                globalResponseBO.setData("");
                if(stat.equalsIgnoreCase(BPConstants.UNAUTHORIZED)) {
                    globalResponseBO.setDesc(APIConstants.UNAUTHORIZED_STRING);
                    globalResponseBO.setCode(APIConstants.UNAUTHORIZED);
                }
                else if(stat.equalsIgnoreCase(APIConstants.SERVER_ERROR_STRING)) {
                    globalResponseBO.setDesc(APIConstants.SERVER_ERROR_STRING);
                    globalResponseBO.setCode(APIConstants.SERVER_ERROR_INT);
                }
                else {
                    globalResponseBO.setDesc(APIConstants.BAD_REQUEST_STRING);
                    globalResponseBO.setCode(APIConstants.BAD_REQUEST);
                }
            }

        } catch( Exception e) {
            log.error("Exception: ", e);
        }
        return globalResponseBO;
    }

    public static JSONObject parseJson(String json) {
        JSONObject jsonObject = null;
        Gson gson=new GsonBuilder().create();
        try {
            jsonObject =gson.fromJson(json,JSONObject.class);
        } catch (Exception ex) {
            log.error("Ex:",ex);
        }
        return jsonObject;
    }

    public static String[] convertJSONArray(JSONArray jsonArray) {

        if(jsonArray==null)
            return null;

        String[] stringArray = new String[jsonArray.size()];
        try {
            for(int i=0; i<jsonArray.size(); i++) {
                stringArray[i] = jsonArray.getString(i);
            }
        } catch (Exception e) {
            log.error("Ex:",e);
        }

        return stringArray;
    }
    public static boolean patternMatches(String inputStr, String regexPattern) {
        return Pattern.compile(inputStr)
                .matcher(regexPattern)
                .matches();
    }

    public static List<Integer> convertJSONArrayToList(JSONArray jsonArray) {

        if(jsonArray==null)
            return null;

        List<Integer> list = new ArrayList<>();
        try {
            for(int i=0; i<jsonArray.size(); i++) {
                if(StringUtils.isNotEmpty(jsonArray.getString(i)))
                    list.add(Integer.parseInt(jsonArray.getString(i)));
            }
        } catch (Exception e) {
            log.error("Ex:",e);
        }

        return list;
    }

    /**
     * create a subList from given list, page number and count per page
     * @param list
     * @param page
     * @param count
     * @return
     */
    public static List getSubList(List list, int page, int count) {
        List subList = new ArrayList<>();

        int toSize = count * page;
        toSize = list.size() > toSize ? toSize : list.size();
        int fromSize = count * (page - 1);

        if (!(fromSize > toSize))
            subList = list.subList(fromSize, toSize);
        return subList;
    }

    public Jedis getRedisConnection() {

        Jedis jedis = null;
        try {
            CacheConfiguration cacheConfiguration= ConfigFactory.create(CacheConfiguration.class);
            String elastic_cache_url = cacheConfiguration.cacheUrl();
            jedis = new Jedis(elastic_cache_url, 6379);
            String check = jedis.ping();
            log.debug("ping status : " + check);
        } catch (Exception ex) {
            log.error("Error getting redis connnection : ", ex);
        }
        return jedis;
    }
    public MongoClient getMongoConnection(){
         MongoClient mongoClient = MongoClients.create(generalConfig.mongoDbUrl());
         return mongoClient;
    }
   public MongoDatabase getMongoDatabase(){
     MongoClient   mongoClient=  getMongoConnection();
     return mongoClient.getDatabase(generalConfig.mongoDatabaseName());
    }
    public ObjectInputStream getFromRedis(Jedis redisConn, String redisKey) {

        ObjectInputStream is = null;
        byte[] redisObject = redisConn.get(redisKey.getBytes());
        if (redisObject != null) {
            try {
                ByteArrayInputStream in = new ByteArrayInputStream(redisObject);
                is = new ObjectInputStream(in);
                log.debug("Retrieved value from redis for :" + redisKey);
            } catch (Exception ex) {
                log.error("Exception in getting data from Redis : ", ex);
                is = null;
            } finally {
                if (redisConn != null) redisConn.disconnect();
            }
        }
        return is;
    }

    public boolean putIntoRedis(Jedis redisConn, String redisKey, Object objValue, int expTime) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        boolean status = false;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(objValue);
            out.flush();
            byte[] objectValue = bos.toByteArray();
            redisConn.set(redisKey.getBytes(), objectValue);
            redisConn.expire(redisKey.getBytes(), expTime);
            status = true;
            log.debug("Stored in Redis for " + redisKey);
        } catch (Exception ex) {
            log.error("Exception in Stroing data to Redis : ", ex);
        } finally {
            if (redisConn != null) {
                redisConn.disconnect();
            }
        }
        return status;
    }

    public static String getTodayDate() {
        java.util.Date d1 = new Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        return sdf.format(c1.getTime());
    }

    public static double getOneDecimalDoubleNumber(double floatValue) {

        try{
            DecimalFormat df1 = new DecimalFormat("###0.0");
            return Double.parseDouble(df1.format(floatValue));
        }catch (Exception e){
            log.error("Exception while number format : ",e);
            return 0.0d;
        }

    }
    public Response sendSMS(String mobile,String message,String type) throws IOException {
        mobile="+91-"+mobile;
        String messagez=smsConfig.getMessage();
        messagez=messagez.replaceAll("<!code>",message);
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
       String baseUrl= smsConfig.getBaseURL()+smsConfig.getSlash()+smsConfig.getVersion()+smsConfig.getSlash()+smsConfig.getSID()+smsConfig.getSlash()+smsConfig.getStrMessage();
        RequestBody Reqbody = new FormBody.Builder()
                .add("to", mobile)
                .add("type", smsConfig.getMessageType())
                .add("sender", smsConfig.getSender())
                .add("body", messagez)
                .add("template_id", smsConfig.getTemplate())
                .build();

        Request request = new Request.Builder()
                //.url("https://api.kaleyra.io/v1/HXIN1731471518IN/messages")
                .url(baseUrl)
                .addHeader("api-key",smsConfig.getSmsApi())
                .addHeader("Content-Type",smsConfig.getContentType())
                .post(Reqbody)
                .build();
        Response response = client.newCall(request).execute();
        return response;
    }
    public Response sendMsg91SMS(String mobile,String otp,String type) throws IOException {
        //https://control.msg91.com/api/v5/otp?mobile=&template_id=
        mobile="91"+mobile;
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        String baseUrl= smsConfig.getMsgBaseURL()+smsConfig.getSlash()+smsConfig.getMsgApi()+smsConfig.getSlash()+smsConfig.getMsgVersion()+smsConfig.getSlash()+smsConfig.getMsgMethod()+"?mobile="+mobile
                +"&template_id="+smsConfig.getMsgTemplate()+"&otp_length="+smsConfig.getMSgOtpLength()+"&otp="+otp+"&sender="+smsConfig.getSender();
        log.info("baseUrl---"+baseUrl);
        RequestBody Reqbody = new FormBody.Builder()
//                .add("mobile", mobile)
                 .add("sender", smsConfig.getSender())
//                .add("otp", otp)
//                .add("otp_length", smsConfig.getMSgOtpLength())
//                .add("template_id", smsConfig.getMsgTemplate())
                .build();

        Request request = new Request.Builder()
                .url(baseUrl)
                .addHeader("accept", smsConfig.getMsgContentType())
                .addHeader("Content-Type",smsConfig.getMsgContentType())
                .addHeader("authkey",smsConfig.getMsgAuthKey())
                .post(Reqbody)
                .build();
        Response response = client.newCall(request).execute();
        return response;
    }

    public String getTwoBigDecimalNumber(BigDecimal bigDecimalValue) throws Exception {

        if ((bigDecimalValue == null) || (bigDecimalValue.equals(new BigDecimal(0)))) return "0.00";
        DecimalFormat df1 = new DecimalFormat("###0.00");
        return df1.format(bigDecimalValue);
    }

    /**
     * Formates input big decimal into rupees format, along with required decimal places.
     *
     * @param bigDecimalValue
     * @param intDecimals     - How many decimal points to display
     * @return
     */
    public static String getRupeeBigDecimal(BigDecimal bigDecimalValue, int intDecimals) {

        DecimalFormat df1;
        String stringReturn = "";

        try {

            if (intDecimals == 2)
                df1 = new DecimalFormat("###0.00");
            else if (intDecimals == 3)
                df1 = new DecimalFormat("###0.000");
            else if (intDecimals == 4)
                df1 = new DecimalFormat("###0.0000");
            else if (intDecimals > 4)
                df1 = new DecimalFormat("###0.00000");
            else
                df1 = new DecimalFormat("###0");

            if (bigDecimalValue == null) {
                if (intDecimals == 2)
                    return "0.00";
                else if (intDecimals == 3)
                    return "0.000";
                else if (intDecimals == 4)
                    return "0.0000";
                else if (intDecimals > 4)
                    return "0.00000";
                else
                    return "0";
            } else {
                boolean negativeFlag = false;

                String strAmount = (df1.format(bigDecimalValue)).split("[.]")[0];
                String strDecimal = "";
                if (intDecimals > 0)
                    strDecimal = "." + (df1.format(bigDecimalValue)).split("[.]")[1];
                else
                    strDecimal = "";

                if (bigDecimalValue.signum() < 0) {
                    negativeFlag = true;
                    if (strAmount.equals("-"))
                        strAmount = "-0";
                    strAmount = String.valueOf(new BigDecimal(strAmount).multiply(new BigDecimal(-1)));
                }
                int stringLength = strAmount.length();
                int intSearch = 1;
                if (stringLength % 2 == 0) intSearch = 0;

                for (int i = 0; i < stringLength; i++) {
                    stringReturn += strAmount.charAt(i);

                    if ((stringLength - 3) > i) {
                        if (i % 2 == intSearch)
                            stringReturn += ",";
                    }
                }
                if (negativeFlag)
                    return "-" + stringReturn + strDecimal;
                else
                    return stringReturn + strDecimal;
            }
        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        return stringReturn;
    }

    public String getIPaddress(HttpServletRequest request) {
        try{
            return request.getRemoteAddr();
        }catch (Exception e){
            log.error("Ex:IPAddress: ", e);
            return "";
        }
    }

    /**
     * calculates the difference between two dates and input should be in yyyy-MM-dd format only
     * @param fromDate
     * @param toDate
     * @return
     */
    public int getYearDifference(String fromDate, String toDate, boolean considerDate) {

        log.debug("Year Diff calculation for From : " + fromDate + " | To : " + toDate);
        int diff = 0;

        try {

            if(fromDate == null || fromDate.trim().length()==0)
                fromDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            else if(toDate == null || toDate.trim().length()==0)
                toDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            String[] from = fromDate.split("-");
            String[] to = toDate.split("-");
            diff = Integer.parseInt(to[0]) - Integer.parseInt(from[0]);
            if (diff > 0) {
                int months = Integer.parseInt(to[1]) - Integer.parseInt(from[1]);
                if (months < 0)
                    diff = diff - 1;
                if (months == 0 && considerDate) {
                    int days = Integer.parseInt(to[2]) - Integer.parseInt(from[2]);
                    if (days < 0)
                        diff = diff - 1;
                }
            }
            log.info("Difference : " + diff);
        } catch (Exception actEx) {
            log.error("Exception :", actEx);
        }
        return diff;
    }

    public String NullCheck(String inputString) {
        try {
            if (inputString == null || inputString.trim().equals(""))
                inputString = "";
        } catch (Exception e) {
            log.info((new StringBuilder("Exception in NullCheck ")).append(e).toString());
            inputString = "";
        }
        return inputString.trim();
    }

    public String getTransactionMsg1(int dayOfWeek, boolean isHoliday, String transactionTime) {
        String returnString = "";

        if (transactionTime == null) {
            return returnString;
        }
        if (transactionTime.trim().equals("")) {
            return returnString;
        }

        try {

            MessagesConfig messagesConfig = ConfigFactory.create(MessagesConfig.class);

            String initTimeStr = generalConfig.transMsgInitTime();
            if (initTimeStr == null) {
                initTimeStr = "23:59:59";
            }
            String finalTimeStr = generalConfig.transMsgFinalTime();
            if (finalTimeStr == null) {
                finalTimeStr = "14:00:01";
            }

            java.sql.Time transTime = java.sql.Time.valueOf(transactionTime);
            java.sql.Time initTime = java.sql.Time.valueOf(initTimeStr);
            java.sql.Time finalTime = java.sql.Time.valueOf(finalTimeStr);


            if (transTime.after(initTime) && transTime.before(finalTime) && (dayOfWeek > 1 && dayOfWeek < 7) && !isHoliday) //except saturday and sunday in tx time
                returnString = messagesConfig.investorTransMsgBefore();
            else
                returnString = messagesConfig.investorTransMsgAfter();

            if (returnString == null) {
                returnString = "";
            }

        } catch (Exception utilEx) {
            log.error("Get Transaction message #TransactionTime : " + transactionTime + " | Exception : " , utilEx);
            returnString = "";
        }

        return returnString;
    }

    public String getServerName(HttpServletRequest request) {

        String serverName;

        if(request == null)
            return "";

        if (StringUtils.isEmpty(request.getHeader("X-Forwarded-Host")))
            serverName = request.getServerName() + ":" + request.getLocalPort();
        else
            serverName = request.getHeader("X-Forwarded-Host");

        return serverName;
    }

    public String getPaymentAllow(int userId) {

        String paymentAllow = "NO";
        try {

            String paymentDemouserFlag = generalConfig.paymentDemoUserFlag();
            if (paymentDemouserFlag != null && paymentDemouserFlag.equalsIgnoreCase("ALL"))
                paymentAllow = "ALL";

            else if (paymentDemouserFlag != null && paymentDemouserFlag.equalsIgnoreCase("YES")) {
                String paymentDemouser = generalConfig.paymentDemoUser();
                log.debug(" demo user list  " + paymentDemouser);
                if (paymentDemouser != null) {
                    boolean isPaymentDemoUser = paymentDemouser.contains("'" + userId + "'");
                    log.debug("isPaymentDemoUser " + isPaymentDemoUser);
                    if (isPaymentDemoUser) {
                        paymentAllow = "YES";
                    }
                }
            }
        } catch (Exception e) {
            log.info("Exception in getPaymentAllow : " , e);
        }

        return paymentAllow;
    }

    /**
     * create ITC value for each scheme
     * @param paymentId
     * @param currentTime
     * @param amcSchemeCode
     * @param amcCode
     * @param userId
     * @return
     * @throws Exception
     */
    public String getITC(int paymentId, String currentTime, String amcSchemeCode, String amcCode, int userId) throws Exception {


        String logPrefix = "#PaymentId : " + paymentId + " | #AmcSchemeCode : " + amcSchemeCode + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "Get ITC");

        String itc = "L_ALL_ALL";
        try {

            String initTimeStr = generalConfig.NLInitialTime();
            String finalTimeStr = generalConfig.NLFinalTime();

            if (StringUtils.isEmpty(initTimeStr))
                initTimeStr = "10:59:59";
            if (StringUtils.isEmpty(finalTimeStr))
                finalTimeStr = "14:00:01";

            java.sql.Time transTime = java.sql.Time.valueOf(currentTime);
            java.sql.Time initTime = java.sql.Time.valueOf(initTimeStr); //new Time(10, 59, 59);
            java.sql.Time finalTime = java.sql.Time.valueOf(finalTimeStr); //new Time(14, 00, 01);

            if (transTime.after(initTime) && transTime.before(finalTime)) {
                itc = "N_ALL_ALL";
            }

            String relianceAmcCode = generalConfig.relianceAmcCode();

            if(StringUtils.isNotEmpty(amcCode) && StringUtils.isNotEmpty(amcSchemeCode) && StringUtils.isNotEmpty(relianceAmcCode) && amcCode.trim().equalsIgnoreCase(relianceAmcCode.trim())) {

                String sClass = schemeSearchDAO.getSClassFromRMFSchemeCheck("RMF", amcSchemeCode.trim());
                if (sClass != null && !sClass.equals(""))
                    itc = sClass + "_RMF_" + amcSchemeCode;
            }

        } catch (Exception utilEx) {
            log.error(logPrefix + "UTIL - ITC CODE GENERATION Exception : " , utilEx);
            throw new Exception(utilEx);
        }
        return itc;
    }

    public boolean validatePositiveNumber(String positiveNumber) {
        int value   =   0;
        try{
            value = Integer.parseInt(positiveNumber);
            if(value<0) return  false;
            else return  true;
        }catch (Exception e){
            return false;
        }
    }
    public boolean dateRangeValidation(String dateValue) {
        int value   =   0;
        try{
            value = Integer.parseInt(dateValue);
            if( (value<0) || (value>28) ) return  false;
            else return  true;
        }catch (Exception e){
            return false;
        }
    }
    public boolean dividentValidate(String optionValue) {
        String[] values = {"Dividend", "Growth"};
        try{
            if(Arrays.stream(values).anyMatch(optionValue::equals)) return  true;
            else return  false;
        }catch (Exception e){
            return false;
        }
    }
    public boolean dividentOptionValidate(String optionValue) {
        String[] values = {"Payout", "Reinvestment"};
        try{
            if(Arrays.stream(values).anyMatch(optionValue::equals)) return  true;
            else return  false;
        }catch (Exception e){
            return false;
        }
    }
    public boolean frequencyValidate(String frequencyValue) {
        String[] values = {"monthly"};
        try{
            if(Arrays.stream(values).anyMatch(frequencyValue::equals)) return  true;
            else return  false;
        }catch (Exception e){
            return false;
        }
    }
    //------------- NullCheck for String -----------------------------------------------------------------
    public static String NullCheckString(String inputString) {
        try {
            if ((inputString == null) || (inputString.trim().equals("") || (String.valueOf(inputString).trim().equalsIgnoreCase(null))))
                inputString = "";
        } catch (Exception e) {
            inputString = "";
        }
        return inputString.trim();
    }

    //------------- NullCheck for Int -----------------------------------------------------------------
    public static int NullCheckInteger(String inputString) {
        int nullCheckInt = 0;
        try {
            if ((inputString == null) || (inputString.trim().equals("") || (String.valueOf(inputString).trim().equalsIgnoreCase(null))))
                inputString = "0";
            nullCheckInt = Integer.parseInt(inputString);
        } catch (Exception e) {
            inputString = "0";
        }
        return nullCheckInt;
    }


    public static boolean validDateFormat(String dateValue, String dateFormat) {

        SimpleDateFormat sdfrmt = new SimpleDateFormat(dateFormat);
        sdfrmt.setLenient(false);
        try
        {
            Date javaDate = sdfrmt.parse(dateValue);
            return true;
        }
        catch (Exception e)
        {   return false;   }

    }
    public boolean sipTypeValidate(String optionValue) {
        String[] values = {"regular", "flexi","stepup","alert"};
        try{
            if(Arrays.stream(values).anyMatch(optionValue::equals)) return  true;
            else return  false;
        }catch (Exception e){
            return false;
        }
    }
    public static String maskBankAccountNumber(String accountNumber) {

        String finalAccountNumber = "";
        try {
            if (accountNumber == null) {
                accountNumber = "";
            }
            StringBuffer givenACNo = new StringBuffer(accountNumber.trim());

            if (accountNumber.length() >= 5) {
                finalAccountNumber = givenACNo.replace(0, accountNumber.length() - 4, "xxxx").toString();
            } else {
                finalAccountNumber = accountNumber;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return finalAccountNumber;
    }

    public boolean displayTotalRedmptionValueFlag(int plId) {

        boolean displayTotalRedemption = false;
        try {


            String plIds = generalConfig.displayTotalRedemptionPlId();
            if (plIds != null) {
                String[] plIdsArray = plIds.split(",");
                for (String aplId : plIdsArray) {
                    if (aplId.equalsIgnoreCase(String.valueOf(plId))) {
                        displayTotalRedemption = true;
                        break;
                    }
                }
            }

        } catch (Exception e) {
            log.error("Display Total Redemption value flag - Exception: ", e);
        }
        return displayTotalRedemption;
    }
    public static Date getDateFromStr(String dateString) {
        Date parsed;
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            parsed = format.parse(dateString);
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe);
        }
        return parsed;
    }
    public static String gettodayDtyyyMMdd() {
        Calendar c = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        // System.out.println(c.getTime());
        String startDate = dateFormat.format(c.getTime());
        return startDate;
    }
    public static String getZeroBigDecimalRupees(BigDecimal bigDecimalValue) {

        if (bigDecimalValue == null) {
            return "0";
        } else {
            String stringReturn = "";
	    	/*
	    	DecimalFormat decimalFormatter = new DecimalFormat("#,##,##,##,##,###.00");
	    	stringReturn = decimalFormatter.format(bigDecimalValue);
	    	//stringReturn	=	String.valueOf(bigDecimalValue);
	    	*/
            boolean negativeFlag = false;
            DecimalFormat df1 = new DecimalFormat("###0.00");

            String strAmount = (df1.format(bigDecimalValue)).split("[.]")[0];
            String strDecimal = (df1.format(bigDecimalValue)).split("[.]")[1];
            if (bigDecimalValue.signum() < 0) {
                negativeFlag = true;
                if (strAmount.equals("-"))
                    strAmount = "-0";
                strAmount = String.valueOf(new BigDecimal(strAmount).multiply(new BigDecimal(-1)));
            }
            int stringLength = strAmount.length();
            int intSearch = 1;
            if (stringLength % 2 == 0) intSearch = 0;

            for (int i = 0; i < stringLength; i++) {
                stringReturn += strAmount.charAt(i);

                if ((stringLength - 3) > i) {
                    if (i % 2 == intSearch)
                        stringReturn += ",";
                }
            }
            if (negativeFlag)
                return "-" + stringReturn;
            else
                return stringReturn;
        }
    }

    public String getNextEcsDate(String ecsdate, String ecsCount) {

        Calendar now = Calendar.getInstance();
        Calendar ecsDate = Calendar.getInstance();
        Calendar ecsCountDate = Calendar.getInstance();

        int[] ecs = new int[3];
        StringTokenizer st = new StringTokenizer(ecsdate, "/");
        int i = 0;

        while (st.hasMoreTokens()) {
            ecs[i] = Integer.parseInt(st.nextToken());
            i++;
        }
        String[] months = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        int currentDate = now.get(Calendar.DATE);
        int currentMonth = now.get(Calendar.MONTH);
        int currentYear = now.get(Calendar.YEAR);
        ecs[1] = ((ecs[1] + 11) % 12);
        ecsDate.set(ecs[2], ecs[1], ecs[0]);

        boolean isStarted = false;
        if (ecsDate.after(now) || ecsDate.equals(now)) {
            ecsCountDate.set(ecs[2], ecs[1], Integer.parseInt(ecsCount));
        } else if (ecsDate.before(now)) {
            isStarted = true;
            ecsCountDate.set(currentYear, currentMonth, Integer.parseInt(ecsCount));

        }

        if (isStarted) {
            if (ecsCountDate.before(now)) {
                now = ecsCountDate;
                currentMonth = now.get(Calendar.MONTH);
                currentYear = now.get(Calendar.YEAR);
                currentMonth += 1;
                if (currentMonth > 11) {
                    currentYear += 1;
                }
            }
        } else {
            if (ecsCountDate.after(now) & (ecsDate.before(ecsCountDate) | ecsDate.equals(ecsCountDate))) {
                now = ecsDate;
                currentMonth = now.get(Calendar.MONTH);
                currentYear = now.get(Calendar.YEAR);
            } else if (ecsCountDate.after(now) & (ecsDate.after(ecsCountDate))) {
                now = ecsDate;
                currentMonth = now.get(Calendar.MONTH);
                currentYear = now.get(Calendar.YEAR);
                currentMonth += 1;
                if (currentMonth > 11) {
                    currentYear += 1;
                }
            } else {
                currentMonth += 1;
                if (currentMonth > 11) {
                    currentYear += 1;
                }
            }
        }

        currentDate = Integer.parseInt(ecsCount);
        currentMonth = currentMonth % 12;
        if (Integer.parseInt(ecsCount) < 10) {
            ecsCount = "0" + currentDate;
        }
        String resultDate = ecsCount + "/" + months[currentMonth] + "/" + currentYear;

        return resultDate;
    }

    public boolean isChangeAmountRestricted(String schemeCode) {

        boolean restricted = false;

        try {

            String[] schemes = generalConfig.changeAmountRestrictedSchemes().split(",");

            for(String scheme : schemes) {

                if(schemes.equals(schemeCode))
                    restricted = true;
            }

        } catch (Exception e) {
            log.error("Exception : ", e);
        }

        return restricted;
    }

    public static String commaSqlString(String s) {
        String[] strings=s.split(",");
        StringBuffer fullString = new StringBuffer();
        for (int i = 0; i < strings.length; i++) {
            if ( (strings.length==1) || (i == (strings.length-1))) {
                fullString = fullString.append("'").append(strings[i]).append("'");
            } else {
                fullString = fullString.append("'").append(strings[i]).append("',");

            }
        }
        return fullString.toString();
    }

    public boolean isSchemeHasRestrictions(String schemeCode) {

        boolean restricted = false;

        try {

            List<String> schemes = generalInfoDAO.getRestrictedSchemes();

            for(String scheme : schemes) {

                if(schemes.equals(schemeCode))
                    restricted = true;
            }

        } catch (Exception e) {
            log.error("Exception : ", e);
        }

        return restricted;
    }

    public boolean isDSP(String schemeCode) {

        boolean restricted = false;

        try {

            String[] schemes = BPConstants.DSP_MICRO_SIP_SCHEMES.split(",");

            for(String scheme : schemes) {

                if(schemes.equals(schemeCode))
                    restricted = true;
            }

        } catch (Exception e) {
            log.error("Exception : ", e);
        }

        return restricted;
    }

    public String getEcsDay(String ecsdate) {
        String ecsDay = "";
        String day = "";
        String month = "day of the month";

        if (ecsdate.equals("1") || ecsdate.equals("21")) {
            day = ecsdate + "st ";
        } else if (ecsdate.equals("2") || ecsdate.equals("22")) {
            day = ecsdate + "nd ";
        } else if (ecsdate.equals("3") || ecsdate.equals("23")) {
            day = ecsdate + "rd ";
        } else {
            day = ecsdate + "th ";
        }
        ecsDay = day + month;
        return ecsDay;
    }

    public String getSTPWeekDay(int date) {
        String stpDay = "";

        if (date <= 6) {
            String[] strDays = new String[]{"", "", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
            stpDay = "Every " + strDays[date];
        }
        return stpDay;
    }

    public static String generateOTP(int length) {

        log.debug("Generate OTP of length : " + length);

        String otp = "";
        Random random = new Random();
        String numbers = "0123456789";
        for (int i = 0; i < length; i++) {
            otp = otp + numbers.charAt(random.nextInt(numbers.length()));
        }
        log.debug("OTP length : " + otp.length());

        return otp;
    }
    public static String getReferenceIdByCustomerID(String customerId){
        String referenceId  =   "";
        if(StringUtils.isNotEmpty(customerId)){
            if(customerId.length()<=7)
                referenceId =   "I".concat(String.format("%7s", customerId).replace(' ', '0'));
            else
                referenceId =   "I".concat(customerId);
        }
        return referenceId;
    }
}

package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class InvestorBankInfoBO {

    private int investorID = 0;
    private String investorName = "";
    private int holdingProfileId = 0;
    private String banklookupid = "";
    private int bankId = 0;
    private String bankName = "";
    private String bankCity = "";
    private String branchAddress = "";
    private String accountNumber = "";
    private String bankAccountType = "";
    private String micrCode = "";
    private String IFSCCode = "";
    private String NEFTCode = "";

}

package com.kp.core.app.dao;


import com.google.gson.Gson;
import com.kp.core.app.model.Products.*;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Repository

public class ProductDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;
    @Autowired
    Gson gson;


    public ProductBO SaveProduct(ProductBO productBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert Product");


        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery = " INSERT INTO product (collection_id, title, body_html, vendor, " +
                    " product_type, handle, published_scope, tags, created_at,updated_at, created_user, updated_user,status,shape) " +
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?,NOW(), NOW(), ?, ?, ?,?)  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[] { "product_id" });

                ps.setLong(1, productBO.getCollection_id());
                ps.setString(2, productBO.getTitle());
                ps.setString(3, productBO.getBodyHtml());
                ps.setString(4, productBO.getVendor());
                ps.setString(5, productBO.getProductType());
                ps.setString(6, productBO.getHandle());
                ps.setString(7, productBO.getPublishedScope());
                ps.setString(8, productBO.getTags());
                ps.setInt(9, userId);
                ps.setInt(10, userId);
                ps.setInt(11, 1);
                ps.setString(12, productBO.getShape());
                return ps;

            }, keyHolder);
            long productId=keyHolder.getKey().longValue();

            if(productId > 0) {
                productBO.setProductId(productId);
            }

        } catch (Exception e) {
            log.error(logPrefix + "Exception in collections insertion: ", e);
        }

        log.debug(logPrefix + "collectionId : " + productBO.getProductId());
        return productBO;
    }
    public ProductBO SaveProductVariant(ProductBO productBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert Product variant");
        ProductBO.ProductVariant  variant=productBO.getProductVariant();
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery="INSERT INTO product_variants " +
                    " (product_id, color, price, weight, title, grams, sku, inventory_quantity, inventory_item_id, inventory_management, \"position\", created_by, modified_by, created_at, modified_at, flag, weight_unit) " +
                    " VALUES(?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?,NOW(),NOW(),?,?)";
                jdbcTemplate.update(connection -> {
                    PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[]{"id"});

                    ps.setLong(1, productBO.getProductId());
                    ps.setString(2, variant.getColor());
                    ps.setDouble(3, variant.getPrice());
                    ps.setDouble(4, variant.getWeight());
                    ps.setString(5, variant.getTitle());
                    ps.setDouble(6, variant.getGrams()==null?0: (Double) variant.getGrams());
                    ps.setString(7, variant.getSku());
                    ps.setInt(8, variant.getInventoryQuantity()==null?0:variant.getInventoryQuantity());
                    ps.setInt(9, variant.getInventoryItemId()==null? 0: variant.getInventoryItemId());
                    ps.setString(10, variant.getInventoryManagement());
                    ps.setInt(11, variant.getPosition());
                    ps.setInt(12, userId);
                    ps.setInt(13, userId);
                    ps.setString(14, "A");
                    ps.setString(15, variant.getWeightUnit());
                   // ps.setString(16, variant.getExpireAt());

                    return ps;

                }, keyHolder);
                long variantId = keyHolder.getKey().longValue();
                if (variantId > 0) {
                    productBO.getProductVariant().setId(variantId);
                    productBO.getProductVariant().setProductId(productBO.getProductId());
                }
           // }
        } catch (Exception e) {
            log.error(logPrefix + "Exception in Product Variant insertion: ", e);
        }

        log.debug(logPrefix + "Product Id : " + productBO.getProductId());
        return productBO;
    }
    public ProductBO saveProductDiscount(ProductBO productBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert Product Discount");
        ProductBO.ProductDiscount productDiscount=productBO.getProductDiscount();
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();

            String sqlQuery="INSERT INTO product_discount\n" +
                    "(product_id, discount_value, discount_unit, create_at, valid_from, valid_until, coupon_code, minimum_order, maximum_discount_amount, is_redeem_allowed)\n" +
                    "VALUES(?, ?, ?, NOW(), TO_TIMESTAMP(?,'YYYY-MM-DD HH:MI:SS'),TO_TIMESTAMP(?,'YYYY-MM-DD HH:MI:SS'), ?, ?, ?, ?)";
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[]{"id"});

                ps.setLong(1, productBO.getProductId());
                ps.setLong(2, productDiscount.getDiscountValue());
                ps.setString(3, productDiscount.getDiscountUnit());
                ps.setString(4, productDiscount.getValidFrom());
                ps.setString(5, productDiscount.getValidUntil());
                ps.setString(6, productDiscount.getCouponCode());
                ps.setInt(7, productDiscount.getMinimumOrder());
                ps.setInt(8, productDiscount.getMaximumDiscountAmount());
                ps.setString(9, productDiscount.getIsredeemAllowed());

                return ps;

            }, keyHolder);
            long discountId = keyHolder.getKey().longValue();
            if (discountId > 0) {
                productBO.getProductDiscount().setId(discountId);
            }
            // }
        } catch (Exception e) {
            log.error(logPrefix + "Exception in Product Discount insertion: ", e);
        }

        log.debug(logPrefix + "Product Id : " + productBO.getProductId());
        return productBO;
    }
    public ProductBO saveProductCategoryDiscount(ProductBO productBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert Product Category Discount");
        ProductBO.ProductCategoryDiscount productCategoryDiscount=productBO.getProductCategoryDiscount();
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();

            String sqlQuery="INSERT INTO product_category_discount\n" +
                    "(product_id, discount_value, discount_unit, create_at, valid_from, valid_until, coupon_code, minimum_order, maximum_discount_amount, is_redeem_allowed)\n" +
                    "VALUES(?, ?, ?, NOW(), ?, ?, ?, ?, ?, ?,?)";
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[]{"id"});

                ps.setLong(1, productBO.getProductId());
                ps.setLong(2, productCategoryDiscount.getDiscountValue());
                ps.setLong(3, productCategoryDiscount.getDiscountUnit());
                ps.setDouble(4, userId);
                ps.setString(5, productCategoryDiscount.getValidFrom());
                ps.setString(6, productCategoryDiscount.getValidUntil());
                ps.setString(7, productCategoryDiscount.getCouponCode());
                ps.setInt(8, productCategoryDiscount.getMinimumOrder());
                ps.setInt(9, productCategoryDiscount.getMaximumDiscountAmount());
                ps.setString(10, productCategoryDiscount.getIsredeemAllowed());

                return ps;

            }, keyHolder);
            long categoryDiscountId = keyHolder.getKey().longValue();
            if (categoryDiscountId > 0) {
                productBO.getProductCategoryDiscount().setId(categoryDiscountId);
            }
            // }
        } catch (Exception e) {
            log.error(logPrefix + "Exception in Product Category discount insertion: ", e);
        }

        log.debug(logPrefix + "Product Id : " + productBO.getProductId());
        return productBO;
    }

    public ProductBO SaveProductImage(ProductBO productBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert Product image");
        List<ProductBO.ProductImage> images=productBO.getProductImages();

        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery = " INSERT INTO product_images (product_id, position, alt,width,src, height, " +
                    " created_at,updated_at, created_by, updated_by,flag) " +
                    " VALUES(?, ?, ?, ?, ?, ?,NOW(), NOW(), ?, ?, ?)  ";
            for (ProductBO.ProductImage image:images) {
                jdbcTemplate.update(connection -> {
                    PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[]{"id"});

                    ps.setLong(1, productBO.getProductId());
                    ps.setInt(2, image.getPosition());
                    ps.setString(3, image.getAlt());
                    ps.setDouble(4, image.getWidth());
                    ps.setString(5, image.getSrc());
                    ps.setDouble(6, image.getHeight());
                    ps.setInt(7, userId);
                    ps.setInt(8, userId);
                    ps.setString(9, "A");
                    return ps;

                }, keyHolder);
                long imageId = keyHolder.getKey().longValue();
                if (imageId > 0) {
                    image.setImageid(imageId);
                    image.setProductId(productBO.getProductId());

                }
            }
        } catch (Exception e) {
            log.error(logPrefix + "Exception in Product Image insertion: ", e);
        }

        log.debug(logPrefix + "Product Id : " + productBO.getProductId());
        return productBO;
    }

    public ProductBO getProductVariant(int userId, ProductBO productBO){

        String logPrefix = "#UserId : " + userId+" variant product id----"+productBO.getProductId();
        List<ProductBO.ProductVariant> variants=new ArrayList<>();
        try {


            String sqlQuery = " select id,product_id,price,sale_price,weight,title,grams,sku,inventory_item_id,\"position\",expire_at,effectfrom_at,barcode,taxable,image_id,requires_shipping from product_variants where product_id='"+productBO.getProductId()+"' order by \"position\" asc ";

             variants = jdbcTemplate.query(sqlQuery, new RowMapper<ProductBO.ProductVariant>() {
                @Override
                public ProductBO.ProductVariant mapRow(ResultSet rs, int rowNum) throws SQLException {
                    ProductBO.ProductVariant variant=new ProductBO.ProductVariant();
                    long id = (rs.getLong("id")>0) ? rs.getLong("id"): 0;
                    long product_id = (rs.getLong("product_id")>0) ? rs.getLong("product_id"): 0;
                    double price = (rs.getDouble("price")>0) ? rs.getInt("price"): 0;
                    double sale_price = (rs.getDouble("sale_price")>0) ? rs.getInt("sale_price"): 0;
                    double weight = (rs.getDouble("weight")>0) ? rs.getInt("weight"): 0;
                    String title = (rs.getString("title") != null) ? rs.getString("title").trim() : "";
                    double grams = (rs.getDouble("grams")>0) ? rs.getInt("grams"): 0;
                    int position = (rs.getInt("position")>0) ? rs.getInt("position"): 0;
                    String sku = (rs.getString("sku") != null) ? rs.getString("sku").trim() : "";
                    String expire_at = (rs.getString("expire_at") != null) ? rs.getString("expire_at").trim() : "";
                    int inventory_item_id = (rs.getInt("inventory_item_id")>0) ? rs.getInt("inventory_item_id"): 0;
                    String effectfrom_at = (rs.getString("effectfrom_at") != null) ? rs.getString("effectfrom_at").trim() : "";
                    String barcode = (rs.getString("barcode") != null) ? rs.getString("barcode").trim() : "";
                    Boolean taxable = rs.getBoolean("taxable");
                    int image_id = (rs.getInt("image_id")>0) ? rs.getInt("image_id"): 0;
                    Boolean requires_shipping = rs.getBoolean("requires_shipping");
                    variant.setId(id);
                    variant.setProductId(product_id);
                    variant.setPrice(price);
                    variant.setSale_price(sale_price);
                    variant.setWeight(weight);
                    variant.setGrams(grams);
                    variant.setSku(sku);
                    variant.setInventoryItemId(inventory_item_id);
                    variant.setPosition(position);
                    variant.setTitle(title);
                    variant.setExpireAt(expire_at);
                    variant.setEffectFrom(effectfrom_at);
                    variant.setBarcode(barcode);
                    variant.setTaxable(taxable);
                    variant.setRequiresShipping(requires_shipping);
                    variant.setImageId(image_id);
                    return variant;
                }


            });


            if(variants.size()>0){
                log.info("variants---"+variants.size());
                productBO.setVariants(variants);
            }

        } catch (Exception daoEx) {
            log.error(logPrefix + "---Exception : " + daoEx.getStackTrace());
        }

        return productBO;
    }
    public ProductBO getImagesbyProductId(int userId, ProductBO productBO){

        String logPrefix = "#UserId : " + userId+" images----"+productBO.getProductId();
        List<ProductBO.ProductImage> imageList=new ArrayList<>();
        try {


                String sqlQuery = " select id,product_id,alt,width,height,src,position from product_images where product_id='"+productBO.getProductId()+"' and status>0";

            imageList=jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {
                ProductBO.ProductImage image=new ProductBO.ProductImage();

                long imgid = (rs.getLong("id")>0) ? rs.getLong("id"): 0;
                double height = (rs.getDouble("height")>0) ? rs.getDouble("height"): 0;
                double width = (rs.getDouble("width")>0) ? rs.getDouble("width"): 0;
                int position = (rs.getInt("position")>0) ? rs.getInt("position"): 0;
                String src = (rs.getString("src") != null) ? rs.getString("src").trim() : "";
                String alt = (rs.getString("alt") != null) ? rs.getString("alt").trim() : "";
                image.setImageid(imgid);
                image.setProductId(productBO.getProductId());
                image.setHeight(height);
                image.setWidth(width);
                image.setPosition(position);
                image.setAlt(alt);
                image.setSrc(src);
                    return image;

            });
            if(imageList.size()>0){
                log.info("imageList---"+imageList.size());
                productBO.setProductImages(imageList);
            }

        } catch (Exception daoEx) {
            log.error(logPrefix + "---Exception : " + daoEx.getStackTrace());
        }

        return productBO;
    }

    public List<ProductBO> getProductList(int userId){

        String logPrefix = "#UserId : " + userId;
        List<ProductBO> productBOList=new ArrayList<>();


        try {

                    String sqlQuery = " select id,product_id,collection_id,title,body_html,vendor,product_type,handle,created_at,published_scope,tags,position,shape from product";
                    productBOList = jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {

                    ProductBO productBO = new ProductBO();
                    long id = (rs.getLong("id")>0) ? rs.getLong("id"): 0;
                    long product_id = (rs.getLong("product_id")>0) ? rs.getLong("product_id") : 0;
                    long collection_id = (rs.getLong("collection_id")>0) ? rs.getLong("collection_id") : 0;
                    String handle = (rs.getString("handle") != null) ? rs.getString("handle").trim() : "";
                    String vendor = (rs.getString("vendor") != null) ? rs.getString("vendor").trim() : "";
                    String title = (rs.getString("title") != null) ? rs.getString("title").trim() : "";
                    String productType = (rs.getString("product_type") != null) ? rs.getString("product_type").trim() : "";
                    String body_html = (rs.getString("body_html") != null) ? rs.getString("body_html").trim() : "";
                    String published_scope = (rs.getString("published_scope") != null) ? rs.getString("published_scope").trim() : "";
                    String tags = (rs.getString("tags") != null) ? rs.getString("tags").trim() : "";
                    String shape = (rs.getString("shape") != null) ? rs.getString("shape").trim() : "sgrid";
                    int position = (rs.getInt("position")>0) ? rs.getInt("position"): 0;

                    productBO.setId(id);
                    productBO.setCollection_id(collection_id);
                    productBO.setProductId(product_id);
                    productBO.setHandle(handle);
                    productBO.setVendor(vendor);
                    productBO.setTitle(title);
                    productBO.setProductType(productType);
                    productBO.setBodyHtml(body_html);
                    productBO.setPublishedScope(published_scope);
                    productBO.setTags(tags);
                    productBO.setShape(shape);
                    productBO.setPosition(position);
                    productBO=getImagesbyProductId(userId,productBO);
                    return productBO;
            });

        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }

        return productBOList;
    }
    public ProductBO getProductDetail(int userId,ProductBO productBO){

        String logPrefix = "#UserId : " + userId;
        final ProductBO productbo=new ProductBO();

        try {

            String sqlQuery = " select id,product_id,collection_id,title,body_html,vendor,product_type,handle,created_at,published_scope,tags,position,shape from product where product_id=?";
            jdbcTemplate.queryForObject(sqlQuery, new Object[]{productBO.getProductId()}, (rs, rowNumber) -> {
                long id = (rs.getLong("id")>0) ? rs.getLong("id"): 0;
                long product_id = (rs.getLong("product_id")>0) ? rs.getLong("product_id") : 0;
                long collection_id = (rs.getLong("collection_id")>0) ? rs.getLong("collection_id") : 0;
                String handle = (rs.getString("handle") != null) ? rs.getString("handle").trim() : "";
                String vendor = (rs.getString("vendor") != null) ? rs.getString("vendor").trim() : "";
                String title = (rs.getString("title") != null) ? rs.getString("title").trim() : "";
                String productType = (rs.getString("product_type") != null) ? rs.getString("product_type").trim() : "";
                String body_html = (rs.getString("body_html") != null) ? rs.getString("body_html").trim() : "";
                String published_scope = (rs.getString("published_scope") != null) ? rs.getString("published_scope").trim() : "";
                String tags = (rs.getString("tags") != null) ? rs.getString("tags").trim() : "";
                String shape = (rs.getString("shape") != null) ? rs.getString("shape").trim() : "sgrid";
                int position = (rs.getInt("position")>0) ? rs.getInt("position"): 0;

                        productbo.setId(id);
                        productbo.setCollection_id(collection_id);
                        productbo.setProductId(product_id);
                        productbo.setHandle(handle);
                        productbo.setVendor(vendor);
                        productbo.setTitle(title);
                        productbo.setProductType(productType);
                        productbo.setBodyHtml(body_html);
                        productbo.setPublishedScope(published_scope);
                        productbo.setTags(tags);
                        productbo.setShape(shape);
                        productbo.setPosition(position);
                        getImagesbyProductId(userId,productbo);
                        return productbo;
            }
            );

        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }

        return productbo;
    }
    public List<ProductBO> getProductListByTag(int userId,String tag){

        String logPrefix = "#UserId : " + userId;
        List<ProductBO> productBOList=new ArrayList<>();
        String tagsString=GeneralUtil.commaSqlString(tag);

        try {

            String sqlQuery = " select id,product_id,collection_id,title,body_html,vendor,product_type,handle,created_at,published_scope,tags,position,shape from product where tags in("+tagsString+")";
            productBOList = jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {

                ProductBO productBO = new ProductBO();
                long id = (rs.getLong("id")>0) ? rs.getLong("id"): 0;
                long product_id = (rs.getLong("product_id")>0) ? rs.getLong("product_id") : 0;
                long collection_id = (rs.getLong("collection_id")>0) ? rs.getLong("collection_id") : 0;
                String handle = (rs.getString("handle") != null) ? rs.getString("handle").trim() : "";
                String vendor = (rs.getString("vendor") != null) ? rs.getString("vendor").trim() : "";
                String title = (rs.getString("title") != null) ? rs.getString("title").trim() : "";
                String productType = (rs.getString("product_type") != null) ? rs.getString("product_type").trim() : "";
                String body_html = (rs.getString("body_html") != null) ? rs.getString("body_html").trim() : "";
                String published_scope = (rs.getString("published_scope") != null) ? rs.getString("published_scope").trim() : "";
                String tags = (rs.getString("tags") != null) ? rs.getString("tags").trim() : "";
                String shape = (rs.getString("shape") != null) ? rs.getString("shape").trim() : "sgrid";
                int position = (rs.getInt("position")>0) ? rs.getInt("position"): 0;

                productBO.setId(id);
                productBO.setCollection_id(collection_id);
                productBO.setProductId(product_id);
                productBO.setHandle(handle);
                productBO.setVendor(vendor);
                productBO.setTitle(title);
                productBO.setProductType(productType);
                productBO.setBodyHtml(body_html);
                productBO.setPublishedScope(published_scope);
                productBO.setTags(tags);
                productBO.setShape(shape);
                productBO.setPosition(position);
                return productBO;
            });

        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }

        return productBOList;
    }

    public List<ProductCategory> getProductCategories(int bpid,String category_Type){

        String logPrefix = "#bpid : " + bpid+"#category_Type:"+category_Type;
        List<ProductCategory> productCategories=new ArrayList<>();


        try {

            String sqlQuery = "select categoryname,description,img as imgUrl from categories where product_type='"+category_Type+"' and bpid="+bpid+" order by place asc\n";
           log.info(sqlQuery);
            productCategories = jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {

                ProductCategory productCategory = new ProductCategory();
                String categoryName = (rs.getString("categoryname") != null) ? rs.getString("categoryname").trim() : "";
                String category_description = (rs.getString("description") != null) ? rs.getString("description").trim() : "";
                String imgUrl = (rs.getString("imgUrl") != null) ? rs.getString("imgUrl").trim() : "";
                productCategory.setCategoryName(categoryName);
                productCategory.setDescription(category_description);
                productCategory.setImgUrl(imgUrl);

                return productCategory;
            });



        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }

        return productCategories;
    }

    public List<ProductCategory> getCollectionCategories(int bpid,String types){

        String logPrefix = "#bpid : " + bpid+"#types:"+types;
        List<ProductCategory> productCategories=new ArrayList<>();

        String strType=GeneralUtil.commaSqlString(types);
        try {

            String sqlQuery = "select categoryid,categoryname,description,img as imgUrl from categories where product_type in("+strType+") and bpid="+bpid+" order by place asc\n";
            log.info(sqlQuery);
            productCategories = jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {
                ProductCategory productCategory = new ProductCategory();
                String categoryid = (rs.getString("categoryid") != null) ? rs.getString("categoryid").trim() : "";
                String categoryName = (rs.getString("categoryname") != null) ? rs.getString("categoryname").trim() : "";
                String category_description = (rs.getString("description") != null) ? rs.getString("description").trim() : "";
                String imgUrl = (rs.getString("imgUrl") != null) ? rs.getString("imgUrl").trim() : "";
                productCategory.setCategoryId(categoryid);
                productCategory.setCategoryName(categoryName);
                productCategory.setDescription(category_description);
                productCategory.setImgUrl(imgUrl);

                return productCategory;
            });



        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }

        return productCategories;
    }



}

package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SchemeAumBO {

    private String n;  //sponsor name
    private String a1;
    private String a2;
    private String a3;
    private String a4;
    private String e; //email
    private String f; //fax
    private String t; //telephone
    private String d; //designation
    private String dt; //date
    private String m; //manager
    private String an; // amc name
    private String aum; // aum
    private String aumd; // aum date
    private String saum; // scheme aum
    private String saumd; //scheme  aum date
}

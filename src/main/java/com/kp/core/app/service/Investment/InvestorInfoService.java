package com.kp.core.app.service.Investment;

import com.kp.core.app.config.DocConfig;
import com.kp.core.app.config.GeneralConfig;
import com.kp.core.app.config.MFErrorsConfig;
import com.kp.core.app.dao.*;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.utils.*;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class InvestorInfoService {

    @Autowired
    private GeneralUtil generalUtil;
    @Autowired
    private InvestorInfoDAO investorInfoDAO;
    @Autowired
    private SchemeSearchDAO schemeSearchDAO;
    @Autowired
    private MandateInfoDAO mandateInfoDAO;
    @Autowired
    private TransactionDAO transactionDAO;
    @Autowired
    private UserDao userDao;
    @Autowired
    private GenerateMandate generateMandate;

    private GeneralConfig generalConfig = ConfigFactory.create(GeneralConfig.class);

    public GlobalResponseBO investorHoldingProfileList(String holdingProfileId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "holdingProfileList fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.list, "HoldingProfile");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<HoldingProfileBO> holdingProfileList = new ArrayList<>();
        String stat = "";
        int hpId = 0;

        try {

            if(StringUtils.isNotEmpty(holdingProfileId))
                hpId = Integer.parseInt(holdingProfileId);

            holdingProfileList = investorInfoDAO.getHoldingProfileList(userId, hpId, "ALL");

            for(HoldingProfileBO holdingProfileBO : holdingProfileList) {

                List<BankInfoBO> bankList = investorInfoDAO.getBankList(Integer.parseInt(holdingProfileBO.getHoldingProfileId()), 0, userId);
                holdingProfileBO.setBanks(bankList);
            }

        } catch (Exception e) {
            log.error(logPrefix + "Investor holding-profiles API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, holdingProfileList, globalResponseBo);

        log.debug(logPrefix + "Investor holding-profiles API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO investorFolioBankList(String holdingProfileId, String investorIdStr, String[] schemeCodes, String[] amcCodes, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "FolioBankList fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.list, "FolioBank");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<FolioBankBO> folioBankList = new ArrayList<>();
        String stat = "", schemeCond = "", amcCond = "";
        int hpId = 0, investorId = 0;

        try {

            if(StringUtils.isEmpty(holdingProfileId) && StringUtils.isEmpty(investorIdStr))
                errorBoList.add(new ErrorBO("input", "HoldingProfileId and InvestorId is empty. Send valid input"));
            else {

                if (StringUtils.isNotEmpty(holdingProfileId))
                    hpId = Integer.parseInt(holdingProfileId);
                if(StringUtils.isNotEmpty(investorIdStr))
                    investorId = Integer.parseInt(investorIdStr);

                if(schemeCodes!=null && schemeCodes.length > 0) {
                    schemeCond = Arrays.toString(schemeCodes)
                            .replace("[", "(")
                            .replace("]", ")");
                }

                if(amcCodes!=null && amcCodes.length > 0) {
                    amcCond = Arrays.toString(amcCodes)
                            .replace("[", "(")
                            .replace("]", ")");
                }

                folioBankList = investorInfoDAO.getFolioBankList(hpId, investorId, schemeCond, amcCond, userId);
            }

        } catch (Exception e) {
            log.error(logPrefix + "Investor folio-bank-list API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, folioBankList, globalResponseBo);

        log.debug(logPrefix + "Investor folio-bank-list API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO investorMandateList(String holdingProfileId, String investorIdStr, String ecsDate, String consumerCode, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "MandateList fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.list, "Mandate");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<MandateInfoBO> mandateList = new ArrayList<>();
        String stat = "";
        int hpId = 0, investorId = 0;

        try {

            if(StringUtils.isEmpty(holdingProfileId) && StringUtils.isEmpty(investorIdStr))
                errorBoList.add(new ErrorBO("input", "HoldingProfileId and InvestorId is empty. Send valid input"));
            else {

                if (StringUtils.isNotEmpty(holdingProfileId))
                    hpId = Integer.parseInt(holdingProfileId);
                if(StringUtils.isNotEmpty(investorIdStr))
                    investorId = Integer.parseInt(investorIdStr);

                mandateList = mandateInfoDAO.getOpenMandListForPurchase(userId, hpId, investorId, ecsDate, "", consumerCode, false);
            }

        } catch (Exception e) {
            log.error(logPrefix + "Investor mandates list API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, mandateList, globalResponseBo);

        log.debug(logPrefix + "Investor mandates list API Status : " + stat);
        return  globalResponseBo;

    }

    public ENachInfoBO insertMandateDetails(CreateMandataBO mandataBO, int userId, List<MandateInfoBO> mandateList, List<ErrorBO> errorBoList, boolean insert, String createdFrom) {

        ENachInfoBO eNachInfoBO = new ENachInfoBO();
        DateBO startDateBO = new DateBO("");
        DateBO endDateBO = new DateBO("");
        String consumerCode;

        try {

            String mandateType = mandataBO.getType().equalsIgnoreCase(BPConstants.MANDATE_TYPE_ENACH) ? "ENACH" : (mandataBO.getType().equalsIgnoreCase(BPConstants.MANDATE_TYPE_NACH) ? "NACH" : "");

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            Date today = dateFormat.parse(DateUtil.getTDate());
            String startDate = dateFormat.format(new Date());
            String endDate = "2099-12-31";
            if(!mandataBO.getValidTillCancel())
                endDate = mandataBO.getValidTill();

            if (dateFormat.parse(startDate).before(today))
                errorBoList.add(new ErrorBO("debitStartDate", "Invalid Debit Start Date"));

            if (dateFormat.parse(endDate).before(today))
                errorBoList.add(new ErrorBO("debitEndDate", "Invalid Debit End Date"));

            BankInfoBO bankInfoBO = investorInfoDAO.getBankInfoUsingUserBankId(Integer.parseInt(mandataBO.getBankId()), Integer.parseInt(mandataBO.getCustomerId()), userId);
            UserBO userBO = investorInfoDAO.getInvestorDetail(Integer.parseInt(mandataBO.getCustomerId()), userId);

            if(insert && errorBoList.size()==0) {
                consumerCode = mandateInfoDAO.insertInvestorOpenMandate(mandataBO, startDate, endDate, mandateType, createdFrom, userId);

                if (StringUtils.isEmpty(consumerCode))
                    errorBoList.add(new ErrorBO("failed", "Investor_Open_Mandate_Details insert failed"));

                else {

                    int status = mandateInfoDAO.insertConsumerCode(consumerCode, userId);

                    if (status == 0)
                        errorBoList.add(new ErrorBO("failed", "SIP_CONSUMER_DETAILS insert failed"));
                    else {

                        status = mandateInfoDAO.insertSIPBankDetails(consumerCode, bankInfoBO, userId);

                        if(status == 0)
                            errorBoList.add(new ErrorBO("failed", "SIP_BANK_DETAILS insert failed"));
                        else {

                            if(mandateList==null)
                                mandateList = new ArrayList<>();

                            MandateInfoBO mandateInfoBO = new MandateInfoBO();

                            //For ENACH get only the required details
                            if(mandateType.equals("ENACH"))
                                mandateInfoBO = mandateInfoDAO.getMandateInfoForENACH(Integer.parseInt(mandataBO.getCustomerId()), consumerCode, userId);

                            else {

                                int hpId = investorInfoDAO.getHpIdFromInvestorId(Integer.parseInt(mandataBO.getCustomerId()));
                                mandateList.addAll(mandateInfoDAO.getOpenMandListForPurchase(userId, hpId, Integer.parseInt(mandataBO.getCustomerId()), "", mandataBO.getBankId(), consumerCode, false));
                                if (mandateList.size() > 0)
                                    mandateInfoBO = mandateList.get(0);
                            }

                            if(StringUtils.isNotEmpty(mandateInfoBO.getConsumerCode())) {
                                status = mandateInfoDAO.insertInvestorDocumentStatus(mandataBO, consumerCode, Integer.parseInt(mandateInfoBO.getHoldingProfileId()), userId);

                                if (status > 0) {

                                    if (mandateType.equals("NACH")) {

                                        startDateBO.setDate(startDate);
                                        endDateBO.setDate(endDate);
                                        boolean generated = generateMandate.generateNACHMandate(mandateInfoBO, startDateBO, endDateBO, mandateType, createdFrom, userBO, userId);
//
//                                     if(!generated)
//                                         errorBoList.add(new ErrorBO("pdf", "Mandate form generation failed"));
                                    }

                                } else
                                    errorBoList.add(new ErrorBO("failed", "INVESTOR_DOCUMENT_STATUS_DETAILS insert failed"));
                            }
                        }
                    }
                }
            }
            else
                consumerCode = mandataBO.getConsumerCode();

            if(mandateType.equals("ENACH")) {

                startDate = dateFormat1.format(dateFormat.parse(startDate));
                endDate = dateFormat1.format(dateFormat.parse(endDate));
                eNachInfoBO.setConsumerCode(consumerCode);
                eNachInfoBO.setVendor("TPSL");
                eNachInfoBO.setFiProduct("MF");
                eNachInfoBO.setDebitStartDate(startDate);
                eNachInfoBO.setDebitEndDate(endDate);
                eNachInfoBO.setMandateAmount("2");
                eNachInfoBO.setCustomerId(mandataBO.getCustomerId());
                eNachInfoBO.setConsumerEmailId(userBO.getEmail());
                eNachInfoBO.setConsumerMobileNo(userBO.getMobile());
                eNachInfoBO.setMandateMaxAmount(String.valueOf(mandataBO.getMaximumAmount()));
                eNachInfoBO.setAccountNo(bankInfoBO.getAccountNo());
                eNachInfoBO.setIfscCode(bankInfoBO.getIfsc());
                //eNachInfoBO.setTpslBankCode(bankInfoBO.getTpslSIBankId());
                eNachInfoBO.setFrequency(generalConfig.tpslEnachFrequency());
                eNachInfoBO.setAccountHolderName(bankInfoBO.getAccountHolderName());

            }

        } catch (Exception e) {
            log.error("Exception in Mandate table insertions : ", e);
        }

        return eNachInfoBO;

    }


    public GlobalResponseBO createNewMandate(CreateMandataBO mandateBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Mandate create");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "Mandate");
        List<ErrorBO> errorBoList = new ArrayList<>();
        MandateInfoBO mandateInfoBO = new MandateInfoBO();
        String stat = "";

        try {

            isValidMandate(mandateBO, errorBoList);

            List<MandateInfoBO> mandateList = new ArrayList<>();
            if(errorBoList.size()==0) {
                insertMandateDetails(mandateBO, userId, mandateList, errorBoList, true, BPConstants.MANDATE_CREATE_SIP);
            }

            if(mandateList.size()>0)
                mandateInfoBO = mandateList.get(0);

        } catch (Exception e) {
            log.error(logPrefix + "Create mandate API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, mandateInfoBO, globalResponseBo);

        log.debug(logPrefix + "Create mandate list API Status : " + stat);
        return  globalResponseBo;

    }


    public void isValidMandate(CreateMandataBO mandataBO, List<ErrorBO> errorBoList) {

        try {

            if(StringUtils.isEmpty(mandataBO.getCustomerId()) || Integer.parseInt(mandataBO.getCustomerId())==0)
                errorBoList.add(new ErrorBO("investorId", "Invalid input"));
            if(StringUtils.isEmpty(mandataBO.getBankId()) || Integer.parseInt(mandataBO.getBankId())==0)
                errorBoList.add(new ErrorBO("bankId", "Invalid input"));
            if(StringUtils.isEmpty(mandataBO.getFrequency()))
                errorBoList.add(new ErrorBO("frequency", "Invalid input"));
            if(StringUtils.isEmpty(mandataBO.getValidTill()))
                errorBoList.add(new ErrorBO("validTill", "Invalid input"));
            if(StringUtils.isEmpty(mandataBO.getType()) || (!mandataBO.getType().equalsIgnoreCase(BPConstants.MANDATE_TYPE_ENACH) && !mandataBO.getType().equalsIgnoreCase(BPConstants.MANDATE_TYPE_NACH)))
                errorBoList.add(new ErrorBO("type", "Invalid input"));
            if(mandataBO.getMaximumAmount()==0)
                errorBoList.add(new ErrorBO("maximumAmount", "Invalid input"));
            else if(mandataBO.getMaximumAmount() > 1000000)
                errorBoList.add(new ErrorBO("maximumAmount", "Maximum upper limit is 10 Lakhs"));

        } catch (Exception e) {
            log.error("Exception in mandate creation - validate : ", e);
        }
    }

    public GlobalResponseBO investorGoalList(String goalType, int userId) {

        String logPrefix = "#UserId : " + userId + " | #GolaType : " + goalType + " | ";
        log.debug(logPrefix + "GoalList fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.list, "GoalBo");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<InvestorGoalBO> goalList = new ArrayList<>();
        String stat = "";

        try {

            goalList = investorInfoDAO.getGoalList(goalType, userId);

        } catch (Exception e) {
            log.error(logPrefix + "Investor goal list API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, goalList, globalResponseBo);

        log.debug(logPrefix + "Investor goal list API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO newInvestorGoal(InvestorGoalBO investorGoalBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "GoalList fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "PlanResponse");
        List<ErrorBO> errorBoList = new ArrayList<>();
        InvestorGoalResponseBO responseBO = new InvestorGoalResponseBO();
        InvestorGoalBO.AdjustedBO adjustedBO = new InvestorGoalBO.AdjustedBO();
        InvestorGoalBO.CustomAssetBO customAssetBO;
        InvestorRiskProfileBO riskProfileBO = new InvestorRiskProfileBO();
        DecimalFormat df = new DecimalFormat("###");
        String stat = "";
        double calcSIP = 0.0d, calcOTI = 0.0d, projectedAmount = 0.0d, cagr = 0.0d, targetAmount = 0.0d;
        int tenure = 0;

        MFErrorsConfig MFErrorsConfig = ConfigFactory.create(MFErrorsConfig.class);

        try {

            //Validations
            if(StringUtils.isEmpty(investorGoalBO.getGoalName()))
                errorBoList.add(new ErrorBO("", "goalName", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
            if((investorGoalBO.getGoalName() != null) && (investorGoalBO.getGoalName().length() < 5 ) && (investorGoalBO.getGoalName().length() > 50))
                errorBoList.add(new ErrorBO("", "goalName", MFErrorsConstants.FIELD_LENGTH_MISMATCH, MFErrorsConfig.getFieldLengthMismatch()+" 5 - 50"));
            if(StringUtils.isEmpty(investorGoalBO.getGoalType()) || !investorInfoDAO.isValidGoalType(investorGoalBO.getGoalType(), userId))
                errorBoList.add(new ErrorBO("","goalType", MFErrorsConstants.GOAL_TYPE_VALUE, MFErrorsConfig.getGoalTypeValue()));
            if(investorGoalBO.getAmount()<=30000)
                errorBoList.add(new ErrorBO("", "amount", MFErrorsConstants.GOAL_AMOUNT_MINMAX, MFErrorsConfig.getGoalAmountMinmax()));
            if(StringUtils.isEmpty(investorGoalBO.getTarget()))
                errorBoList.add(new ErrorBO("","target", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
            if(StringUtils.isEmpty(investorGoalBO.getRiskType()))
                errorBoList.add(new ErrorBO("","riskType", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
            else {
                //Get Master details of given riskType if present
                riskProfileBO = investorInfoDAO.getRiskProfile(investorGoalBO.getRiskType(), userId);

                if(riskProfileBO.getRiskProfileId()<=0)
                    errorBoList.add(new ErrorBO("","riskType", MFErrorsConstants.GOAL_RISK_PROFILE_MISSING, MFErrorsConfig.getGoalRiskProfileMissing()));
            }
            if(investorGoalBO.getQuestionnaire()==null)
                errorBoList.add(new ErrorBO("","questionnaire", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));

            if(investorGoalBO.getAdjusted()!=null) {
                adjustedBO = investorGoalBO.getAdjusted();
                if(StringUtils.isEmpty(adjustedBO.getTarget()))
                    errorBoList.add(new ErrorBO("","adjusted.target", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                if(adjustedBO.getInflation()<=3 || adjustedBO.getInflation()>=16)
                    errorBoList.add(new ErrorBO("","inflation", MFErrorsConstants.GOAL_INFLATION_MINMAX, MFErrorsConfig.getGoalInflationMinmax()));
                if(adjustedBO.getAmount()<=0)
                    errorBoList.add(new ErrorBO("", "adjusted.amount", MFErrorsConstants.GOAL_AMOUNT_MINMAX, MFErrorsConfig.getGoalInflationMinmax()));

                try {
                    String[] target = adjustedBO.getTarget().split("-");
                    String[] now = (new SimpleDateFormat("yyyy-MM-dd").format(new Date())).split("-");

                    tenure = ((Integer.parseInt(target[0]) - Integer.parseInt(now[0])) * 12) + (Integer.parseInt(target[1]) - Integer.parseInt(now[1]));
                    if(tenure<=6)
                        errorBoList.add(new ErrorBO("","adjusted.tenure", MFErrorsConstants.GOAL_TARGET_VAL, MFErrorsConfig.getGoalTargetVal()));
                    if( (Integer.parseInt(target[1])<=0) && (Integer.parseInt(target[1])>=13) )
                        errorBoList.add(new ErrorBO("","adjusted.target", MFErrorsConstants.GOAL_TARGET_DATE_FORMAT_INVALID, MFErrorsConfig.getGoalTargetDateFormatInvalid()));

                }catch (Exception e){
                    errorBoList.add(new ErrorBO("","adjusted.target", MFErrorsConstants.GOAL_TARGET_DATE_FORMAT_INVALID, MFErrorsConfig.getGoalTargetDateFormatInvalid()));
                }
            }

            if(investorGoalBO.getRiskType().equalsIgnoreCase("Custom")) {
                customAssetBO = investorGoalBO.getCustomAssetAllocation();
                if(customAssetBO==null)
                    errorBoList.add(new ErrorBO("","customAssetAllocation", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                else {
                    if(customAssetBO.getEquity()+customAssetBO.getDebt()+customAssetBO.getGold() == 100){
                        riskProfileBO.setEquity(customAssetBO.getEquity());
                        riskProfileBO.setDebt(customAssetBO.getDebt());
                        riskProfileBO.setGold(customAssetBO.getGold());
                    }else{
                        errorBoList.add(new ErrorBO("","customAssetAllocation", MFErrorsConstants.GOAL_CUSTOM_ALLOCATION, MFErrorsConfig.getGoalCustomAllocation()));
                    }
                }
            }

            if(errorBoList.size()==0) {

                //Check if goal already exists
                if (investorInfoDAO.isExistingGoal(investorGoalBO.getGoalName(), investorGoalBO.getGoalType(), userId))
                    errorBoList.add(new ErrorBO("", "goalName", MFErrorsConstants.GOAL_UNIQUE, MFErrorsConfig.getGoalUnique() ));

                else {
                    //Calculate required values
                    targetAmount = investorGoalBO.getAdjusted().getAmount();

                    try{
                        String[] target = investorGoalBO.getTarget().split("-");
                        String[] now = (new SimpleDateFormat("yyyy-MM-dd").format(new Date())).split("-");

                        tenure = ((Integer.parseInt(target[0]) - Integer.parseInt(now[0])) * 12) + (Integer.parseInt(target[1])-Integer.parseInt(now[1]));

                        if(tenure<=6)
                            errorBoList.add(new ErrorBO("","tenure", MFErrorsConstants.GOAL_TARGET_VAL, MFErrorsConfig.getGoalTargetVal()));

                        if( (Integer.parseInt(target[1])<=0) && (Integer.parseInt(target[1])>=13) )
                            errorBoList.add(new ErrorBO("","adjusted.target", MFErrorsConstants.GOAL_TARGET_DATE_FORMAT_INVALID, MFErrorsConfig.getGoalTargetDateFormatInvalid()));

                    }catch (Exception e){
                        errorBoList.add(new ErrorBO("","target", MFErrorsConstants.GOAL_TARGET_DATE_FORMAT_INVALID, MFErrorsConfig.getGoalTargetDateFormatInvalid()));
                    }

                    if(errorBoList.size()==0){
                        //SIP & Lumpsum amount calculation
                        if(targetAmount > 10000) {
                            calcSIP = targetAmount / tenure;
                            calcOTI = 0;
                        }
                        else {
                            calcSIP = 0;
                            calcOTI = targetAmount;
                        }

                        //CAGR calculation
                        cagr =  (riskProfileBO.getEquity() * BPConstants.EQUITY_RETURNS/100) +
                                (riskProfileBO.getDebt() * BPConstants.DEBT_RETURNS/100) +
                                (riskProfileBO.getGold() * BPConstants.GOLD_RETURNS/100);

                        //todo: Projected amount calculation


                        responseBO.setCalculatedSipAmount(calcSIP);
                        responseBO.setCalculatedSipAmountFormatted(df.format(calcSIP));
                        responseBO.setCalculatedOtiAmount(calcOTI);
                        responseBO.setCalculatedOtiAmountFormatted(df.format(calcOTI));
                        responseBO.setTargetAmount(adjustedBO.getAmount());
                        responseBO.setTargetAmountFormatted(df.format(adjustedBO.getAmount()));
                        responseBO.setProjectedAmount(0);
                        responseBO.setProjectedAmountFormatted("0");
                        responseBO.setSipAmount(calcSIP);
                        responseBO.setSipAmountFormatted(df.format(calcSIP));
                        responseBO.setOtiAmount(calcOTI);
                        responseBO.setOtiAmountFormatted(df.format(calcOTI));
                        responseBO.setCagr(df.format(cagr));
                        responseBO.setCagrInfo("");

                        //Insert into PortfolioInfo
                        int portfolioId = investorInfoDAO.createPortfolio(investorGoalBO.getGoalName(), "Goal", userId);
                        if (portfolioId == 0)
                            errorBoList.add(new ErrorBO("","PortfolioInfo", MFErrorsConstants.DB_ERROR, MFErrorsConfig.getDbError()));
                        else {
                            //Insert into InvestorGoalDetails
                            int goalId = investorInfoDAO.createGoal(investorGoalBO, responseBO, portfolioId, userId);
                            //goalId = 123;
                            if (goalId == 0)
                                errorBoList.add(new ErrorBO("","InvestorGoalDetails", MFErrorsConstants.DB_ERROR, MFErrorsConfig.getDbError()));
                            else {

                                riskProfileBO.setQuestionnaire(investorGoalBO.getQuestionnaire());
                                riskProfileBO.setGoalId(goalId);

                                //Insert into InvestorRiskProfile
                                int status = investorInfoDAO.createRiskProfile(riskProfileBO, userId);
                                status  =   1;
                                if (status == 0)
                                    errorBoList.add(new ErrorBO("","InvestorRiskProfile", MFErrorsConstants.DB_ERROR, MFErrorsConfig.getDbError()));

                                else
                                    responseBO.setGoalId(String.valueOf(goalId));

                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "Create goal response API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, responseBO, globalResponseBo);

        log.debug(logPrefix + "Create goal response API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO createCartAndAddSchemesOrENACH(SchemeCartBO schemeCartBO, int userId, String validation, boolean isUpdate) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create cart");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "AddToCartResponse");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<SchemeCartBO.Schemes> totalList = new ArrayList<>();
        CreateMandataBO createMandataBO = new CreateMandataBO();
        String stat = "";
        int cartId = 0;
        int investmentTotal =   0;
        HashMap<String, Object> result = new HashMap<>();
        boolean isCartAvailable, toValidate = false;
        MFErrorsConfig MFErrorsConfig = ConfigFactory.create(MFErrorsConfig.class);
        try {

            if (schemeCartBO == null)
                errorBoList.add(new ErrorBO("request", "Request Body cannot be empty"));

            else {

                if(isUpdate && StringUtils.isEmpty(schemeCartBO.getId()))
                    errorBoList.add(new ErrorBO("","id", "Invalid cartId"));

                if(!isUpdate && StringUtils.isNotEmpty(schemeCartBO.getId())) {
                    isCartAvailable = investorInfoDAO.isCartAvailable(Integer.parseInt(schemeCartBO.getId()));
                    if(isCartAvailable)
                        errorBoList.add(new ErrorBO("id", "Cart already exists"));
                    else
                        errorBoList.add(new ErrorBO("id", "Invalid cartId"));
                }

                if(isUpdate) {
                    cartId = Integer.parseInt(schemeCartBO.getId());
                    isCartAvailable = investorInfoDAO.isCartAvailable(cartId);
                    if(!isCartAvailable)
                        errorBoList.add(new ErrorBO("cartId", "Cart does not exist"));
                    else if(investorInfoDAO.isCartUpdateRetricted(cartId))
                        errorBoList.add(new ErrorBO("cartId", "Cart can no longer be updated"));
                }


                if (schemeCartBO.getMf() == null && schemeCartBO.getEmandate()==null)
                    errorBoList.add(new ErrorBO("", "mf/emandate", MFErrorsConstants.CART_PRODUCT_TYPE_VALIDATION, MFErrorsConfig.getCartProductTypeValidation()));

                else if (schemeCartBO.getMf() != null) {
                    if (StringUtils.isEmpty(schemeCartBO.getHoldingProfileId()))
                        errorBoList.add(new ErrorBO("","holdingProfileId", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                }

                else if(schemeCartBO.getEmandate() != null) {

                    createMandataBO = schemeCartBO.getEmandate();
                    createMandataBO.setCustomerId(schemeCartBO.getCustomerId());
                    createMandataBO.setType("eMandate");
                    isValidMandate(createMandataBO, errorBoList);
                }

                if(errorBoList.size()==0) {

                    String mediumThru = null;
                    if (schemeCartBO.getAppInfo() != null)
                        mediumThru = schemeCartBO.getAppInfo().getOs();

                    if(StringUtils.isNotEmpty(validation) && validation.equalsIgnoreCase("true"))
                        toValidate = true;

                    //validate and insert MF details
                    if(schemeCartBO.getMf() != null) {

                        if (schemeCartBO.getMf().getOti() != null && schemeCartBO.getMf().getOti().getSchemes().size() > 0) {
                            totalList.addAll(schemeCartBO.getMf().getOti().getSchemes());

                            for (SchemeCartBO.Schemes schemeBO : schemeCartBO.getMf().getOti().getSchemes()) {

                                investmentTotal += schemeBO.getAmount();
                            }
                            if (investmentTotal != schemeCartBO.getMf().getOti().getTotalAmount())
                                errorBoList.add(new ErrorBO("", "totalAmount", MFErrorsConstants.GOAL_OTI_TOTAL_MISMATCH, MFErrorsConfig.getGoalOtiTotalMismatch()));
                        }


                        if (schemeCartBO.getMf().getSip() != null && schemeCartBO.getMf().getSip().getSchemes().size() > 0) {

                            investmentTotal = 0;
                            totalList.addAll(schemeCartBO.getMf().getSip().getSchemes());

                            for (SchemeCartBO.Schemes schemeBO : schemeCartBO.getMf().getSip().getSchemes()) {

                                if (!generalUtil.sipTypeValidate(schemeBO.getSipType()))
                                    errorBoList.add(new ErrorBO(schemeBO.getId(), "sipType", MFErrorsConstants.INVALID_JSON_TAG, MFErrorsConfig.getInvalidJsonTag()));

                                if (schemeBO.getSipType().equals("regular"))
                                    investmentTotal += generalUtil.NullCheckInteger(String.valueOf(schemeBO.getRegular().getAmount()));
                                if (schemeBO.getSipType().equals("flexi"))
                                    investmentTotal += generalUtil.NullCheckInteger(String.valueOf(schemeBO.getFlexi().getAmount()));
                                if (schemeBO.getSipType().equals("alert"))
                                    investmentTotal += generalUtil.NullCheckInteger(String.valueOf(schemeBO.getAlert().getAmount()));
                                if (schemeBO.getSipType().equals("stepup"))
                                    investmentTotal += generalUtil.NullCheckInteger(String.valueOf(schemeBO.getStepup().getAmount()));
                            }
                            if (investmentTotal != schemeCartBO.getMf().getSip().getTotalAmount())
                                errorBoList.add(new ErrorBO("", "totalAmount", MFErrorsConstants.GOAL_SIP_TOTAL_MISMATCH, MFErrorsConfig.getGoalSipTotalMismatch()));

                        }

                        if (totalList.size() > 0) {
                            if (!toValidate || isMFValidPurchase(Integer.parseInt(schemeCartBO.getHoldingProfileId()), totalList, userId, errorBoList)) {


                                for (SchemeCartBO.Schemes schemeBO : totalList) {

                                    cartId = investorInfoDAO.insertCartDetailsMF(cartId, Integer.parseInt(schemeCartBO.getHoldingProfileId()), schemeCartBO.getHoldingProfileName(), schemeCartBO.getHoldingProfilePan(), schemeBO, "PUR", mediumThru, userId);
                                    if (cartId == 0)
                                        errorBoList.add(new ErrorBO(schemeBO.getId(), schemeBO.getSchemeCode(), "Insert/Update failed"));
                                }
                            }
                        } else
                            errorBoList.add(new ErrorBO("", "investmentType oti-sip", MFErrorsConstants.MANDATORY_FIELD_TAG_NOT_FOUND, MFErrorsConfig.getMandatoryFieldTagNotFound()));
                    }

                    else if(schemeCartBO.getEmandate() != null) {
                        cartId = investorInfoDAO.insertCartDetailsEMandate(cartId, Integer.parseInt(schemeCartBO.getHoldingProfileId()), createMandataBO, mediumThru, userId);
                        if (cartId == 0)
                            errorBoList.add(new ErrorBO("emandate", "Insert/Update failed"));
                    }

                    if(cartId!=0 && schemeCartBO.getAppInfo()!=null)
                        investorInfoDAO.insertCartAppInfo(cartId, schemeCartBO.getAppInfo(), userId);
                }
            }
        } catch(Exception e){
            log.error(logPrefix + "Create/Update cart API Exception : ", e);
            errorBoList.add(new ErrorBO("", "sipType", MFErrorsConstants.INVALID_JSON_TAG, MFErrorsConfig.getInvalidJsonTag()));
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;
        else {
            result.put("cartId", String.valueOf(cartId));
            result.put("addedOn", java.time.LocalDateTime.now().toString());
            result.put("result", true);
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, result, globalResponseBo);

        log.debug(logPrefix + "Create/Update cart API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO fetchInvestorCart(String cartId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #CartId : " + cartId + " | ";
        log.debug(logPrefix + "Fetch Cart");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "CartItem");
        SchemeCartBO schemeCartBO = new SchemeCartBO();
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            if(StringUtils.isEmpty(cartId) || Integer.parseInt(cartId)==0)
                errorBoList.add(new ErrorBO("cartId", "Invalid input"));

            else {
                schemeCartBO = investorInfoDAO.fetchInvestorCart(Integer.parseInt(cartId), userId, false);
                AppInfoBO appInfoBO = investorInfoDAO.fetchCartAppInfo(Integer.parseInt(cartId), userId);
                schemeCartBO.setId(cartId);
                schemeCartBO.setAppInfo(appInfoBO);
            }

        } catch (Exception e) {
            log.error(logPrefix + "Fetch Cart API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, schemeCartBO, globalResponseBo);

        log.debug(logPrefix + "Fetch Cart API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO deactivateCart(String cartId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #CartId : " + cartId + " | ";
        log.debug(logPrefix + "Deactivate Cart");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "DeleteCartResponse");
        List<ErrorBO> errorBoList = new ArrayList<>();
        HashMap<String, Object> result = new HashMap<>();
        String stat = "";

        try {

            if(StringUtils.isEmpty(cartId) || Integer.parseInt(cartId)==0)
                errorBoList.add(new ErrorBO("cartId", "Invalid input"));
            if(!investorInfoDAO.isCartAvailable(Integer.parseInt(cartId)))
                errorBoList.add(new ErrorBO("cartId", "Cart does not exist"));

            else {
                 int status = investorInfoDAO.deactivateCart(Integer.parseInt(cartId), userId);
                 if(status==0)
                     errorBoList.add(new ErrorBO("failed", "Cart delete failed"));
            }

        } catch (Exception e) {
            log.error(logPrefix + "Delete Cart API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;
        else {
            result.put("cartId", String.valueOf(cartId));
            result.put("updatedOn", java.time.LocalDateTime.now().toString());
            result.put("result", true);
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, result, globalResponseBo);

        log.debug(logPrefix + "Fetch Cart API Status : " + stat);
        return  globalResponseBo;

    }

    public boolean isMFValidPurchase(int holdingProfileId, List<SchemeCartBO.Schemes> schemeList, int userId, List<ErrorBO> errorList) {

        String logPrefix = "#HoldingProfileId : " + holdingProfileId + " | UserId : " + userId + " | ";
        log.debug(logPrefix + "Checking if a valid purchase");

        boolean isValidPurchase = false;
        String previosAmc = "";
        HashMap<String, Double> amcInvestments = new HashMap<>();
        MFErrorsConfig MFErrorsConfig = ConfigFactory.create(MFErrorsConfig.class);
        HashMap<String, Double> mandateAvailableLimit = new HashMap<>();
        try {

            boolean isAadhaarInvestor = investorInfoDAO.isAadhaarInvestor(holdingProfileId);

            for(SchemeCartBO.Schemes schemeBO : schemeList) {

                int schemeCode = Integer.parseInt(schemeBO.getSchemeCode());
                int amount = schemeBO.getAmount();
                String id;
                String bankId;
                String invType = "OTI";
                String sipTypes = "regular, flexi, stepup, alert";
                String superSavingSchemes = "1659,2637,15502";

                if(StringUtils.isEmpty(schemeBO.getId()))
                    errorList.add(new ErrorBO("", "id", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));

                else {

                    if (StringUtils.isEmpty(schemeBO.getSchemeCode()))
                        errorList.add(new ErrorBO(schemeBO.getId(), "schemeCode", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                    if(!generalUtil.validatePositiveNumber(schemeBO.getSchemeCode()))
                        errorList.add(new ErrorBO(schemeBO.getId(), "schemeCode", MFErrorsConstants.PISITIVE_NUMBERS, MFErrorsConfig.getPisitiveNumbers()));
                    if (StringUtils.isEmpty(schemeBO.getOption()))
                        errorList.add(new ErrorBO(schemeBO.getId(), "option", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                    if(!generalUtil.dividentValidate(schemeBO.getOption()))
                        errorList.add(new ErrorBO(schemeBO.getId(), "option", MFErrorsConstants.INVESTMENT_OPTION_VALIDATE, MFErrorsConfig.getInvestmentOptionValidate()));
                    if( (schemeBO.getOption().equals("Dividend")) && (!generalUtil.dividentOptionValidate(schemeBO.getDividendOption())) )
                        errorList.add(new ErrorBO(schemeBO.getId(), "dividendOption", MFErrorsConstants.DIVIDEND_OPTION_VALIDATE, MFErrorsConfig.getDividendOptionValidate()));
                    if (schemeBO.getPayment()==null)
                        errorList.add(new ErrorBO(schemeBO.getId(), "payment", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                    if (StringUtils.isEmpty(schemeBO.getBankId()))
                        errorList.add(new ErrorBO(schemeBO.getId(), "bankId", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                    if (StringUtils.isEmpty(schemeBO.getGoalId()))
                        errorList.add(new ErrorBO(schemeBO.getId(), "goalId", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                    if (StringUtils.isEmpty(schemeBO.getFolio()))
                        errorList.add(new ErrorBO(schemeBO.getId(), "folio", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                    if(!generalUtil.validatePositiveNumber(String.valueOf(schemeBO.getAmount())))
                        errorList.add(new ErrorBO(schemeBO.getId(), "amount", MFErrorsConstants.PISITIVE_NUMBERS, MFErrorsConfig.getPisitiveNumbers()));


                    id = schemeBO.getId();
                    bankId = schemeBO.getBankId();

                    if (StringUtils.isNotEmpty(schemeBO.getSipType())) {

                        invType = "SIP";
                        SchemeCartBO.SipDetail sipScheme = new SchemeCartBO.SipDetail();

                        if (schemeBO.getSipDate() == 0)
                            errorList.add(new ErrorBO(schemeBO.getId(), "sipDate", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                        if(!generalUtil.dateRangeValidation(String.valueOf(schemeBO.getSipDate())))
                            errorList.add(new ErrorBO(schemeBO.getId(), "sipDate", MFErrorsConstants.INVALID_DATA, MFErrorsConfig.getInvalidData()));


                        if(!sipTypes.contains(schemeBO.getSipType()))
                            errorList.add(new ErrorBO(schemeBO.getId(), "sipType", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));

                        if (schemeBO.getSipType().equalsIgnoreCase(BPConstants.REGULAR_SIP_TYPE) || schemeBO.getRegular()!=null) {
                            sipScheme = schemeBO.getRegular();

                        }else if (schemeBO.getSipType().equalsIgnoreCase(BPConstants.FLEXI_SIP_TYPE) || schemeBO.getFlexi()!=null) {
                            sipScheme = schemeBO.getFlexi();

                            if (StringUtils.isEmpty(String.valueOf(sipScheme.getMaximumAmount())))
                                errorList.add(new ErrorBO(schemeBO.getId(), "maximumAmount", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                            if (sipScheme.getMaximumAmount() < 500)
                                errorList.add(new ErrorBO(schemeBO.getId(), "maximumAmount", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                            if(!generalUtil.validatePositiveNumber(String.valueOf(sipScheme.getMaximumAmount())))
                                errorList.add(new ErrorBO(schemeBO.getId(), "maximumAmount", MFErrorsConstants.PISITIVE_NUMBERS, MFErrorsConfig.getPisitiveNumbers()));

                        } else if (schemeBO.getSipType().equalsIgnoreCase(BPConstants.ALERT_SIP_TYPE) || schemeBO.getAlert()!=null) {
                            sipScheme = schemeBO.getAlert();

                            if (StringUtils.isEmpty(sipScheme.getStartDate()))
                                errorList.add(new ErrorBO(schemeBO.getId(), "startDate", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                            if(!generalUtil.validDateFormat(sipScheme.getStartDate(), "YYYY-MM-dd"))
                                errorList.add(new ErrorBO(schemeBO.getId(), "startDate", MFErrorsConstants.INVALID_DATE_FORMAT, MFErrorsConfig.getInvalidDateFormat()));
                            if (StringUtils.isEmpty(sipScheme.getEndDate()))
                                errorList.add(new ErrorBO(schemeBO.getId(), "endDate", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
                            if(!generalUtil.validDateFormat(sipScheme.getEndDate(), "YYYY-MM-dd"))
                                errorList.add(new ErrorBO(schemeBO.getId(), "endDate", MFErrorsConstants.INVALID_DATE_FORMAT, MFErrorsConfig.getInvalidDateFormat()));

                        } else if (schemeBO.getSipType().equalsIgnoreCase(BPConstants.STEPUP_SIP_TYPE) || schemeBO.getStepup()!=null) {
                            sipScheme = schemeBO.getStepup();

                            if (sipScheme.getStepupAmount() < 500)
                                errorList.add(new ErrorBO(schemeBO.getId(), "stepupAmount", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));
//                            if (sipScheme.getFinalAmount() == 0)
//                                errorList.add(new ErrorBO(schemeBO.getId(), "finalAmount", "Invalid input"));
                        }

                        amount = sipScheme.getAmount();

                        if (!schemeBO.getSipType().equalsIgnoreCase(BPConstants.ALERT_SIP_TYPE)) {
                            if (StringUtils.isEmpty(sipScheme.getConsumerCode()))
                                errorList.add(new ErrorBO(schemeBO.getId(), "consumerCode", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));

                            if (sipScheme.getTenure() <= 5)
                                errorList.add(new ErrorBO(schemeBO.getId(), "tenure", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));

                            if(StringUtils.isNotEmpty(sipScheme.getConsumerCode()) && schemeBO.getSipDate()>0) {

                                double availableAmount = mandateInfoDAO.getAvalbleDayLimitAmt(sipScheme.getConsumerCode(), String.valueOf(schemeBO.getSipDate()), 0, "", userId);

                                String key = sipScheme.getConsumerCode() + "_" + schemeBO.getSipDate();
                                if(mandateAvailableLimit.containsKey(key))
                                    availableAmount = mandateAvailableLimit.get(key);

                                mandateAvailableLimit.put(key, availableAmount - amount);

                                if(availableAmount < amount)
                                    errorList.add(new ErrorBO(schemeBO.getId(), "consumerCode", MFErrorsConstants.CART_MANDATE_AVAILABLE_LIMIT, MFErrorsConfig.getCartMandateAvailableLimit()));
                            }
                        }
                        if (StringUtils.isEmpty(sipScheme.getFrequency()))
                            errorList.add(new ErrorBO(schemeBO.getId(), "frequency", MFErrorsConstants.MANDATORY_FIELD_NOT_EMPTY, MFErrorsConfig.getMandatoryFieldNotEmpty()));

                        if(!generalUtil.frequencyValidate(sipScheme.getFrequency()))
                            errorList.add(new ErrorBO(schemeBO.getId(), "frequency", MFErrorsConstants.FREQUENCY_VALIDATE, MFErrorsConfig.getFrequencyValidate()));
                    }

                    //Aadhaar Investor MaxLimit Validation
                    if(isAadhaarInvestor) {

                        JSONObject amcInvDetail = investorInfoDAO.getAMCInvestmentDetails(String.valueOf(holdingProfileId), schemeBO.getSchemeCode());
                        double amcAmount = (Double) amcInvDetail.get("amcAmount");
                        String fundHouseName = amcInvDetail.getString("amcName");
                        String amcCode = amcInvDetail.getString("amcCode");

                        if (amcInvestments.containsKey(amcCode)) {
                            if (amcInvestments.get(amcCode) + amount > BPConstants.AADHAR_MAX_AMOUNT)
                                errorList.add(new ErrorBO(schemeBO.getId(), "amount", MFErrorsConstants.CART_AADHAR_INVESTOR_LIMIT, MFErrorsConfig.getCartAadharInvestorLimit().replace("<<fundHouseName>>", fundHouseName)));
                            amcInvestments.put(amcCode, amcInvestments.get(amcCode) + amount);
                        } else {
                            if (amcAmount + amount > BPConstants.AADHAR_MAX_AMOUNT)
                                errorList.add(new ErrorBO(schemeBO.getId(), "amount", MFErrorsConstants.CART_AADHAR_INVESTOR_LIMIT, MFErrorsConfig.getCartAadharInvestorLimit().replace("<<fundHouseName>>", fundHouseName)));
                            amcInvestments.put(amcCode, amcAmount + amount);
                        }
                    }

                    HashMap<String, String> invDetails = investorInfoDAO.getInvestorDetails(holdingProfileId, bankId);
                    HashMap<String, String> schemeDetails = schemeSearchDAO.getSchemeDetails(schemeCode);

                    //NRI Validation
                    if (StringUtils.isNotEmpty(invDetails.get("isNri")) && invDetails.get("isNri").equals("1")) {

                        log.debug(logPrefix + "NRI Validation for id : " + schemeBO.getId());

                        if (schemeSearchDAO.isSchemeAllowedForNRI(schemeCode)) {
                            if (StringUtils.isNotEmpty(invDetails.get("country")) && (invDetails.get("country").equals("2") || invDetails.get("country").equals("11"))) {
                                if (BPConstants.USA_CANADA_ONLY_AMC.contains(schemeDetails.get("amcCode")))
                                    errorList.add(new ErrorBO(id, "amc", MFErrorsConstants.CART_NRI_RESTRICTION, MFErrorsConfig.getCartNriRestriction()));
                            } else if (StringUtils.isNotEmpty(invDetails.get("nationality")) && (invDetails.get("nationality").equals("2") || invDetails.get("nationality").equals("11"))) {
                                if (BPConstants.NATIONALITY_WISE_AMC_RESTRICT.contains(schemeDetails.get("amcCode")))
                                    errorList.add(new ErrorBO(id, "amc", MFErrorsConstants.CART_NRI_RESTRICTION, MFErrorsConfig.getCartNriRestriction()));
                            } else {

                                //NRE Validation
                                if (StringUtils.isNotEmpty(invDetails.get("bankAccType")) && invDetails.get("bankAccType").equals("4")) {

                                    log.debug(logPrefix + "NRE Validation for id : " + schemeBO.getId());

                                    if (previosAmc.equals(""))
                                        previosAmc = schemeDetails.get("amcCode");
                                    else if (!previosAmc.equalsIgnoreCase(schemeDetails.get("amcCode")))
                                        errorList.add(new ErrorBO(id, "amc", MFErrorsConstants.CART_NRI_MULTIPLE_AMC_RESTRICTION,  MFErrorsConfig.getCartNriMultipleAmcRestriction()));
                                }
                            }
                        } else
                            errorList.add(new ErrorBO(id, "schemeCode", MFErrorsConstants.CART_NRI_NOT_ALLOWED_SCHEME, MFErrorsConfig.getCartNriNotAllowedScheme()));
                    }

                    //Minor only scheme Validation
                    if (StringUtils.isNotEmpty(invDetails.get("isMinor")) && invDetails.get("isMinor").equals("0") && schemeSearchDAO.isOnlyMinorAllowedScheme(schemeCode))
                        errorList.add(new ErrorBO(id, "schemeCode", MFErrorsConstants.CART_MINOR_SCHEME, MFErrorsConfig.getCartMinorScheme()));

                    else if (StringUtils.isNotEmpty(schemeDetails.get("classCode")) && schemeDetails.get("classCode").equals("24") && invType.equals("SIP") && !superSavingSchemes.contains(schemeBO.getSchemeCode()))
                        errorList.add(new ErrorBO(id, "schemeCode", "LQ Schemes not allowed for SIP"));

//                    else if (StringUtils.isNotEmpty(schemeDetails.get("showUp")) && schemeDetails.get("showUp").equalsIgnoreCase("F"))
//                        errorList.add(new ErrorBO(id, "schemeCode", "Not available for purchase"));

                    //IR Type Validation
                    else if (StringUtils.isNotEmpty(invDetails.get("irType")) && invDetails.get("irType").equalsIgnoreCase("OE") && BPConstants.OE_AMCS.contains(schemeDetails.get("amcCode")))
                        errorList.add(new ErrorBO(id, "amc",MFErrorsConstants.CART_OE_RESTRICTED_AMC,  MFErrorsConfig.getCartOeRestrictedAmc()));

                    else if ((schemeCode == BPConstants.IDFC_PE_G_SCODE || schemeCode == BPConstants.IDFC_PE_D_SCODE || schemeCode == BPConstants.REL_SC_D_SCODE ||
                            schemeCode == BPConstants.REL_SC_G_SCODE || schemeCode == BPConstants.SBI_SMC_D_SCODE || schemeCode == BPConstants.SBI_SMC_G_SCODE) && invType.equals("OTI")) {
                        errorList.add(new ErrorBO(id, "investmentType", MFErrorsConstants.CART_LUMPSUM_NOT_SUPPORT, MFErrorsConfig.getCartLumpsumNotSupport()));
                    }

                    //Scheme Minimum Investment Validation
                    int minInvest = 0;
                    if(invType.equals("OTI"))
                        minInvest = StringUtils.isNotEmpty(schemeDetails.get("minInvest")) ? Integer.parseInt(schemeDetails.get("minInvest")) : 0;
                    else if(invType.equals("SIP"))
                        minInvest = StringUtils.isNotEmpty(schemeDetails.get("sipMinInvest")) ? Integer.parseInt(schemeDetails.get("sipMinInvest")) : 0;
                    if(amount < minInvest)
                            errorList.add(new ErrorBO(id, "amount", MFErrorsConstants.SCHEME_MIN_MISMATCH, MFErrorsConfig.getSchemeMinMismatch()));

                }
            }

            if(errorList.size()==0)
                isValidPurchase = true;

        } catch (Exception e) {
            log.error(logPrefix + "Valid purchase check Exception : ", e);
        }

        return isValidPurchase;
    }

    public GlobalResponseBO dashBoardDetails(String holdingProfileId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "Dashboard details fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "PortfolioDashboard");
        List<ErrorBO> errorBoList = new ArrayList<>();
        DashBoardBO dashBoardBO = new DashBoardBO();
        String stat = "";
        int hpId = 0;

        try {

            if(StringUtils.isNotEmpty(holdingProfileId))
                hpId = Integer.parseInt(holdingProfileId);

            int plId = userDao.getUserPlId(userId);

            boolean displayRedFlag = generalUtil.displayTotalRedmptionValueFlag(plId);

            List<InvestmentCostBO> costList = userDao.getInvestmentCostList(userId, displayRedFlag);

            int finalHpId = hpId;
            double investedAmount = costList.stream().filter(investmentCostBO -> investmentCostBO.getHoldingProfileId() == finalHpId).mapToDouble(investmentCostBO -> investmentCostBO.getInvestmentAmount()).sum();
            double redemptionAmount = userDao.getRedemptionValue(userId);

            dashBoardBO = userDao.getMFTotalValues(userId, hpId, investedAmount, redemptionAmount, displayRedFlag);

            dashBoardBO.setCurrentValueOfInvestmentFormatted(GeneralUtil.getRupeeBigDecimal(dashBoardBO.getCurrentValueOfInvestment(), 0));
            dashBoardBO.setInvestedAmountFormatted(GeneralUtil.getRupeeBigDecimal(dashBoardBO.getInvestedAmount(), 0));
            dashBoardBO.setGainAmountFormatted(GeneralUtil.getRupeeBigDecimal(dashBoardBO.getGainAmount(), 0));
            dashBoardBO.setChangeAmountFormatted(GeneralUtil.getRupeeBigDecimal(dashBoardBO.getChangeAmount(), 0));
            dashBoardBO.setAnnualReturns(userDao.getCAGR(userId));


        } catch (Exception e) {
            log.error(logPrefix + "Investor dashboard API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, dashBoardBO, globalResponseBo);

        log.debug(logPrefix + "Investor dashboard API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO investedSchemeList(String holdingProfileId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "Invested scheme list fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.list, "InvestedScheme");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<InvestedSchemeBO> investedSchemes = new ArrayList<>();
        String stat = "";
        int hpId = 0;

        try {

            if(StringUtils.isNotEmpty(holdingProfileId))
                hpId = Integer.parseInt(holdingProfileId);

            int plId = userDao.getUserPlId(userId);

            boolean displayRedFlag = generalUtil.displayTotalRedmptionValueFlag(plId);

            List<InvestmentCostBO> costList = userDao.getInvestmentCostList(userId, displayRedFlag);

            investedSchemes = transactionDAO.getInvestedSchemeList(hpId, userId, costList);


        } catch (Exception e) {
            log.error(logPrefix + "Invested scheme list API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, investedSchemes, globalResponseBo);

        log.debug(logPrefix + "Invested scheme list API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO currentSIPList(String holdingProfileId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "Current SIP list fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.list, "CurrentSipScheme");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<SIPDetailsBO> sipList = new ArrayList<>();
        String stat = "";
        int hpId = 0;

        try {

            if(StringUtils.isNotEmpty(holdingProfileId))
                hpId = Integer.parseInt(holdingProfileId);

            int plId = userDao.getUserPlId(userId);
            boolean displayRedFlag = generalUtil.displayTotalRedmptionValueFlag(plId);

            List<InvestmentCostBO> costList = userDao.getInvestmentCostList(userId, displayRedFlag);
            sipList = transactionDAO.getCurrentSIPList(hpId, userId, costList);


        } catch (Exception e) {
            log.error(logPrefix + "Current SIP list API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, sipList, globalResponseBo);

        log.debug(logPrefix + "Current SIP list API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO editCurrentSIP(SIPEditBO sipEditBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | #SipRefId : " + sipEditBO.getReferenceId() + " | ";
        log.debug(logPrefix + "Current SIP Edit");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "SimpleResponse");
        List<ErrorBO> errorBoList = new ArrayList<>();
        HashMap<String, Object> result = new HashMap<>();
        String stat = "";
        int status = 0;
        String allowedActions = BPConstants.ACTION_CHANGE_FLEXI_AMOUNT + "," + BPConstants.ACTION_CHANGE_AMOUNT + "," + BPConstants.ACTION_STOP_SIP;

        try {

            if(StringUtils.isEmpty(sipEditBO.getReferenceId()) || Integer.parseInt(sipEditBO.getReferenceId())<=0)
                errorBoList.add(new ErrorBO("referenceId", "Invalid input"));

            if(StringUtils.isEmpty(sipEditBO.getActionType()) || (!allowedActions.contains(sipEditBO.getActionType())))
                errorBoList.add(new ErrorBO("actionType", "Invalid input"));

            if(errorBoList.size()==0) {

                if(sipEditBO.getActionType().equals(BPConstants.ACTION_STOP_SIP))
                    status = transactionDAO.deactivateSIP(Integer.parseInt(sipEditBO.getReferenceId()), sipEditBO.getRemarks(), userId);

                else {

                    boolean isFlexi = false;
                    int changeAmount = sipEditBO.getChange_amount();
                    if (sipEditBO.getActionType().equals(BPConstants.ACTION_CHANGE_FLEXI_AMOUNT)) {
                        isFlexi = true;
                        changeAmount = sipEditBO.getChange_flexi_amount();
                    }
                    double availableAmount = mandateInfoDAO.getMandateLimitForSIPEdit(Integer.parseInt(sipEditBO.getReferenceId()), userId, isFlexi);
                    if (availableAmount < changeAmount)
                        errorBoList.add(new ErrorBO("amount", "Mandate Limit exceeds"));

                    else {
                        status = transactionDAO.updateSIPAmount(Integer.parseInt(sipEditBO.getReferenceId()), changeAmount, sipEditBO.getSchemeCode(), sipEditBO.getRemarks(), userId, isFlexi);
                        log.debug(logPrefix + "Update Status : " + status);
                    }
                }

                if(status <=0)
                    errorBoList.add(new ErrorBO(sipEditBO.getActionType(), "DB update failed"));
            }


        } catch (Exception e) {
            log.error(logPrefix + "Current SIP Edit API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;
        else {
            result.put("code", 200);
            result.put("result", true);
            result.put("description", "Success");
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, result, globalResponseBo);

        log.debug(logPrefix + "Current SIP edit API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO currentSTPList(String holdingProfileId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "Current STP list fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.list, "StpScheme");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<STPDetailsBO> stpList = new ArrayList<>();
        String stat = "";
        int hpId = 0;

        try {

            if(StringUtils.isNotEmpty(holdingProfileId))
                hpId = Integer.parseInt(holdingProfileId);

            stpList = transactionDAO.getCurrentSTPList(hpId, userId);


        } catch (Exception e) {
            log.error(logPrefix + "Current STP list API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, stpList, globalResponseBo);

        log.debug(logPrefix + "Current STP list API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO editCurrentSTP(SIPEditBO stpEditBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | #StpId : " + stpEditBO.getReferenceId() + " | ";
        log.debug(logPrefix + "Current STP Edit");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "SimpleResponse");
        List<ErrorBO> errorBoList = new ArrayList<>();
        HashMap<String, Object> result = new HashMap<>();
        String stat = "";
        int status = 0;
        String allowedActions = BPConstants.ACTION_STOP_STP;

        try {

            if(StringUtils.isEmpty(stpEditBO.getReferenceId()) || Integer.parseInt(stpEditBO.getReferenceId())<=0)
                errorBoList.add(new ErrorBO("referenceId", "Invalid input"));

            if(StringUtils.isEmpty(stpEditBO.getActionType()) || (!allowedActions.contains(stpEditBO.getActionType())))
                errorBoList.add(new ErrorBO("actionType", "Invalid input"));

            if(errorBoList.size()==0) {

                if(stpEditBO.getActionType().equals(BPConstants.ACTION_STOP_SIP))
                    status = transactionDAO.deactivateSIP(Integer.parseInt(stpEditBO.getReferenceId()), stpEditBO.getRemarks(), userId);
            }

        } catch (Exception e) {
            log.error(logPrefix + "Current SIP Edit API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;
        else {
            result.put("code", 200);
            result.put("result", status>0);
            result.put("description", status>0 ? "Success" : "Failed");
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, result, globalResponseBo);

        log.debug(logPrefix + "Current STP edit API Status : " + stat);
        return  globalResponseBo;

    }

    public String documentDownload(String documentType, String documentId, String investorId, String holdingProfileId, int userId) {

        String logPrefix = "#DocumentType : " + documentType + " | #DocumentId : " + documentId + " | #InvestorId : " + investorId + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "Document download");
        String filePath = "";

        try {

            if(StringUtils.isEmpty(documentType) || StringUtils.isEmpty(documentId))
                return filePath;

            if(documentType.equalsIgnoreCase(BPConstants.DOCUMENT_TYPE_NACH)) {

                if(StringUtils.isNotEmpty(investorId) && Integer.parseInt(investorId)>0) {
                    DocConfig configuration = ConfigFactory.create(DocConfig.class);
                    filePath = configuration.pdfPath() + userId + File.separator + investorId + "-" + documentId + ".pdf";
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        return filePath;
    }
}

package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class OpenMandateBO {

    private int investorId = 0;
    private String investorName;
    private int userBankId = 0;
    private String consumerCode = "";
    private String formType = "";
    private int noOfYears = 0;
    private DateBO startDate;
    private DateBO endDate;
    private String endDateDisp = "";
    private int upperLimit = 0;
    private String upperLimit_formatted = "";
    private int createdUser = 0;
    private int modifiedUser = 0;
    private String createdFrom = "";
    private String bankLookupId = "";
    private String bankName = "";
    private String bankAccountNo = "";
    private String micrCode = "";
    private String bankNameAccNo = "";
    private String bankNameAccNo1 = "";
    private IdDesc SIPFormTypeTDays = new IdDesc();
    private IdDesc status = new IdDesc();
    private DateBO createdDate;
    private double avlbleDayLimitAmt = 0;
    private int avlbleNoOfInstall = 0;
    private String accountType;
    private DateBO receivedDate;
    private String sipDate = "0";
    private String isOE = "false";
    private String amcCode;
    private String amcName;
    private String mandateType = "NACH";
    private String values;
    private String reGenMandate;
    private String disableInPlan = "N";
    private int newUpperLimit = 0;
    private String siInstructionID;
    private String morphedBankAccountNum;
    private String irType = "";
    private String enachMandateRequestId;
    private DateBO fEligibleDate = new DateBO();
    private DateBO siDebitDate = new DateBO();
    private String mandateCancel;
}

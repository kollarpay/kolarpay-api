package com.kp.core.app.controller.plan;
        import com.google.gson.Gson;
        import com.kp.core.app.enums.ResponseDataObjectType;
        import com.kp.core.app.model.CartBO;
        import com.kp.core.app.model.GlobalResponseBO;
        import com.kp.core.app.model.PlanBO;
        import com.kp.core.app.service.CartService;
        import com.kp.core.app.service.Plan.PlanService;
        import com.kp.core.app.utils.APIGeneralUtil;
        import lombok.extern.slf4j.Slf4j;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.MediaType;
        import org.springframework.http.ResponseEntity;
        import org.springframework.util.MultiValueMap;
        import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/plan")
public class PlanController {

    @Autowired
    PlanService planService;
    @Autowired
    Gson gson;
    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createProduct(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#plan create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "planCreate");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                PlanBO PlanBO = gson.fromJson(payload, PlanBO.class);
                globalResponseBO = planService.createPlan(PlanBO, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#Plan create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

package com.kp.core.app.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class PaymentAuthTokenBO{
    @Expose
    public String status;
    @Expose
    public String message;
    @Expose
    public String subCode;
    @Expose
    public Data data;
    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Data{
        @Expose
        public String token;
        @Expose
        public int expiry;
    }
}


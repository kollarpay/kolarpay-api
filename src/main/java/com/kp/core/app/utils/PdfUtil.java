package com.kp.core.app.utils;

import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.pdf.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Map;

@Slf4j
@Component
public class PdfUtil {

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private NumberConverter numberConverter;

    public String createPdf(String templateName, Map map, String path, String fileName) throws Exception {

        String logPrefix = "#FilePath : " + path + " | #FileName : " + fileName + " | ";
        Context ctx = new Context();
        FileOutputStream os = null;
        String outputFilePath="";
        try {
            if (StringUtils.isEmpty(templateName))
                return fileName;

            if (map != null) {
                Iterator itMap = map.entrySet().iterator();
                while (itMap.hasNext()) {
                    Map.Entry pair = (Map.Entry) itMap.next();
                    ctx.setVariable(pair.getKey().toString(), pair.getValue());
                }
            }
            String processedHtml = templateEngine.process(templateName, ctx);
            os = new FileOutputStream(new File(path+fileName));
            HtmlConverter.convertToPdf(processedHtml, os);
            log.info(logPrefix + "PDF created successfully."+templateName);
            outputFilePath = path + fileName;
        } catch (Exception e) {
            log.error("Ex:AdvisoryReport:PDFGEN:",e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    log.error(logPrefix + "Exception:",e);
                }
            }
        }
        return outputFilePath;
    }

    public void addPageNumbers(String filePath, String destPath, String numberingFormat, int offset_X, int offset_Y) throws IOException {
        int page_counter = 1;
        int pageNumberRemove    =   1;

        try {

            File file = new File(filePath);
            PDDocument document = PDDocument.load(file);
            for (PDPage page : document.getPages()) {

                if ((pageNumberRemove == 1) || (document.getNumberOfPages() == pageNumberRemove)) { // page number removed in first page & last page.
                    ++pageNumberRemove;
                } else {
                    PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true, false);
                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.TIMES_ITALIC, 12);
                    PDRectangle pageSize = page.getMediaBox();
                    float x = pageSize.getLowerLeftX() - 30;
                    float y = pageSize.getLowerLeftY();
                    contentStream.newLineAtOffset(x + pageSize.getWidth() - offset_X, y + offset_Y);
                    String text = MessageFormat.format(numberingFormat, page_counter);
                    contentStream.showText(text);
                    contentStream.endText();
                    contentStream.close();
                    ++page_counter;
                    ++pageNumberRemove;
                }

            }

            document.save(new File(destPath));
            document.close();

        } catch (Exception e) {
            log.error("Exception in adding page numbers to pdf : " , e);
        }
    }

    public void passwordProtectPdf(String filePath, String destPath, String userPassword, String ownerPassword) {

        try {

            PdfReader reader = new PdfReader(filePath);
            WriterProperties props = new WriterProperties().setStandardEncryption(userPassword.toLowerCase().getBytes(),
                    ownerPassword.toLowerCase().getBytes(),
                    EncryptionConstants.ALLOW_PRINTING,
                    EncryptionConstants.ENCRYPTION_AES_128 | EncryptionConstants.DO_NOT_ENCRYPT_METADATA);
            // Write to PDF along with encryption properties
            PdfWriter writer = new PdfWriter(new FileOutputStream(destPath), props);
            PdfDocument pdfDoc = new PdfDocument(reader, writer);
            pdfDoc.close();

        } catch (Exception e) {
            log.error("Exception in pdf password protect : ", e);
        }

    }
}

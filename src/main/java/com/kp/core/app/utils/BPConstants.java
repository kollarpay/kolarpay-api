package com.kp.core.app.utils;

import com.kp.core.app.model.IdDesc;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BPConstants {

    public static final String SUCCESS = "success";
    public static final String FAIL = "fail";
    public static final String UNAUTHORIZED = "unAuthorized";
    public static final String CURRENCY_INDIA = "INR";

    public static final String TYPE_EMAIL = "email";
    public static final String TYPE_PAN = "pan";
    public static final String EMAIL_REGEX = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";


    //MF Scheme Plan Types
    public static final String PLAN_TYPE_SIP = "SIP";
    //MF Scheme Search
    public static final String SCHEME_SEARCH_FI = "1";
    public static final String SCHEME_SEARCH_GROWTH = "2";
    public static final String SCHEME_SEARCH_DIVIDEND = "3";
    public static final String SCHEME_SEARCH_RATED = "4";
    public static final IdDesc SCHEME_SEARCH_FI_ID = new IdDesc("1", "FI");
    public static final IdDesc SCHEME_SEARCH_GROWTH_ID = new IdDesc("2", "Growth");
    public static final IdDesc SCHEME_SEARCH_DIVIDEND_ID = new IdDesc("3", "Dividend");
    public static final IdDesc SCHEME_SEARCH_RATED_ID = new IdDesc("4", "Rated");

    public static final int SHEME_COUNT_PER_PAGE = 15;
    //AMC NOT ALLOWED FOR USA/CANADA
    public static final String USA_CANADA_ONLY_AMC = "400001,400002,400005,400006,400008,400009,400012,400013,400014,400015,400016,400017,400018,400019,400020,400021,400022,400023,400024,400025,400027,400028,400030,400031,400033,400034,400035,400037,400039,400040,400041,400042,400043,400044,400047,400048,400052,400004,400010,400054,400055,400056";
    public static final String OE_AMCS = "400001,400002,400004,400005,400006,400007,400009,400012,400013,400015,400018,400019,400021,400023,400025,400027,400028,400030,400032,400033,400035,400040,400042,400043,400049,400029,400055";
    public static final String NATIONALITY_WISE_AMC_RESTRICT = "400012";
    //Specific conditions
    public static final String SIP_SHOWUP_FI_RECOMEND = "2906,9767";
    public static final int IDFC_PE_G_SCODE = 2906;
    public static final int IDFC_PE_D_SCODE = 2905;
    public static final int REL_SC_G_SCODE = 12758;
    public static final int REL_SC_D_SCODE = 12760;
    public static final int SBI_SMC_G_SCODE = 7885;
    public static final int SBI_SMC_D_SCODE = 7886;
    public static final String ALLOW_TRANSACT_ACTIVE = "A";
    public static final String ALLOW_TRANSACT_DEACTIVE = "D";

    public static final String PRIMARY_RELATIONSHIP_ID = "F";
    public static final String GUARDIAN_RELATIONSHIP_ID = "G";
    public static final String SECONADARY_RELATIONSHIP_ID = "S";
    public static final String TERTIARY_RELATIONSHIP_ID = "T";

    public static final String PRIMARY_RELATIONSHIP_DESC = "primary";
    public static final String SECONADARY_RELATIONSHIP_DESC = "secondary";
    public static final String TERTIARY_RELATIONSHIP_DESC = "tertiary";

    public static final String GROWTH = "Growth";
    public static final String DIVIDEND = "Dividend";
    public static final String DIVIDEND_PAYOUT = "Payout";
    public static final String DIVIDEND_REINVESTMENT = "Reinvestment";

    public static final String BANK_CATEGORY_PRIMARY = "primary";
    public static final String BANK_CATEGORY_SECONDARY = "secondary";
    public static final String BANK_CATEGORY_OTHERS = "others";
    public static final String ACC_TYPE_CURRENT = "Current";
    public static final String ACC_TYPE_SAVINGS = "Savings";
    public static final String ACC_TYPE_NRO = "NRO";
    public static final String ACC_TYPE_NRE = "NRE";
    public static final String MODE_OF_HOLDING_SINGLE = "Individual";
    public static final String MODE_OF_HOLDING_JOINT = "Joint";
    public static final String MODE_OF_HOLDING_ANYONE_SURVIVOR = "Either_Or_Survivor";

    public static  final String SCHEME_TYPE_OPEN_ENDED = "Open-ended";
    public static  final String SCHEME_TYPE_CLOSE_ENDED = "Close-ended";


    public static final String REGULAR_SIP_TYPE = "REGULAR";
    public static final String STEPUP_SIP_TYPE = "STEPUP";
    public static final String FLEXI_SIP_TYPE = "FLEXI";
    public static final String ALERT_SIP_TYPE = "ALERT";
    public static final String INSURE_SIP_TYPE = "INSSIP";

    public static  final String FREQUENCY_MONTHLY = "monthly";
    public static  final String FREQUENCY_HALF_YEARLY = "half-yearly";
    public static  final String FREQUENCY_YEARLY = "yearly";

    public static  final String MANDATE_TYPE_NACH = "Mandate";
    public static  final String MANDATE_TYPE_ENACH = "eMandate";

    public static final String MANDATE_CREATE_SIP = "SIPFLOW";

    public static final String ONLINE_ENROLLMENT = "OE";
    public static final String ONLINE_REGISTRATION = "ON";
    public static final String EKYC_REGISTRATION = "EKYC";

    public static final double UNITS = 1;
    public static final double TENS = 10 * UNITS;
    public static final double HUNDREDS = 10 * TENS;
    public static final double THOUSANDS = 10 * HUNDREDS;
    public static final double TENTHOUSANDS = 10 * THOUSANDS;
    public static final double LAKHS = 10 * TENTHOUSANDS;
    public static final double TENLAKHS = 10 * LAKHS;
    public static final double CRORES = 10 * TENLAKHS;
    public static final double TENCRORES = 10 * CRORES;

    public static final String PARTNER_USER_TYPE = "PARTNER";
    public static final String ADMIN_USER_TYPE = "ADMIN";
    public static final String NORMAL_USER_TYPE = "USER";


    public static final String ENCRYPTION_ALGORITHM = "SHA-512";

    public static final double AADHAR_MAX_AMOUNT = 50000.0;

    public static final double EQUITY_RETURNS = 12;
    public static final double DEBT_RETURNS = 6;
    public static final double GOLD_RETURNS = 8;

    public static final String LQ_SCHEMES_NAV_MSG = "Please note that per applicable SEBI guidelines for liquid funds, the applicable NAV for the liquid fund(s) this transaction would be that of the day prior to the next business day after the transaction date.";

    public static final String SIP_STOP_RESTRICTION_DATE = "2013-10-21";
    public static final int MINIMUM_NUMBER_OF_INSTALLMENTS_TO_STOP = 12;

    public static final String ACTION_MAKE_PAYMENT = "make_payment";
    public static final String ACTION_CHANGE_AMOUNT = "change_amount";
    public static final String ACTION_CHANGE_FLEXI_AMOUNT = "change_flexi_amount";
    public static final String ACTION_SKIP = "skip_next_installment";
    public static final String ACTION_PAUSE = "pause_sip";
    public static final String ACTION_STOP_SIP = "stop_sip";
    public static final String ACTION_EXTEND = "extend_sip";
    public static final String ACTION_CHANGE_DATE = "change_date";
    public static final String ACTION_EDIT = "edit";
    public static final String ACTION_STOP_STP = "stop_stp";

    public static final String DSP_MICRO_SIP_SCHEMES = "714,12529";

    public static final String PAID_STATUS_TC = "Transaction Complete";
    public static final String PAID_STATUS_TS = "Transaction Success";
    public static final String PAID_STATUS_PS = "Payment Success";
    public static final String PAID_STATUS_PF = "Payment Failed";
    public static final String PAID_STATUS_PW = "Payment Waiting";

    public static final String DOCUMENT_TYPE_NACH = "mandate-form";



    public static final String INVALID_INPUT = "Invalid input";
    public static final String PAYMENT_MODE_BANK_TRANSFER="BANK-TRANSFER";
    public static final String PAYMENT_SUB_MODE_NEFT="NEFT";
    public static final String PAYMENT_SUB_MODE_UPI="UPI";
    public static final String PAYMENT_SUB_MODE_NET_BANKING="NET_BANKING";
    public static final String MODE_NETBANKING = "netbanking";
    public static final String ORDER_CREATION_STATUS_CREATED = "created";
    public static final String SIMPLE_ERROR_RESPONSE = "SimpleErrorResponse";


}

package com.kp.core.app.service.Utilities;

import com.kp.core.app.config.JWTSettings;
import com.kp.core.app.dao.BPAuthenticationDAO;
import com.kp.core.app.dao.InvestorInfoDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.utils.*;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.GeneralUtil;
import com.kp.core.app.utils.JWTTokenFactory;
import com.kp.core.app.utils.RawAccessJwtToken;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Service
@Slf4j
public class BPAuthenticationService {

    public static final String
            JWT_ACCESS_TOKEN = "accessToken",
            JWT_REFRESH_TOKEN = "refreshToken";

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private BPAuthenticationDAO authenticationDAO;

    @Autowired
    private InvestorInfoDAO investorInfoDAO;

    @Autowired
    private JWTTokenFactory jwtTokenFactory;

    @Autowired
    private RawAccessJwtToken rawAccessJwtToken;

    public GlobalResponseBO loginCredentials(AuthRequest authRequest, String id) {

        String logPrefix = "#id : " + id + " | ";
        log.debug(logPrefix + "Login POST API");

        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "Login");
        HashMap<String, String> tokens = new HashMap<>();
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        UserDetailsBO userBo=new UserDetailsBO();
        try {

            if(StringUtils.isEmpty(authRequest.getGrantType()))
                errorBoList.add(new ErrorBO("grantType", BPConstants.INVALID_INPUT));

            else  {
                String grantType = authRequest.getGrantType();


                switch (grantType) {

                    case "credentials":

                        if(StringUtils.isEmpty(authRequest.getEmailId()))
                            errorBoList.add(new ErrorBO("emailId", BPConstants.INVALID_INPUT));
                        if(StringUtils.isEmpty(authRequest.getPassword()))
                            errorBoList.add(new ErrorBO("password", BPConstants.INVALID_INPUT));
                       else {
                             userBo = authenticationDAO.getValidateUserSignIn(authRequest.getEmailId(), authRequest.getPassword());
                            if (userBo != null) {
                                tokens = createToken(userBo);
                            } else {
                                stat = APIConstants.UNAUTHORIZED_STRING;
                            }
                        }

                        break;
                    case "mcredentials":

                        if(StringUtils.isEmpty(authRequest.getMobileno()))
                            errorBoList.add(new ErrorBO("mobile No", BPConstants.INVALID_INPUT));
                        if(StringUtils.isEmpty(authRequest.getPassword()))
                            errorBoList.add(new ErrorBO("password", BPConstants.INVALID_INPUT));
                        else {
                             userBo = authenticationDAO.getmobileNoValidateUserSignIn(authRequest.getMobileno(), authRequest.getPassword());
                            if (userBo != null) {
                                tokens = createToken(userBo);
                            } else {
                                stat = APIConstants.UNAUTHORIZED_STRING;
                            }
                        }

                        break;

                    case "refresh_token":

                        if(StringUtils.isEmpty(authRequest.getRefreshToken()))
                            errorBoList.add(new ErrorBO("refreshToken", BPConstants.INVALID_INPUT));

                        else {
                            JWTSettings jwtSettings = ConfigFactory.create(JWTSettings.class);
                            HashMap<String, Object> tokenStatus = rawAccessJwtToken.parseToken(jwtSettings.tokenSigningKey(), authRequest.getRefreshToken());

                            if (tokenStatus != null) {
                                Boolean invalidToken = extractTokenFlag(tokenStatus, "invalidToken");
                                Boolean expiredToken = extractTokenFlag(tokenStatus, "expiredToken");
                                tokens.put("invalidToken", String.valueOf(invalidToken));
                                tokens.put("expiredToken", String.valueOf(expiredToken));
                                if (!invalidToken && !expiredToken) {
                                    tokens = createTokenUsingRefreshToken(authRequest.getRefreshToken());
                                }
                            }
                        }
                        break;
                    case "impersonate":

                        if(StringUtils.isEmpty(authRequest.getToken()))
                            errorBoList.add(new ErrorBO("Token", BPConstants.INVALID_INPUT));

                        else {
                            JWTSettings jwtSettings = ConfigFactory.create(JWTSettings.class);
                            HashMap<String, Object> tokenStatus = rawAccessJwtToken.parseToken(jwtSettings.tokenSigningKey(), authRequest.getToken());
                          String username=  jwtTokenFactory.extractUsername(authRequest.getToken());
                          UserDetailsBO userDetailsBO = new UserDetailsBO();
                          UserDetailsBO userClientDetailsBO = new UserDetailsBO();
                          String userTypeError="";
                          if(username!=null) {

                              userDetailsBO= getUserDetailsFromUserName(username);
                              if(userDetailsBO==null) userTypeError="admin not found";
                              if(userDetailsBO!=null && !userDetailsBO.getRole().equalsIgnoreCase("admin")){
                                  userTypeError="Only admins can impersonate";
                              }
                              userClientDetailsBO=getUserDetailsFromUserName(userBo.getClientId());
                              if(userClientDetailsBO==null) userTypeError="client not found";

                          }
                            if (tokenStatus != null && username != null) {
                                Boolean invalidToken = extractTokenFlag(tokenStatus, "invalidToken");
                                Boolean expiredToken = extractTokenFlag(tokenStatus, "expiredToken");
                                tokens.put("invalidToken", String.valueOf(invalidToken));
                                tokens.put("expiredToken", String.valueOf(expiredToken));
                               if(!userTypeError.isEmpty()) tokens.put("userTypeError", String.valueOf(userTypeError));
                                if (!invalidToken && !expiredToken && userTypeError.isEmpty()) {
                                    tokens = createTokenUsingUserId(username);
                                }
                            }
                        }
                        break;
                    default:
                        stat = APIConstants.BAD_REQUEST_STRING;
                }
            }

           if(!errorBoList.isEmpty())
                stat = APIConstants.BAD_REQUEST_STRING;

        } catch (Exception e) {
            log.error("{}Exception in Login POST API : ", logPrefix, e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, tokens, globalResponseBo);

        return globalResponseBo;
    }



    public HashMap<String, String> createToken(UserDetailsBO userBo) {

        HashMap<String, String> tokens = null;

        if (userBo != null) {
            tokens = new HashMap<>();
            AccessJwtToken accessJwtToken = jwtTokenFactory.createAccessJwtToken(userBo);
            JwtToken refreshToken = jwtTokenFactory.createRefreshToken(userBo);
            tokens.put(JWT_ACCESS_TOKEN, accessJwtToken.getToken());
            tokens.put(JWT_REFRESH_TOKEN, refreshToken.getToken());
            authenticationDAO.insertUserToken(userBo, accessJwtToken, refreshToken);
        }
        return tokens;
    }

    public static Boolean extractTokenFlag(HashMap tokenStatus, String name) {
        return tokenStatus.get("expiredToken") != null && (Boolean) tokenStatus.get(name);
    }

    public HashMap<String, String> createTokenUsingRefreshToken(String token) {

        HashMap<String, String> tokens = null;
        try {
            String userId = authenticationDAO.fetchUserIdByRefreshToken(token);
            if (userId != null) {
                UserDetailsBO userBo = new UserDetailsBO();
                userBo.setUserName(userId);
                userBo.setUserId(Integer.parseInt(userId));
                tokens = createToken(userBo);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return tokens;
    }
    public HashMap<String, String> createTokenUsingUserId(String userId) {

        UserDetailsBO userDetailsBO=new UserDetailsBO();
        HashMap<String, String> tokens=null;
        try {
            userDetailsBO = authenticationDAO.getUserFromUserName(userId);
            if (userDetailsBO != null) {
                tokens = createToken(userDetailsBO);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return tokens;
    }
    public UserDetailsBO getUserDetailsFromUserName(String userId) {

        UserDetailsBO userDetailsBO=new UserDetailsBO();
        try {
            userDetailsBO = authenticationDAO.getUserFromUserName(userId);

        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return userDetailsBO;
    }
}

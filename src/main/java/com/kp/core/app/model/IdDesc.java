package com.kp.core.app.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IdDesc {
    private String key = "";
    private String value = "";
    private String desc = "";
    private String orgid = "";

    public IdDesc(String key, String desc) {
        this.key = key;
        this.desc = desc;
    }
    public IdDesc(String key, String desc,String orgid) {
        this.key = key;
        this.desc = desc;
        this.orgid=orgid;
    }
    public IdDesc(String key, String value, String desc,String orgid) {
        this.key = key;
        this.value = value;
        this.desc = desc;
        this.orgid=orgid;

    }

    public IdDesc() {
    }

}

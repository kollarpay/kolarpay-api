package com.kp.core.app.phonpe.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentStatusResponse {
    private boolean success;
    private String code;
    private String message;
    private Data data;
    private PaymentInstrument paymentInstrument;


    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PaymentInstrument {
        private String type;
        private String utr;
        private String upiTransactionId;
        private String accountHolderName;
        private String cardNetwork;
        private String accountType;
        private String cardType;
        private String pgTransactionId;
        private String bankTransactionId;
        private String pgAuthorizationCode;
        private String arn;
        private String bankId;
        private String brn;
        private String pgServiceTransactionId;
    }
    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data {
        private String merchantId;
        private String merchantTransactionId;
        private String transactionId;
        private int amount;
        private String state;
        private String responseCode;
        private PaymentInstrument paymentInstrument;

    }
}

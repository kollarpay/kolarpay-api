package com.kp.core.app.service.mandate;

import com.kp.core.app.dao.GeneralInfoDAO;
import com.kp.core.app.dao.MandateDao;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.MandateBo;
import com.kp.core.app.service.Payment.PaymentServices;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.GeneralUtil;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
@Slf4j
public class MandateService {

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private MandateDao mandateDao;
    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    private final
    PaymentServices paymentServices;

    public MandateService(PaymentServices paymentServices) {
        this.paymentServices = paymentServices;
    }

    public GlobalResponseBO createMandate(MandateBo mandateBo, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create subscription Service");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createSubscription");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            isValidMandate(mandateBo, errorBoList);
            if(errorBoList.isEmpty()) {
                mandateDao.createMandate(mandateBo,userId);
                if(mandateBo!=null && !StringUtil.isNullOrEmpty(mandateBo.getSubscriptionId())){
                   // paymentServices.createPhonePeSubscription(mandateBo,errorBoList);
                }
            }


        } catch (Exception e) {
            log.error(logPrefix + "Create Subscription API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, mandateBo, globalResponseBo);

        log.debug(logPrefix + "Create Subscription API Status : " + stat);
        return  globalResponseBo;

    }
    public void isValidMandate(MandateBo mandateBo, List<ErrorBO> errorBoList) {

        try {

            if(StringUtil.isNullOrEmpty(mandateBo.getId()))
                errorBoList.add(new ErrorBO("subscription Id", "Invalid subscription id"));


        } catch (Exception e) {
            log.error("Exception in mandate creation - validate : ", e);
        }
    }
}

package com.kp.core.app.razorpay.model;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RzpInitiatePaymentResponseBO {

    @SerializedName("razorpay_payment_id")
    private String razorpayPaymentId;
    private String link;
}

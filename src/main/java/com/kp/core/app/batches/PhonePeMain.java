package com.kp.core.app.batches;




import com.google.common.hash.Hashing;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.Base64;

import com.password4j.BcryptFunction;
import com.password4j.Hash;
import com.password4j.Password;
import com.password4j.types.Bcrypt;



public class PhonePeMain {

    public static void main(String[] args) throws NoSuchAlgorithmException {

        LocalDate localDate = LocalDate.now();
        System.out.println("localDate----"+localDate);
        localDate.plusMonths(12);

        LocalDate localDate2 = LocalDate.now().plusMonths(12);
                System.out.println("localDate2----"+localDate2);

         String merchantId="PGTESTPAYUAT84";
         String authRequestId="TX90234945";
     String payload="{\n" +
             "  \"merchantId\": \"M222W6VUE0DVB\",\n" +
             "  \"merchantSubscriptionId\": \"TSUB0000000000000001\",\n" +
             "  \"merchantUserId\": \"MU123456789\",\n" +
             "  \"authWorkflowType\": \"PENNY_DROP\",\n" +
             "  \"amountType\": \"FIXED\",\n" +
             "  \"amount\": 39900,\n" +
             "  \"frequency\": \"MONTHLY\",\n" +
             "  \"recurringCount\": 12,\n" +
             "  \"mobileNumber\": \"8973552208\",\n" +
             "  \"deviceContext\": {\n" +
             "  \"phonePeVersionCode\": 400922\n" +
             "  }\n" +
             "}";
        //String payload="{\"merchantID\":\"PGTESTPAYUAT84\",\"merchantSubscriptionID\":\"sub_SIdnnIVwdiG8Di\",\"merchantUserID\":\"10043\",\"authWorkflowType\":\"PENNY_DROP\",\"amountType\":\"VARIABLE\",\"amount\":200,\"frequency\":\"MONTHLY\",\"recurringCount\":11,\"mobileNumber\":\"8973552208\",\"deviceContext\":{\"phonePEVersionCode\":400922}}";

//        String payload="{\n" +
//                "  \"merchantId\": \"PGTESTPAYUAT84\",\n" +
//                "  \"merchantTransactionId\": \"MT7850590068188104\",\n" +
//                "  \"merchantUserId\": \"MUID123\",\n" +
//                "  \"amount\": 10000,\n" +
//                "  \"redirectUrl\": \"https://webhook.site/redirect-url\",\n" +
//                "  \"redirectMode\": \"REDIRECT\",\n" +
//                "  \"callbackUrl\": \"https://webhook.site/callback-url\",\n" +
//                "  \"mobileNumber\": \"9999999999\",\n" +
//                "  \"paymentInstrument\": {\n" +
//                "    \"type\": \"PAY_PAGE\"\n" +
//                "  }\n" +
//                "}";
//         payload="{\n" +
//                 "    \"merchantId\": \"PGTESTPAYUAT84\",\n" +
//                 "    \"merchantUserId\": \"MU123456789\",\n" +
//                 "    \"subscriptionId\": \"OM2405081231246578606316\",\n" +
//                 "    \"authRequestId\": \"TX123456789\",\n" +
//                 "    \"amount\": 39900,\n" +
//                 "    \"paymentInstrument\": {\n" +
//                 "      \"type\": \"UPI_INTENT\",\n" +
//                 "      \"targetApp\": \"com.phonepe.app\"\n" +
//                 "    },\n" +
//                 "    \"deviceContext\" : {\n" +
//                 "    \"deviceOS\" : \"ANDROID\"\n" +
//                 "    }\n" +
//                 "} ";
        String sandBoxsaltKey="4fe201c8-32e9-4143-821b-00296f0f19d7";
       // String productSaltKey="558b1969-fa36-46ff-87d8-7fe18df8b251";
      String base64=  Base64.getEncoder().encodeToString(payload.getBytes());
        System.out.println("base64----"+base64);
        String checksum=base64+"/pg/v1/pay"+sandBoxsaltKey;
         checksum=base64+"/v3/recurring/subscription/create"+sandBoxsaltKey;
         String subscriptionStatusChecksum="/v3/recurring/auth/status/"+merchantId+"/"+authRequestId+sandBoxsaltKey;
        // checksum=base64+"/v3/recurring/auth/init"+sandBoxsaltKey;
        System.out.println("sha256hex1----"+checksum);
        MessageDigest md = MessageDigest.getInstance( "SHA-256" ) ;
        byte[] sha256hex =md.digest(checksum.getBytes( StandardCharsets.UTF_8 ) ) ;
        String sha256hex1=toHexString(sha256hex)+"###1";
        String sha256hex2=sha256(checksum)+"###1";
        String sha256hex4=sha256(subscriptionStatusChecksum)+"###1";

        System.out.println("sha256hex1----"+sha256hex1);
        System.out.println("sha256hex2----"+sha256hex2);
        System.out.println("sha256hex2----"+sha256hex4);
       // SHA256(“/v3/recurring/subscription/status/{merchantId}/{merchantSubscriptionId}” + saltKey) + “###” + saltIndex
        String sha256hex3 = org.apache.commons.codec.digest.DigestUtils.sha256Hex(checksum);
        final String hashed = Hashing.sha256()
                .hashString(checksum, StandardCharsets.UTF_8)
                .toString();
        System.out.println("hashed----"+hashWith256(checksum));
        BcryptFunction bcrypt = BcryptFunction.getInstance(Bcrypt.B, 12);
        Hash hash = Password.hash("suresh")
                .addPepper("sureshrajan")
                .with(bcrypt);
        System.out.println("password 4j encoded ---"+hash.getResult());
       // String hasValue="$2b$12$NrDWbLPBqXXkPlnYyRIDLONrhpxYQUV7FpwZlbkprTeU2iPG22EpG";
        String  hasValue="$2b$12$31oFrSKCvXpbFwUVYU1SrO2XspJ34DZR2ShHbEoE02gVFY9a5UQqm";
        boolean verified = Password.check("suresh", hasValue)
                .addPepper("sureshrajan")
                .with(bcrypt);
 System.out.println("password 4j verified ---"+verified);






    }
    static String hashWith256(String textToHash) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] byteOfTextToHash = textToHash.getBytes(StandardCharsets.UTF_8);
        byte[] hashedByetArray = digest.digest(byteOfTextToHash);
        String encoded = Base64.getEncoder().encodeToString(hashedByetArray);
        return encoded;
    }
    public static String toHexString( byte[ ] hash )
    {
        // For converting byte array into signum representation
        BigInteger number = new BigInteger( 1, hash ) ;
        // For converting message digest into hex value
        StringBuilder hexString = new StringBuilder( number.toString( 16 ) ) ;

        // Pad with leading zeros
        while ( hexString.length( ) < 32 )
        {
            hexString.insert( 0,  " 0 " ) ;
        }
        return hexString.toString( ) ;
    }
    public static String sha256(final String base) {
        try{
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(base.getBytes("UTF-8"));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }


}

package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/brospay/config/properties/payment.properties")
@Config.HotReload(unit = TimeUnit.MINUTES, value = 3)
public interface PaymentConfig extends Reloadable {

    @Key("slash")
    @DefaultValue("/")
    String getSlash();
    @Key("RZP.CURRENCY.INDIA")
    @DefaultValue("INR")
    String rzpIndianCurrency();
    @Key("RAZP.EXTERNAL.API.KEY")
    @DefaultValue("rzp_test_b0lntTfwbp4gCB")
    String paymentExternalApiKey();
    @Key("RAZP.EXTERNAL.API.VALUE")
    @DefaultValue("HDEPAYdPwDkXCQrJi3vpJzRu")
    String paymentExternalApiSecret();

    @Key("PAYMENT.EXTERNAL.API.VERSION")
    @DefaultValue("2022-01-01")
    String paymentExternalApiVersion();


    @Key("PAYMENT.EXTERNAL.BASE.URL")
    @DefaultValue("https://api.razorpay.com")
    String PaymentBaseUrl();
    @Key("PAYMENT.EXTERNAL.VERSION.URL")
    @DefaultValue("/v1")
    String PaymentVersion();
    @Key("PAYMENT.EXTERNAL.CUSTOMER.URL")
    @DefaultValue("/customers")
    String paymentCustomerCreateUrl();

    @Key("PAYMENT.EXTERNAL.CONTENT.TYPE")
    @DefaultValue("application/json")
    String getContentTypeJson();

    @Key("CAF.EXTERNAL.PAYOUT.API.KEY")
    @DefaultValue("CF160428CCO1J9D84OK74C7OSF30")
    String cafPayoutApiKey();
    @Key("CAF.EXTERNAL.PAYOUT.API.VALUE")
    @DefaultValue("cf31d40c44f892bb646adbb95df478883b751036")
    String cafPayoutApiSecret();
    @Key("CAF.EXTERNAL.PAYOUT.URL")
    //@DefaultValue("https://payout-api.cashfree.com/payout")
    @DefaultValue("https://payout-gamma.cashfree.com/payout")
    String capPayoutUrl();

    @Key("CAF.EXTERNAL.PAYOUT.VERSION.URL")
    @DefaultValue("v1")
    String capPayoutVersion();
    @Key("CAF.EXTERNAL.PAYOUT.AUTH.URL")
    @DefaultValue("authorize")
    String capPayoutAuth();
    @Key("CAF.EXTERNAL.PAYOUT.VERIFYBANK.VERSION.URL")
    @DefaultValue("v1")
    String capPayoutVerifyBankVersion();
    @Key("CAF.EXTERNAL.PAYOUT.VERIFYBANK.URL")
    @DefaultValue("asyncValidation/bankDetails")
    String capPayoutVerifyBank();
    @Key("CAF.EXTERNAL.PAYOUT.VERIFYBANKSTATUS.URL")
    @DefaultValue("getValidationStatus/bank")
    String capPayoutVerifyBankStatus();

    @Key("PAYMENT.EXTERNAL.BASE.PHONEPE.URL")
    @DefaultValue("https://api-preprod.phonepe.com/apis/pg-sandbox")
    String getPhonePeBaseUrl();
    @Key("PAYMENT.EXTERNAL.BASE.PHONEPE.VERSION")
    @DefaultValue("v3")
    String getPhonePeVersion();
    @Key("PHONEPE.EXTERNAL.API.KEY")
    @DefaultValue("4fe201c8-32e9-4143-821b-00296f0f19d7")
    String getPhonePeApiKey();
    @Key("PHONEPE.EXTERNAL.API.KEY.INDEX")
    @DefaultValue("1")
    String getPhonePeApiKeyIndex();
    @Key("PHONEPE.EXTERNAL.API.MERCHANTID")
    @DefaultValue("PGTESTPAYUAT84")
    String getPhonePeApiMerchantId();
    @Key("PHONEPE.EXTERNAL.API.ANDROID.VERSIONCODE")
    @DefaultValue("400922")
    long getPhonePeApiAndroidVersionCode();
    @Key("PHONEPE.EXTERNAL.API.REDIRCT.URL")
    @DefaultValue("http://ec2-13-235-94-98.ap-south-1.compute.amazonaws.com:8080/api/core/payment-status/check")
    String getPhonePeApiRedirectUrl();
    @Key("PHONEPE.EXTERNAL.SUBSCRIPTION.CREATE.URL")
    @DefaultValue("recurring/subscription/create")
    String getPhonePeApiSubscriptionCreateUrl();
    @Key("PHONEPE.EXTERNAL.SUBSCRIPTION.AUTH.URL")
    @DefaultValue("recurring/auth/init")
    String getPhonePeApiSubscriptionAuthUrl();
    @Key("PHONEPE.EXTERNAL.API.ANDROID.STRING")
    @DefaultValue("ANDROID")
    String getPhonePeApiAndroidString();



}

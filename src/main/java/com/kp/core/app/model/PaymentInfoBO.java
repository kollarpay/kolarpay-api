package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentInfoBO {

    private String paymentId;
    private MFDetails mf;
    private ENachInfoBO enach;
    private List<MandateInfoBO>mandates;
    private Boolean paymentRetry;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MFDetails {

        private String name;
        private String mobile;
        private String holdingProfileId;
        private String goalId;
        private double totalAmount;
        private String totalAmountFormatted;
        private List<SchemeDetails> schemes;

    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SchemeDetails {

        private String schemeCode;
        private String schemeName;
        private String schemeClass;
        private String amcCode;
        private double amount;
        private String itc;
        private String tpslSchemeCode;
        private String bankId;
        private String amcSchemeCode;
        private String transactionReferenceNo;
        private String holdingProfileId;
    }
}

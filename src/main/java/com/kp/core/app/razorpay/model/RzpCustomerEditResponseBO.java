package com.kp.core.app.razorpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RzpCustomerEditResponseBO {

    private String id;
    private String entity;
    private String name;
    private String email;
    private String contact;
    private String gstin;
    private CustomerEditNotesBO notes;
    private int created_at;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CustomerEditNotesBO {
        private String mobile;
        private String labelId;

    }
}

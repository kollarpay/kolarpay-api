package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductSearchRequestBO {

    private String query;
    private String orderBy;
    private String orderType;
    private int page;
    private int size;
    private Boolean sip;
    private Boolean oti;
    private Boolean nri;
    private Boolean rated;
    private String[] schemeType;
    private String[] schemeCodes;
    private List<NameValueBO> amcs;
    private List<NameValueBO> categories;
    private List<NameValueBO> subCategories;
    private List<NameValueBO> risk;
    private List<NameValueBO> ratings;
    private List<ValueBO> searchCode;


    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ValueBO {

        private String name;
        private String value;
        private Boolean sort;
    }
}

package com.kp.core.app.model.Products;


        import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
        import com.fasterxml.jackson.annotation.JsonInclude;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;
        import lombok.Getter;
        import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDiscount {

    @SerializedName("id")
    @Expose
    public Long id;

    @SerializedName("product_id")
    @Expose
    public Long productId;

    @SerializedName("discount_value")
    @Expose
    public Long discountValue;

    @SerializedName("discount_unit")
    @Expose
    public Long discountUnit;

    @SerializedName("valid_from")
    @Expose
    public String validFrom;

    @SerializedName("valid_until")
    @Expose
    public String validUntil;

    @SerializedName("coupon_code")
    @Expose
    public String couponCode;

    @SerializedName("minimum_order")
    @Expose
    public int minimumOrder;

    @SerializedName("maximum_discount_amount")
    @Expose
    public int maximumDiscountAmount;

    @SerializedName("is_redeem_allowed")
    @Expose
    public String isredeemAllowed;

    @SerializedName("created_at")
    @Expose
    public String createdAt;



}


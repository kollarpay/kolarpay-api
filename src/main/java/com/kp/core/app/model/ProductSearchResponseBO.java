package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductSearchResponseBO {

    private List<SchemeDetailBO> content;
    private int page;
    private int totalPages;
    private int size;
    private int count;
    private boolean first;
    private boolean last;


    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SchemeDetailBO {
        private String name;
        private String schemeCode;
        private String category;
        private String subCategory;
        private String categoryId;
        private String subCategoryId;
        private String amc;
        private String amcCode;
        private int rating;
        private double nav;
        private String navFormatted;
        private String navAsOn;
        private String investorId;
        private String expenseRation;
        private double maximumInvestment;
        private String maximumInvestmentFormatted;
        private double minimumInvestment;
        private String minimumInvestmentFormatted;
        private double multiplesOfInvestment;
        private String multiplesOfInvestmentFormatted;
        private double additionalInvestmentMultiples;
        private String additionalInvestmentMinimumFormatted;
        private Boolean rated;
        private Boolean sip;
        private Boolean oti;
        private double sipMinimumInvestment;
        private String sipMinimumInvestmentFormatted;
        private double sipMultiplesOfInvestment;
        private String sipMultiplesOfInvestmentFormatted;
        private double oneYearReturns;
        private double threeYearReturns;
        private double fiveYearReturns;
        private String risk;
        private String riskId;
        private Boolean watchlist;
        private String exitLoad;
        private String benchmark;
        private String fundType;
        private String[] dividendOptions;
        private String option;
        private String ineligibleCountries;
        private String closeDate;
        private String maturityDate;
        private String tenure;
        private String schemeType;
        private int typeCode;
        private int minimumSipTenure;
        private String minimumSipTenureUnit;
        private int[] sipDates;
        private Boolean nri;
        private Boolean nfo;
        private Boolean superSavings;
        private double minSwitchOutAmount;
        private String minSwitchOutAmountFormatted;
        private double minSwitchOutMultiplesAmount;

    }

}

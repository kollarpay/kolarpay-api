package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PowerSTPSchemeBO {

    private String holdingProfileId;
    private String folio;
    private String goalId; //portfolioId
    private String fromSchemeCode;
    private String toSchemeCode;
    private double totalAmount;
    private double monthlyAmount;
    private String frequency;
    private String dividendOption;
    private int stpDate;
    private String stpId;
    private String toSchemeName;
    private String fromSchemeName;
    private String goalName;
    private String holdingProfileName;
    private List<String> actions;

}

package com.kp.core.app.model.request;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Getter
@Setter
public class PaymentInitiateRequestBO {
    private String paymentId;
    private String customerId;
    private String userBankId;
    private String sid;
    private String productType;
    private String mode;
    private String subMode;
    private BigInteger amount;
    private String vpa;
    private int userId;
    private String bankCode;
    private String callbackUrl;
}

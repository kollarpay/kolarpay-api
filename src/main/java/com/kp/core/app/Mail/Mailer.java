package com.kp.core.app.Mail;

import com.kp.core.app.config.SendMailConfiguration;
import com.kp.core.app.model.PrivateLabelBO;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

@Slf4j
@Component
public class Mailer {

    SendMailConfiguration configuration = ConfigFactory.create(SendMailConfiguration.class);

    public boolean sendMail(PrivateLabelBO privateLabelBO, String toMail, HashMap<String, String> hashMap, String mailerCategory, String subjectText) {

        Boolean mailFlag = false;

        Email from = new Email(privateLabelBO.getMailFrom(), privateLabelBO.getLabelShortName());
        String subject = subjectText;
        Email to = new Email(toMail);
        Content content = new Content("text/html", "Hi");
        Mail mail = new Mail(from, subject, to, content);
        mail = genericSubstution(hashMap, mail);

        // Set mailer category
        log.debug("MailerCategory:" + mailerCategory+" |tomail: "+toMail + "| MailFrom: " + privateLabelBO.getMailFrom() + "| LabelShortName: " + privateLabelBO.getLabelShortName() + "| PLID: " + privateLabelBO.getPLId());
        if (StringUtils.isNotEmpty(mailerCategory))
            mail.addCategory(mailerCategory); // Categories

        SendGrid sg = null;
        Request request = new Request();
        try {
            String sendGridKey = "";
            if (privateLabelBO.getPLId() > 99) {
                sendGridKey = configuration.partnerSendgridApiKey();
            } else {
                sendGridKey = configuration.sendgridApiKey();
            }

            sg = new SendGrid(sendGridKey);
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            log.info("Mailer Response :" + response.getBody());
            if (response.getStatusCode() == 200 || response.getStatusCode() == 202 ) {
                log.info("Common_Mailer_Response: "+response.getBody() + " :: " + response.getHeaders());
                mailFlag = true;
            }
        } catch (IOException ex) {
            log.error("Mail Exception==" + ex);
        } catch (Exception ex) {
            log.error("SendGrid Mail Exception==", ex);
        }
        return mailFlag;
    }

    public static Mail genericSubstution(HashMap<String, String> hashMap, Mail mail) {
        hashMap.forEach((k, v) -> {
            if (k.equalsIgnoreCase("-templateId-")) {
                mail.setTemplateId(v);
            }
            else if (k.equalsIgnoreCase("-nomineeCc-")) {
                mail.personalization.get(0).addCc(new Email(v));
            } else {
                mail.personalization.get(0).addSubstitution(k, v);
            }
        });

        return mail;
    }

}

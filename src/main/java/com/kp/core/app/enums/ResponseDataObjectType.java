package com.kp.core.app.enums;

public enum ResponseDataObjectType {
    object, array, list
}

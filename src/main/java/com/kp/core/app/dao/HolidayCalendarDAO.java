package com.kp.core.app.dao;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class HolidayCalendarDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int weekDay() {

        int weekDay = 0;

        try {

            String timeSql = "select datepart(weekday,DATEADD(MI, 330, GETDATE())) wd";
            weekDay = jdbcTemplate.queryForObject(timeSql, new Object[]{}, Integer.class);

        } catch (Exception e) {
            log.error("Exception in finding week day : ", e);
        }

        return weekDay;
    }

    public boolean isHoliday() {

        String reason = "";
        boolean isHoliday = false;

        try {

            String timeSql = "select hc.Reason Reason from HolidayCalendar hc where hc.Flag='A' and convert(varchar(10), hc.DateOfHoliday,101)=convert(varchar(10), DATEADD(MI, 330, GETDATE()),101)";
            reason = jdbcTemplate.queryForObject(timeSql, new Object[]{}, String.class);

            if(StringUtils.isEmpty(reason)) {
                isHoliday = true;
                log.debug("Holiday Reason : " + reason);
            }

        } catch (Exception e) {
            log.error("Exception in finding isHoliday : ", e);
        }

        return isHoliday;
    }
}

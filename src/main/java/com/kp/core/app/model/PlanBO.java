package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kp.core.app.model.request.Message;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanBO {
    @Expose
    private String id;
    @SerializedName("plan_id")
    @Expose
    private String planId;

    @SerializedName("planname")
    @Expose
    private String planName;

    @SerializedName("description")
    @Expose
    private String planDescription;

    @SerializedName("plantype")
    @Expose
    private String planType;

    @Expose
    private IdDesc planTypeIdDesc;

    @SerializedName("startdate")
    @Expose
    private String startDate;

    @SerializedName("enddate")
    @Expose
    private String endDate;

    @SerializedName("maxcycle")
    @Expose
    private int maxCycle;

    @SerializedName("amount")
    @Expose
    private double amount;

    @SerializedName("maxamount")
    @Expose
    private double MaxAmount;

    @SerializedName("intervaltype")
    @Expose
    private int intervalType;

    @SerializedName("intervals")
    @Expose
    private int intervals;

    @SerializedName("position")
    @Expose
    private int position;

    @SerializedName("active")
    @Expose
    private int active;

    @SerializedName("remarks")
    @Expose
    private String remarks;

    @SerializedName("created_user")
    @Expose
    private String created_user;

    @SerializedName("updated_user")
    @Expose
    private String updated_user;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("updated_at")
    @Expose
    private String updated_at;

    @SerializedName("expires_at")
    @Expose
    private String expires_at;
    @Expose
    private Message message;
}


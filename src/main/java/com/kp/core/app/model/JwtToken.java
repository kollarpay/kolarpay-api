package com.kp.core.app.model;

public interface JwtToken {
    String getToken();
}

package com.kp.core.app.dao;


import com.google.gson.Gson;
import com.kp.core.app.model.FrontPageBo;
import com.kp.core.app.model.FrontPageProductBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.thymeleaf.util.StringUtils;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Repository

public class FrontPageDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;
    @Autowired
    Gson gson;


    public FrontPageBo insertFrontPageCollection(FrontPageBo frontPageBo, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert to FrontPage");


        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery = " INSERT INTO frontpage (bpid, collection_name, title, " +
                    " body_html,background, width,height,max_items,shape,collection_type,position,published_scope,created_user,updated_user,status) " +
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?,?)  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[] {"collection_id"});

                ps.setInt(1, frontPageBo.getBpid());
                ps.setString(2, frontPageBo.getCollectionName());
                ps.setString(3, frontPageBo.getTitle());
                ps.setString(4, frontPageBo.getBodyHTML());
                ps.setString(5, frontPageBo.getBackground());
                ps.setDouble(6, frontPageBo.getWidth());
                ps.setDouble(7, frontPageBo.getHeight());
                ps.setInt(8, frontPageBo.getMaxItems());
                ps.setString(9, frontPageBo.getShape());
                ps.setString(10, frontPageBo.getCollectionType());
                ps.setInt(11, frontPageBo.getPosition());
                ps.setString(12, frontPageBo.getPublishedScope());
                ps.setInt(13, userId);
                ps.setInt(14, userId);
                ps.setInt(15, frontPageBo.getStatus());
                return ps;

            }, keyHolder);
            long collection_id=keyHolder.getKey().longValue();

            if(collection_id > 0) {
                frontPageBo.setCollectionID(collection_id);
            }
        } catch (Exception e) {
            log.error(logPrefix + "Exception in frontPage  insertion: ", e);
        }

        log.debug(logPrefix + "front page collectionId : " + frontPageBo.getCollectionID());
        return frontPageBo;
    }
    public FrontPageProductBo insertFrontPageProducts(FrontPageProductBo frontPageProductBo, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert to FrontPage");


        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery = " INSERT INTO frontpage_products (bpid,collection_id,product_name, title, " +
                    " body_html,background, width,height,product_type,position,published_scope,created_user,updated_user,status,tags) " +
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?,?,?)  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[] {"product_id"});

                ps.setInt(1, frontPageProductBo.getBpid());
                ps.setLong(2, frontPageProductBo.getCollectionID());
                ps.setString(3, frontPageProductBo.getProductName());
                ps.setString(4, frontPageProductBo.getTitle());
                ps.setString(5, frontPageProductBo.getBodyHTML());
                ps.setString(6, frontPageProductBo.getBackground());
                ps.setDouble(7, frontPageProductBo.getWidth());
                ps.setDouble(8, frontPageProductBo.getHeight());
                ps.setString(9, frontPageProductBo.getProductType());
                ps.setInt(10, frontPageProductBo.getPosition());
                ps.setString(11, frontPageProductBo.getPublishedScope());
                ps.setInt(12, userId);
                ps.setInt(13, userId);
                ps.setInt(14, frontPageProductBo.getStatus());
                String[] tags =frontPageProductBo.getTags();
                Array arrayTags = connection.createArrayOf("text", tags);
                ps.setArray(15, arrayTags);
                return ps;

            }, keyHolder);
            long product_id=keyHolder.getKey().longValue();

            if(product_id > 0) {
                frontPageProductBo.setProductId(product_id);
            }
        } catch (Exception e) {
            log.error(logPrefix + "Exception in frontPage  insertion: ", e);
        }

        log.debug(logPrefix + "front page collectionId : " + frontPageProductBo.getCollectionID());
        return frontPageProductBo;
    }

    public List<FrontPageBo> getFrontPageCollectionList(int userId,int bpid){

        String logPrefix = "#UserId : " + userId;
        List<FrontPageBo> frontPagecollectionBOList=new ArrayList<>();


        try {
            String sqlQuery = " select id,bpid,collection_id, collection_name, title,body_html,background, width,height,max_items,collection_type,position,published_scope,status,shape from frontpage where bpid="+bpid;

                    frontPagecollectionBOList = jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {
                        FrontPageBo frontPageBo = new FrontPageBo();
                    long id = (rs.getLong("id")>0) ? rs.getLong("id"): 0;
                    long collection_id = (rs.getLong("collection_id")>0) ? rs.getLong("collection_id") : 0;
                    String collection_name = StringUtils.isEmpty(rs.getString("collection_name")) ?  "":rs.getString("collection_name").trim();
                    String title = StringUtils.isEmpty(rs.getString("title")) ? "":rs.getString("title").trim() ;
                    String body_html = StringUtils.isEmpty(rs.getString("body_html")) ? "":rs.getString("body_html").trim() ;
                    String background = StringUtils.isEmpty(rs.getString("background")) ? "":rs.getString("background").trim() ;
                    int width = (rs.getInt("width")>0) ? rs.getInt("width"): 0;
                    int height = (rs.getInt("height")>0) ? rs.getInt("height"): 0;
                    int max_items = (rs.getInt("max_items")>0) ? rs.getInt("max_items"): 0;
                    String collection_type = (rs.getString("collection_type") != null) ? rs.getString("collection_type").trim() : "";
                    int position = (rs.getInt("position")>0) ? rs.getInt("position"): 0;
                    String published_scope = (rs.getString("published_scope") != null) ? rs.getString("published_scope").trim() : "";
                    int status = (rs.getInt("status")>0) ? rs.getInt("status"): 0;
                    String shape = (rs.getString("shape") != null) ? rs.getString("shape").trim() : "";



                        frontPageBo.setId(id);
                        frontPageBo.setBpid(bpid);
                        frontPageBo.setCollectionID(collection_id);
                        frontPageBo.setCollectionName(collection_name);
                        frontPageBo.setTitle(title);
                        frontPageBo.setHeight(height);
                        frontPageBo.setWidth(width);
                        frontPageBo.setMaxItems(max_items);
                        frontPageBo.setBackground(background);
                        frontPageBo.setCollectionType(collection_type);
                        frontPageBo.setBodyHTML(body_html);
                        frontPageBo.setPublishedScope(published_scope);
                        frontPageBo.setPosition(position);
                        frontPageBo.setStatus(status);
                        frontPageBo.setShape(shape);
                        List<FrontPageProductBo> frontPageProductBoList= getFrontPageProductList(userId,bpid,collection_id);
                        if(frontPageProductBoList.size()>0) {
                            frontPageBo.setFrontPageProducts(frontPageProductBoList);
                        }

                return frontPageBo;
            });

        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }

        return frontPagecollectionBOList;
    }

    public List<FrontPageProductBo> getFrontPageProductList(int userId,int bpid,long collectionId){

        String logPrefix = "#UserId : " + userId;
        List<FrontPageProductBo> frontPageProductBOList=new ArrayList<>();
        String conditionSql="";
        if(collectionId>0){
            conditionSql=" AND collection_id="+collectionId;
        }

        try {
            String sqlQuery = " select product_id,bpid,collection_id, product_name, title,body_html,background, width,height,product_type,position,published_scope,status,tags,img_url,product_url from frontpage_products where bpid="+bpid+conditionSql;

            frontPageProductBOList = jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {
                FrontPageProductBo frontPageProductBo = new FrontPageProductBo();
                long product_id = (rs.getLong("product_id")>0) ? rs.getLong("product_id"): 0;
                long collection_id = (rs.getLong("collection_id")>0) ? rs.getLong("collection_id") : 0;
                String product_name = StringUtils.isEmpty(rs.getString("product_name")) ?  "":rs.getString("product_name").trim();
                String title = StringUtils.isEmpty(rs.getString("title")) ? "":rs.getString("title").trim() ;
                String body_html = StringUtils.isEmpty(rs.getString("body_html")) ? "":rs.getString("body_html").trim() ;
                String background = StringUtils.isEmpty(rs.getString("background")) ? "":rs.getString("background").trim() ;
                int width = (rs.getInt("width")>0) ? rs.getInt("width"): 0;
                int height = (rs.getInt("height")>0) ? rs.getInt("height"): 0;
                String product_type = (rs.getString("product_type") != null) ? rs.getString("product_type").trim() : "";
                int position = (rs.getInt("position")>0) ? rs.getInt("position"): 0;
                String published_scope = (rs.getString("published_scope") != null) ? rs.getString("published_scope").trim() : "";
                int status = (rs.getInt("status")>0) ? rs.getInt("status"): 0;
                Array tagsArr = rs.getArray("tags");
                String[] strTags={};
                if(tagsArr!=null){
                    strTags = (String[]) tagsArr.getArray();
                }
                String img_url = StringUtils.isEmpty(rs.getString("img_url")) ? "":rs.getString("img_url").trim() ;
                String product_url = StringUtils.isEmpty(rs.getString("product_url")) ? "":rs.getString("product_url").trim() ;

                frontPageProductBo.setProductId(product_id);
                frontPageProductBo.setBpid(bpid);
                frontPageProductBo.setCollectionID(collection_id);
                frontPageProductBo.setProductName(product_name);
                frontPageProductBo.setTitle(title);
                frontPageProductBo.setHeight(height);
                frontPageProductBo.setWidth(width);
                frontPageProductBo.setBackground(background);
                frontPageProductBo.setProductType(product_type);
                frontPageProductBo.setBodyHTML(body_html);
                frontPageProductBo.setPublishedScope(published_scope);
                frontPageProductBo.setPosition(position);
                frontPageProductBo.setStatus(status);
                frontPageProductBo.setTags(strTags);
                frontPageProductBo.setImgURL(img_url);
                frontPageProductBo.setProductURL(product_url);


                return frontPageProductBo;
            });

        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }

        return frontPageProductBOList;
    }



}

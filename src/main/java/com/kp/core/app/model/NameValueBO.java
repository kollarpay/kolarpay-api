package com.kp.core.app.model;

public class NameValueBO {

    private String value = "";
    private String name;

    public NameValueBO(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public NameValueBO() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

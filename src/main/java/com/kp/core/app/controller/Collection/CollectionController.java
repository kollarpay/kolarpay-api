package com.kp.core.app.controller.Collection;

import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.CollectionBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.service.Collection.CollectionService;
import com.kp.core.app.utils.APIGeneralUtil;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;




@RestController
@Slf4j
@RequestMapping("/collections")
public class CollectionController {

    @Autowired
    CollectionService collectionService;

    @Autowired
    Gson gson;



    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createCollection(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#collection create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "collection");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CollectionBO collectionBO = gson.fromJson(payload, CollectionBO.class);
                globalResponseBO = collectionService.createCollection(collectionBO, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#collection create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> getAllCollection(@RequestParam("tags") String tags,@RequestHeader MultiValueMap<String, Object> headers) throws Exception {

        log.debug("#get all publish collection POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "collections");
        String strTags=StringUtils.isEmpty(tags)?"":tags;

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = collectionService.collections(userId,strTags);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#collection create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
//    @GetMapping(path = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<GlobalResponseBO> getAllCollectionCategory(@RequestParam("categoryType") String categoryType,@RequestHeader MultiValueMap<String, Object> headers) throws Exception {
//
//        log.debug("#get all publish collection POST API Request");
//        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "collections-category");
//
//        int userId = 0;
//        try {
//            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
//            log.debug("#UserId : " + userId);
//
//            if(userId > 0) {
//
//                globalResponseBO = collectionService.collectionsCategory(userId,tags);
//
//            }
//
//        } catch (Exception e) {
//            log.error("#UserId : " + userId + "| Exception : ", e);
//        }
//        log.debug("#collection create POST API Response : " + globalResponseBO.toString());
//        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
//    }
}
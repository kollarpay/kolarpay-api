package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/brospay/config/properties/auth_api.properties")
@Config.HotReload(unit = TimeUnit.MINUTES, value = 20)

public interface JWTSettings extends Reloadable {

    @Config.Key("tokenExpirationTime")
    @DefaultValue("36000")
    Integer tokenExpirationTime();

    @DefaultValue("brospay.com")
    @Config.Key("tokenIssuer")
    String tokenIssuer();

    @DefaultValue("9J7IrpnNPq8z8MQTzjJwVaQyi6izICKnipN2ugPHc3Y1JI5QmeBqiY6F9mYXLh7lTVRZtIjtJbdUC0oqYavZvMwZya2L7ceYlGyk")
    @Config.Key("tokenSigningKey")
    String tokenSigningKey();

    @DefaultValue("42000")
    @Config.Key("refreshTokenExpTime")
    Integer refreshTokenExpTime();

}

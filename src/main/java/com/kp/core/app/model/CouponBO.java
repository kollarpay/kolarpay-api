package com.kp.core.app.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class CouponBO {
    @SerializedName("id")
    @Expose
    public Long id;
    @SerializedName("product_id")
    @Expose
    public Long productId;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("bodyHtml")
    @Expose
    public String bodyHtml;
    @SerializedName("productType")
    @Expose
    private String productType;
    @SerializedName("handle")
    @Expose
    private String handle;
    @SerializedName("publishedCcope")
    @Expose
    private String published_scope;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("productOptions")
    @Expose
    private String product_options;
    @SerializedName("productImages")
    @Expose
    private String product_images;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("position")
    @Expose
    private int position;
    @SerializedName("stockUnits")
    @Expose
    private int stock_units;
    @SerializedName("categoryId")
    @Expose
    private long category_id;
    @SerializedName("rewardPointsCredit")
    @Expose
    private long reward_points_credit;
}

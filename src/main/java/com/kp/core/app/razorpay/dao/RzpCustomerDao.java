package com.kp.core.app.razorpay.dao;

import com.google.gson.Gson;
import com.kp.core.app.config.GeneralConfig;
import com.kp.core.app.dao.AutoNumberGenerator;
import com.kp.core.app.model.BankInfoBO;
import com.kp.core.app.model.CustomerBankInfo;
import com.kp.core.app.model.CustomerInfoBO;
import com.kp.core.app.model.CustomerRequest;
import com.kp.core.app.razorpay.enums.AmountUnitEnum;
import com.kp.core.app.razorpay.model.*;
import com.kp.core.app.utils.BPConstants;
import com.kp.core.app.utils.EncryptionDecryption;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
public class RzpCustomerDao {


    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private AutoNumberGenerator autoNumberGenerator;
    @Autowired
    private EncryptionDecryption encryptionDecryption;
    private GeneralConfig generalConfig = ConfigFactory.create(GeneralConfig.class);
    @Autowired
    Gson gson;

    public String saveCustomer(CustomerRequest customerRequest) {

        String logPrefix = "#customerId :  ";

        log.debug(logPrefix + "Insert customer basic detail");
        String customerId="";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();

            String sqlQuery = " INSERT INTO customerbasicdetail\n" +
                    "(name,email,mobilenumber,userid,created_user,updated_user, allow_trans, mailflag, flag, customerlimit,dob)  " +
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, customerRequest.getName());
                ps.setString(2, customerRequest.getEmailId());
                ps.setString(3, customerRequest.getMobileNumber());
                ps.setInt(4, customerRequest.getUserId());
                ps.setInt(5, customerRequest.getUserId());
                ps.setInt(6, customerRequest.getUserId());
                ps.setString(7, "A");
                ps.setString(8, "A");
                ps.setString(9, "A");
                ps.setInt(10, -1);
                ps.setDate(11, Date.valueOf(customerRequest.getDob()));

                return ps;

            }, keyHolder);

            if (keyHolder.getKeys().size() > 1) {
                customerId = String.valueOf(keyHolder.getKeys().get("customerid"));
                if(!StringUtils.isEmpty(customerId)){
                    customerRequest.setCustomerId(customerId);
                }
            }


        } catch (Exception e) {
            log.error(logPrefix + "Exception in customer create insertion: ", e);
        }

        log.debug(logPrefix + "customerId : " + customerId);
        return customerId;
    }
    public Map<String,String> getRzCustomer(String bpReferenceId, String product, String paymentMode) {

        String logPrefix = "#bpReference id : " + bpReferenceId;

        log.debug(logPrefix + "razorpay_customer_mapping details fetch");
        Map<String, String> rzpCustIdEmail = new HashMap<>();


        try {
            if(StringUtils.isEmpty(paymentMode) || (paymentMode.equalsIgnoreCase("UPI") && !generalConfig.upiMidEnable()))
                paymentMode = "NEFT";
            String sqlQuery = " SELECT razorpay_customer_id,bz_email FROM razorpay_customer_mapping WHERE bz_reference_id=?  AND product_type=? AND PaymentMode = ? and flag='A' ";

            jdbcTemplate.query(sqlQuery, new Object[]{bpReferenceId,product,paymentMode}, (rs, rowNumber) -> {
                rzpCustIdEmail.put("rzpCustomerId", rs.getString("razorpay_customer_id"));
                rzpCustIdEmail.put("emailId", rs.getString("bz_email"));
                return 1;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in getRzCustomer : ", e);
        }

        return rzpCustIdEmail;
    }
    public CustomerInfoBO getCustomerByCustomerId(String customerId) {

        String logPrefix = "#customer email : " + customerId;

        log.debug(logPrefix + "customerBasic details fetch");

        CustomerInfoBO customerInfoBO = new CustomerInfoBO();

        try {

            String sqlQuery = " select u.userid,c.customerid,c.firstname,c.mobilenumber,c.email,c.allow_trans,c.customerlimit,c.dob from userinfo u join customerbasicdetail c on u.userid=c.userid " +
                    " where c.customerid=? ";

            jdbcTemplate.query(sqlQuery, new Object[]{customerId}, (rs, rowNumber) -> {
                String allowTransact = StringUtils.isNotEmpty(rs.getString("allow_trans"))? rs.getString("allow_trans").trim() : "";
                int customerLimit = rs.getInt("customerlimit");
                customerInfoBO.setName(rs.getString("firstname"));
                customerInfoBO.setCustomerId(rs.getString("customerid"));
                customerInfoBO.setEmail(rs.getString("email"));
                customerInfoBO.setMobile(rs.getString("mobilenumber"));
                customerInfoBO.setActivated(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && customerLimit!=49999);
                customerInfoBO.setDateOfBirth(rs.getString("dob"));
                List<BankInfoBO> bankInfoBOList=getCustomerBankByCustomerId(customerId);
                customerInfoBO.setBanks(bankInfoBOList);
                return customerInfoBO;
            });



        } catch (Exception e) {
            log.error(logPrefix + "Exception in customerBasic details fetch : ", e);
        }

        log.debug("customerBasic customerId : " + customerId+"::name"+customerInfoBO.getName());

        return customerInfoBO;
    }
    public List<BankInfoBO> getCustomerBankByCustomerId(String customerId) {

        String logPrefix = "#customer bankinfo : " + customerId;

        log.debug(logPrefix + "customerBank details fetch");

        List<BankInfoBO> bankInfoBOList=new ArrayList<>();

        try {

            String sqlQuery = "select customerid,banklookupid,bankname,bankcity,accountnumber,ifsccode,bvrefid,verified,active  from customerbankinfo  where customerid=? ";

            bankInfoBOList= jdbcTemplate.query(sqlQuery, new Object[]{customerId}, (rs, rowNumber) -> {
                BankInfoBO bankInfoBO=new BankInfoBO();
                bankInfoBO.setCustomerId(rs.getString("customerid"));
                bankInfoBO.setBankLookUpId(rs.getString("banklookupid"));
                bankInfoBO.setBankName(rs.getString("bankname"));
                bankInfoBO.setAccountNo(rs.getString("accountnumber"));
                bankInfoBO.setIfsc(rs.getString("ifsccode"));
                return bankInfoBO;
            });


        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        log.debug("customerBank size : " + bankInfoBOList.size());

        return bankInfoBOList;
    }
    public CustomerInfoBO getCustomerInfo(String email, String mobilenumber) {

        String logPrefix = "#customer email : " + email + " | mobilenumber"+mobilenumber;

        log.debug(logPrefix + "customerBasic details fetch");

        CustomerInfoBO customerInfoBO = new CustomerInfoBO();

        try {

            String sqlQuery = " select u.userid,c.customerid,c.firstname,c.mobilenumber,c.email,c.allow_trans,c.customerlimit,c.dob from userinfo u join customerbasicdetail c on u.userid=c.userid " +
                    " where c.email=? and c.mobilenumber=? ";

            jdbcTemplate.query(sqlQuery, new Object[]{email,mobilenumber}, (rs, rowNumber) -> {
                String allowTransact = StringUtils.isNotEmpty(rs.getString("allow_trans"))? rs.getString("allow_trans").trim() : "";
                int customerLimit = rs.getInt("customerlimit");
                customerInfoBO.setName(rs.getString("firstname"));
                customerInfoBO.setCustomerId(rs.getString("customerid"));
                customerInfoBO.setEmail(rs.getString("email"));
                customerInfoBO.setMobile(rs.getString("mobilenumber"));
                customerInfoBO.setActivated(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && customerLimit!=49999);
                customerInfoBO.setDateOfBirth(rs.getString("dob"));
                return customerInfoBO;
            });



        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        log.debug("customerInfo email : " + customerInfoBO.getEmail()+"::name"+customerInfoBO.getName());

        return customerInfoBO;
    }
    public Map<String, RzpBankBO> getRazorPayBanks() throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("Fetch razorPayBanks ").append("|");
        Map<String, RzpBankBO> rzpBankBOHashMap = new HashMap<>();

        try {
            String sqlQuery = " select razorpay_bank_id, razorpay_bank_name, bz_bank_id from razorpay_banks where flag = 'A' ";

            jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {

                RzpBankBO rzpBankBO = new RzpBankBO();
                rzpBankBO.setRazorpayBankId(rs.getString("razorpay_bank_id"));
                rzpBankBO.setRazorpayBankName(rs.getString("razorpay_bank_name"));
                rzpBankBO.setBz_bank_id(rs.getString("bz_bank_id"));
                rzpBankBOHashMap.put(rs.getString("bz_bank_id"), rzpBankBO);

                return rzpBankBOHashMap;

            });

        } catch (EmptyResultDataAccessException e) {
            log.debug("{} | No records", logPrefix);
        } catch (Exception e) {
            log.error("{} | Exception : {} ", logPrefix, e, e);
            throw new Exception(e);
        }

        return rzpBankBOHashMap;
    }
    public int updateBPEmail(String rzpCustomerId, String emailId, int userId) throws Exception {

        StringBuilder logPrefix =  new StringBuilder().append("Update Customer EmailId #rzpCustomerId : ").append(rzpCustomerId).append("#EmailId : ").append(emailId)
                .append(rzpCustomerId).append("#UserId : ").append(userId).append(" | ");
        log.debug(logPrefix.toString());

        int status = 0;
        try {

            String sqlQuery = " update razorpay_customer_mapping set bz_email = ?, updated_user = ?, updated_at = GETDATE() " +
                    " where flag = 'A' and razorpay_customer_id = ? ";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setString(1, emailId);
                ps.setInt(2, userId);
                ps.setString(3, rzpCustomerId);

            });

        } catch (Exception e) {
            log.error("{} | Exception : {} ", logPrefix, e, e);
            throw new Exception(e);
        }

        return status;
    }
    public int insertRazorPayCustomerMapping(RzpCustomerCreationRequestBO customerCreationRequestBO, String customerId, String product, String paymentMode, int userId) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("razorpay_customer mapping Insert | #contact : ").append(customerCreationRequestBO.getContact())
                .append(" | #Email : ").append(customerCreationRequestBO.getEmail()).append(" | #customerId : ").append(customerId).append(" | #Product : ").append(product)
                .append(" | #PaymentMode : ").append(paymentMode).append(" | ");
        log.debug(logPrefix.toString());

        int status;

        try {

            if(StringUtils.isEmpty(paymentMode) || (paymentMode.equalsIgnoreCase("UPI") && !generalConfig.upiMidEnable()))
                paymentMode = "NEFT";

            String existQuery = "SELECT COUNT(bz_reference_id) FROM razorpay_customer_mapping WHERE bz_reference_id = ? AND product_type = ? AND PaymentMode = ? AND flag='A' ";

            int count = jdbcTemplate.queryForObject(existQuery, new Object[]{customerCreationRequestBO.getContact(), product, paymentMode}, Integer.class);

            if(count==0) {

                String sqlQuery = " insert into razorpay_customer_mapping  " +
                        " (bz_reference_id, reference_id, product_type, bz_email, bz_reference_type, mobile_number, bp_id, created_user, modified_user, flag, created_at, updated_at, PaymentMode) " +
                        " values (?, ?, ?, ?, ?, ?, ?, ?, ?, 'A', GETDATE(), GETDATE(), ?)";

                String finalPaymentMode = paymentMode;

                status = jdbcTemplate.update(sqlQuery, ps -> {

                    ps.setString(1, customerCreationRequestBO.getContact());
                    ps.setString(2, customerId);
                    ps.setString(3, product);
                    ps.setString(4, customerCreationRequestBO.getEmail());
                    ps.setString(5, "customerId");
                    ps.setString(6, customerCreationRequestBO.getNotes().getMobile());
                    ps.setString(7, customerCreationRequestBO.getNotes().getLabelId());
                    ps.setInt(8, userId);
                    ps.setInt(9, userId);
                    ps.setString(10, finalPaymentMode);

                });
                log.info("{} Status : {} ", logPrefix, status);
            }
            else {
                status = count;
            }
        } catch (Exception e) {
            log.error("{} Exception : {}", logPrefix, e, e);
            throw new Exception(e);
        }

        return status;
    }
    public int updateRazorPayCustomerMapping(String referenceId, String email, String product, String paymentMode, RzpCustomerCreationResponseBO customerCreationResponseBO, int userId) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("razorpay_customer mapping Update | #referenceId : ").append(referenceId).append("| Email : ").append(email).append(" | RzpCustomerId :  " + customerCreationResponseBO.getId()).append(" | ");
        log.debug(logPrefix.toString());

        int status;

        try {

            if(StringUtils.isEmpty(paymentMode) || (paymentMode.equalsIgnoreCase("UPI") && !generalConfig.upiMidEnable()))
                paymentMode = "NEFT";

            String sqlQuery = " Update razorpay_customer_mapping set razorpay_customer_id = ? , razorpay_customer_name = ?, modified_user = ?, updated_at = GETDATE() " +
                    " Where bz_reference_id = ? and product_type = ? and bz_email = ? and PaymentMode = ? ";

            String finalPaymentMode = paymentMode;

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setString(1, customerCreationResponseBO.getId());
                ps.setString(2, customerCreationResponseBO.getName());
                ps.setInt(3, userId);
                ps.setString(4, referenceId);
                ps.setString(5, product);
                ps.setString(6, email);
                ps.setString(7, finalPaymentMode);

            });
            log.info("{} Status : {} ", logPrefix, status);
        } catch (Exception e) {
            log.error("{} Exception : {}", logPrefix, e, e);
            throw new Exception(e);
        }

        return status;
    }
    public RzpCustomerCreationResponseBO getRzpCustomerMappingResponse(String bpReferenceId, String emailId, String product, String paymentMode) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("Fetch RzpCustomer response | #bpReferenceId : ").append(bpReferenceId).append(" | #EmailId : ").append(emailId)
                .append(" | #Product : ").append(product).append(" | #PaymentMode : ").append(paymentMode).append("|");
        RzpCustomerCreationResponseBO customerCreationResponseBO = new RzpCustomerCreationResponseBO();

        try {
            String sqlQuery = " SELECT bz_email, razorpay_customer_id, bz_reference_type, bz_mobile, bp_id, razorpay_customer_name  " +
                    " FROM razorpay_customer_mapping" +
                    " WHERE flag = 'A' and  bz_reference_id = ? AND bz_email = ? and product_type = ? and PaymentMode = ? ";

            customerCreationResponseBO = jdbcTemplate.queryForObject(sqlQuery, new Object[]{bpReferenceId, emailId, product, paymentMode}, (rs, rowNumber) -> {

                RzpCustomerCreationResponseBO customerCreationResponseBOtemp = new RzpCustomerCreationResponseBO();
                RzpCustomerCreationResponseBO.CustomerCreationNotesBO notesBO   =   new RzpCustomerCreationResponseBO.CustomerCreationNotesBO();
                customerCreationResponseBOtemp.setId(rs.getString("razorpay_customer_id"));
                customerCreationResponseBOtemp.setEmail(emailId);
                customerCreationResponseBOtemp.setName(rs.getString("razorpay_customer_name"));
                notesBO.setUser_mobile(rs.getString("bz_mobile"));
                notesBO.setUser_bp_id(rs.getString("bp_id"));
                customerCreationResponseBOtemp.setNotes(notesBO);

                return customerCreationResponseBOtemp;
            });

        } catch (EmptyResultDataAccessException e) {
            log.debug("{} | No records", logPrefix);
        } catch (Exception e) {
            log.error("{} | Exception : {} ", logPrefix, e, e);
            throw new Exception(e);
        }

        log.debug("{} #CreationResponseBO : {}", logPrefix, gson.toJson(customerCreationResponseBO));
        return customerCreationResponseBO;
    }
    public Map<String, String> getRzpVirtualAccountId(String rzpCustomerId, String product) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("Fetch Razorpay VirtualId Id | #rzpCustomerId : ").append(rzpCustomerId).append("|");
        Map<String, String> rzpVirtualAccountIdMap = new HashMap<>();

        try {

            String sqlQuery = " select razorpay_virtual_account_id, tpv_enabled from razorpay_customer_virtual_account_mapping " +
                    " where flag = 'A' and razorpay_customer_id = ? and product_type = ? and tpv_enabled = '1' ";

            rzpVirtualAccountIdMap  =   jdbcTemplate.query(sqlQuery, new Object[]{rzpCustomerId, product} , new ResultSetExtractor<Map>(){
                @Override
                public Map extractData(ResultSet rs) throws SQLException, DataAccessException {
                    HashMap<String,String> mapRet= new HashMap<String,String>();
                    while(rs.next()){
                        mapRet.put(rs.getString("razorpay_virtual_account_id"),rs.getString("tpv_enabled"));
                    }
                    return mapRet;
                }
            });
        } catch (EmptyResultDataAccessException e) {
            log.debug("{} | No records", logPrefix);
        } catch (Exception e) {
            log.error("{} | Exception : {} ", logPrefix, e, e);
            throw new Exception(e);
        }

        log.debug("{} #rzpVirtualId size : {}", logPrefix, rzpVirtualAccountIdMap.size());
        return rzpVirtualAccountIdMap;
    }
    public List<RzpBankBO> getVirtualBankDetails(String rzpVirtualAccountId) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("Fetch Created Virtual bank details ").append("|");
        List<RzpBankBO> rzpBankBOList   =   new ArrayList<>();

        try {
            String sqlQuery = " select bz_bank_id, bz_bank_account_no, bz_bank_ifsc  from razorpay_virtual_accounts where flag = 'A' and razorpay_virtual_account_id = ? ";

            jdbcTemplate.query(sqlQuery, new Object[]{rzpVirtualAccountId}, (rs, rowNumber) -> {

                RzpBankBO rzpBankBO = new RzpBankBO();
                rzpBankBO.setBz_bank_id(rs.getString("bz_bank_id"));
                rzpBankBO.setBz_bank_account_no(rs.getString("bz_bank_account_no"));
                rzpBankBO.setBz_bank_ifsc(rs.getString("bz_bank_ifsc"));
                rzpBankBOList.add(rzpBankBO);

                return rzpBankBOList;

            });

        } catch (EmptyResultDataAccessException e) {
            log.debug("{} | No records", logPrefix);
        } catch (Exception e) {
            log.error("{} | Exception : {} ", logPrefix, e, e);
            throw new Exception(e);
        }

        log.debug("{} #BankBOResponseBO : {}", logPrefix, gson.toJson(rzpBankBOList));
        return rzpBankBOList;
    }
    public int getUserId(String paymentId) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("Fetch UserId | #PaymentId : ").append(paymentId).append("|");
        int userId = 0;

        try {

            String sqlQuery = " SELECT DISTINCT UserId FROM PAYMENT_USER_DETAILS WHERE PaymentId = ? AND Active = 1 ";

            userId = jdbcTemplate.queryForObject(sqlQuery, new Object[]{paymentId}, Integer.class);

        } catch (EmptyResultDataAccessException e) {
            log.debug("{} | No records", logPrefix);
        } catch (Exception e) {
            log.error("{} | Exception : {} ", logPrefix, e, e);
            throw new Exception(e);
        }

        log.debug("{} #UserId : {}", logPrefix, userId);
        return userId;
    }
    public int insertRazorPayOrderHistory(RzpOrderCreationResponseBO orderCreationResponseBO, int userId) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("razorpay_order_history Insert | #rzpOrderId : ").append(orderCreationResponseBO.getId()).append("|");
        log.debug(logPrefix.toString());

        int status;

        try {

            String sqlQuery = " INSERT INTO razorpay_order_history (razorpay_order_id, transaction_amount, " +
                    //" bank_account_no, ifsc, bank_name, customer_name," +
                    " created_user, created_at, updated_user, " +
                    " updated_at, flag)" +
                    " VALUES (?, ?, ?," +
                    //" ?, ?, ?, ?," +
                    " GETDATE()," +
                    " ?, GETDATE(), ?)";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setString(1, orderCreationResponseBO.getId());
                ps.setString(2, String.valueOf(orderCreationResponseBO.getAmount()));
//                ps.setString(3, bankInfoBO.getAccountNo());
//                ps.setString(4, bankInfoBO.getIfsc());
//                ps.setString(5, bankInfoBO.getBankName());
//                ps.setString(6, bankInfoBO.getAccountHolderName());
                ps.setInt(3, userId);
                ps.setInt(4, userId);
                ps.setString(5, "A");

            });

        } catch (Exception e) {
            log.error("{} Exception : {}", logPrefix, e, e);
            throw new Exception(e);
        }

        return status;
    }
    public int insertRazorPayOrderHistoryWithBank(RzpOrderCreationResponseBO orderCreationResponseBO, BankInfoBO bankInfoBO, int userId) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("razorpay_order_history Insert | #rzpOrderId : ").append(orderCreationResponseBO.getId()).append("|");
        log.debug(logPrefix.toString());

        int status;

        try {

            String sqlQuery = " INSERT INTO razorpay_order_history (razorpay_order_id, transaction_amount, " +
                    " bank_account_no, ifsc, bank_name, customer_name," +
                    " created_user, created_at, updated_user, " +
                    " updated_at, flag)" +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, GETDATE()," +
                    " ?, GETDATE(), ?)";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setString(1, orderCreationResponseBO.getId());
                ps.setString(2, String.valueOf(orderCreationResponseBO.getAmount()));
                ps.setString(3, bankInfoBO.getAccountNo());
                ps.setString(4, bankInfoBO.getIfsc());
                ps.setString(5, bankInfoBO.getBankName());
                ps.setString(6, bankInfoBO.getAccountHolderName());
                ps.setInt(7, userId);
                ps.setInt(8, userId);
                ps.setString(9, "A");

            });

        } catch (Exception e) {
            log.error("{} Exception : {}", logPrefix, e, e);
            throw new Exception(e);
        }

        return status;
    }
    public int checkExistingRecordsForPaymentId(String paymentId, String product) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("Check records in razorpay_payments | #PaymentId : ").append(paymentId).append(" | #product : ").append(product).append(" | ");
        int count = 0;

        try {

            String sqlQuery = " SELECT COUNT(bz_payment_id) FROM razorpay_payments WHERE bz_payment_id = ? AND flag = 'A' AND bz_product_type = ? ";

            count = jdbcTemplate.queryForObject(sqlQuery, new Object[]{paymentId, product}, Integer.class);

        } catch (EmptyResultDataAccessException e) {
            log.debug("{} | No records", logPrefix);
        } catch (Exception e) {
            log.error("{} | Exception : {} ", logPrefix, e, e);
            throw new Exception(e);
        }

        log.debug("{} #count : {}", logPrefix, count);
        return count;
    }
    public int insertRazorPayPayments(RzpInitiatePaymentRequestBO initiatePaymentRequestBO, String rzpPaymentId, String fiPaymentId, String rzpTransactionId, String fiProduct, int userId) throws Exception {

        StringBuilder logPrefix = new StringBuilder().append("razorpay_payments Insert | #rzpPaymentId : ").append(rzpPaymentId).append("|");
        log.debug(logPrefix.toString());

        int status;

        try {

            String sqlQuery = " INSERT INTO razorpay_payments (razorpay_customer_id, razorpay_order_id, razorpay_payment_id," +
                    " bz_payment_id, razorpay_transaction_id, bz_product_type, transaction_amount, amount_unit, razorpay_payment_mode, flag, " +
                    " created_user, created_at, modified_user, modified_at)" +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 'A', ?, DATE()," +
                    " ?, DATE())";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setString(1, initiatePaymentRequestBO.getCustomerId());
                ps.setString(2, initiatePaymentRequestBO.getOrderId());
                ps.setString(3, rzpPaymentId);
                ps.setString(4, fiPaymentId);
                ps.setString(5, rzpTransactionId);
                ps.setString(6, fiProduct);
                ps.setString(7, String.valueOf(initiatePaymentRequestBO.getAmount()));
                ps.setString(8, AmountUnitEnum.paise.name());
                ps.setString(9, initiatePaymentRequestBO.getMethod());
                ps.setInt(10, userId);
                ps.setInt(11, userId);

            });

        } catch (Exception e) {
            log.error("{} razorpay_payments Exception : {}", logPrefix, e, e);
            throw new Exception(e);
        }

        return status;
    }
}

//package com.kp.core.app.repository;
//
//import com.kp.core.app.model.Ifsccodes;
//import org.springframework.data.mongodb.repository.MongoRepository;
//
//import java.util.List;
//
//public interface IfscocdeRepository extends MongoRepository<Ifsccodes,String> {
//  List<Ifsccodes> findIfsccodesByIfsccodeContainingIgnoreCaseOrAddressContainingIgnoreCaseOrCityContainsIgnoreCase(String ifsccode, String address, String city);
//}

package com.kp.core.app.razorpay.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RzpValidateVPARequestBO {

    private String vpa;

    public RzpValidateVPARequestBO(String vpa) {
        this.vpa = vpa;
    }
}

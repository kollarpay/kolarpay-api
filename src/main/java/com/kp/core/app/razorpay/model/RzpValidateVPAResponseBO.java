package com.kp.core.app.razorpay.model;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RzpValidateVPAResponseBO {

    private String vpa;
    private Boolean success;
    @SerializedName("customer_name")
    private String customerName;
}

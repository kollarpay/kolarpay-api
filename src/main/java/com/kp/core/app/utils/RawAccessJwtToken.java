package com.kp.core.app.utils;

import com.kp.core.app.config.JWTSettings;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.function.Function;

@Component
@Slf4j

public class RawAccessJwtToken {

    JWTSettings jwtSettings = ConfigFactory.create(JWTSettings.class);

    public HashMap<String, Object> parseToken(String signingKey, String token) {
        HashMap<String, Object> response = new HashMap<>();
        Boolean invalidToken = Boolean.FALSE;
        Boolean expiredToken = Boolean.FALSE;
        Claims claims=null;
        try {
            claims= extractAllClaims(token);
        } catch (UnsupportedJwtException
                | MalformedJwtException
                | IllegalArgumentException
                | SignatureException ex) {
            log.error("Invalid JWT Token", ex);
            invalidToken = true;
        } catch (ExpiredJwtException expiredEx) {
            log.info("JWT Token is expired", expiredEx);
            expiredToken = true;
        } finally {
            response.put("invalidToken", invalidToken);
            response.put("expiredToken", expiredToken);
            response.put("claims", claims);
        }
        return response;
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(jwtSettings.tokenSigningKey().getBytes())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }


}

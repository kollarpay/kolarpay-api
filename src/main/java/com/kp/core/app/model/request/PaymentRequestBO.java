package com.kp.core.app.model.request;

import com.kp.core.app.model.BankInfoBO;
import com.kp.core.app.model.CustomerBankInfo;
import com.kp.core.app.model.PlanBO;
import com.kp.core.app.model.SubscriptionBO;
import com.kp.core.app.razorpay.model.RzpOrderCreationRequestBO;
import com.kp.core.app.utils.BPConstants;
import io.netty.util.internal.StringUtil;

import java.math.BigInteger;
import java.util.Map;

public class PaymentRequestBO {

    public CustomerDetailsRequestBO customerDetailsRequestBO(CustomerDetailsRequestBO customerDetailsRequestBO,OrderBO orderBO){
        CustomerDetailsRequestBO.CustomerDetails customerDetails=customerDetailsRequestBO.getCustomer_details();
        customerDetails.setCustomer_id(orderBO.getCustomerInfoBO().getCustomerId()+"");
        customerDetails.setCustomerEmail(orderBO.getCustomerInfoBO().getEmail());
        customerDetails.setCustomer_phone(orderBO.getCustomerInfoBO().getMobile());
        customerDetailsRequestBO.setCustomer_details(customerDetails);
        customerDetailsRequestBO.setOrder_amount(orderBO.getAmount()+"");
        customerDetailsRequestBO.setOrder_currency(BPConstants.CURRENCY_INDIA);
        return customerDetailsRequestBO;
    }
    public RzpOrderCreationRequestBO razorpayCreateOrderBO(RzpOrderCreationRequestBO OrderCreateRequestBO, OrderBO orderBO){
        OrderCreateRequestBO.setAmount(orderBO.getAmount().multiply(BigInteger.valueOf(100))); //set amount in paise
        OrderCreateRequestBO.setCurrency(BPConstants.CURRENCY_INDIA);
        OrderCreateRequestBO.setReceipt(orderBO.getPaymentID());
        RzpOrderCreationRequestBO.OrderCreationNotesBO orderCreationNotesBO= new RzpOrderCreationRequestBO.OrderCreationNotesBO();
        orderCreationNotesBO.setPaymentId(orderBO.getPaymentID());
        orderCreationNotesBO.setProductType(orderBO.getProductType());
        OrderCreateRequestBO.setNotes(orderCreationNotesBO);
        return OrderCreateRequestBO;
    }

    public PlanRequestBO planRequestBO(PlanBO planBO,PlanRequestBO planRequestBO){
        planRequestBO.setPlanId(planBO.getPlanId());
        planRequestBO.setPlanName(planBO.getPlanName());
        planRequestBO.setType(planBO.getPlanType());
        planRequestBO.setAmount(planBO.getAmount());
        planRequestBO.setMaxAmount(planBO.getMaxAmount());
        planRequestBO.setIntervalType(planRequestBO.getIntervalType());
        planRequestBO.setMaxCycles(planRequestBO.getMaxCycles());
        return planRequestBO;
    }
    public SubscriptionRequestBO subscriptionRequestBO(SubscriptionBO subscriptionBO, SubscriptionRequestBO subscriptionRequestBO){
        subscriptionRequestBO.setSubscriptionId(subscriptionBO.getSubscriptionId());
        subscriptionRequestBO.setPlanId(subscriptionBO.getPlanId());
        subscriptionRequestBO.setCustomerName(subscriptionBO.getCustomerName());
        subscriptionRequestBO.setCustomerEmail(subscriptionBO.getCustomerEmail());
        subscriptionRequestBO.setCustomerPhone(subscriptionBO.getCustomerPhone());
        //subscriptionRequestBO.setFirstChargeDate(subscriptionBO.getF());
        subscriptionRequestBO.setAuthAmount(subscriptionBO.getAuthAmount());
        subscriptionRequestBO.setReturnUrl("");
        return subscriptionRequestBO;
    }
    public Map<String, String> bankVerifyRequestBO(Map<String, String> params, CustomerBankInfo customerBankInfo){
        params.put("phone",customerBankInfo.getPhone());
        params.put("name",customerBankInfo.getAccountName());
        params.put("bankAccount",customerBankInfo.getAccountNumber());
        params.put("ifsc",customerBankInfo.getIfscCode());
        return params;
    }
    public CustomerBankInfo getCustomerBankInfo(CustomerBankInfo customerBankInfo,BankVerifyRequestBO bankVerifyRequestBO){
        customerBankInfo.setDebtorName(bankVerifyRequestBO.getData().getNameAtBank());
        customerBankInfo.setDebtorAccountNo(bankVerifyRequestBO.getData().getBankAccount());
        customerBankInfo.setBvRefid(bankVerifyRequestBO.getData().getBvRefId());
        customerBankInfo.setNameMatch(bankVerifyRequestBO.getData().getNameMatchScore());
        customerBankInfo.setNameMatchDouble(!StringUtil.isNullOrEmpty(bankVerifyRequestBO.getData().getNameMatchScore())?Double.parseDouble(bankVerifyRequestBO.getData().getNameMatchScore()):0.0);
        customerBankInfo.setVerified(bankVerifyRequestBO.getData().getAccountExists().equalsIgnoreCase("YES")?1:0);
        customerBankInfo.setBankCategory(!StringUtil.isNullOrEmpty(customerBankInfo.getBankCategory())?customerBankInfo.getBankCategory():"P");
        return customerBankInfo;
    }



}

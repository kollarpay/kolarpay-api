package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ENachInfoBO {

    private String consumerCode;
    private String vendor;
    private String fiProduct;
    private String debitStartDate;
    private String debitEndDate;
    private String mandateAmount;
    private String tpslBankCode;
    private String tpslSIBankCode;
    private String consumerEmailId;
    private String consumerMobileNo;
    private String mandateMaxAmount;
    private String accountNo;
    private String ifscCode;
    private String customerId;
    private String frequency;
    private String accountHolderName;
}

package com.kp.core.app.phonpe.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionCreate {
    private String merchantId;
    private String merchantSubscriptionId;
    private String merchantUserId;
    private String authWorkflowType;
    private String amountType;
    private long amount;
    private String frequency;
    private long recurringCount;
    private String mobileNumber;
    private String request;
    private DeviceContext deviceContext;
}

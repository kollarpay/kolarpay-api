package com.kp.core.app.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class NumberConverter {
    private double getPlace(String number) {
        switch (number.length()) {
            case 1:
                return BPConstants.UNITS;
            case 2:
                return BPConstants.TENS;
            case 3:
                return BPConstants.HUNDREDS;
            case 4:
                return BPConstants.THOUSANDS;
            case 5:
                return BPConstants.TENTHOUSANDS;
            case 6:
                return BPConstants.LAKHS;
            case 7:
                return BPConstants.TENLAKHS;
            case 8:
                return BPConstants.CRORES;
            case 9:
                return BPConstants.TENCRORES;
        }// switch
        return 0.0;
    }// getPlace

    private String getWord(int number) {
        switch (number) {
            case 1:
                return "One";
            case 2:
                return "Two";
            case 3:
                return "Three";
            case 4:
                return "Four";
            case 5:
                return "Five";
            case 6:
                return "Six";
            case 7:
                return "Seven";
            case 8:
                return "Eight";
            case 9:
                return "Nine";
            case 0:
                return "Zero";
            case 10:
                return "Ten";
            case 11:
                return "Eleven";
            case 12:
                return "Tweleve";
            case 13:
                return "Thirteen";
            case 14:
                return "Fourteen";
            case 15:
                return "Fifteen";
            case 16:
                return "Sixteen";
            case 17:
                return "Seventeen";
            case 18:
                return "Eighteen";
            case 19:
                return "Ninteen";
            case 20:
                return "Twenty";
            case 30:
                return "Thirty";
            case 40:
                return "Forty";
            case 50:
                return "Fifty";
            case 60:
                return "Sixty";
            case 70:
                return "Seventy";
            case 80:
                return "Eighty";
            case 90:
                return "Ninty";
            case 100:
                return "Hundred";
        } // switch
        return "";
    } // getWord

    private String cleanNumber(String number) {
        String cleanedNumber;
        String[] numbers;
        number.replace('.', ' ');
        String delimeter = " ";
        numbers = number.split(delimeter);
        cleanedNumber = numbers[0];
        return cleanedNumber;
    }

    public String convertNumber(String number) {
        number = cleanNumber(number);
        double num = 0.0;
        try {
            num = Double.parseDouble(number);
        } catch (Exception e) {
            return "Invalid Number Sent to Convert";
        } // catch

        String returnValue = "";
        while (num > 0) {
            number = "" + (int) num;
            double place = getPlace(number);
            if (place == BPConstants.TENS
                    || place == BPConstants.TENTHOUSANDS
                    || place == BPConstants.TENLAKHS
                    || place == BPConstants.TENCRORES) {
                int subNum = Integer.parseInt(number.charAt(0) + ""
                        + number.charAt(1));

                if (subNum >= 21 && (subNum % 10) != 0) {
                    returnValue += getWord(Integer.parseInt(""
                            + number.charAt(0)) * 10)
                            + " " + getWord(subNum % 10);
                } // if
                else {
                    returnValue += getWord(subNum);
                }// else

                if (place == BPConstants.TENS) {
                    num = 0;
                }// if
                else if (place == BPConstants.TENTHOUSANDS) {
                    num -= subNum * BPConstants.THOUSANDS;
                    returnValue += " Thousands ";
                }// if
                else if (place == BPConstants.TENLAKHS) {
                    num -= subNum * BPConstants.LAKHS;
                    returnValue += " Lakhs ";
                }// if
                else if (place == BPConstants.TENCRORES) {
                    num -= subNum * BPConstants.CRORES;
                    returnValue += " Crores ";
                }// if
            }// if
            else {
                int subNum = Integer.parseInt("" + number.charAt(0));

                returnValue += getWord(subNum);
                if (place == BPConstants.UNITS) {
                    num = 0;
                }// if
                else if (place == BPConstants.HUNDREDS) {
                    num -= subNum * BPConstants.HUNDREDS;
                    returnValue += " Hundred ";
                }// if
                else if (place == BPConstants.THOUSANDS) {
                    num -= subNum * BPConstants.THOUSANDS;
                    returnValue += " Thousand ";
                }//if
                else if (place == BPConstants.LAKHS) {
                    num -= subNum * BPConstants.LAKHS;
                    returnValue += " Lakh ";
                }//if
                else if (place == BPConstants.CRORES) {
                    num -= subNum * BPConstants.CRORES;
                    returnValue += " Crore ";
                }//if
            }//else
        }//while
        return returnValue;
    }

}

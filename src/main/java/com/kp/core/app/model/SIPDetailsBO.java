package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SIPDetailsBO {

    private Boolean payment;
    private String schemeName;
    private String schemeCode;
    private String amcCode;
    private String folio;
    private double units;
    private String unitsFormatted;
    private String goalId;
    private String goalName;
    private int sipDate;
    private String referenceId;
    private String bankId;
    private String option;
    private String dividendOption;
    private String sipType;
    private RegularSIP regular;
    private FlexiSIP flexi;
    private StepupSIP stepup;
    private AlertSIP alert;
    private int tenure;
    private int completedInstallments;
    private String nextInstallmentOn;
    private int nextInstallmentAmount;
    private String nextInstallmentAmountFormatted;
    private String tag;
    private String description;
    private String category;
    private BigDecimal currentValue;
    private String currentValueFormatted;
    private BigDecimal totalGain;
    private String totalGainFormatted;
    private Boolean stepUpFlag;
    private String bankName;
    private String bankAccountNumber;
    private String holdingProfileName;
    private String consumerCode;
    private List<String> actions;


    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class RegularSIP {

        private String consumerCode;
        private int tenure;
        private int amount;
        private String frequency;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class FlexiSIP {

        private String consumerCode;
        private int tenure;
        private int amount;
        private String frequency;
        private int maximumAmount;
        private int flexiAmount;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class StepupSIP {

        private String consumerCode;
        private int tenure;
        private int amount;
        private String frequency;
        private String stepupfrequeny;
        private int stepupAmount;
        private int finalAmount;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AlertSIP {

        private int amount;
        private String frequency;
        private String endDate;
        private String startDate;
    }
}

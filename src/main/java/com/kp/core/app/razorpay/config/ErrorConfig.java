package com.kp.core.app.razorpay.config;

import com.kp.core.app.razorpay.utils.ErrorsConstants;
import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/razorpay/wifs/config/properties/ErrorCodes.properties")
@Config.HotReload(type = Config.HotReloadType.ASYNC, unit = TimeUnit.MINUTES, value = 10)

public interface ErrorConfig extends Reloadable {

    //I/O ERROR CODES

    @Key(ErrorsConstants.REQUIRED_DATA_NOT_FOUND)
    String requiredDataNotFound();

    //VPA VALIDATION
    @Key(ErrorsConstants.INVALID_VPA)
    String invalidVPA();

    //INITIATE PAYMENT VALIDATION
    @Key(ErrorsConstants.INVALID_PAYMENTID)
    String invalidPaymentId();

    @Key(ErrorsConstants.INVALID_CUSTOMERID)
    String invalidCustomerId();

    @Key(ErrorsConstants.INVALID_BANK_INFO)
    String invalidBankInfo();

    @Key(ErrorsConstants.INVALID_ORDER_ID)
    String invalidOrderId();

    @Key(ErrorsConstants.INELIGIBLE_USER)
    String ineligibleUser();

    @Key(ErrorsConstants.INVALID_PAYMENT_MODE)
    String invalidPaymentMode();

    @Key(ErrorsConstants.INVALID_CONTACT_NO)
    String invalidContactNo();

    //INTERNAL API ERROR CODES
    @Key(ErrorsConstants.INTERNAL_API_FAIL)
    String internalAPIFail();

    //EXTERNAL API ERROR CODES

    @Key(ErrorsConstants.VALIDATE_VPA_API_FAIL)
    String validateVPAApiFail();

    @Key(ErrorsConstants.CUSTOMER_ACCOUNT_API_FAIL)
    String customerAccountApiFail();

    @Key(ErrorsConstants.ORDER_CREATION_API_FAIL)
    String orderCreationApiFail();

    @Key(ErrorsConstants.CUSTOMER_EDIT_API_FAIL)
    String customerEditApiFail();

    @Key(ErrorsConstants.PAYMENT_INITIATE_API_FAIL)
    String paymentInitiateApiFail();

    //DB ERROR CODES
    @Key(ErrorsConstants.DB_INSERT_FAIL)
    String dbInsertionFail();

    @Key(ErrorsConstants.DB_UPDATE_FAIL)
    String dbUpdateFail();

    //SYSTEM ERROR CODES
    @Key(ErrorsConstants.SERVER_ERROR)
    String serverError();

    @Key(ErrorsConstants.VIRTUAL_ACCOUNT_EXIST)
    String virtualAccountExist();

    @Key(ErrorsConstants.CUSTOM_ACCOUNT_SERVICE)
    String customAccountService();

    @Key(ErrorsConstants.CUSTOM_ACCOUNT_DOES_NOT_EXIST)
    String customerAccountDoesNotExist();

    @Key(ErrorsConstants.CUSTOMER_EMAIL_UPDATE_FAILED)
    String customerEmailUpdateFailed();

    @Key(ErrorsConstants.VIRTUAL_ACCOUNT_SERVICE)
    String virtualAccountService();

    @Key(ErrorsConstants.VIRTUAL_ACCOUNT_DOES_NOT_EXIST)
    String virtualAccountDoesNotExist();

    @Key(ErrorsConstants.CUSTOMERID_INVALID_CONTACT)
    String customerIdInvalidContact();

    @Key(ErrorsConstants.BANK_NOT_SUPPORTED)
    String getBankNotSupported();

    @Key(ErrorsConstants.INPUT_ERROR_VALIDATION)
    String getInputErrorValidation();

    @Key(ErrorsConstants.ROUTE_TRANSFER_ACCOUNT_MAPPING_NOT_AVAILABLE)
    @DefaultValue("Route transfer account mapping not available")
    String routeTransferAccountMappingNotAvailable();

    @Key(ErrorsConstants.ROUTE_TRANSFER_SERVICE_NOT_AVAILABLE)
    @DefaultValue("Route transfer service not available")
    String routeTransferServiceNotAvailable();

    @Key(ErrorsConstants.ROUTE_TRANSFER_EXIST)
    @DefaultValue("Transfer already completed")
    String routeTransferExist();

    @Key(ErrorsConstants.ROUTE_TRANSFER_AMOUNT_MISMATCH)
    @DefaultValue("Transfer amount is not matching with the transaction amount")
    String routeTransferAmountMismatch();

    @Key(ErrorsConstants.WEBHOOK_PAYLOAD_PARSE_ERROR)
    @DefaultValue("Error in parsing webhook payload")
    String webhookPayloadParseError();

    @Key(ErrorsConstants.VIRTUAL_ACCOUNT_EDIT_BANK_ADDITION_ERROR)
    @DefaultValue("Virtual account addition failed while modifying")
    String virtualAccountEditAdditionError();

    @Key(ErrorsConstants.VIRTUAL_ACCOUNT_EDIT_BANK_DELETION_ERROR)
    @DefaultValue("Virtual account deletion failed while modifying")
    String virtualAccountEditDeletionError();

    @Key(ErrorsConstants.VIRTUAL_ACCOUNT_EDIT_MODIFY_ERROR)
    @DefaultValue("Virtual account edit failed")
    String virtualAccountEditError();


    @Key(ErrorsConstants.VIRTUAL_ACCOUNT_EDIT_SERVICE_NOT_AVAILABLE)
    @DefaultValue("Virtual account edit service not available")
    String virtualAccountEditServiceNotAvailableError();


    @Key(ErrorsConstants.PAYMENT_HEALTH_STATUS_METHOD_NOT_AVAILABLE)
    @DefaultValue("Payment gateway method not available")
    String paymentMethodNotAvailable();

    @Key(ErrorsConstants.PAYMENT_HEALTH_STATUS_GATEWAY_NOT_AVAILABLE)
    @DefaultValue("Payment gateway not available")
    String paymentGatewayNotAvailable();

    @Key(ErrorsConstants.PAYMENT_HEALTH_STATUS_GATEWAY_SERVICE_UNAVAILABLE)
    @DefaultValue("Payment gateway service unavailable")
    String paymentGatewayServiceNotAvailable();

    @Key(ErrorsConstants.PAYMENT_UPDATE_STATUS_ERROR)
    @DefaultValue("Payment status update failed")
    String paymentStatusUpdateError();

    @Key(ErrorsConstants.PAYMENT_INFO_FETCH_ERROR)
    @DefaultValue("Payment information fetch failed")
    String paymentInfoFetchError();

}

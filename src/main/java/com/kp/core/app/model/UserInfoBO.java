package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfoBO {

    private int userId;
    private String emailId;
    private String mobile;
    private String password;
    private String enPassword;
    private String name;
    private int pId;
    private String token;
    private int managementUserId;
    private String managementUserRoles;
    private IdDesc userType;
    private String smsOTP;
    private String verificationId;
    private String referenceId;
    private String routes;
    private int lockCount;
    private String mverify;
    private String everify;
    private int emailMarketing;
    private int mobileMarketing;
    private List<CustomerInfoBO> customers;

}

package com.kp.core.app.model;
        import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
        import com.fasterxml.jackson.annotation.JsonInclude;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;
        import lombok.Getter;
        import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class FrontPageProductBo {
    @SerializedName("id")
    @Expose
    public Long id;

    @SerializedName("bpid")
    @Expose
    private int bpid;

    @SerializedName("collectionID")
    @Expose
    private long collectionID;

    @SerializedName("productId")
    @Expose
    private long productId;

    @SerializedName("productName")
    @Expose
    private String productName;

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("bodyHTML")
    @Expose
    private String bodyHTML;
    @SerializedName("background")
    @Expose
    private String background;
    @SerializedName("width")
    @Expose
    private double width;
    @SerializedName("height")
    @Expose
    private double height;
    @SerializedName("maxItems")
    @Expose
    private int maxItems;
    @SerializedName("productType")
    @Expose
    private String productType;
    @SerializedName("position")
    @Expose
    private int position;
    @SerializedName("publishedScope")
    @Expose
    private String publishedScope;
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("imgURL")
    @Expose
    private String imgURL;

    @SerializedName("productURL")
    @Expose
    private String productURL;

    @SerializedName("tags")
    @Expose
    private String[] tags;

}

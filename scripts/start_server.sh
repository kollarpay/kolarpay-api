#!/bin/bash
cd /opt/bpcoreapi/
export AWS_SECRET_ACCESS_KEY=`cat /opt/bpcoreapi/AWS_SECRET_ACCESS_KEY`
export AWS_ACCESS_KEY_ID=`cat /opt/bpcoreapi/AWS_ACCESS_KEY_ID`
export JAEGER_HOST=`cat /opt/bpcoreapi/JAEGER_HOST`
export DB_SECRET=`cat /opt/bpcoreapi/DB_SECRET`
export DB_USER=`cat /opt/bpcoreapi/DB_USER`
export DB_NAME=`cat /opt/bpcoreapi/DB_NAME`
export DB_URL=`cat /opt/bpcoreapi/DB_URL`
export ELASTIC_SEARCH_SCHEME_INSERT_ENABLED=`cat /opt/bpcoreapi/ELASTIC_SEARCH_SCHEME_INSERT_ENABLED`
export STAGE_NAME=`cat /opt/bpcoreapi/stagename`
export MONGODB_URL=`cat /opt/bpcoreapi/MONGODB_URL`
#kill -9 `pgrep -f java`
export
cd /home/ec2-user/server
java -Dserver.port=8080 -jar /home/ec2-user/server/kp-core-api-0.0.1-SNAPSHOT.jar > kp-core-api.log &

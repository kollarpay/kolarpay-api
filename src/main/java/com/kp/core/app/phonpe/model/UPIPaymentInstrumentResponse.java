package com.kp.core.app.phonpe.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UPIPaymentInstrumentResponse {
    private String type;
    private String utr;
}

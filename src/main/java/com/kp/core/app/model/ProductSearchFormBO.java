package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductSearchFormBO {

    private List<NameValueBO> amcs;
    private List<NameValueBO> categories;
    private List<NameValueBO> subCategories;
    private List<NameValueBO> ratings;
    private List<NameValueBO> risk;
    private List<NameValueBO> searchCodes;
    private List<NameValueBO> orderBy;
}

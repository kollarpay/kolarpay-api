package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SchemeSearchESResponseBO {

    private int took;
    private boolean timed_out;
    private Shards _shards;
    private Hits hits;


    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Shards {

        private int total;
        private int successful;
        private int skipped;
        private int failed;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Hits {

        private Total total;
        private double max_score;
        private List<HitList> hits;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Total {

        private int value;
        private String relation;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class HitList {

        private String _index;
        private String _type;
        private String _id;
        private double score;

//        @JsonProperty("_source")
        private ProductSearchResponseBO.SchemeDetailBO _source;

    }
}

package com.kp.core.app.service.Order;

import com.google.gson.Gson;
import com.kp.core.app.dao.CustomerDao;
import com.kp.core.app.dao.OrderDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.CustomerInfoBO;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.request.CustomerDetailsRequestBO;
import com.kp.core.app.model.request.OrderBO;
import com.kp.core.app.razorpay.model.RzpOrderCreationRequestBO;
import com.kp.core.app.service.Payment.PaymentServices;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
public class OrderService {

    private final GeneralUtil generalUtil;

    private final OrderDAO orderDAO;

    private final CustomerDao customerDao;
    private final
    Gson gson;

    private final
    PaymentServices paymentServices;

    @Autowired
    public OrderService(PaymentServices paymentService, Gson gson, CustomerDao customerDao, GeneralUtil generalUtil, OrderDAO orderDAO) {
        this.paymentServices = paymentService;
        this.gson = gson;
        this.customerDao = customerDao;
        this.generalUtil = generalUtil;
        this.orderDAO = orderDAO;
    }
    public GlobalResponseBO createOrder(OrderBO orderBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create order");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createOrder");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            isValidOrder(orderBO, errorBoList);


            if(errorBoList.size()==0 ) {
               CustomerInfoBO customerInfoBO= customerDao.getCustomerInfo(orderBO.getCustomerID());
               orderBO.setCustomerInfoBO(customerInfoBO);
                orderDAO.createOrder(orderBO,userId);
                if(orderBO!=null && orderBO.getOrderID()!=""){
                 paymentServices.makeOrderCreationCall(orderBO,errorBoList);
                }
            }


        } catch (Exception e) {
            log.error(logPrefix + "Create Order API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, orderBO, globalResponseBo);

        log.debug(logPrefix + "Create Order API Status : " + stat);
        return  globalResponseBo;

    }
    public void isValidOrder(OrderBO orderBO, List<ErrorBO> errorBoList) {

        try {
            if(StringUtils.isEmpty(orderBO.getCustomerID()))
                errorBoList.add(new ErrorBO("customer Id", "Invalid input"));
            if(orderBO.getAmount().compareTo(BigInteger.ZERO)<0)
                errorBoList.add(new ErrorBO("Order Amount", "Invalid input"));

        } catch (Exception e) {
            log.error("Exception in Collection creation - validate : ", e);
        }
    }

}

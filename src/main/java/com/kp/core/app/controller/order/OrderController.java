package com.kp.core.app.controller.order;

import com.google.gson.Gson;
import com.kp.core.app.config.GeneralConfig;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.request.OrderBO;
import com.kp.core.app.service.Order.OrderService;
import com.kp.core.app.utils.APIGeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private Gson gson;
    @Autowired
    private OrderService orderService;

    private GeneralConfig config = ConfigFactory.create(GeneralConfig.class);

    @Autowired
    public OrderController(Gson gson, OrderService orderService) {
        this.gson = gson;
        this.orderService = orderService;
    }

    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createOrder(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#OrderToken create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "createOrder");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                OrderBO orderBO = gson.fromJson(payload, OrderBO.class);
                globalResponseBO = orderService.createOrder(orderBO, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#mandates create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

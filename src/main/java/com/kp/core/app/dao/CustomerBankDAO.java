package com.kp.core.app.dao;

import com.google.gson.Gson;
import com.kp.core.app.model.CustomerBankInfo;
import com.kp.core.app.model.InvestorBankInfoBO;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;

@Slf4j
@Repository
public class CustomerBankDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;

    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    Gson gson;


    public CustomerBankInfo createCustomerBank(CustomerBankInfo customerBankInfo, int userid) {

        String logPrefix = "#customerID : " + customerBankInfo.getCustomerId() + " | #UserId : " + userid;
        log.debug(logPrefix + "customerBankInfo Insert");

        int bankAutoId=0;

        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();

            String insertQuery_IT = "INSERT INTO customerbankinfo " +
                    " (customerid,userbankid,banklookupid,bankname,bankcity, branchaddress,accountnumber, " +
                    "  bankaccounttype,micrcode,ifsccode,neftcode,customertype,bankcategory,DEBTORNAME,DEBTORACCOUNTNO,DEBTORBANKNAME,DEBOTRBANKCODE,bvrefid,nameMatchScore,verified, created_user,updated_user,created_at,updated_at, " +
                    " remarks,createdfrom,active) " +
                    " VALUES (?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                    " now(), now(), ?, ?,1)";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(insertQuery_IT, new String[] { "id" });
                ps.setInt(1, Integer.parseInt(customerBankInfo.getCustomerId()));
                ps.setInt(2, 1); //todo : exit load
                ps.setString(3, customerBankInfo.getBanklookupId());
                ps.setString(4, customerBankInfo.getBankName());
                ps.setString(5, customerBankInfo.getBankCity());
                ps.setString(6, customerBankInfo.getBranchAddress());
                ps.setString(7, customerBankInfo.getAccountNumber());
                ps.setString(8, customerBankInfo.getAccountType());
                ps.setString(9, customerBankInfo.getMicrCode());
                ps.setString(10, customerBankInfo.getIfscCode());
                ps.setString(11, customerBankInfo.getNeftCode());
                ps.setString(12, customerBankInfo.getCustomertype());
                ps.setString(13, customerBankInfo.getBankCategory());
                ps.setString(14, customerBankInfo.getDebtorName());
                ps.setString(15, customerBankInfo.getDebtorAccountNo());
                ps.setString(16, customerBankInfo.getDebtorBankName());
                ps.setString(17, customerBankInfo.getDebtorBankCode());
                ps.setString(18, customerBankInfo.getBvRefid());
                ps.setDouble(19, customerBankInfo.getNameMatchDouble());
                ps.setInt(20, customerBankInfo.getVerified());
                ps.setInt(21, userid);
                ps.setInt(22, userid);
                ps.setString(23, customerBankInfo.getRemarks());
                ps.setString(24, customerBankInfo.getCreatedFrom());
                return ps;

            }, keyHolder);
            bankAutoId=keyHolder.getKey().intValue();
            customerBankInfo.setId(bankAutoId+"");

        } catch (Exception e) {
            log.error(logPrefix + "Exception in customerBankInfo insert : ", e);
        }
        return customerBankInfo;

    }
    public boolean isBankAvailable(CustomerBankInfo customerBankInfo) {

        boolean isBankAvailable = false;
         String customerId=customerBankInfo.getCustomerId();
         String id=customerBankInfo.getId();


        try {
            String presentQuery ="  select count(1) as total from customerbankinfo where customerid=? and id=? and (active=1 or active is null)  \n ";
            int isPresent = jdbcTemplate.queryForObject(presentQuery, Integer.class,Integer.parseInt(customerId),Integer.parseInt(id));
            if(isPresent!=0)
                isBankAvailable = true;

        } catch (Exception e) {
            log.error("Exception in isBankAvailable availability check : ", e);
        }
        return isBankAvailable;
    }

    public int deactivateBank(String customerId,int bankId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #bankId : " + bankId + " | ";

        int status = 0;

        try {

            String sqlQuery = " UPDATE customerbankinfo SET Active = 0,updated_user = ?, updated_at = now() WHERE id=? and customerid=?";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {
                ps.setInt(1, userId);
                ps.setInt(2, bankId);
                ps.setInt(3, Integer.parseInt(customerId));
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in customerbankinfo deactivate: ", e);
        }
        return status;
    }
    public boolean isPrimaryBankAvailable(String customerId) {

        boolean isPrimaryBankAvailable = false;
        String logPrefix = "#customerId : " + customerId;
         log.info("Begin isPrimaryBankAvailable logPrefix--"+logPrefix);
        try {
            String presentQuery ="select count(1) as total from customerbankinfo where customerid=? and (active=1 or active is null) and bankcategory='P'  \n ";

            int isPresent = jdbcTemplate.queryForObject(presentQuery,Integer.class,Integer.parseInt(customerId));
            if(isPresent!=0)
                isPrimaryBankAvailable = true;

        } catch (Exception e) {
            log.error(logPrefix + " Exception in isPrimaryBankAvailable availability check : ", e);
        }
        return isPrimaryBankAvailable;
    }
    public int primaryBankUpdate(String customerId,int bankId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #bankId : " + bankId + " | ";

        int status = 0;

        try {

            String sqlQuery = " UPDATE customerbankinfo SET bankcategory = NULL, updated_user = ?, updated_at = now() WHERE bankcategory='P' and customerid=? ";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {
                ps.setInt(1, userId);
                ps.setInt(2, Integer.parseInt(customerId));
            });
            if(status>0) primaryBankCategoryUpdate(customerId,bankId,userId);

        } catch (Exception e) {
            log.error(logPrefix + "Exception in customerbankinfo bankcategory update: ", e);
        }
        return status;
    }
    public void primaryBankCategoryUpdate(String customerId, int bankId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #bankId : " + bankId + " | ";

        int status = 0;

        try {

            String sqlQuery = " UPDATE customerbankinfo SET bankcategory = 'P',ModifiedUser = ?, ModifiedDate = now() WHERE id=? bankcategory='P' and customerid=?";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {
                ps.setInt(1, userId);
                ps.setInt(2, bankId);
                ps.setInt(3, Integer.parseInt(customerId));
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in customerbankinfo bankcategory update: ", e);
        }
    }
}

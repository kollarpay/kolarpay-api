package com.kp.core.app.service.AdvisorPortfolioReview;

import com.kp.core.app.dao.UserDao;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.PortFolioBO;
import com.kp.core.app.model.UserBO;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.BPConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rsuresh on 25-09-2021.
 */
@Service
@Slf4j
public class UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    GeneralUtil generalUtil;

    /**
     * for given email / pan investor list is returned
     * @param type email/pan
     * @param query
     * @param advUserId
     * @return
     */
    public GlobalResponseBO getInvestorList(String type, String query, int advUserId) {

        String logPrefix = "#AdvUserId : " + advUserId + " | ";

        log.debug(logPrefix + "Investor List Input : Query : " + query + "|| Type : " + type);
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "InvestorDetail");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<UserBO> userList = new ArrayList<>();
        String stat = "";

        try {

            if(StringUtils.isEmpty(type) && !type.equalsIgnoreCase(BPConstants.TYPE_PAN) && !type.equalsIgnoreCase(BPConstants.TYPE_EMAIL))
                errorBoList.add(new ErrorBO("type","Invalid input"));

            else if(StringUtils.isEmpty(query))
                errorBoList.add(new ErrorBO("query","Invalid input"));

            else
                userList = userDao.fetchInvestorList(type, query, advUserId);

            if(errorBoList.size()>0)
                stat = APIConstants.BAD_REQUEST_STRING;

            globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, userList, globalResponseBo);

        } catch (Exception e) {
            log.error(logPrefix + "Exception :", e);
        }

        log.debug(logPrefix + "Search investor API Status : " + stat);
        return globalResponseBo;
    }

    /**
     * for given array of investors portfolio list is returned
     * @param investors
     * @param advUserId
     * @return
     */
    public GlobalResponseBO getPortfolioList(List<Integer> investors, int advUserId) {

        String logPrefix = "#AdvUserId : " + advUserId + " | ";

        log.debug(logPrefix + "Investor List Input : investors : " + Arrays.toString(investors.toArray()));
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "Portfolio");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<PortFolioBO> portfolioList = new ArrayList<>();
        String stat = "";

        try {

            if(investors.size()==0) {
                errorBoList.add(new ErrorBO("investors", "Invalid input"));
                stat = APIConstants.BAD_REQUEST_STRING;
            }
            else
                 portfolioList = userDao.fetchPortfolioList(investors, advUserId);


            globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, portfolioList, globalResponseBo);

        } catch (Exception e) {
            log.error(logPrefix + "Exception :", e);
        }

        log.debug(logPrefix + "Portfolio list API Status : " + stat);
        return globalResponseBo;
    }
}

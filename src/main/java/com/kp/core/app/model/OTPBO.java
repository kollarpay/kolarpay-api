package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OTPBO {

    private int userId;
    private String emailId;
    private String mobile;
    private String smsOTP;
    private String verificationId;
    private String productType;
    private String msgType;
    private String referenceId;
    private IdDesc idDesc;
    private String validOTP;
}

package com.kp.core.app.service.Users;

import com.kp.core.app.Mail.MailerBL;
import com.kp.core.app.dao.*;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.utils.*;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
@Slf4j
public class UserInfoService {

    @Autowired
    private GeneralUtil generalUtil;


    @Autowired
    private UserDao userDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private MailerBL mailerBL;


    public GlobalResponseBO registerMobile(UserRequest userRequest, List<ErrorBO> errorBoList, boolean insert, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        int userId=0;
        String stat = "";
        UserInfoBO userInfoBO=new UserInfoBO();
        userRequest.setMessageType("mobile");
        int typecode=2;
      String  mobileNumber= userRequest.getMobileno();
      String emailId=  userRequest.getEmailId();
        try {
            String antiXsrfToken = UUID.randomUUID().toString();

            if (StringUtils.isEmpty(mobileNumber) && StringUtils.isEmpty(emailId)) {
                errorBoList.add(new ErrorBO("LOGIN ID", "Login Id shouldn't be empty"));
            }
            if (StringUtils.isNotEmpty(mobileNumber) && mobileNumber.trim().length()<10)
                errorBoList.add(new ErrorBO("MOBILE_NO", "Enter valid Mobile Number"));
            if (StringUtils.isNotEmpty(emailId) && GeneralUtil.patternMatches(emailId,APIConstants.EMAIL_PATTERN_FORMAT))
                errorBoList.add(new ErrorBO("EMAIL ID", "Enter valid Email Id"));
            if(StringUtils.isNotEmpty(emailId)){
                userRequest.setMessageType("email");
                typecode=1;
            }
            if(StringUtils.isNotEmpty(mobileNumber)){
                userRequest.setMessageType("mobilenumber");
                typecode=2;
            }

            if((StringUtils.isNotEmpty(mobileNumber) || StringUtils.isNotEmpty(emailId)) && errorBoList.isEmpty()){
                userInfoBO=userDao.getUserInfoBO(typecode,userRequest);
                userId=userInfoBO.getUserId();
                if(userId>0) {
                    insert = false;
                    userInfoBO.setRoutes("motp");
                }
                if(!StringUtils.isEmpty(userInfoBO.getPassword())){
                    userInfoBO.setRoutes("password");
                }

            }


            if(insert && errorBoList.isEmpty()) {
                userId = userDao.userMobileRegistrationInsert(userRequest);
                userInfoBO.setUserId(userId);
                userInfoBO.setRoutes("motp");
                if (userId==0)
                    errorBoList.add(new ErrorBO("failed", "user creation failed"));
            }

            if(userId>0 && errorBoList.isEmpty() && "motp".equalsIgnoreCase(userInfoBO.getRoutes())) {
                userRequest.setReferenceId(antiXsrfToken);
                userInfoBO.setReferenceId(antiXsrfToken);
                userRequest.setSmsOTP(GeneralUtil.generateOTP(6));
                userDao.otpGenInsert(userRequest,userId);
                //Response response=generalUtil.sendSMS(userRequest.getMobileno(),userRequest.getSmsOTP(),"");
              Response response=generalUtil.sendMsg91SMS(userRequest.getMobileno(),userRequest.getSmsOTP(),"");
                log.info("msg91 message response---"+response.toString());

            }

        } catch (Exception e) {
            log.error("Exception in MOBILE table update : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, userInfoBO, globalResponseBo);

        return globalResponseBo;

    }
    public GlobalResponseBO verifyOTP(OTPBO otpbo, List<ErrorBO> errorBoList, String createdFrom) {

        log.debug("Input:: UserId : " + otpbo.getUserId() + " || otp : " + otpbo.getVerificationId());
        String stat = "";
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        HashMap<String, Object> resultData = new HashMap<>();

        try {
            if (otpbo.getUserId() == 0)
                errorBoList.add(new ErrorBO("UserId", "Invalid Id"));
            if (StringUtils.isEmpty(otpbo.getVerificationId()))
                errorBoList.add(new ErrorBO("OTP", "Invalid OTP"));
            if (StringUtils.isEmpty(otpbo.getReferenceId()))
                errorBoList.add(new ErrorBO("OTP", "Reference Id missing"));

            if (errorBoList.isEmpty())  {
               boolean isExpired= userDao.isOTPExpired(otpbo);
                if (isExpired) {

                    if (otpbo.getVerificationId().equalsIgnoreCase(otpbo.getSmsOTP())) {
                        otpbo.setValidOTP("VALID");
                        userDao.updateVerification(otpbo.getMsgType(), otpbo.getUserId());
                    }
                    else
                        errorBoList.add(new ErrorBO("OTP", "Invalid OTP"));

                } else {
                    errorBoList.add(new ErrorBO("OTP", "OTP Expired"));
                }

            }else{
                stat=BPConstants.FAIL;
            }
        } catch (Exception e) {
            log.error("Exception wile sending OTP for Verify OTP", e);
        }
        finally {
            if(!errorBoList.isEmpty()){
                stat=APIConstants.SERVER_ERROR_STRING;
            }
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, otpbo, globalResponseBo);

        return globalResponseBo;
    }


    public GlobalResponseBO updateUserEmailIdDetails(UserRequest userRequest, List<ErrorBO> errorBoList, boolean insert, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        int userId=0;
        String stat = "";
        UserInfoBO userInfoBO=new UserInfoBO();

        try {

            if (StringUtils.isEmpty(userRequest.getMobileno()))
                errorBoList.add(new ErrorBO("MOBILE_NO", "Mobile Number shouldn't be empty"));
            if (!StringUtils.isEmpty(userRequest.getMobileno()) && userRequest.getMobileno().trim().length()<10)
                errorBoList.add(new ErrorBO("MOBILE_NO", "Enter valid Mobile Number"));

            if (StringUtils.isEmpty(userRequest.getEmailId()))
                errorBoList.add(new ErrorBO("EMAIL_ID", "EmailId shouldn't be empty"));
            if(!StringUtils.isEmpty(userRequest.getEmailId()) && !userRequest.getEmailId().matches(BPConstants.EMAIL_REGEX))
                errorBoList.add(new ErrorBO("EMAIL_ID", "Enter valid EmailId"));
            if(errorBoList.isEmpty()){
                userInfoBO=userDao.getUserInfoBO(5,userRequest);
                if(userInfoBO.getUserId()==0){
                    errorBoList.add(new ErrorBO("EMAIL_ID", "EmailId not found"));
                    APIGeneralUtil.buildErrorResponse(APIConstants.NOT_FOUND, APIConstants.NOT_FOUND_STRING, globalResponseBo);
                }
                else userId=  userInfoBO.getUserId();
            }else{
                APIGeneralUtil.buildErrorResponse(APIConstants.BAD_REQUEST, APIConstants.INVALID_INPUT_STRING, globalResponseBo);
            }

            if(insert && errorBoList.isEmpty()) {
                int isupdated = userDao.updateUserEmail(userRequest);

                if (isupdated==0)
                    errorBoList.add(new ErrorBO("FAILED", "User email update failed"));

            }
            if(userId>0) {
                userRequest.setUserId(userId);
                userInfoBO=userDao.getUserInfoBO(4,userRequest);

            }
//            if(!errorBoList.isEmpty()){
//                APIGeneralUtil.buildErrorResponse(APIConstants.NOT_FOUND, APIConstants.NOT_FOUND_STRING, globalResponseBo);
//            }

        } catch (Exception e) {
            log.error("Exception in UserInfo table insertions : ", e);
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(globalResponseBo.getCode().toString(), errorBoList, userInfoBO, globalResponseBo);

        return globalResponseBo;

    }


    public GlobalResponseBO updatePassword(UserRequest userRequest, List<ErrorBO> errorBoList, boolean insert, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        int userId=0;
        String stat = "";
        UserInfoBO userInfoBO=new UserInfoBO();

        try {

            if (StringUtils.isEmpty(userRequest.getEmailId()))
                errorBoList.add(new ErrorBO("EMAIL_ID", "EmailId shouldn't be empty"));
            if(!StringUtils.isEmpty(userRequest.getEmailId()) && !userRequest.getEmailId().matches(BPConstants.EMAIL_REGEX))
                errorBoList.add(new ErrorBO("EMAIL_ID", "Enter valid EmailId"));

            if (StringUtils.isEmpty(userRequest.getPassword()))
                errorBoList.add(new ErrorBO("PASSWORD", "Password shouldn't be empty"));
            if (!StringUtils.isEmpty(userRequest.getPassword()) && userRequest.getPassword().length()<5)
                errorBoList.add(new ErrorBO("PASSWORD", "Password should be minimum 6 char"));


            if(insert && errorBoList.isEmpty()) {
                userId = userDao.updatePassword(userRequest);

                if (userId==0)
                    errorBoList.add(new ErrorBO("FAILED", "Password update failed"));

            }
            if(userId>0) {
                userRequest.setUserId(userId);
                userInfoBO=userDao.getUserInfoBO(4,userRequest);

            }

        } catch (Exception e) {
            log.error("Exception in UserInfo table insertions : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, userInfoBO, globalResponseBo);

        return globalResponseBo;

    }
    public GlobalResponseBO getUserInfo(int type,String searchStr, List<ErrorBO> errorBoList, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        String customerId="";
        String stat = "";
        List<CustomerInfoBO> customerInfoList=new ArrayList<>();
        log.info("type--"+type+"--SearchStr---"+searchStr);
        UserRequest userRequest=new UserRequest();
        UserInfoBO userInfoBO=new UserInfoBO();

        try {

            if (StringUtils.isEmpty(searchStr))
                errorBoList.add(new ErrorBO("invalid data", "data shouldn't be empty"));

            if (!StringUtils.isEmpty(searchStr) && searchStr.trim().length()<4)
                errorBoList.add(new ErrorBO("invalid data", "Data should not be less than 4 char"));

            int userId=0;
            if(type==4 && searchStr!=null && !searchStr.trim().isEmpty()) {
                userId=Integer.parseInt(searchStr);
                userRequest.setUserId(userId);
            }

            if(errorBoList.isEmpty()) {

                if(userId>0) {
                    userRequest.setUserId(userId);
                    userInfoBO=userDao.getUserInfoBO(4,userRequest);
                    customerInfoList= customerDao.getCustomerInfoList(0,searchStr);
                    if(!customerInfoList.isEmpty()) {
                        userInfoBO.setCustomers(customerInfoList);
                    }

                }
            }

        } catch (Exception e) {
            log.error("Exception in userinfo table search : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, userInfoBO, globalResponseBo);

        return globalResponseBo;

    }

    public GlobalResponseBO signup(UserRequest userRequest, List<ErrorBO> errorBoList, boolean insert, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        int userId=0;
        String stat = "";
        UserInfoBO userInfoBO=new UserInfoBO();
        String  mobileNumber= userRequest.getMobileno();
        String emailId=  userRequest.getEmailId();
        try {
            String antiXsrfToken = UUID.randomUUID().toString();

            if (StringUtils.isEmpty(userRequest.getFirstname()))
                errorBoList.add(new ErrorBO("Name", "Name shouldn't be empty"));

            if (StringUtils.isNotEmpty(emailId) && GeneralUtil.patternMatches(emailId,APIConstants.EMAIL_PATTERN_FORMAT))
                errorBoList.add(new ErrorBO("EMAIL ID", "Enter valid Email Id"));
            if (StringUtils.isNotEmpty(mobileNumber) && mobileNumber.trim().length()<10)
                errorBoList.add(new ErrorBO("MOBILE_NO", "Enter valid Mobile Number"));

            if (StringUtils.isEmpty(userRequest.getPassword()))
                errorBoList.add(new ErrorBO("PASSWORD", "Password shouldn't be empty"));
            if (!StringUtils.isEmpty(userRequest.getPassword()) && userRequest.getPassword().length()<8)
                errorBoList.add(new ErrorBO("PASSWORD", "Password should be minimum 8 char"));



            if(insert && errorBoList.isEmpty()) {
                userId = userDao.userMobileRegistrationInsert(userRequest);
                userInfoBO.setUserId(userId);
                userInfoBO.setRoutes("signup");
                if (userId==0)
                    errorBoList.add(new ErrorBO("failed", "user creation failed"));
            }

            if(userId>0 && errorBoList.isEmpty()) {
                userRequest.setReferenceId(antiXsrfToken);
                userInfoBO.setReferenceId(antiXsrfToken);
                userRequest.setSmsOTP(GeneralUtil.generateOTP(6));
                userDao.otpGenInsert(userRequest,userId);
              //  Response response=generalUtil.sendMsg91SMS(userRequest.getMobileno(),userRequest.getSmsOTP(),"");
            //    log.info("msg91 message response---"+response.toString());
                mailerBL.welcomeEmail(userId);
            }

        } catch (Exception e) {
            log.error("Exception in MOBILE table update : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, userInfoBO, globalResponseBo);

        return globalResponseBo;

    }


}

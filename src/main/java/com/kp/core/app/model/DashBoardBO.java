package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashBoardBO {

    private BigDecimal currentValueOfInvestment;
    private String currentValueOfInvestmentFormatted;
    private BigDecimal investedAmount;
    private String investedAmountFormatted;
    private BigDecimal gainAmount;
    private String gainAmountFormatted;
    private String gainPercentage;
    private BigDecimal changeAmount;
    private String changeAmountFormatted;
    private String changePercentage;
    private String annualReturns;

}

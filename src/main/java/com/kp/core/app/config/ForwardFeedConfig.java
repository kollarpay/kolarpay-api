package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/brospay/config/properties/DBFForwardfeed.properties")
@Config.HotReload(type = Config.HotReloadType.ASYNC, unit = TimeUnit.MINUTES, value = 10)

public interface ForwardFeedConfig extends Reloadable {

    @Config.Key("OE_THIRTEEN_HOUR_START_TIME")
    @Config.DefaultValue("0")
    String OEThirteenHrStartTime();

    @Config.Key("OE_THIRTEEN_HOUR_END_TIME")
    @Config.DefaultValue("0")
    String OEThirteenHrEndTime();

}

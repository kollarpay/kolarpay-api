package com.kp.core.app.razorpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RzpCustomerCreationRequestBO {

    private String name;
    private String contact;
    private String email;
    private String fail_existing;
    private String currency;
    private CustomerCreationNotesBO notes;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CustomerCreationNotesBO {
        private String mobile;
        private String labelId;

    }

}

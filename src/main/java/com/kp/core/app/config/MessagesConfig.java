package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/brospay/config/properties/Messages.properties")
@Config.HotReload(type = Config.HotReloadType.ASYNC, unit = TimeUnit.MINUTES, value = 10)

public interface MessagesConfig extends Reloadable {

    @Config.Key("InvestTransactionMsg1Before")
    @Config.DefaultValue("")
    String investorTransMsgBefore();

    @Config.Key("InvestTransactionMsg1Before")
    @Config.DefaultValue("")
    String investorTransMsgAfter();
}

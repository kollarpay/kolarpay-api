package com.kp.core.app.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter

public class CustomerRequest {
    private int userId;
    private int bpId;
    private String name;
    private String emailId;
    private String mobileNumber;
    private String entityName;
    private String type;
    private String customerId;
    private String dob;
    private String password;
    private int marketing;
    private List<NameValueBO> Names;

}

package com.kp.core.app.razorpay.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RzpCustomerCreationResponseBO {

    private String id;
    private String entity;
    private String name;
    private String email;
    private String fail_existing;
    private String gstin;
    private String created_at;
    private CustomerCreationNotesBO notes;

    @Getter
    @Setter
    public static class CustomerCreationNotesBO {
        private String user_mobile;
        private String user_bp_id;

    }


}

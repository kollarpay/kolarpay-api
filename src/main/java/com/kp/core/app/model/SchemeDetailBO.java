package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SchemeDetailBO implements Serializable {

    private String name;
    private String schemeCode;
    private String category;
    private String subCategory;
    private String amc;
    private String amcCode;
    private double rating;
    private double nav;
    private int minimumInvestment;
    private boolean rated;
    private boolean sip;
}

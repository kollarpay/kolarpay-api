
package com.kp.core.app.controller.product;

import com.google.gson.Gson;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.Products.ProductBO;
import com.kp.core.app.service.Product.ProductService;
import com.kp.core.app.service.Utilities.AmazonClient;
import com.kp.core.app.utils.APIGeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@RestController
@Slf4j
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    Gson gson;

    @Autowired
    private AmazonClient amazonClient;

    ProductController(AmazonClient amazonClient) {
        this.amazonClient = amazonClient;
    }

    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createProduct(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#product create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "productCreate");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                ProductBO productBO = gson.fromJson(payload, ProductBO.class);
                globalResponseBO = productService.createProduct(productBO, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#product create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }


    @PostMapping(path="/uploadFile",consumes ={MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE }, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> uploadFile(@RequestPart("product_id") String productId,@RequestPart("file") List<MultipartFile> file,@RequestHeader MultiValueMap<String, Object> headers) {
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "productImgupload");
        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);
            ProductBO productBO=new ProductBO();

            if(userId > 0 && productId!=null && Integer.parseInt(productId)>0) {

                List<ProductBO.ProductImage> imgFiles=amazonClient.uploadFile(file);
                productBO.setProductImages(imgFiles);
                globalResponseBO = productService.createProductImage(productBO,userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#collection create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @DeleteMapping("/deleteFile")
    public String deleteFile(@RequestPart(value = "url") String fileUrl) {
        return this.amazonClient.deleteFileFromS3Bucket(fileUrl);
    }

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> getAllProduct(@RequestHeader MultiValueMap<String, Object> headers) throws Exception {

        log.debug("#get all publish products POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "products");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                globalResponseBO = productService.getproducts(userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#collection create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/product", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> ProductById(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("# GET product by ID API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "getProductById");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                ProductBO productBO = gson.fromJson(payload, ProductBO.class);
                globalResponseBO = productService.getproductDetail(userId,productBO);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#product create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/tags", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> ProductByTag(@RequestParam("tags") String tags,@RequestHeader MultiValueMap<String, Object> headers, @RequestBody(required = false) String payload) throws Exception {

        log.debug("# GET product by ID API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "getProductByTag");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                ProductBO productBO = gson.fromJson(payload, ProductBO.class);
                globalResponseBO = productService.getproductTag(userId,tags);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#product create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> getProductCategories(@RequestParam("categoryType") String categoryType,@RequestHeader MultiValueMap<String, Object> headers) throws Exception {

        log.debug("#get product category Get API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "products-category");

        int bpid = 0;
        try {
            bpid = Integer.parseInt(APIGeneralUtil.getHeader(headers, "bpid"));

            log.debug(categoryType+":::#bpid : " + bpid);

            if(bpid > 0) {

                globalResponseBO = productService.getproductCategory(bpid,categoryType);

            }

        } catch (Exception e) {
            log.error("#bpid : " + bpid + "| Exception : ", e);
        }
        log.debug("#Product Category get API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

}

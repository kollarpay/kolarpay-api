package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExecutionSummaryBO {

    private SummaryBO executionSummary;
    private SummaryBO cash;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SummaryBO{

        private List<DescriptionBO> summaries;
        private int total;
        private double allocation;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DescriptionBO{

        private String description;
        private List<SchemeBO> schemes;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SchemeBO{

        private String schemeCode;
        private String schemeName;
        private String category;
        private String subCategory;
        private String description;
        private String amount;
        private String amountFormatted;
        private String allocation;
        private String proposed;
        private String comment;
        private double rating;
        private int ratingFormatted;
    }
}

//package com.kp.core.app.service.Subscription;
//
//import com.kp.core.app.dao.GeneralInfoDAO;
//import com.kp.core.app.enums.ResponseDataObjectType;
//import com.kp.core.app.model.ErrorBO;
//import com.kp.core.app.model.GlobalResponseBO;
//import com.kp.core.app.model.SubscriptionBO;
//import com.kp.core.app.phonpe.dao.SubscriptionDAO;
//import com.kp.core.app.service.Payment.PaymentServices;
//import com.kp.core.app.utils.APIConstants;
//import com.kp.core.app.utils.GeneralUtil;
//import io.netty.util.internal.StringUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
//
//@Service
//@Slf4j
//public class SubscriptionService {
//
//    @Autowired
//    private GeneralUtil generalUtil;
//
//    @Autowired
//    private SubscriptionDAO subscriptionDAO;
//    @Autowired
//    private GeneralInfoDAO generalInfoDAO;
//
//    private final
//    PaymentServices paymentServices;
//
//    public SubscriptionService(PaymentServices paymentServices) {
//        this.paymentServices = paymentServices;
//    }
//
//
//    public GlobalResponseBO createSubscriptions(SubscriptionBO subscriptionBO, int userId) {
//
//        String logPrefix = "#UserId : " + userId + " | ";
//        log.debug(logPrefix + "Create subscription Service");
//        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createSubscription");
//        List<ErrorBO> errorBoList = new ArrayList<>();
//        String stat = "";
//
//        try {
//
//           // isValidPlan(planBO, errorBoList);
//
//
//            if(errorBoList.isEmpty()) {
//                subscriptionDAO.createSubscription(subscriptionBO,userId);
//                if(subscriptionBO!=null && !StringUtil.isNullOrEmpty(subscriptionBO.getSubscriptionId())){
//                   // paymentServices.createPhonePeSubscription(subscriptionBO,errorBoList);
//                }
//            }
//
//
//        } catch (Exception e) {
//            log.error(logPrefix + "Create Subscription API Exception : ", e);
//            stat = APIConstants.SERVER_ERROR_STRING;
//        }
//
//        if(errorBoList.size()>0)
//            stat = APIConstants.BAD_REQUEST_STRING;
//
//        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, subscriptionBO, globalResponseBo);
//
//        log.debug(logPrefix + "Create Subscription API Status : " + stat);
//        return  globalResponseBo;
//
//    }
//    public void isValidSubscription(SubscriptionBO subscriptionBO, List<ErrorBO> errorBoList) {
//
//        try {
//
//            if(StringUtil.isNullOrEmpty(subscriptionBO.getSubscriptionId()))
//                errorBoList.add(new ErrorBO("subscription Id", "Invalid subscription id"));
//
//
//        } catch (Exception e) {
//            log.error("Exception in subscription creation - validate : ", e);
//        }
//    }
//}

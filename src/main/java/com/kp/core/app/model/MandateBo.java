package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MandateBo {
    private String id;
    private String subscriptionId;
    private String customerId;
    private String formType;
    private String frequency;
    private String recurringCount;
    private String upperLimit;
    private String start_at;
    private String expiry_by;
    private String created_user;
    private String created_at;
    private String updated_user;
    private String updated_at;
    private String active;
    private String from;
    private String remarks;
    private String comments;

}


package com.kp.core.app.phonpe.dao;

import com.google.gson.Gson;
import com.kp.core.app.model.SubscriptionBO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;

@Slf4j
@Repository

public class SubscriptionDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Autowired
    Gson gson;

    public void createSubscription(SubscriptionBO subscriptionBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.info("{}Insert subscription", logPrefix);


        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery = " INSERT INTO subscriptions (planid,type,startdate,enddate,ecsdate," +
                    "amount,ecsamount,consumercode,folio,noofinstallment,frequency," +
                    "portfolioid,active,formtype,affecton,status," +
                    "remarks,initialpayment,subscriptionrefid,orderid,maxamount," +
                    "flexiamount,stepupfrequency,stepupamount,reason,datechange," +
                    "perpetual,medium,created_at,updated_at, created_user, updated_user,ipaddress) " +
                    " VALUES( ?, ?, ?, ?, ?, ?,?,?,?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?,?, ?,NOW(), NOW(), ?, ?,?)  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, subscriptionBO.getPlanId());
                ps.setString(2, subscriptionBO.getSubscriptionType());
                ps.setString(3, subscriptionBO.getStartDate());
                ps.setString(4, subscriptionBO.getEndDate());
                ps.setInt(5, subscriptionBO.getEcsDate());
                ps.setDouble(6, subscriptionBO.getAmount());
                ps.setDouble(7, subscriptionBO.getEcsamount());
                ps.setString(8, subscriptionBO.getConsumerCode());
                ps.setString(9, subscriptionBO.getFolio());
                ps.setInt(10, subscriptionBO.getNoofinstallment());
                ps.setString(11, subscriptionBO.getFrequency().equalsIgnoreCase("MONTHLY")?"M":subscriptionBO.getFrequency());
                ps.setInt(12, subscriptionBO.getPortfolioId());
                ps.setInt(13, subscriptionBO.getActive());
                ps.setString(14, subscriptionBO.getFormType());
                ps.setString(15, subscriptionBO.getAffectOn());
                ps.setString(16, subscriptionBO.getStatus());
                ps.setString(17, subscriptionBO.getRemarks());
                ps.setString(18, subscriptionBO.getInitialPayment());
                ps.setString(19, subscriptionBO.getSubscriptionRefId());
                ps.setString(20, subscriptionBO.getOrderId());
                ps.setDouble(21, subscriptionBO.getMaxAmount());
                ps.setDouble(22, subscriptionBO.getFlexiAmount());
                ps.setString(23, subscriptionBO.getStepUpFrequency());
                ps.setDouble(24, subscriptionBO.getStepUpAmount());
                ps.setString(25, subscriptionBO.getReason());
                ps.setString(26, subscriptionBO.getDateChange());
                ps.setInt(27, subscriptionBO.getPerpetual());
                ps.setString(28, subscriptionBO.getMedium());
                ps.setInt(29, userId);
                ps.setInt(30, userId);
                ps.setString(31, subscriptionBO.getIpAddress());
                return ps;

            }, keyHolder);
            String id;
            String subscriptionid;
            if (keyHolder.getKeys().size() > 1) {
                id = keyHolder.getKeys().get("id").toString();
                subscriptionBO.setId(id);
                subscriptionid = keyHolder.getKeys().get("subscriptionid").toString();
                subscriptionBO.setSubscriptionId(subscriptionid);
            }

        } catch (Exception e) {
            log.error("{}Exception in subscription insertion: ", logPrefix, e);
        }

        log.debug("{}subscription id  generate: {}", logPrefix, subscriptionBO.getSubscriptionId());
    }
}
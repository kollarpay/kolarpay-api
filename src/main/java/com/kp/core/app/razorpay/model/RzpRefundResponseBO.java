package com.kp.core.app.razorpay.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RzpRefundResponseBO {

    private String id;
    private String entity;
    private int amount;
    private String receipt;
    private String currency;
    private String payment_id;
    private String[] notes;
    private String created_at;
    private String batch_id;
    private String status;
    private String speed_processed;
    private String speed_requested;
    private RefundAcquirerData acquirer_data;
    private String remarks;

    @Getter
    @Setter
    public static class RefundAcquirerData {
        private String arn;
    }

}

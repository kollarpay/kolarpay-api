package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kp.core.app.model.Products.ProductCategory;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)


public class CollectionBO {
    @SerializedName("id")
    @Expose
    public Long id;
    @SerializedName("collection_id")
    @Expose
    public Long collection_id;
    @SerializedName("handle")
    @Expose
    public String handle;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("published_at")
    @Expose
    public String publishedAt;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("tags")
    @Expose
    public String tags;
    @SerializedName("template_suffix")
    @Expose
    public String templateSuffix;
    @SerializedName("products_count")
    @Expose
    public Integer productsCount;
    @SerializedName("collection_type")
    @Expose
    public String collectionType;
    @SerializedName("published_scope")
    @Expose
    public String publishedScope;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("body_html")
    @Expose
    public String bodyHtml;

    @SerializedName("alt")
    @Expose
    public String alt;
    @SerializedName("width")
    @Expose
    public Integer width;
    @SerializedName("height")
    @Expose
    public Integer height;
    @SerializedName("src")
    @Expose
    public String src;
    @SerializedName("position")
    @Expose
    public Integer position;
    @SerializedName("bpid")
    @Expose
    public Integer bpId;
    @Expose
    public List<ProductCategory> productCategoryList;

}
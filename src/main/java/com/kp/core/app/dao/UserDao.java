package com.kp.core.app.dao;

import com.kp.core.app.model.*;
import com.kp.core.app.utils.EncryptionDecryption;
import com.kp.core.app.utils.BPConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.*;

/**
 * Created by rsuresh on 25-09-2021.
 */
@Slf4j
@Repository
public class UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private AutoNumberGenerator autoNumberGenerator;
    @Autowired
    private EncryptionDecryption encryptionDecryption;


    /**
     * for given emailId or PAN Investor list is returned
     * @param type
     * @param query
     * @param advUserId
     * @return
     */
    public List<UserBO> fetchInvestorList(String type, String query, int advUserId) {

        String logPrefix = "#AdvUserId : " + advUserId + " | ";

        log.debug(logPrefix + "Input : Query : " + query + "|| Type : " + type);

        List<UserBO> userlist = new ArrayList<>();
        try {
            String sqlQuery = " SELECT DISTINCT IBD.UserID, IBD.InvestorId, IBD.InvestorName, IBD.PAN, UI.Email, IA.Mobile " +
                              " FROM UserInfo UI " +
                              " JOIN InvestorBasicDetail IBD on IBD.UserID = UI.userId and IBD.AllowTransact = 'A' and IBD.InvestmentLimit = -1 " +
                              " join InvestorAddress IA on IA.InvestorID = IBD.InvestorID and IA.MailingAddress = IA.AddressType and IA.InvestorType = 'MF'" +
                              " WHERE UI.Email = ? or IBD.pan = ? " +
                              " ORDER BY IBD.InvestorName ";

            userlist = jdbcTemplate.query(sqlQuery, new Object[]{query, query}, (rs, rowNumber) -> {
                UserBO userBo = new UserBO();
                userBo.setId(rs.getInt("InvestorId"));
                userBo.setName(rs.getString("InvestorName"));
                userBo.setEmail(rs.getString("Email"));
                userBo.setMobile(rs.getString("Mobile"));
                userBo.setPan(rs.getString("Pan"));
                return userBo;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching investor list: ", e);
        }

        log.debug(logPrefix + "User List Size : " + userlist.size());
        return userlist;
    }

    /**
     * for given investor ids fetch and return portfolio list
     * @param investors
     * @param advUserId
     * @return
     */
    public List<PortFolioBO> fetchPortfolioList(List<Integer> investors, int advUserId) {

        String logPrefix = "#AdvUserId : " + advUserId + " | ";

        log.debug(logPrefix + "Input : Investors : " + Arrays.toString(investors.toArray()));
        List<PortFolioBO> portfolioList = new ArrayList<>();
        try {

            SqlParameterSource parameters = new MapSqlParameterSource("ids", investors);

            String sqlQuery = " select distinct PO.PortfolioID, PO.PortfolioName, PO.PortfolioType from PositionsInfo PI" +
                    " join InvestorAdditionalInfo IAI on IAI.HoldingProfileID = PI.HoldingProfileID and IAI.PrimaryInvestorID in (:ids) " +
                    " join PortfolioInfo PO on PO.portfolioId = PI.portfolioId and PO.Active = 1 and PI.units > 0 " +
                    " where PI.active = 'A' ";

            portfolioList = namedParameterJdbcTemplate.query(sqlQuery, parameters, (rs, rowNumber) -> {
                PortFolioBO portFolioBO = new PortFolioBO();
                portFolioBO.setId(rs.getInt("PortfolioID"));
                portFolioBO.setName(rs.getString("PortfolioName"));
                portFolioBO.setType(rs.getString("PortfolioType"));
                return portFolioBO;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching portfolio list: ", e);
        }

        log.debug(logPrefix + "PortFolio List Size : " + portfolioList.size());
        return portfolioList;
    }

    /**
     * fetch userId for given investors
     * @param investors
     * @return
     */
    public int fetchUserId(String[] investors) {

        String logPrefix = "#Investors : " + Arrays.toString(investors) + " | ";

        log.debug(logPrefix + "UserId Fetch");
        int userId = 0;
        try {
            List<String> investorIds = Arrays.asList(investors);
            SqlParameterSource parameters = new MapSqlParameterSource("ids", investorIds);

            String sqlQuery = " SELECT DISTINCT UI.UserId FROM UserInfo UI JOIN InvestorBasicDetail IBD ON UI.UserId = IBD.UserId WHERE CONVERT (VARCHAR, IBD.InvestorId, 25) IN (:ids)";

            userId = namedParameterJdbcTemplate.queryForObject(sqlQuery, parameters, Integer.class);

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching userid: ", e);
        }

        log.debug(logPrefix + "UserId : " + userId);
        return userId;
    }

    public String getCAGR(int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "CAGR Fetch");
        String cagr = "";
        try {

            String sqlQuery = " SELECT CAGR FROM USER_CAGR WHERE USERID = ? ";

            cagr = jdbcTemplate.queryForObject(sqlQuery, new Object[]{userId}, String.class);

            if(StringUtils.isEmpty(cagr))
                cagr = "N/A*";
            else if(cagr.equalsIgnoreCase("NC"))
                cagr = "Not Computed";

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching cagr: ", e);
        }

        log.debug(logPrefix + "CAGR : " + cagr);
        return cagr;
    }

    public int getUserPlId(int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "PlId Fetch");
        int plId = 0;
        try {

            String sqlQuery = " SELECT PLID FROM UserInfo WHERE USERID = ? ";

            plId = jdbcTemplate.queryForObject(sqlQuery, new Object[]{userId}, Integer.class);


        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching plId: ", e);
        }

        log.debug(logPrefix + "PlId : " + plId);
        return plId;
    }

    public List<InvestmentCostBO> getInvestmentCostList(int userId, boolean displayRedFlag) {

        String logPrefix = "#UserId : " + userId + " | ";
        List<InvestmentCostBO> costList = new ArrayList<>();

        try {

            String sqlQuery = "{ CALL WF_USER_DASHBOARD_COST_LIST ( ? ) }";

            if(displayRedFlag)
                sqlQuery = "{ call WF_USER_DASHBOARD_COST_LIST_FINATOZ ( ? ) }";

            costList = jdbcTemplate.query(sqlQuery, new Object[]{userId}, (rs, rowNumber) -> {

                InvestmentCostBO costBO = new InvestmentCostBO();

                costBO.setPortfolioId(rs.getInt("PORTFOLIOID"));
                costBO.setFolioNumber(rs.getString("FOLIONUMBER").trim());
                costBO.setSchemeCode(rs.getString("SchemeCode"));
                costBO.setInvestmentAmount(rs.getDouble("CalculatedAmount"));
                costBO.setHoldingProfileId(rs.getInt("HoldingProfileID"));
                return costBO;
            });


        } catch (Exception e) {
            log.error(logPrefix + "InvestedAmount fetch - Exception : ", e);
        }

        return costList;
    }

    public double getRedemptionValue(int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        double redAmount = 0d;

        try {

            String sqlQuery = "{ CALL WF_USER_DASHBOARD_REDEMPTION_LIST_FINATOZ ( ? ) }";

            List<Double> redAmountList = jdbcTemplate.query(sqlQuery, new Object[]{userId}, (rs, rowNumber) -> rs.getDouble("CalculatedAmount"));

            redAmount = redAmountList.stream().mapToDouble(Double::doubleValue).sum();

        } catch (Exception e) {
            log.error(logPrefix + "RedemptionAmount fetch - Exception : ", e);
        }

        return redAmount;
    }

    public DashBoardBO getMFTotalValues(int userId, int holdingProfileId, double invAmount, double redAmount, boolean displayRedFlag) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        DashBoardBO dashBoardBO = new DashBoardBO();
        DecimalFormat df = new DecimalFormat("###0.00");

        try {

            String appendHpId = "";
            if(holdingProfileId!=0)
                appendHpId = " AND IHP.HOLDINGPROFILEID = " + holdingProfileId;

            String sqlQuery = "SELECT SUM(PI.UNITS * ACN.NAVRS) AS TOTAL, SUM(PI.UNITS * ISNULL(ACN.PREVNAV, ACN.NAVRS)) AS PREVTOTAL " +
                    " FROM POSITIONSINFO PI " +
                    " INNER JOIN INVESTORHOLDINGPROFILE IHP ON PI.HOLDINGPROFILEID = IHP.HOLDINGPROFILEID " +
                    " INNER JOIN AFT_CURRENTNAV ACN ON PI.SCHEMECODE = ACN.SCHEMECODE " +
                    " WHERE PI.ACTIVE ='A' AND IHP.USERID = ? AND PI.UNITS > 0 " + appendHpId;

            dashBoardBO = jdbcTemplate.queryForObject(sqlQuery, new Object[]{userId}, (rs, rowNumber) -> {

                DashBoardBO dashBoard = new DashBoardBO();

                double currentValue = rs.getDouble("TOTAL");
                double prevValue = rs.getDouble("PREVTOTAL");
                double gain = currentValue - invAmount;
                double change = currentValue - prevValue;
                double gainPercent = 0d;
                double changePercent = 0d;

                if(displayRedFlag && redAmount!=0)
                    gain += redAmount;
                if(gain!=0 && invAmount!=0)
                    gainPercent = (gain / invAmount) * 100;
                if(change!=0 && prevValue!=0)
                    changePercent = (change / prevValue) * 100;


                dashBoard.setCurrentValueOfInvestment(new BigDecimal(df.format(currentValue)));
                dashBoard.setInvestedAmount(new BigDecimal(df.format(invAmount)));
                dashBoard.setGainAmount(new BigDecimal(df.format(gain)));
                dashBoard.setGainPercentage(df.format(gainPercent));
                dashBoard.setChangeAmount(new BigDecimal(df.format(change)));
                dashBoard.setChangePercentage(df.format(changePercent));

                return dashBoard;
            });

        } catch (Exception e) {
            log.error(logPrefix + "InvestedAmount fetch - Exception : ", e);
        }

        return dashBoardBO;
    }

    public String getHoldingprofileName(int holdingProfileId) {

        String logPrefix = "#HoldingprofileIxd : " + holdingProfileId + " | ";

        log.debug(logPrefix + "HoldingProfileName fetch");

        AtomicReference<String> holdingProfileName = new AtomicReference<>("");

        try {
            String sqlQuery = " SELECT IBD.InvestorName, IAI.RelationshipId FROM InvestorAdditionalInfo IAI" +
                    " INNER JOIN InvestorHoldingProfile IHP ON IHP.HoldingProfileId = IAI.HoldingProfileId  " +
                    " INNER JOIN InvestorBasicDetail IBD ON IBD.InvestorId = IAI.AssociatedInvestorId " +
                    " WHERE IHP.HoldingProfileId = ? ORDER BY IAI.RelationshipId ";

            List<Integer> status = jdbcTemplate.query(sqlQuery, new Object[]{holdingProfileId}, (rs, rowNumber) -> {

                String relationshipId = rs.getString("RelationshipId");
                String nameConcat = "";

                if(StringUtils.isNotEmpty(relationshipId) && !relationshipId.trim().equalsIgnoreCase(BPConstants.PRIMARY_RELATIONSHIP_ID)) {
                    if (relationshipId.equalsIgnoreCase(BPConstants.GUARDIAN_RELATIONSHIP_ID))
                        nameConcat = " Rep by U/G ";
                    else
                        nameConcat = " / ";
                }

                holdingProfileName.set(holdingProfileName.get() + nameConcat + rs.getString("InvestorName"));

                return 1;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching holdingProfileName : ", e);
        }

        return holdingProfileName.get();
    }


    public PrivateLabelBO getPrivateLabelBOByUserId(int UserId) throws Exception {

        PrivateLabelBO returnPLBO = new PrivateLabelBO();
        try {
            HashMap<Integer, PrivateLabelBO> PLBOHash = getPrivateLabelBO(String.valueOf(UserId), 3);
            Set<Integer> hashKeySet = PLBOHash.keySet();

            //Only One Key will be there in this hashKeySet, because UserId is unique in DB
            for (int key : hashKeySet) {
                returnPLBO = PLBOHash.get(key);
            }
        } catch (Exception daoEX) {
            log.error("PLBO BY USERID $ I/P: " + UserId + " #EXCPN: " + daoEX);
            throw daoEX;
        }
        return returnPLBO;
    }

    public HashMap<Integer, PrivateLabelBO> getPrivateLabelBO(String q, int typeCode) {
        HashMap<Integer, PrivateLabelBO> plBo = new HashMap<Integer, PrivateLabelBO>();
        String sw1 = "";

        try {

            if (typeCode == 1) {
                sw1 = " and p1.pId='" + q + "'";
            } else if (typeCode == 2) {
                sw1 = " join userInfo u1 on u1.pid=p1.pId and  u1.pId='" + q + "' ";
            } else if (typeCode == 3) {
                sw1 = " join userInfo u1 on u1.pid=p1.pId and  u1.userId='" + q + "' ";
            } else if (typeCode == 4) {
                sw1 = " join userInfo u1 on u1.pid=p1.pId and  u1.email='" + q + "' ";
            }

            String sql = "select p1.pId,p1.Name " +
                    ",p2.mailFrom" +
                    ",p2.mailPassword " +
                    ",p2.mailCc" +
                    ",p2.mailBcc " +
                    ",p2.mailSupport " +
                    ",p2.mailFrom " +
                    ",p2.mailHost " +
                    ",p2.mailPort " +
                    ",p2.mailContact " +
                    ",p2.mailContactPhone " +
                    ",p2.mailRegards " +
                    ",p2.smsUserId " +
                    ",p2.smsPassword  " +
                    ",p2.mailSenderSign  " +
                    ",p2.webTitle " +
                    ", p1.LabelShortName, p1.URLShortName, p1.URL " +
                    ", p2.mailTransaction " +
                    ", p2.mailtransactionpassword " +
                    ", p1.pType,p1.logo " +
                    " from p_master p1 join p_mail_sms p2 on p1.pId=p2.pId " + sw1;

            jdbcTemplate.query(sql, new Object[]{}, (rs, rowNumber) -> {

                PrivateLabelBO pBo = new PrivateLabelBO();

                pBo.setPLId(rs.getInt("pId"));
                pBo.setPlName(rs.getString("name"));
                pBo.setMailFrom(rs.getString("mailFrom"));
                pBo.setMailpassword(rs.getString("mailPassword"));
                pBo.setMailCc(rs.getString("mailCc"));
                pBo.setMailBcc(rs.getString("mailBcc"));
                pBo.setMailSupport(rs.getString("mailSupport"));
                pBo.setMailHost(rs.getString("mailHost"));
                pBo.setMailPort(rs.getString("mailPort"));
                pBo.setSmsUserId(rs.getString("smsUserId"));
                pBo.setSmsPassword(rs.getString("smsPassword"));
                pBo.setMailRegards(rs.getString("mailRegards"));
                pBo.setMailContact(rs.getString("mailContact"));
                pBo.setMailContactPhone(rs.getString("mailContactPhone"));
                pBo.setMailSign(rs.getString("mailSenderSign"));
                pBo.setWebTitle(rs.getString("webTitle"));

                pBo.setMailTransaction(rs.getString("mailTransaction").trim());
                pBo.setMailTransactionPaswd(rs.getString("mailtransactionpassword").trim());

                pBo.setLabelShortName(rs.getString("LabelShortName"));
                pBo.setURLShortName(rs.getString("URLShortName"));
                pBo.setURL(rs.getString("URL"));
                pBo.setPlType(rs.getString("pType"));
                pBo.setLogo(rs.getString("logo"));
                pBo.setNewLogo(rs.getString("logo"));

                plBo.put(pBo.getPLId(), pBo);

                return plBo;

                });

        } catch (Exception e) {
            log.error("Exception while fetching privatelabelbo: ", e);
        }
        return plBo;
    }

    public List<UserBO> getUserMailId(int userId) {

        String logPrefix = " | UserID: " + userId + " | ";
        log.debug(logPrefix + " get user mail id. ");

        List<UserBO> userlist = new ArrayList<>();

        try {

            String sql = " SELECT UI.NAME, UI.EMAIL, UI.MOBILENUMBER, UI.pid FROM USERINFO UI WHERE UI.USERID = ? ";

            userlist = jdbcTemplate.query(sql, new Object[]{userId}, (rs, rowNumber) -> {

                UserBO userBO = new UserBO();
                userBO.setName(rs.getString("NAME"));
                userBO.setEmail(rs.getString("EMAIL"));
                userBO.setMobile(rs.getString("MOBILENUMBER"));
                userBO.setPlId(rs.getInt("pid"));

                return userBO;
            });

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        log.debug(logPrefix + "User Info List size : " + userlist.size());
        return userlist;
    }
    public int userRegistrationInsert(UserRequest userRequest) {

        String logPrefix = "#UserId :  ";

        log.debug(logPrefix + "Insert Userinfo");
        int userid=0;
        String encryptUserPassword;

        try {
            encryptUserPassword = encryptionDecryption.Encryption(userRequest.getPassword());

            KeyHolder keyHolder = new GeneratedKeyHolder();

            String sqlQuery = " INSERT INTO userinfo (password, name, email, mobileNumber, region,userType, verified, partnerid, referraltype " +
                    " ,referralemail, flag, pid, smsFlag, userreferred,remarks,jTag,reftag,usercomment,ipaddress," +
                    " lockFlag,lockcount,superid,created_user,updated_user)  " +
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,currval('userinfo_userid_seq'),currval('userinfo_userid_seq'))  ";

               jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[] { "userid" });
                ps.setString(1, encryptUserPassword);
                ps.setString(2, "");
                ps.setString(3, userRequest.getEmailId());
                ps.setString(4, "");
                ps.setString(5, "IN");
                ps.setString(6, "RU");
                ps.setString(7, "A");
                ps.setInt(8, userRequest.getPartnerId());//partnerid
                ps.setString(9, "");
                ps.setString(10, "");
                ps.setString(11, "A");
                ps.setInt(12, userRequest.getPid());
                ps.setString(13, "A");
                ps.setString(14, "");
                ps.setString(15, "");
                ps.setString(16, "");
                ps.setString(17, "");
                ps.setString(18, "");
                ps.setString(19, "");
                ps.setString(20, "A");
                ps.setInt(21, 0);
                ps.setInt(22, 0);
                return ps;

            }, keyHolder);
            userid=keyHolder.getKey().intValue();

        } catch (Exception e) {
            log.error(logPrefix + "Exception in user create insertion: ", e);
        }

        log.debug(logPrefix + "userid : " + userid);
        return userid;
    }
    public int updateUserEmail(UserRequest userRequest) {

        String logPrefix = "#UserId :  ";

        log.debug(logPrefix + "update Userinfo EmailId");
        int userid=0;
        int affected_rows = 0;




        try {

            String sqlQuery = " UPDATE userinfo SET EMAIL=? WHERE MOBILENUMBER=? AND VERIFIED='TRUE' AND FLAG='A'";

            affected_rows=jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery);
                ps.setString(1, userRequest.getEmailId());
                ps.setString(2, userRequest.getMobileno());
                return ps;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in userinfo email id update: ", e);
        }

        log.debug(logPrefix + "userid : " + userid);
        return affected_rows;
    }
    public int updatePassword(UserRequest userRequest) {

        String logPrefix = "#UserId :  ";

        log.debug(logPrefix + "update Userinfo password");
        int userid=0;
        int affected_rows = 0;
       String  encryptUserPassword = encryptionDecryption.Encryption(userRequest.getPassword());




        try {

            String sqlQuery = " UPDATE userinfo SET PASSWORD=? WHERE EMAIL=? AND VERIFIED='TRUE' AND FLAG='A'";

            affected_rows=jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery);
                ps.setString(1, encryptUserPassword);
                ps.setString(2, userRequest.getEmailId());
                return ps;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in userinfo password update: ", e);
        }

        log.debug(logPrefix + "userid : " + userid);
        return userid;
    }
    public void updateVerification(String type, int userid) {

        String logPrefix = "#UserId :  ";

        log.debug(logPrefix + "update Userinfo");
        int affected_rows = 0;
        String modifyColumns="";
        if(type.equals("mobile")) modifyColumns=" mverify=? ";
        if(type.equals("email")) modifyColumns=" everify=? ";

        try {

            String sqlQuery = " UPDATE userinfo SET "+modifyColumns+" WHERE USERID=? VERIFIED='TRUE' AND FLAG='A'";
            int finalUserid = userid;
            affected_rows=jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery);
                ps.setString(1,"TRUE");
                ps.setInt(2, finalUserid);
                return ps;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in userinfo update: ", e);
        }
        if(affected_rows==0) userid=-1;
        log.debug(logPrefix + "userid : " + userid);
    }
    public int userMobileRegistrationInsert(UserRequest userRequest) {

        String logPrefix = "#UserId :  ";

        log.debug(logPrefix + "Insert Userinfo using mobile no");
        int userid=0;

        try {


            KeyHolder keyHolder = new GeneratedKeyHolder();

            String sqlQuery = " INSERT INTO userinfo (password_hash,first_name,last_name, email, mobileNumber, region,userType, verified, partnerid, " + //9 columns
                    " flag, pid, smsflag,emailflag, referredby,usercomment,ipaddress, lockFlag,lockcount,superid,created_by,updated_by)  " +
                    " VALUES( ?,?,?,?,?,?,?,?,?," +
                             "?,?,?,?,?,?,?,?,?," +
                             "?,currval('userinfo_userid_seq'),currval('userinfo_userid_seq'))  ";
//,dob,gender,profile_picture
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[] { "userid" });
                ps.setString(1, StringUtils.isEmpty(userRequest.getEncodePassword())?"":userRequest.getEncodePassword());
                ps.setString(2, StringUtils.isEmpty(userRequest.getFirstname())?"":userRequest.getFirstname());
                ps.setString(3, StringUtils.isEmpty(userRequest.getLastname())?"":userRequest.getLastname());
                ps.setString(4, StringUtils.isEmpty(userRequest.getEmailId())?"":userRequest.getEmailId());
                ps.setString(5, StringUtils.isEmpty(userRequest.getMobileno())?"":userRequest.getMobileno());
                ps.setString(6,"IN" );
                ps.setString(7, StringUtils.isEmpty(userRequest.getUserType())?"RU":userRequest.getUserType());
                ps.setString(8, "TRUE");
                ps.setInt(9, userRequest.getPartnerId());//partnerid
                ps.setString(10, "A"); //flag
                ps.setInt(11, userRequest.getPid()); //pid
                ps.setInt(12, 0); //smsflag
                ps.setInt(13, 0); //emailflag
                ps.setString(14, userRequest.getReferenceId()); //referredby
                ps.setString(15, ""); //usercomment
                ps.setString(16, userRequest.getIpAddress()); //ipaddress
                ps.setString(17, ""); //lockFlag
                ps.setInt(18, 0); //lockcount
                ps.setString(19, ""); //superid
                return ps;

            }, keyHolder);
            userid=keyHolder.getKey().intValue();

        } catch (Exception e) {
            e.printStackTrace();
           //log.error(logPrefix + "Exception in user create insertion:: ", e);
        }

        log.debug(logPrefix + "userid : " + userid);
        return userid;
    }
    public void otpGenInsert(UserRequest userRequest,int userid) {
        String logPrefix = "#UserId :  ";
        log.debug(logPrefix + "Insert OTP_GEN_VERIFICATION ");
        boolean isInserted=false;
        try {
            String sqlQuery = " INSERT INTO OTP_GEN_VERIFICATION (userId,msgType,productType,referenceId,otp,created_at,created_by,updated_at,updated_by,flag,expires_at,status)  " +
                    " VALUES(?, ?, ?, ?, ?,NOW(),?,NOW(),?,'A',NOW()+ interval '5 minute','A') ";

         int count= jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery);
                ps.setInt(1, userid);
                ps.setString(2, userRequest.getMessageType());
                ps.setString(3, userRequest.getProductType());
                ps.setString(4, userRequest.getReferenceId());
                ps.setString(5, userRequest.getSmsOTP());
                ps.setInt(6, userid);
                ps.setInt(7, userid);
                return ps;
            });
         if(count>0){
          isInserted=true;
         }

        } catch (Exception e) {
            log.error(logPrefix + "Exception in OTP_GEN_VERIFICATION create insertion: ", e);
        }

        log.debug(logPrefix + "isInserted : " + isInserted);
    }

    public UserInfoBO getUserInfoBO(int typeCode,UserRequest userRequest) {

        String logPrefix = " | typeCode: " + typeCode + " | ";
        log.debug(logPrefix + " get user mail id. ");
        UserInfoBO userInfoBO=new UserInfoBO();
        String appendSql="";
        String appendValues="";
        if(typeCode==1){
            appendSql=" UI.EMAIL=?";
            appendValues=userRequest.getEmailId();
        }else if(typeCode==2) {
            appendSql = " UI.MOBILENUMBER=? ";
            appendValues=userRequest.getMobileno();
        }
        else if(typeCode==3){
            appendSql=" UI.EMAIL=? AND UI.MOBILENUMBER=?";
            appendValues= userRequest.getEmailId()+","+userRequest.getMobileno();

        }
        else if(typeCode==4){
            appendSql="UI.USERID=?";
        }
        if(typeCode==5){
            appendSql=" UI.MOBILENUMBER=? and UI.EMAIL=''";
            appendValues=userRequest.getMobileno();
        }
        try {

            String sql = " SELECT UI.USERID,UI.first_name,UI.password_hash, UI.EMAIL, UI.MOBILENUMBER, UI.PID,UI.mverify,UI.everify,UI.lockcount FROM USERINFO UI WHERE "+appendSql+" AND UI.VERIFIED='TRUE' and UI.FLAG='A' ";
            log.info("sql--"+sql);
                   jdbcTemplate.query(sql, new Object[]{typeCode==4?userRequest.getUserId():appendValues}, (rs, rowNumber) -> {
                    userInfoBO.setUserId(rs.getInt("USERID"));
                    userInfoBO.setName(rs.getString("first_name"));
                    userInfoBO.setPassword(rs.getString("password_hash"));
                    userInfoBO.setEmailId(rs.getString("EMAIL"));
                    userInfoBO.setMobile(rs.getString("MOBILENUMBER"));
                    userInfoBO.setPId(rs.getInt("PID"));
                    userInfoBO.setMverify(rs.getString("mverify"));
                    userInfoBO.setEverify(rs.getString("everify"));
                    userInfoBO.setLockCount(rs.getInt("lockcount"));
                return userInfoBO;
            });

        }

        catch (Exception e) {
            log.error("Exception in get UserInfo: ", e);
        }
        log.debug(logPrefix + "User Info : " );
        return userInfoBO;
    }
    public OTPBO getOTPDetailsToVerify(int userId,String referenceId) throws Exception {
        String logPrefix = "Fetch OTP details from db | UserId: " + userId + " ";

        log.debug(logPrefix + " getOTPDetailsToVerify ");
        OTPBO otpbo=new OTPBO();
        String appendSql="";
        if(referenceId!=""){
          appendSql=" REFERENCEID ='"+referenceId+"' ";
        }
        try {
            String sql = " SELECT OTP, PRODUCTTYPE, MESGTYPE FROM OTP_GEN_VERIFICATION WHERE USERID = ? "+appendSql+" AND FLAG = 'A'";
            jdbcTemplate.query(sql, new Object[]{userId}, (rs, rowNumber) -> {
                otpbo.setSmsOTP(rs.getString("OTP"));
                otpbo.setProductType(rs.getString("PRODUCTTYPE"));
                otpbo.setMsgType(rs.getString("MESGTYPE"));
                return otpbo;
            });

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        log.debug(logPrefix + "OTP_GEN_VERIFICATION : get sms otp "+otpbo.getSmsOTP() );
        return otpbo;
    }

    public boolean isOTPExpired(OTPBO otpbo) throws Exception {
        String logPrefix = "Fetch OTP details from db | UserId: " + otpbo.getUserId() + " ";
        boolean isExpried=false;
        log.debug(logPrefix + " getOTPDetailsToVerify ");
        int userId=otpbo.getUserId();
         String referenceId=otpbo.getReferenceId();
        log.info(referenceId+"userId---"+userId);
        try {
            //String sqlQuery = " SELECT otp,REFERENCEID FROM OTP_GEN_VERIFICATION WHERE USERID = ? AND REFERENCEID=?  AND FLAG = 'A'";
             String sqlQuery = " SELECT otp FROM OTP_GEN_VERIFICATION WHERE USERID = ? AND REFERENCEID=? AND expires_at>now() AND FLAG = 'A'";
            log.info("userId---"+sqlQuery);

            jdbcTemplate.query(sqlQuery, new Object[]{userId, referenceId}, (rs, rowNumber) -> {
                otpbo.setSmsOTP(rs.getString("OTP"));
                return otpbo;
            });
            if(!StringUtils.isEmpty(otpbo.getSmsOTP())){
                isExpried=true;
            }

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        log.debug(logPrefix + "OTP_GEN_VERIFICATION isExpried : " +isExpried);
        return isExpried;
    }


}

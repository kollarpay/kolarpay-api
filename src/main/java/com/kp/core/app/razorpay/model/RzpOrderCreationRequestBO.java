package com.kp.core.app.razorpay.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.math.BigInteger;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RzpOrderCreationRequestBO {

    private BigInteger amount;
    private String method;
    private String receipt;
    private String currency;
    @SerializedName("bank_account")
    @JsonProperty("bank_account")
    private BankAccountBO bankAccount;
    private OrderCreationNotesBO notes;


    @Getter
    @Setter
    public static class BankAccountBO {

        @SerializedName("account_number")
        @JsonProperty("account_number")
        private String accountNumber;
        private String name;
        private String ifsc;

        public BankAccountBO(String accountNumber, String name, String ifsc) {
            this.accountNumber = accountNumber;
            this.name = name;
            this.ifsc = ifsc;
        }


    }


    @Getter
    @Setter
    public static class OrderCreationNotesBO {
        private String paymentId;
        private String productType;

    }


}

package com.kp.core.app.controller.Payment;

import com.kp.core.app.config.GeneralConfig;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.CreateMandataBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.PaymentStatusBO;
import com.kp.core.app.model.request.CashFreeOrderBO;
import com.kp.core.app.service.Investment.InvestorInfoService;
import com.kp.core.app.service.Payment.PaymentService;
import com.kp.core.app.utils.APIGeneralUtil;
import com.kp.core.app.utils.GeneralUtil;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;

/**
 * Internal APIs used in fi-payment-gateway
 * accessed by passing unique api access key in header
 */

@RestController
@Slf4j
@RequestMapping("/internal")
public class InternalDetailsController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private InvestorInfoService investorInfoService;

    @Autowired
    private Gson gson;

    private GeneralConfig config = ConfigFactory.create(GeneralConfig.class);

    /**
     * returns payment information based on cartId
     * called during payment-initiate
     * @param headers
     * @param cartId
     * @param request
     * @return
     */
    @GetMapping(path = "/payment-info", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> paymentInfo(@RequestHeader MultiValueMap<String, Object> headers,
                                                        @RequestParam(name = "cartId", required = false) String cartId,
                                                        @RequestParam(name = "paymentId", required = false) String paymentId,
                                                        @RequestParam(name = "retry", required = false) boolean retry,
                                                        HttpServletRequest request) {

        log.debug("#payment-info GET API Request | #CartId : " + cartId);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "PaymentInfoBo");

        try {

            String xApiKey = APIGeneralUtil.getHeader(headers, "x-api-key");

            if(StringUtils.isNotEmpty(xApiKey) && xApiKey.equals(config.internalAPIKey())) {

                globalResponseBO = paymentService.getMFPaymentInfo(cartId, paymentId, generalUtil.getIPaddress(request), retry);
            }

        } catch (Exception e) {
            log.error("#CartId : " + cartId + "| Exception : ", e);
        }
        log.info("#payment-info GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * updates the payment status in multiple tables
     * called after payment completion (success/failure)
     * @param headers
     * @param payload
     * @return
     */
    @PostMapping(path = "/payment-status", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> paymentStatus(@RequestHeader MultiValueMap<String, Object> headers,
                                                          @RequestBody String payload) {

        log.debug("#payment-info GET API Request | #Payload : " + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "SimpleResponse");

        try {

            String xApiKey = APIGeneralUtil.getHeader(headers, "x-api-key");
            int userId = 444444; // :todo user id need to get --  Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));

            if(StringUtils.isNotEmpty(xApiKey) && xApiKey.equals(config.internalAPIKey())) {

                PaymentStatusBO paymentStatusBO = gson.fromJson(payload, PaymentStatusBO.class);
                if(paymentStatusBO.getPaidThru().equals("TPSL"))
                    globalResponseBO = paymentService.updateMFPaymentStatus(userId, paymentStatusBO);
                else if(paymentStatusBO.getPaidThru().equals("NACH"))
                    globalResponseBO = paymentService.updateMandateMFPaymentStatus(userId, paymentStatusBO);
                else if(paymentStatusBO.getPaidThru().equals("NEFT"))
                    globalResponseBO = paymentService.updateNEFTMFPaymentStatus(userId, paymentStatusBO);
                else
                    log.error("invalid paid thru"); //:todo error list need to add here
            }

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        log.debug("#payment-info GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @PostMapping(path = "/pay-token", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createMandate(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#mandates create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "PaymentGenToken");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CreateMandataBO mandataBO = gson.fromJson(payload, CreateMandataBO.class);
                globalResponseBO = investorInfoService.createNewMandate(mandataBO, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#mandates create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @PostMapping(path = "/order-token", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createOrderToken(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#mandates create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "PaymentGenToken");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CashFreeOrderBO cashFreeOrderBO = gson.fromJson(payload, CashFreeOrderBO.class);
               // globalResponseBO = investorInfoService.createOrder(createOrderRequest, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#mandates create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @GetMapping(path = "/investor/holding-profiles", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> holdingProfiles(@RequestHeader MultiValueMap<String, Object> headers,
                                                            @RequestParam("holdingProfileId") String holdingProfileId) throws Exception {

        log.debug("#holding-profiles GET API Request | HoldingProfileId : " + holdingProfileId);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "HoldingProfile");

        try {
            String xApiKey  = APIGeneralUtil.getHeader(headers, "x-api-key");

            if(StringUtils.isNotEmpty(xApiKey) && xApiKey.equals(config.internalAPIKey())) {

                globalResponseBO = investorInfoService.investorHoldingProfileList(holdingProfileId, 0);
            }

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        log.debug("#holding-profiles GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

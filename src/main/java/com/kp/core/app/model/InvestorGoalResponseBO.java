package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class InvestorGoalResponseBO {

    private String goalId;
    private double calculatedSipAmount = 0.0d;
    private String calculatedSipAmountFormatted = "";
    private double calculatedOtiAmount = 0.0d;
    private String calculatedOtiAmountFormatted = "";
    private double targetAmount = 0.0d;
    private String targetAmountFormatted = "";
    private double projectedAmount = 0.0d;
    private String projectedAmountFormatted = "";
    private double sipAmount = 0.0d;
    private String sipAmountFormatted = "";
    private double otiAmount = 0.0d;
    private String otiAmountFormatted = "";
    private String cagr = "0";
    private String cagrInfo = "0";
}

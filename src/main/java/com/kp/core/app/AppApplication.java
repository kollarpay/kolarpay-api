package com.kp.core.app;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Created by rsuresh on 25-09-2021
 */

@SpringBootApplication
@Slf4j

public class AppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);
        log.info("******* KP CORE Application Starts ********");
    }
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}

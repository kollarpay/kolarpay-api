package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortfolioSummaryBO {

    private String description;
    private String currentAllocation;
    private String proposedAllocation;
    private String comment;

    private String category;
    private String subCategory;
    private int currentAmount;
    private String currentAmountFormatted;
    private int proposedAmount;
    private String proposedAmountFormatted;
    private String schemeCode;
    private double rating;
    private int ratingFormatted;

}

package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.utils.APIConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class GlobalResponseBO {

    private Integer code = 200;
    private String desc = "Success";
    private List<ErrorBO> errors = new ArrayList<>();
    private Boolean success = Boolean.FALSE;
    private String type = "";
    private String name = "";
    private Object data = new Object();

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<ErrorBO> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorBO> errors) {
        this.errors = errors;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public GlobalResponseBO(String error) {
        this.code = APIConstants.BAD_REQUEST;
        this.desc = APIConstants.BAD_REQUEST_STRING;
        this.errors = Arrays.asList(new ErrorBO("Invalid",error));
    }

    public GlobalResponseBO() {
        this.code = APIConstants.SERVER_ERROR_INT;
        this.desc = APIConstants.SERVER_ERROR_STRING;
    }

    public GlobalResponseBO(ResponseDataObjectType type, String name) {
        this.setName(name);
        this.setType(type.name());
        this.setData(type == ResponseDataObjectType.object ? new Object() : new ArrayList<>());
        this.setSuccess(false);
        this.setDesc(APIConstants.SERVER_ERROR_STRING);
        this.setCode(APIConstants.SERVER_ERROR_INT);
    }



    @Override
    public String toString() {
        return "GlobalResponseBo{" +
                "code=" + code +
                ", desc='" + desc + '\'' +
                ", errors=" + errors +
                ", success=" + success +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", data=" + data +
                '}';
    }

    public void setErrorMsg(String message) {
        this.errors= Arrays.asList(new ErrorBO("Execption",message));
    }
}

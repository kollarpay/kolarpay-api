package com.kp.core.app.dao;


import com.kp.core.app.config.GeneralConfig;
import com.kp.core.app.model.IdDesc;
import com.kp.core.app.model.NameValueBO;
import com.kp.core.app.model.ProductSearchFormBO;
import com.kp.core.app.model.SchemeDetailBO;
import com.kp.core.app.utils.BPConstants;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Slf4j
@Repository
public class SchemeSearchDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    /**
     * get list of AMC's active with us
     * @param advUserId
     * @return
     */
    public List<IdDesc> getAmcList(int advUserId) {

        String logPrefix = "#AdvuserId : " + advUserId + " | ";
        log.debug(logPrefix + "AMC List fetch");
        List<IdDesc> amcList = new ArrayList<>();
        try {

            String sqlQuery = " SELECT AMC_Code, AMC AS AMC_Name FROM WealthAMCMaster WHERE UPD_FLAG='A' ";

            amcList = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> {

                IdDesc idDesc = new IdDesc();
                idDesc.setKey(rs.getString("AMC_Code"));
                idDesc.setValue(rs.getString("AMC_Name"));
                return idDesc;
            });
        } catch (Exception e) {
            log.error(logPrefix + "fetch AmcList Exception : ", e);
        }
        log.debug(logPrefix + "AMC List size : " + amcList.size());
        return amcList;
    }

    public List<NameValueBO> getAmcListInNameValue(String investorType, int advUserId) {

        String logPrefix = "#AdvuserId : " + advUserId + " | ";
        log.debug(logPrefix + "AMC List fetch");
        List<NameValueBO> amcList = new ArrayList<>();
        try {

            String appendOERestriction = "";
            if(StringUtils.isNotEmpty(investorType) && investorType.equalsIgnoreCase(BPConstants.ONLINE_ENROLLMENT))
                appendOERestriction = " AND AMC_Code NOT IN (" + BPConstants.OE_AMCS + ") ";

            String sqlQuery = " SELECT AMC_Code, FUND_SNAME FROM WealthAMCMaster WHERE UPD_FLAG='A' " + appendOERestriction +
                              " ORDER BY FUND_SNAME ";

            amcList = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> {

                NameValueBO nameValueBO = new NameValueBO();
                nameValueBO.setValue(rs.getString("AMC_Code"));
                nameValueBO.setName(rs.getString("FUND_SNAME").replace("MF", "").trim());
                return nameValueBO;
            });
        } catch (Exception e) {
            log.error(logPrefix + "fetch AmcList Exception : ", e);
        }
        log.debug(logPrefix + "AMC List size : " + amcList.size());
        return amcList;
    }

    /**
     * get list of scheme categories
     * @param advUserId
     * @return
     */
    public List<IdDesc> getCategoryList(int advUserId) {

        String logPrefix = "#AdvuserId : " + advUserId + " | ";
        log.debug(logPrefix + "Category List fetch");
        List<IdDesc> categoryList = new ArrayList<>();
        try {

            String sqlQuery = " select distinct AssetClass from AdvisoryReportAssetClass ";

            categoryList = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> {

                IdDesc idDesc = new IdDesc();
                idDesc.setDesc(rs.getString("AssetClass"));
                return idDesc;
            });
        } catch (Exception e) {
            log.error(logPrefix + "fetch CategoryList Exception : ", e);
        }
        log.debug(logPrefix + "Category List size : " + categoryList.size());
        return categoryList;
    }

    /**
     * get list of scheme subCategories
     * @param advUserId
     * @return
     */
    public List<IdDesc> getSubCategoryList(int advUserId) {

        String logPrefix = "#AdvuserId : " + advUserId + " | ";
        log.debug(logPrefix + "SubCategory List fetch");
        List<IdDesc> subCategoryList = new ArrayList<>();
        try {

            String sqlQuery = " select distinct Category from AdvisoryReportAssetClass ";

            subCategoryList = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> {

                IdDesc idDesc = new IdDesc();
                idDesc.setValue(rs.getString("Category"));
                idDesc.setKey(rs.getString("Category"));
                return idDesc;
            });
        } catch (Exception e) {
            log.error(logPrefix + "fetch SubCategoryList Exception : ", e);
        }
        log.debug(logPrefix + "Subcategory List size : " + subCategoryList.size());
        return subCategoryList;
    }

    /**
     * get list of schemes applying given conditions
     * @param amcCode
     * @param category
     * @param subCategory
     * @param queryStr
     * @param searchType
     * @param investorIds
     * @param order
     * @param advUserId
     * @return
     */
    public List<SchemeDetailBO> getSchemeSearchList(String amcCode, String category, String subCategory, String queryStr, String searchType, String[] investorIds, String order, int advUserId) {

        GeneralConfig configuration = ConfigFactory.create(GeneralConfig.class);

        String logPrefix = "#Investors : " + Arrays.toString(investorIds) + " | #AdvuserId : " + advUserId + " | #Filter : " + searchType + " | ";
        log.debug(logPrefix + "Scheme List fetch");
        List<SchemeDetailBO> schemeList = new ArrayList<>();
        String  appendAMC = "",
                appendCategory = "",
                appendSubCategory = "",
                appendSchemeQuery = "",
                appendNRICond = "",
                appendUSCANADAAMC = "",
                orderByQuery = "",
                appendSearchType = "",
                SIP_ShowUp_Schemes = configuration.sipShowUpSchemes();
        try {

            if(StringUtils.isNotEmpty(searchType)) {
                if (searchType.equalsIgnoreCase(BPConstants.SCHEME_SEARCH_RATED))
                    appendSearchType = " AND WS.FI_STAR>0 ";
                else if (searchType.equalsIgnoreCase(BPConstants.SCHEME_SEARCH_DIVIDEND))
                    appendSearchType = " AND SD.OPT_CODE = 2 ";
                else if (searchType.equalsIgnoreCase(BPConstants.SCHEME_SEARCH_GROWTH))
                    appendSearchType = " AND SD.OPT_CODE = 1 ";
            }

            /** NRI based condition */
            HashMap<String, String> nriDetails = getSchemeRestrictions(investorIds);

            String isNRI = nriDetails.get("isNRI");
            if(StringUtils.isEmpty(isNRI) || Integer.parseInt(isNRI)>0)
                appendNRICond = " INNER JOIN AFT_INVST_ELIGIBILITY AIE ON SD.PRIMARY_FD_CODE = AIE.SCHEMECODE AND AIE.ELIG_CODE = 5 ";

            String restrictedAMCs = nriDetails.get("restrictedAMC");
            if(StringUtils.isNotEmpty(restrictedAMCs))
                appendUSCANADAAMC = " AND WS.AMC_CODE NOT IN (" +restrictedAMCs + ") ";

            if(nriDetails.containsKey("restrictOEAMC"))
                appendUSCANADAAMC = " AND WS.AMC_CODE NOT IN (" +nriDetails.get("restrictOEAMC") + ") ";

            /** filter based on AMC code */
            if (StringUtils.isNotEmpty(amcCode) && !amcCode.equalsIgnoreCase("ALL") && !amcCode.equals("0"))
                appendAMC = " AND WS.AMC_CODE = " + amcCode;
            category = (category == null ? "" : category.trim());

            /** filter based on category */
            if (!"ALL".equalsIgnoreCase(category) && !"0".equals(category) && !"".equals(category))
                appendCategory = " AND ARA.ASSETCLASS = '" + category + "' ";

            /** filter based on subCategory */
            if (!StringUtils.isEmpty(subCategory) )
                appendSubCategory = " AND ARA.Category = '"+ subCategory+"'";

            /** filter based on search string */
            if (StringUtils.isNotEmpty(queryStr))
                appendSchemeQuery = " AND SM.SCHEME_NAME LIKE '%" + queryStr + "%' ";

            orderByQuery = " WS.FI_STAR DESC, SM.SCHEME_NAME ";
            switch (order) {
                case "0":
                    orderByQuery = " SM.SCHEME_NAME ";
                    break;
                case "1":
                    orderByQuery = " SM.SCHEME_NAME desc";
                    break;
                case "2":
                    orderByQuery = " WS.FI_STAR ";
                    break;
                case "3":
                    orderByQuery = " WS.FI_STAR DESC, SM.SCHEME_NAME ";
                    break;
                default:
                    break;
            }

            String sqlQuery;
            if(StringUtils.isNotEmpty(searchType) && searchType.equalsIgnoreCase(BPConstants.SCHEME_SEARCH_FI))
                sqlQuery = " SELECT SM.AMC_CODE, SM.SCHEMECODE, SM.SCHEME_NAME, CN.NAVRS CURRENTNAV, (CONVERT(VARCHAR(25),SD.NFO_OPEN,105)) LAUNCHDATE, SD.SIP, SD.TYPE_CODE, " +
                        " CONVERT(VARCHAR(11), CN.NAVDATE, 106) AS NAVDATE, WS.FI_STAR, WS.SHOWUP, SD.STATUS, SD.MININVT, SD.INC_INVEST, SD.CLASSCODE,  " +
                        " SIPD.SCHEMECODE SIPSCHEME, SIPD.SIPMININVEST, SIPD.SIPADDNINVEST, ARA.AssetClass AS CATEGORY, ARA.Category as SUBCATEGORY " +
                        " FROM AFT_SCHEME_MASTER SM " +
                        " INNER JOIN FI_SELECT_FUNDS FS ON SM.SCHEMECODE = FS.SCHEMECODE AND FS.ACTIVE ='A' " +
                        " INNER JOIN WealthAMCMaster WAM ON WAM.AMC_CODE = SM.AMC_CODE " +
                        " INNER JOIN WEALTHSCHEMEMASTER WS ON WS.SCHEMECODE=SM.SCHEMECODE and WS.UPD_FLAG='A' " + appendUSCANADAAMC + appendAMC +
                        " AND ( WS.SHOWUP = 'T' OR FS.SCHEMECODE = '" + BPConstants.IDFC_PE_G_SCODE + "' ) AND (WS.SHOWUP = 'T' OR FS.SCHEMECODE in (" + BPConstants.SIP_SHOWUP_FI_RECOMEND + "))" +
                        " INNER JOIN AMCBANKMASTER ABM ON WS.SCHEMECODE = ABM.SchemeCode " +
                        " INNER JOIN AFT_SCHEME_DETAILS SD ON SM.SCHEMECODE=SD.SCHEMECODE AND SD.TYPE_CODE = 1 AND SD.STATUS='Active' " + appendSearchType +
                        " AND SD.[PLAN] in (2, 6) AND SD.ELF='F' AND SD.ETF='F' " + appendNRICond + appendSchemeQuery +
                        " INNER JOIN AdvisoryReportAssetClass ARA ON ARA.CLASSCODE=SD.CLASSCODE  "+ appendCategory + appendSubCategory +
                        " INNER JOIN AFT_CURRENTNAV CN ON SM.SCHEMECODE=CN.SCHEMECODE " +
                        " LEFT JOIN AFT_MF_SIP SIPD ON SM.SCHEMECODE = SIPD.SCHEMECODE AND SIPD.FREQUENCY='MONTHLY' " +
                        " INNER JOIN AFT_VRO_Mapping VRM ON SM.SCHEMECODE = VRM.AFT_SCHEME_CODE AND LEN(VRM.TPSLSCHEMECODE) > 0 " +
                        " ORDER BY FS.SRANK, SM.SCHEME_NAME " ;
            else
            sqlQuery = "SELECT DISTINCT SM.AMC_CODE, SM.SCHEMECODE, SM.SCHEME_NAME, SD.SIP, SD.TYPE_CODE,  SD.STATUS, SD.CLASSCODE, SD.MININVT, SD.INC_INVEST," +
                    " CN.NAVRS CURRENTNAV, CONVERT(VARCHAR(11), CN.NAVDATE, 106) AS NAVDATE, WS.FI_Star, WS.SHOWUP, " +
                    " SIPD.SCHEMECODE SIPSCHEME, SIPD.SIPMININVEST, SIPD.SIPADDNINVEST,  ARA.AssetClass AS CATEGORY, ARA.Category as SUBCATEGORY " +
                    " FROM AFT_SCHEME_MASTER SM  " +
                    " INNER JOIN AFT_SCHEME_DETAILS SD ON SD.SCHEMECODE = SM.SCHEMECODE " +
                    " INNER JOIN WealthAMCMaster WAM ON WAM.AMC_CODE = SD.AMC_CODE " +
                    " INNER JOIN AFT_VRO_Mapping VRM ON SM.SCHEMECODE = VRM.AFT_SCHEME_CODE  " +
                    " LEFT JOIN AFT_MF_SIP SIPD ON SM.SCHEMECODE = SIPD.SCHEMECODE AND SIPD.FREQUENCY='MONTHLY' " +
                    " INNER JOIN WEALTHSCHEMEMASTER WS ON  WS.SCHEMECODE=VRM.AFT_SCHEME_CODE AND WS.UPD_FLAG='A' " +
                    "   AND (WS.SHOWUP = 'T' OR WS.SCHEMECODE IN (" + SIP_ShowUp_Schemes + "))" + appendAMC + appendUSCANADAAMC  + appendNRICond  +
                    " INNER JOIN AMCBANKMASTER ABM ON WS.SCHEMECODE = ABM.SCHEMECODE  " +
                    " INNER JOIN AFT_CURRENTNAV CN ON ABM.SCHEMECODE=CN.SCHEMECODE  " +
                    " INNER JOIN AdvisoryReportAssetClass ARA ON ARA.CLASSCODE=SD.CLASSCODE " + appendCategory + appendSubCategory +
                    " WHERE SD.[PLAN] in (2, 6) AND SD.ELF='F' AND SD.ETF='F' AND SD.TYPE_CODE = 1 AND SD.STATUS='Active'  " +
                    " AND LEN(VRM.TPSLSCHEMECODE) > 0 " + appendSchemeQuery + appendSearchType +
                    " ORDER BY "+ orderByQuery ;

            schemeList = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> {

                SchemeDetailBO schemeDetailBO = new SchemeDetailBO();
                schemeDetailBO.setName(rs.getString("SCHEME_NAME"));
                schemeDetailBO.setSchemeCode(rs.getString("SCHEMECODE"));
                schemeDetailBO.setCategory(rs.getString("CATEGORY"));
                schemeDetailBO.setSubCategory(rs.getString("SUBCATEGORY"));
                schemeDetailBO.setAmcCode(rs.getString("AMC_CODE"));
                schemeDetailBO.setRating(rs.getDouble("FI_Star"));

                if(rs.getInt("MININVT")<500)
                    schemeDetailBO.setMinimumInvestment(500);
                else
                    schemeDetailBO.setMinimumInvestment(rs.getInt("MININVT"));

                if((rs.getInt("SIPSCHEME")>0 && (rs.getString("SIP").equalsIgnoreCase("T") && (rs.getInt("TYPE_CODE")==1))
                        && rs.getInt("CLASSCODE")!=24 && rs.getString("STATUS").equalsIgnoreCase("Active"))
                        || SIP_ShowUp_Schemes.contains(rs.getString("SCHEMECODE"))) {
                    schemeDetailBO.setSip(true);
                }
                if(schemeDetailBO.getRating()>0)
                    schemeDetailBO.setRated(true);
                return schemeDetailBO;
            });
        } catch (Exception e) {
            log.error(logPrefix + "fetch SchemeSearchList Exception : ", e);
        }
        log.debug(logPrefix + "Scheme List size : " + schemeList.size());
        return schemeList;
    }

    /**
     * get NRI restriction if any of the given investors is NRI
     * @param investors
     * @return
     */
    public HashMap<String, String> getSchemeRestrictions(String[] investors) {

        String logPrefix = "#Investors : " + Arrays.toString(investors) + " | ";
        log.debug(logPrefix + "Country wise AMC Restriction if NRI Fetch");
        HashMap<String, String> NRIDetails = new HashMap<>();
        List<String> nationality;
        List<String> countryCode;
        String countryRestriction = "";
        try {

            String invCond = Arrays.toString(investors)
                    .replace("[", "(")
                    .replace("]", ")");

            //Check for NRI investors
            String sqlQuery = "SELECT COUNT (isNRI) FROM InvestorBasicDetail WHERE InvestorId IN " + invCond + " AND isNRI = '1' ";
            int isNRI = jdbcTemplate.queryForObject(sqlQuery, new Object[]{}, Integer.class);

            //if NRI Check for US or Canada based investors
            if(isNRI != 0) {
                sqlQuery = " SELECT DISTINCT COUNTRY FROM InvestorAddress WHERE InvestorID IN " + invCond + " AND INVESTORTYPE = 'MF' AND ADDRESSTYPE IN ('H', 'C')";
                countryCode = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> rs.getString("COUNTRY"));

                sqlQuery = "SELECT DISTINCT NATIONALITY FROM InvestorBasicDetail WHERE InvestorID IN " + invCond;
                nationality = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> rs.getString("NATIONALITY"));

                String usaCode = countryCode.stream().filter(code -> code.equals("2")).findAny().orElse(null);
                String canadaCode = countryCode.stream().filter(code -> code.equals("11")).findAny().orElse(null);
                if (StringUtils.isNotEmpty(usaCode) || StringUtils.isNotEmpty(canadaCode))
                    countryRestriction = BPConstants.USA_CANADA_ONLY_AMC;
                else {
                    usaCode = nationality.stream().filter(code -> code.equals("2")).findAny().orElse(null);
                    canadaCode = nationality.stream().filter(code -> code.equals("11")).findAny().orElse(null);
                    if (StringUtils.isNotEmpty(usaCode) || StringUtils.isNotEmpty(canadaCode))
                        countryRestriction = BPConstants.NATIONALITY_WISE_AMC_RESTRICT;
                }
            }

            //OE investor verification
            sqlQuery = "SELECT COUNT (investor) FROM InvestorBasicDetail WHERE InvestorId IN " + invCond + " AND irType = 'OE' ";
            int oeInvestorFlag = jdbcTemplate.queryForObject(sqlQuery, new Object[]{}, Integer.class);
            if(oeInvestorFlag>0)
                NRIDetails.put("restrictOEAMC", BPConstants.OE_AMCS);

            NRIDetails.put("isNRI", String.valueOf(isNRI));
            NRIDetails.put("restrictedAMC", countryRestriction);

        } catch (Exception e) {
            log.error(logPrefix + "NRIRestrictions Exception : ", e);
        }
        return NRIDetails;
    }

    public ProductSearchFormBO getProductSearchMFForm(String investorType, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Scheme Search Form fetch");
        ProductSearchFormBO productSearchFormBO = new ProductSearchFormBO();
        List<NameValueBO> amcList = new ArrayList<>();
        List<NameValueBO> categoryList = new ArrayList<>();
        List<NameValueBO> subCategoryList = new ArrayList<>();
        List<NameValueBO> riskList = new ArrayList<>();
        List<NameValueBO> ratingList = new ArrayList<>();
        List<NameValueBO> searchCodeList = new ArrayList<>();
        List<NameValueBO> orderByList = new ArrayList<>();
        try {

            amcList = getAmcListInNameValue(investorType, userId);

            categoryList = generalInfoDAO.getLookUpListInNameValue("SCHEMECLASS", false);

            String sqlQuery = " SELECT ClassCode, ClassName from AFT_SCLASS_MST ORDER BY ClassName ";

            subCategoryList = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> {

                NameValueBO nameValueBO = new NameValueBO();
                nameValueBO.setValue(rs.getString("ClassCode"));
                nameValueBO.setName(rs.getString("ClassName"));
                return nameValueBO;
            });


            for(int i=1; i<=5; i++)
                ratingList.add(new NameValueBO( i+"", i+""));

            riskList = generalInfoDAO.getLookUpListInNameValue("RiskType", false);

            searchCodeList = generalInfoDAO.getLookUpListInNameValue("MFSearchCode", false);

            orderByList = generalInfoDAO.getLookUpListInNameValue("MFOrderBy", true);

            productSearchFormBO.setAmcs(amcList);
            productSearchFormBO.setCategories(categoryList);
            productSearchFormBO.setSubCategories(subCategoryList);
            productSearchFormBO.setRatings(ratingList);
            productSearchFormBO.setRisk(riskList);
            productSearchFormBO.setSearchCodes(searchCodeList);
            productSearchFormBO.setOrderBy(orderByList);

        } catch (Exception e) {
            log.error(logPrefix + "fetch Scheme Search Form Exception : ", e);
        }
        log.debug(logPrefix + "AMC List Size : " + amcList.size() + "| Category List size : " + categoryList.size() + " | SubCategory List size : " + subCategoryList.size()
                            + "| Risk List size : " + riskList.size() + " | OrderBy List size : " + orderByList.size());
        return productSearchFormBO;
    }

    public boolean isSchemeAllowedForNRI(int schemeCode) {

        boolean isSchemeAllowedForNRI = false;

        try {
            String presentQuery =" SELECT IE.SCHEMECODE FROM AFT_SCHEME_DETAILS SD, AFT_INVST_ELIGIBILITY IE "
                                + " WHERE IE.SCHEMECODE = SD.PRIMARY_FD_CODE AND SD.SCHEMECODE= ? AND ELIG_CODE = 5 ";

            int isPresent = jdbcTemplate.queryForObject(presentQuery, new Object[]{schemeCode}, Integer.class);

            if(isPresent!=0)
                isSchemeAllowedForNRI = true;

        } catch (Exception e) {
            log.error("Exception in scheme allowed for NRI check : ", e);
        }
        return isSchemeAllowedForNRI;
    }

    public boolean isOnlyMinorAllowedScheme(int schemeCode) {

        boolean isOnlyMinorAllowedScheme = false;

        try {
            String presentQuery =" SELECT MINORELIGIBLE FROM WEALTHSCHEMEMASTER WHERE SCHEMECODE= ? ";

            String isPresent = jdbcTemplate.queryForObject(presentQuery, new Object[]{schemeCode}, String.class);

            if(StringUtils.isNotEmpty(isPresent) && "Y".equalsIgnoreCase(isPresent.trim()))
                isOnlyMinorAllowedScheme = true;

        } catch (Exception e) {
            log.error("Exception in scheme allowed only for Minor check : ", e);
        }
        return isOnlyMinorAllowedScheme;
    }

    public HashMap<String, String> getSchemeDetails(int schemeCode) {

        String logPrefix = "#SchemeCode : " + schemeCode + " | ";
        log.debug(logPrefix + "AMC fetch");
        HashMap<String, String> amcMap = new HashMap<>();
        try {

            String sqlQuery = " SELECT SD.AMC_CODE,VM.RTCODE, SD.ClassCode, WS.ShowUp, SD.MININVT, SIPD.SIPMININVEST " +
                              " FROM AFT_VRO_MAPPING VM " +
                              " INNER JOIN AFT_SCHEME_DETAILS SD ON VM.AFT_SCHEME_CODE = SD.SCHEMECODE " +
                              " INNER JOIN WEALTHSCHEMEMASTER WS ON WS.SCHEMECODE = SD.SCHEMECODE " +
                              " LEFT JOIN AFT_MF_SIP SIPD ON SD.SCHEMECODE = SIPD.SCHEMECODE AND SIPD.FREQUENCY='MONTHLY' " +
                              " WHERE SD.SCHEMECODE  = ? ";

            amcMap = jdbcTemplate.queryForObject(sqlQuery, new Object[]{schemeCode}, (rs, rowNumber) -> {

                HashMap<String, String> result = new HashMap<>();
                result.put("amcCode", rs.getString("AMC_Code"));
                result.put("rtCode", rs.getString("RTCODE"));
                result.put("showUp", rs.getString("ShowUp"));
                result.put("classCode", rs.getString("classCode"));
                result.put("minInvest", rs.getString("MININVT"));
                result.put("sipMinInvest", StringUtils.isEmpty(rs.getString("SIPMININVEST")) ? "500" : rs.getString("SIPMININVEST"));
                return result;
            });

        } catch (Exception e) {
            log.error(logPrefix + "fetch amc, rt code Exception : ", e);
        }

        return amcMap;
    }

    public HashMap<String, String> getSchemeDetailsForTransaction(int schemeCode) {

        String logPrefix = "#SchemeCode : " + schemeCode + " | ";
        log.debug(logPrefix + "AMC fetch");
        HashMap<String, String> amcMap = new HashMap<>();
        try {

            String sqlQuery = " SELECT AVM.AmcSchemeCode, AVM.TpslSchemeCode, ASD.Amc_Code FROM AFT_VRO_MAPPING AVM " +
                    " JOIN AFT_SCHEME_DETAILS ASD ON ASD.SchemeCode = AVM.AFT_Scheme_Code " +
                    " WHERE AVM.AFT_Scheme_Code = ? ";

            amcMap = jdbcTemplate.queryForObject(sqlQuery, new Object[]{schemeCode}, (rs, rowNumber) -> {

                HashMap<String, String> result = new HashMap<>();
                result.put("amcSchemeCode", rs.getString("AmcSchemeCode"));
                result.put("tpslSchemeCode", rs.getString("TpslSchemeCode"));
                return result;
            });

        } catch (Exception e) {
            log.error(logPrefix + "fetch amcSchemeCode, tpslSchemeCode Exception : ", e);
        }

        return amcMap;
    }

    public String getSClassFromRMFSchemeCheck(String amcCode, String schemeCode) {

        String sClass = "";

        try {

            String timeSql = "SELECT sclass FROM RMFSchemeCheck WHERE AMCCode = ? AND SchemeCode = ? AND Active = 'A'";

            sClass = jdbcTemplate.queryForObject(timeSql, new Object[]{amcCode, schemeCode}, String.class);

        } catch (Exception e) {
            log.error("Exception in get SCLASS from RMFSchemeCheck  : ", e);
        }

        return sClass;
    }
}

package com.kp.core.app.model.Products;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public  class Variant {

    @SerializedName("id")
    @Expose
    public Long id;
    @SerializedName("product_id")
    @Expose
    public Long productId;
    @SerializedName("color")
    @Expose
    public String color;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("sku")
    @Expose
    public String sku;
    @SerializedName("position")
    @Expose
    public Integer position;
    @SerializedName("inventory_policy")
    @Expose
    public String inventoryPolicy;
    @SerializedName("compare_at_price")
    @Expose
    public Double compareAtPrice;
    @SerializedName("fulfillment_service")
    @Expose
    public String fulfillmentService;
    @SerializedName("inventory_management")
    @Expose
    public String inventoryManagement;
    @SerializedName("option1")
    @Expose
    public String option1;
    @SerializedName("option2")
    @Expose
    public String option2;
    @SerializedName("option3")
    @Expose
    public String option3;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("taxable")
    @Expose
    public Boolean taxable;
    @SerializedName("barcode")
    @Expose
    public String barcode;
    @SerializedName("grams")
    @Expose
    public Integer grams;
    @SerializedName("image_id")
    @Expose
    public Integer imageId;
    @SerializedName("weight")
    @Expose
    public Double weight;
    @SerializedName("weight_unit")
    @Expose
    public String weightUnit;
    @SerializedName("inventory_item_id")
    @Expose
    public Integer inventoryItemId;
    @SerializedName("inventory_quantity")
    @Expose
    public Integer inventoryQuantity;
    @SerializedName("old_inventory_quantity")
    @Expose
    public Integer oldInventoryQuantity;
    @SerializedName("presentment_prices")
    @Expose
    public List<PresentmentPrice> presentmentPrices = null;
    @SerializedName("requires_shipping")
    @Expose
    public Boolean requiresShipping;
    @SerializedName("expire_at")
    @Expose
    public String expireAt;



}
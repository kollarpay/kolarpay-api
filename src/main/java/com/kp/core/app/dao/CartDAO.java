package com.kp.core.app.dao;

import com.google.gson.Gson;
import com.kp.core.app.model.CartBO;
import com.kp.core.app.model.CartItemBO;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;

import jakarta.transaction.Transactional;
import org.springframework.util.StopWatch;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Slf4j
@Repository

public class CartDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;
    @Autowired
    Gson gson;

    public CartBO createCart(CartBO cartBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert cart");


        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery = " INSERT INTO cart (userid,type,firstname,middlename,lastname," +
                    "email,mobile,line1,line2,city,country,paymentid,medium,appdata,ipaddress, expires_at, " +
                    " created_at,updated_at, created_user, updated_user,active) " +
                    " VALUES( ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?,?,?,NOW(), NOW(), ?, ?, 1)  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

                ps.setInt(1, userId);
                ps.setString(2, cartBO.getType());
                ps.setString(3, cartBO.getFirstname());
                ps.setString(4, cartBO.getMiddlename());
                ps.setString(5, cartBO.getLastname());
                ps.setString(6, cartBO.getEmail());
                ps.setString(7, cartBO.getMobile());
                ps.setString(8, cartBO.getLine1());
                ps.setString(9, cartBO.getLine2());
                ps.setString(10, cartBO.getCity());
                ps.setString(11, cartBO.getCountry());
                ps.setString(12, cartBO.getPaymentid());
                ps.setString(13, cartBO.getMedium());
                ps.setString(14, cartBO.getAppdata());
                ps.setString(15, cartBO.getIpaddress());
                ps.setString(16, cartBO.getExpires_at());
                ps.setInt(17, userId);
                ps.setInt(18, userId);
                return ps;

            }, keyHolder);
           String id;
           String cartID;
            if (keyHolder.getKeys().size() > 1) {
                id = keyHolder.getKeys().get("id").toString();
                cartBO.setId(id+"");
                cartID = keyHolder.getKeys().get("cart_id").toString();
                cartBO.setCartID(cartID);
            }

        } catch (Exception e) {
            log.error(logPrefix + "Exception in cart insertion: ", e);
        }

        log.debug(logPrefix + "cart id  generate: " + cartBO.getCartID());
        return cartBO;
    }
    public void cartItesmInsert(CartBO cartBO) {
        StopWatch timer = new StopWatch();
        String sql = "INSERT INTO cart_item (CART_ID, PRODUCTID, quantity, sku, active)"
                + " VALUES(?,?,?,?,1)";
        timer.start();
        jdbcTemplate.batchUpdate(sql, new CartItemBatchPreparedStatementSetter(cartBO));
        timer.stop();
        log.info("cartItemsInsert -> Total time in seconds: " + timer.getTotalTimeMillis());
    }
    public class CartItemBatchPreparedStatementSetter implements BatchPreparedStatementSetter {

        private List<CartItemBO> cartItems;
        private String  cartID;
        public CartItemBatchPreparedStatementSetter(CartBO cartBO) {
            super();
            cartID=cartBO.getCartID();
            this.cartItems = cartBO.getItems();
        }

        @Override
        public void setValues(PreparedStatement ps, int i) {

            try {
                CartItemBO cartItemBO = cartItems.get(i);
                ps.setString(1, cartID);
                ps.setString(2, cartItemBO.getProductid());
                ps.setInt(3, StringUtil.isNullOrEmpty(cartItemBO.getQuantity())?0:Integer.parseInt(cartItemBO.getQuantity()));
                ps.setString(4, cartItemBO.getSku());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getBatchSize() {
            return cartItems.size();
        }
    }
}

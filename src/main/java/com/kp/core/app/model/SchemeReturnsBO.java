package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SchemeReturnsBO {

    private String rt;
    private String y1;
    private String y3;
    private String y5;
    private String m1;
    private String m3;
    private String m6;
    private String sii;
}

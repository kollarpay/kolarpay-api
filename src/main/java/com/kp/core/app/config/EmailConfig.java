package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/brospay/config/properties/email_template_id.properties")
//@Config.Sources("file:/usr/local/logs/bros/config/properties/email_template_id.properties")
@Config.HotReload(type = Config.HotReloadType.ASYNC, unit = TimeUnit.MINUTES, value = 10)

public interface EmailConfig extends Reloadable {

    @Config.Key("WELCOME.EMAIL.TEMPLATEID")
    String getWelcomeEmailTemplateId();

}

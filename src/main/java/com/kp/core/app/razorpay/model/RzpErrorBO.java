package com.kp.core.app.razorpay.model;

import lombok.Getter;
import lombok.Setter;
import net.sf.json.JSONObject;

@Getter
@Setter
public class RzpErrorBO {

    private ErrorBO error;

    @Getter
    @Setter
    public static class ErrorBO {

        private String code;
        private String description;
        private String source;
        private String step;
        private String reason;
        private JSONObject metadata;
        private String field;
    }
}

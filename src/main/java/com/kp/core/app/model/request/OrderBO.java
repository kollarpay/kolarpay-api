package com.kp.core.app.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.kp.core.app.model.CustomerInfoBO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class OrderBO {
    private String orderID;
    private String mode;
    private String productType;
    private BigInteger amount;
    private String receipt;
    private String currency;
    private String customerID;
    private String paymentID;
    private String paymentGatewayID;
    private String paymentOrderID;
    private String orderType;
    private int orderMode;
    private String orderDate;
    private String shipperID;
    private String shipperDt;
    private String orderTime;
    private String created_user;
    private String createdDate;
    private String updateUser;
    private String updateDate;
    private String ipAddress;
    private String remarks;
    private String medium;
    private CustomerInfoBO customerInfoBO;
    private List<OrderDetailsBO> orderDetailsBOList;


}

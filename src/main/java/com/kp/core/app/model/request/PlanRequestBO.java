package com.kp.core.app.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.kp.core.app.model.CustomerInfoBO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanRequestBO {
    private String planId;
    private String planName;
    private String type;
    private int maxCycles;
    private double amount;
    private String intervalType;
    private String description;
    private double maxAmount;
}

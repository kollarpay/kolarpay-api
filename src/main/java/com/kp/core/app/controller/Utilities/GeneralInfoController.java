package com.kp.core.app.controller.Utilities;

import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.service.Utilities.GeneralInfoService;
import com.kp.core.app.utils.APIGeneralUtil;
import com.kp.core.app.utils.GeneralUtil;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@Slf4j
@RequestMapping("/")
public class GeneralInfoController {

    @Autowired
    Gson gson;

    @Autowired
    GeneralUtil generalUtil;

    @Autowired
    GeneralInfoService generalInfoService;

    @GetMapping(path = "/lookup", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> lookUp(@RequestHeader MultiValueMap<String, Object> headers,
                                                   @RequestParam("type") String[] lookUpType) throws Exception {

        log.debug("#lookup API Request : " + Arrays.toString(lookUpType));
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.array, "Lookup");
        try {
            globalResponseBO = generalInfoService.lookUpList(lookUpType);


        } catch (Exception e) {
            log.error( "| lookup API Exception : ", e);
        }
        log.debug("#lookup API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/lookupsvg", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> lookUpSvg(@RequestHeader MultiValueMap<String, Object> headers,
                                                   @RequestParam("types") String[] lookUpTypes) throws Exception {

        log.debug("#lookup API Request | #investors : " + Arrays.toString(lookUpTypes));
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.array, "lookupsvg");
        try {


            globalResponseBO = generalInfoService.lookUpSvgList(lookUpTypes);


        } catch (Exception e) {
            log.error("lookupsvg API Exception : ", e);
        }
        log.debug("#lookupsvg API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/lookupvalue", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> lookUpById(@RequestHeader MultiValueMap<String, Object> headers,
                                                   @RequestParam("lookupupid") String lookUpId,@RequestParam("lookupuptype") String lookupType) throws Exception {

        log.debug("#lookup API Request | #lookUpId : " + lookUpId);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.array, "Lookup");
        try {
            globalResponseBO = generalInfoService.lookUpValue(lookUpId,lookupType);


        } catch (Exception e) {
            log.error("| lookup API Exception : ", e);
        }
        log.debug("#lookup API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @GetMapping(path = "/user-profile", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> userProfile(@RequestHeader MultiValueMap<String, Object> headers) throws Exception {

        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "User");
        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {
                String token = APIGeneralUtil.getHeader(headers, "x-bp-access-token");
                globalResponseBO = generalInfoService.userProfile(userId, token);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| lookup API Exception : ", e);
        }
        log.debug("#user-profile API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

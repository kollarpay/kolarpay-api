package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentStatusBO {

    private String message;
    private String liquidSchemeMessage;
    private String status;
    private String paymentId;
    private String transactionId;
    private String statusCode;
    private String paidStatus;
    private String paidThru;
    private String paymentMode;
    private String transactionDate;
    private String consumerCode;
    private String mandateRegNo;

}

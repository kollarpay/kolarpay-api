package com.kp.core.app.service.Product;

import com.kp.core.app.dao.CollectionDAO;
import com.kp.core.app.dao.GeneralInfoDAO;
import com.kp.core.app.dao.ProductDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.CollectionBO;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.IdDesc;
import com.kp.core.app.model.Products.ProductBO;
import com.kp.core.app.model.Products.ProductCategory;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Service
@Slf4j
public class ProductService {

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private GeneralInfoDAO generalInfoDAO;


    public GlobalResponseBO createProduct(ProductBO productBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create Product");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createProduct");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            isValidProduct(productBO, errorBoList);


            if(errorBoList.size()==0 ) {
                productDAO.SaveProduct(productBO,userId);
                Long productId=  productBO.getProductId();
                if(productId>0 && productBO.getProductVariant()!=null){
                    productDAO.SaveProductVariant(productBO,userId);
                }
                if(productId>0 && productBO.getProductDiscount()!=null && productBO.getProductVariant().getId()>0){
                    productDAO.saveProductDiscount(productBO,userId);
                }
                if(productId>0 && productBO.getProductCategoryDiscount()!=null && productBO.getProductVariant().getId()>0){
                    productDAO.saveProductCategoryDiscount(productBO,userId);
                }
                if(productId>0 && productBO.getProductImages()!=null){
                    productDAO.SaveProductImage(productBO,userId);
                }

            }


        } catch (Exception e) {
            log.error(logPrefix + "Create mandate API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, productBO, globalResponseBo);

        log.debug(logPrefix + "Create Collection API Status : " + stat);
        return  globalResponseBo;

    }
    public GlobalResponseBO createProductImage(ProductBO productBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create Product Image");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createProductImage");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            isValidProductId(productBO, errorBoList);


            if(errorBoList.size()==0 ) {
                productDAO.SaveProductImage(productBO,userId);
            }


        } catch (Exception e) {
            log.error(logPrefix + "Create mandate API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, productBO, globalResponseBo);

        log.debug(logPrefix + "Create Collection API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO getproducts(int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "get all published Product");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "allproducts");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        List<ProductBO> productBOList=new ArrayList<>();
        try {


            if(errorBoList.size()==0 ) {
                productBOList=productDAO.getProductList(userId);
            }


        } catch (Exception e) {
            log.error(logPrefix + "get Collections API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, productBOList, globalResponseBo);

        log.debug(logPrefix + "Collections API Status : " + stat);
        return  globalResponseBo;

    }
    public GlobalResponseBO getproductDetail(int userId,ProductBO productBO) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "get published Product");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "productById");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        try {


            if(errorBoList.size()==0 ) {
                productBO=productDAO.getProductDetail(userId,productBO);
            }


        } catch (Exception e) {
            log.error(logPrefix + "get Collections API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, productBO, globalResponseBo);

        log.debug(logPrefix + "Collections API Status : " + stat);
        return  globalResponseBo;

    }
    public void isValidProduct(ProductBO productBO, List<ErrorBO> errorBoList) {

        try {

            if(StringUtils.isEmpty(productBO.getTitle()))
                errorBoList.add(new ErrorBO("title", "Invalid input"));


        } catch (Exception e) {
            log.error("Exception in Collection creation - validate : ", e);
        }
    }
    public void isValidProductId(ProductBO productBO, List<ErrorBO> errorBoList) {

        try {

            if(productBO.getProductId()>0)
                errorBoList.add(new ErrorBO("Product Id", "Invalid product id"));


        } catch (Exception e) {
            log.error("Exception in Collection creation - validate : ", e);
        }
    }

    public GlobalResponseBO getproductTag(int userid,String tags) {

        String logPrefix = "#userid : " + userid + " | ";
        log.debug(logPrefix + "get all Product list by tag");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "product_list_tag");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        List<ProductBO> productList=new ArrayList<>();
        HashMap<String,List<ProductBO>> productMap= new HashMap<>();

        try {

            if(errorBoList.size()==0 ) {
                productList=productDAO.getProductListByTag(userid,tags);
                for(ProductBO productBO:productList){
                    productDAO.getProductVariant(userid,productBO);
                }
                for(ProductBO productBO:productList){
                    productDAO.getImagesbyProductId(userid,productBO);
                }

            }
            productMap.put("products",productList);


        } catch (Exception e) {
            log.error(logPrefix + "get Product List by tag API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, productMap, globalResponseBo);

        log.debug(logPrefix + "product list by tag API Status : " + stat);
        return  globalResponseBo;

    }
    public GlobalResponseBO getproductCategory(int bpid,String categoryType) {

        String logPrefix = "#bpid : " + bpid + " | ";
        log.debug(logPrefix + "get all Product categories");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "product_categories");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        List<ProductCategory> ProductCategoryList=new ArrayList<>();

        try {

            if(errorBoList.size()==0 ) {
                ProductCategoryList=productDAO.getProductCategories(bpid,categoryType);
                for(ProductCategory proCat:ProductCategoryList){
                    List<IdDesc> idDescList=generalInfoDAO.getLookUpList(proCat.getCategoryName());
                    proCat.setCategoryList(idDescList);
                }
            }


        } catch (Exception e) {
            log.error(logPrefix + "get Product Categories API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, ProductCategoryList, globalResponseBo);

        log.debug(logPrefix + "Collections API Status : " + stat);
        return  globalResponseBo;

    }

}


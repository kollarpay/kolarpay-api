package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SchemeListBO {

    private String schemeCode;
    private String goalId;
    private String schemeName;
    private String goalName;
    private double units;
    private String unitsFormatted;
    private double amount;
    private String amountFormatted;
    private String folio;
    private double currentNav;
    private String currentNavFormatted;
    private int ratings;

}

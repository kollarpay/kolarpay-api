package com.kp.core.app.service.AdvisorPortfolioReview;

import com.kp.core.app.dao.SchemeSearchDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.BPConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import redis.clients.jedis.Jedis;

import java.io.ObjectInputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
public class SchemeSearchService {

    @Autowired
    GeneralUtil generalUtil;

    @Autowired
    SchemeSearchDAO schemeSearchDAO;

    /**
     * list of AMC's, Categories and SubCategories of schemes
     * @param advUserId
     * @return
     */
    public GlobalResponseBO getSchemeSearchForm(int advUserId) {

        String logPrefix = "#AdvUserId : " + advUserId + " | ";

        log.debug(logPrefix + "Scheme Search Form");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "SchemeSearchForm");
        List<ErrorBO> errorBoList = new ArrayList<>();
        HashMap<String, Object> response = new HashMap<>();
        String stat = "";

        try {

            //fetch AMC list
            List<IdDesc> amcList = schemeSearchDAO.getAmcList(advUserId);
            if(amcList.size()==0)
                errorBoList.add(new ErrorBO("amcList", "No AMCs found"));

            else{
                //fetch Category list
                List<IdDesc> categoryList = schemeSearchDAO.getCategoryList(advUserId);
                if(categoryList.size()==0)
                    errorBoList.add(new ErrorBO("category", "No Categories found"));

                else {
                    //fetch SubCategory list
                    List<IdDesc> subCategoryList = schemeSearchDAO.getSubCategoryList(advUserId);
                    if(subCategoryList.size()==0)
                        errorBoList.add(new ErrorBO("subCategory", "No SubCategories found"));
                    else {

                        List<IdDesc> filters = new ArrayList<>();
                        filters.add(BPConstants.SCHEME_SEARCH_FI_ID);
                        filters.add(BPConstants.SCHEME_SEARCH_GROWTH_ID);
                        filters.add(BPConstants.SCHEME_SEARCH_DIVIDEND_ID);
                        filters.add(BPConstants.SCHEME_SEARCH_RATED_ID);
                        //set response
                        response.put("amc", amcList);
                        response.put("filters", filters);
                        response.put("category", categoryList);
                        response.put("subCategory", subCategoryList);
                    }
                }
            }
            if(errorBoList.size()>0)
                stat = APIConstants.BAD_REQUEST_STRING;

            globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, response, globalResponseBo);

        } catch (Exception e) {
            log.error(logPrefix + "Exception :", e);
        }
        log.debug(logPrefix + "Scheme-search Form API Status : " + stat);
        return globalResponseBo;
    }

    /**
     * get list of schemes satisfying provided conditions
     * save the list in redis cache with SessionId as redisKey
     * set pagination url
     * @param amcCode
     * @param category
     * @param subCategory
     * @param queryStr
     * @param searchType
     * @param investorIds
     * @param order
     * @param advUserId
     * @return
     */
    public GlobalResponseBO getSchemeSearchList(String amcCode, String category, String subCategory, String queryStr, String searchType, String[] investorIds, String order, int advUserId) {

        String logPrefix = "#AdvUserId : " + advUserId + " | #Filter : " + searchType + " | ";

        log.debug(logPrefix + "Scheme Search POST");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "SchemeSearchResult");
        List<ErrorBO> errorBoList = new ArrayList<>();
        SchemeSearchBO schemeSearchBO = new SchemeSearchBO();
        List<SchemeDetailBO> schemeList = new ArrayList<>();
        String stat = "", sessionId;
        int page=1, count= BPConstants.SHEME_COUNT_PER_PAGE;
        boolean saved = false;

        try {

            if(investorIds.length==0)
                errorBoList.add(new ErrorBO("investors", "Invalid input"));
            else {
                //fetch scheme list
                schemeList = schemeSearchDAO.getSchemeSearchList(amcCode, category, subCategory, queryStr, searchType, investorIds, order, advUserId);
                if (schemeList.size() != 0) {

                    /** save schemeList in redis cache */
                    //get sessionId to use as redisKey
                    sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
                    //set up redis connection
                    Jedis redisConn = generalUtil.getRedisConnection();
                    //save the list into redis cache
                    if(redisConn != null)
                        saved = generalUtil.putIntoRedis(redisConn, sessionId, schemeList, 28800);

                    if(saved) {
                        //create subList of schemes from the actual schemeList
                        List subList = generalUtil.getSubList(schemeList, page, count);
                        schemeSearchBO.setSchemes(subList);
                        schemeSearchBO.setCount(subList.size());
                        schemeSearchBO.setSize(schemeList.size());
                        schemeSearchBO.setPage(page);

                        //set pagination API url
                        if (schemeList.size() > (count)) {
                            schemeSearchBO.setNext("/portfolio-review/scheme-search?page=" + 2 + "&id=" + URLEncoder.encode(sessionId, StandardCharsets.UTF_8.toString()));
                        } else {
                            schemeSearchBO.setNext("");
                        }
                    }
                    else
                        errorBoList.add(new ErrorBO("schemeList", "Redis Cache Save failed"));
                }
            }
            if(errorBoList.size()>0)
                stat = APIConstants.BAD_REQUEST_STRING;

            globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, schemeSearchBO, globalResponseBo);

        } catch (Exception e) {
            log.error(logPrefix + "Exception :", e);
        }
        log.debug(logPrefix + "Scheme-search POST API Status : " + stat);
        return globalResponseBo;
    }

    /**
     * get list of schemes for given page number
     * fetch actual schemeList from redisCache using given id as redisKey
     * create sublist from actual schemeList
     * @param page
     * @param id
     * @param advUserId
     * @return
     */
    public GlobalResponseBO getSchemeSearchListPagination(int page, String id, int advUserId) {

        String logPrefix = "#AdvUserId : " + advUserId + " | ";

        log.debug(logPrefix + "Scheme Search GET");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "SchemeSearchResult");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<SchemeDetailBO> schemeList = new ArrayList<>();
        SchemeSearchBO schemeSearchBO = new SchemeSearchBO();
        String stat = "";
        int count= BPConstants.SHEME_COUNT_PER_PAGE;
        ObjectInputStream is = null;

        try {

            if(StringUtils.isEmpty(id))
                errorBoList.add(new ErrorBO("id", "Invalid input"));
            if(page==0)
                errorBoList.add(new ErrorBO("page", "Invalid input"));

            else {
                /** Fetch SchemeList saved in Redis cache */
                //set up redis connection
                Jedis redisConn = generalUtil.getRedisConnection();
                //get InputStream from redis Cache
                if (redisConn != null)
                    is = generalUtil.getFromRedis(redisConn, id);
                //read InputStream to Object - schemeList
                if (is != null)
                    schemeList = (List<SchemeDetailBO>) is.readObject();

                if (schemeList.size() == 0)
                    errorBoList.add(new ErrorBO("schemes", "No schemes found"));
                else {

                    //create subList from saved schemeList
                    List subList = generalUtil.getSubList(schemeList, page, count);
                    schemeSearchBO.setSchemes(subList);
                    schemeSearchBO.setCount(subList.size());
                    schemeSearchBO.setSize(schemeList.size());
                    schemeSearchBO.setPage(page);
                    //set pagination API url
                    if (schemeList.size() > (count)) {
                        schemeSearchBO.setNext("/portfolio-review/scheme-search?page=" + (page + 1) + "&id=" + URLEncoder.encode(id, StandardCharsets.UTF_8.toString()));
                    } else {
                        schemeSearchBO.setNext("");
                    }
                }
            }
            if(errorBoList.size()>0)
                stat = APIConstants.BAD_REQUEST_STRING;

            globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, schemeSearchBO, globalResponseBo);

        } catch (Exception e) {
            log.error(logPrefix + "Exception :", e);
        }
        log.debug(logPrefix + "Scheme-search GET API Status : " + stat);
        return globalResponseBo;
    }
}

package com.kp.core.app.controller.coupon;

import com.google.gson.Gson;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.service.apps.FrontPageService;
import com.kp.core.app.utils.APIGeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
@RequestMapping("/coupon")
public class CouponController {

    @Autowired
    FrontPageService frontPageService;

    @Autowired
    Gson gson;


    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> getAllTemplateCollection(@RequestHeader MultiValueMap<String, Object> headers) throws Exception {

        log.debug("#get all publish coupon page  POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "coupon");

        int userId = 0;
        int bpid = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            bpid = Integer.parseInt(APIGeneralUtil.getHeader(headers, "bpid"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = frontPageService.getAllFrontPage(userId,bpid);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#collection create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

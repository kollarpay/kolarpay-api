package com.kp.core.app.controller.Investment;

import com.kp.core.app.dao.InvestorInfoDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.model.*;
import com.kp.core.app.service.Investment.InvestorInfoService;
import com.kp.core.app.service.Investment.TransactionService;
import com.kp.core.app.utils.APIGeneralUtil;
import com.kp.core.app.utils.GeneralUtil;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

@RestController
@Slf4j
@RequestMapping("/investor")
public class InvestorInfoController {

    @Autowired
    Gson gson;

    @Autowired
    InvestorInfoService investorInfoService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    InvestorInfoDAO investorInfoDAO;

    @Autowired
    GeneralUtil generalUtil;


    /**
     * List of holding profiles of given user
     * @param headers
     * @param holdingProfileId
     * @return
     * @throws Exception
     */
    @GetMapping(path = "/holding-profiles", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> holdingProfiles(@RequestHeader MultiValueMap<String, Object> headers,
                                                            @RequestParam(required = false, name = "holdingProfileId") String holdingProfileId) throws Exception {

        log.debug("#holding-profiles GET API Request | HoldingProfileId : " + holdingProfileId);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.list, "HoldingProfile");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = investorInfoService.investorHoldingProfileList(holdingProfileId, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#holding-profiles GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * List of folio bank details for given holdingProfileId
     * @param headers
     * @param holdingProfileId
     * @return
     * @throws Exception
     */
    @GetMapping(path = "/folio-bank-list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> folioBankList(@RequestHeader MultiValueMap<String, Object> headers,
                                                          @RequestParam(required = false, name = "holdingProfileId") String holdingProfileId,
                                                          @RequestParam(required = false, name = "investorId") String investorId,
                                                          @RequestParam(required = false, name = "schemeCodes") String[] schemeCodes,
                                                          @RequestParam(required = false, name = "amcCodes") String[] amcCodes) throws Exception {

        log.debug("#folio-bank-list GET API Request | HoldingProfileId : " + holdingProfileId);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.list, "FolioBank");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = investorInfoService.investorFolioBankList(holdingProfileId, investorId, schemeCodes, amcCodes, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#folio-bank-list GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * list of Investor's available mandates
     * @param headers
     * @param holdingProfileId
     * @param investorId
     * @param ecsDate
     * @param consumerCode
     * @return
     * @throws Exception
     */
    @GetMapping(path = "/mandates", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> mandateList(@RequestHeader MultiValueMap<String, Object> headers,
                                                        @RequestParam(required = false, name = "holdingProfileId") String holdingProfileId,
                                                        @RequestParam(required = false, name = "investorId") String investorId,
                                                        @RequestParam(required = false, name = "ecsDate") String ecsDate,
                                                        @RequestParam(required = false, name = "consumerCode") String consumerCode) throws Exception {

        log.debug("#mandates list GET API Request | HoldingProfileId : " + holdingProfileId);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.list, "Mandate");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = investorInfoService.investorMandateList(holdingProfileId, investorId, ecsDate, consumerCode, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#mandates list GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * create new mandate and return all the mandate details
     * @param headers
     * @param payload
     * @return
     * @throws Exception
     */
    @PostMapping(path = "/mandates", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createMandate(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#mandates create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "Portfolios");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CreateMandataBO mandataBO = gson.fromJson(payload, CreateMandataBO.class);
                globalResponseBO = investorInfoService.createNewMandate(mandataBO, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#mandates create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * List of Investor's portfolios / goals
     * @param headers
     * @return
     * @throws Exception
     */
    @GetMapping(path = "/goals", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> goalList(@RequestHeader MultiValueMap<String, Object> headers,
                                                          @RequestParam(required = false, name = "goalType") String goalType) throws Exception {

        log.debug("#mandates list GET API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.list, "GoalBo");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = investorInfoService.investorGoalList(goalType, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#mandates list GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * create new Goal
     * @param headers
     * @param payload
     * @return
     * @throws Exception
     */
    @PostMapping(path = "/goals", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createNewGoal(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#goals POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "PlanResponse");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                InvestorGoalBO investorGoalBO = gson.fromJson(payload, InvestorGoalBO.class);
                globalResponseBO = investorInfoService.newInvestorGoal(investorGoalBO, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#goals POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }


    /**
     * create new cart
     * @param headers
     * @param payload
     * @param validation
     * @return
     * @throws Exception
     */
    @PostMapping(path = "/cart", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createAndAddSchemesToCart(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload,
                                                                      @RequestParam(required = false, name = "validation") String validation) throws Exception {

        log.debug("#cart POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "AddToCartResponse");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                SchemeCartBO schemeCartBO = gson.fromJson(payload, SchemeCartBO.class);
                globalResponseBO = investorInfoService.createCartAndAddSchemesOrENACH(schemeCartBO, userId, validation, false);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#cart POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * update / modify existing cart
     * @param headers
     * @param payload
     * @param validation
     * @return
     * @throws Exception
     */
    @PutMapping(path = "/cart", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> updateCart(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload,
                                                       @RequestParam(required = false, name = "validation") String validation) throws Exception {

        log.debug("#cart PUT API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "AddToCartResponse");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                SchemeCartBO schemeCartBO = gson.fromJson(payload, SchemeCartBO.class);
                globalResponseBO = investorInfoService.createCartAndAddSchemesOrENACH(schemeCartBO, userId, validation, true);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#cart PUT API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * List of existing cart items
     * @param headers
     * @param cartId
     * @return
     * @throws Exception
     */
    @GetMapping(path = "/cart", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> investorCartList(@RequestHeader MultiValueMap<String, Object> headers, @RequestParam("cartId") String cartId) throws Exception {

        log.debug("#cart scheme list GET API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "CartItem");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                globalResponseBO = investorInfoService.fetchInvestorCart(cartId, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#cart scheme list GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }


    /**
     * delete existing cart
     * @param headers
     * @param cartId
     * @return
     * @throws Exception
     */
    @DeleteMapping(path = "/cart", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> deactivateCart(@RequestHeader MultiValueMap<String, Object> headers, @RequestParam("cartId") String cartId) throws Exception {

        log.debug("#cart DELETE API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "DeleteCartResponse");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                globalResponseBO = investorInfoService.deactivateCart(cartId, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#cart DELETE API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * buy items in existing cart
     * @param headers
     * @param cartId
     * @return
     * @throws Exception
     */
    @GetMapping(path = "/cart/buy", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> investorBuyCart(@RequestHeader MultiValueMap<String, Object> headers, @RequestParam("cartId") String cartId) throws Exception {

        log.debug("#cart buy GET API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "AddToCartResponse");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                //globalResponseBO = transactionService.investorBuyCart(cartId, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#cart scheme list GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @GetMapping(path = "/dashboard", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> userDashBoard(@RequestHeader MultiValueMap<String, Object> headers,
                                                          @RequestParam(name = "holdingProfileId", required = false) String holdingProfileId) throws Exception {

        log.debug("#dashboard GET API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "PortfolioDashboard");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                globalResponseBO = investorInfoService.dashBoardDetails(holdingProfileId, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#dashboard GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @GetMapping(path = "/invested-schemes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> investedSchemes(@RequestHeader MultiValueMap<String, Object> headers,
                                                          @RequestParam("holdingProfileId") String holdingProfileId) throws Exception {

        log.debug("#invested-schemes GET API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.list, "InvestedScheme");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                globalResponseBO = investorInfoService.investedSchemeList(holdingProfileId, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#invested-schemes GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @GetMapping(path = "/current-sips", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> currentSIPs(@RequestHeader MultiValueMap<String, Object> headers,
                                                            @RequestParam("holdingProfileId") String holdingProfileId) throws Exception {

        log.debug("#current-sips GET API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.list, "CurrentSipScheme");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                globalResponseBO = investorInfoService.currentSIPList(holdingProfileId, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#current-sips GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @PostMapping(path = "/current-sips", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> editSIP(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#current-sips POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "SimpleResponse");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                SIPEditBO sipEditBO = gson.fromJson(payload, SIPEditBO.class);
                globalResponseBO = investorInfoService.editCurrentSIP(sipEditBO, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#current-sips POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @GetMapping(path = "/current-stps", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> currentSTPs(@RequestHeader MultiValueMap<String, Object> headers,
                                                        @RequestParam("holdingProfileId") String holdingProfileId) throws Exception {

        log.debug("#current-stps GET API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.list, "StpScheme");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                globalResponseBO = investorInfoService.currentSTPList(holdingProfileId, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#current-stps GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @PostMapping(path = "/current-stps", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> editSTP(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#current-stps POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "SimpleResponse");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                SIPEditBO stpEditBO = gson.fromJson(payload, SIPEditBO.class);
                globalResponseBO = investorInfoService.editCurrentSTP(stpEditBO, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#current-stps POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @GetMapping(path = "/documents/download", produces = "application/pdf")
    public ResponseEntity<byte[]> download(@RequestHeader MultiValueMap<String, Object> headers,
                                                         @RequestParam("documentType") String documentType,
                                                         @RequestParam("documentId") String documentId,
                                                         @RequestParam("investorId") String investorId,
                                                         @RequestParam(name = "holdingProfileId", required = false) String holdingProfileId) throws Exception {

        log.debug("#download API Request | #DocumentId : " + documentId + "| #DocumentType : " + documentType);
        String filePath = "";
        HttpHeaders pdfheaders = new HttpHeaders();
        byte[] pdfContents = null;

        int userId = 0;

        try {

            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.info("Request:UserId " + userId);

            if(userId>0) {

                filePath = investorInfoService.documentDownload(documentType, documentId, investorId, holdingProfileId, userId);

                if (!StringUtils.isEmpty(filePath)) {

                    Path path = Paths.get(filePath);

                    Resource resource = new UrlResource(path.toUri());

                    pdfContents = Files.readAllBytes(path);

                    pdfheaders.add("consumes", MediaType.ALL_VALUE);
                    pdfheaders.setAccept(Arrays.asList(MediaType.APPLICATION_PDF));
                    pdfheaders.add("content-disposition", "attachment; filename=" + resource.getFilename());
                }
            }

            return new ResponseEntity<>(pdfContents, pdfheaders, HttpStatus.OK);

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
            throw  new IOException("Document download - File generation failed : DocumentId - " + documentId);
        }
    }

}

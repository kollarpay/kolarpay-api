package com.kp.core.app.razorpay.model;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RzpPaymentStatusResponseBO {

    private String id;
    private int entity;
    private String currency;
    private String status;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("invoice_id")
    private String invoiceId;
    private Boolean international;
    private String method;
    @SerializedName("amount_refunded")
    private int amountRefunded;
    @SerializedName("refund_status")
    private String refundStatus;
    private Boolean captured;
    private String description;
    @SerializedName("card_id")
    private String cardId;
    private String bank;
    private String wallet;
    private String vpa;
    private String email;
    private String contact;
    @SerializedName("customer_id")
    private String customerId;
    private String[] notes;
    private int fee;
    private int tax;
    @SerializedName("error_code")
    private int errorCode;
    @SerializedName("error_description")
    private String errorDescription;
    @SerializedName("error_source")
    private String errorSource;
    @SerializedName("error_step")
    private String errorStep;
    @SerializedName("error_reason")
    private String errorReason;
    @SerializedName("acquirer_data")
    private AcquirerDataBO acquirerData;
    @SerializedName("created_at")
    private String createdAt;

    @Getter
    @Setter
    public static class AcquirerDataBO {
        private String rrn;
    }
}

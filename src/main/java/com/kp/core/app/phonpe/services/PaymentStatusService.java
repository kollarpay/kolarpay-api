package com.kp.core.app.phonpe.services;

import com.google.gson.Gson;
import com.kp.core.app.config.PaymentConfig;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.phonpe.dao.SubscriptionDAO;
import com.kp.core.app.phonpe.model.PaymentEncodeResponse;
import com.kp.core.app.phonpe.model.PaymentStatusResponse;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
@Slf4j
public class PaymentStatusService {
    private final PaymentConfig paymentConfig = ConfigFactory.create(PaymentConfig.class);
    private final RestTemplate restTemplate;

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private SubscriptionDAO subscriptionDAO;
    private final Gson gson;

    public PaymentStatusService(RestTemplate restTemplate, Gson gson) {
        this.restTemplate = restTemplate;
        this.gson = gson;
    }

    public GlobalResponseBO updateTransactionStatus(PaymentEncodeResponse paymentEncodeResponse, int userId) {
        String logPrefix = "#UserId : " + userId + " | ";
        log.info("{}update transaction status Service", logPrefix);
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "updateTransactionStatus");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        PaymentStatusResponse paymentStatusResponse=new PaymentStatusResponse();
        try {
            String paymentResponse=paymentEncodeResponse.getResponse();

            byte[] paymentResponseBytes = Base64.getDecoder().decode(paymentResponse);
            String decodedString = new String(paymentResponseBytes);
            log.info("#paymentStatus POST API Response{}", decodedString);
            paymentStatusResponse = gson.fromJson(decodedString, PaymentStatusResponse.class);


        } catch (Exception e) {
            log.error("{}updateTransactionStatus API Exception : ", logPrefix, e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if (!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, paymentStatusResponse, globalResponseBo);

        log.debug(logPrefix + "Create Subscription API Status : " + stat);
        return globalResponseBo;
    }
}



package com.kp.core.app.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.kp.core.app.model.Products.ProductBO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CashFreeOrderBO {
    private String orderId;
    private double orderAmount;
    private String order_token;
    private String orderCurrency;
    private String orderNote;
    private String customerName;
    private String customerPhone;
    private String customerEmail;
    private String sellerPhone;
    private String returnUrl;
    private String notifyUrl;
    private String pc;
    private String customerId;
    private List<ProductBO> productBOS;
}

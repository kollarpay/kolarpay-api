package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortfolioHealthCheckBO {

    private List<Ratings> ratings;
    private AllocationBO assetAllocation;
    private AllocationBO categoryAllocation;
    private SecurityChoiceBO securityChoice;
    private DescriptionListBO securityConcentration;
    private DescriptionListBO liquidity;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Ratings{

        private String category;
        private double previousRating;
        private double postReviewRating;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AllocationBO{

        private double currentRating;
        private double postReviewRating;
        private List<AssetBO> assets;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AssetBO {

        @SerializedName("class")
        @JsonProperty("class")
        private String className;
        private String subCategory;
        private double currentAllocation;
        private double targetAllocation;
        private double proposedAllocation;
        private String currentAllocationWarning;
        private String proposedAllocationWarning;
        private double currentAllocationLimit;
        private double proposedAllocationLimit;
        private String comment;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class DescriptionListBO{

        private double currentRating;
        private double postReviewRating;
        private List<DescriptionBO> descriptions;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class DescriptionBO{

        private String description;
        private double currentAllocation;
        private double proposedAllocation;
        private int currentSecurities;
        private int proposedSecurities;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class SecurityChoiceBO{

        private double currentRating;
        private double postReviewRating;
        private List<RatingBO> ratings;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class RatingBO{

        private String rating;
        private AssetValueBO equity = new AssetValueBO();
        private AssetValueBO debt = new AssetValueBO();
        private AssetValueBO gold = new AssetValueBO();
        private AssetValueBO total = new AssetValueBO();
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class AssetValueBO{

        private double current = 0d;
        private double proposed = 0d;
        private int currentCount = 0;
        private int proposedCount = 0;
    }

}

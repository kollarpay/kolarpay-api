package com.kp.core.app.dao;

import com.kp.core.app.config.GeneralConfig;
import com.kp.core.app.model.*;
import com.kp.core.app.utils.DateUtil;
import com.kp.core.app.utils.BPConstants;
import com.kp.core.app.utils.GeneralUtil;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Repository
public class MandateInfoDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;

    @Autowired
    private InvestorInfoDAO investorInfoDAO;

    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    @Autowired
    private TransactionDAO transactionDAO;

    @Autowired
    Gson gson;

    @Autowired
    private GeneralUtil generalUtil;

    private GeneralConfig generalConfig = ConfigFactory.create(GeneralConfig.class);

    public MandateInfoBO getMandateInfoForENACH(int investorId, String consumerCode, int userId) {

        String logPrefix = "#UserId : " + userId + " | #ConsumerCode : " + consumerCode + " | #InvestorId : " + investorId;

        log.debug(logPrefix + "Mandate Info fetch");

        MandateInfoBO mandateBO =  new MandateInfoBO();

        try {


            String sqlQuery = " SELECT DISTINCT IOM.ConsumerCode, IOM.InvestorID, IOM.UpperLimit, IAI.HoldingProfileId, IBD.InvestorName, IB.UserBankId as BankId, IB.bankAccountType, " +
                    " IB.accountNumber, IB.IFSCCode, IB.BANKCATEGORY, IB.Active, IOM.DayOfMonth, IOM.MandateType, IB.ModeOfHolding, SI.SI_BankCode AS SI_TPSLBankId, " +
                    " IBD.InvestmentLimit, IBD.AllowTransact, IBD.isBankVerified, IBD.IrType, " +
                    " CASE " +
                    " WHEN IB.banklookupid = '999' THEN IB.bankname " +
                    " ELSE (SELECT lookupdescription FROM lookup WHERE lookupid = IB.banklookupid AND lookuptype = 'BANKLIST') " +
                    " END AS bankname " +
                    " FROM INVESTOR_OPEN_MANDATE_DETAILS IOM " +
                    " JOIN InvestorBankInfo IB ON IB.Investor_ID = IOM.InvestorID AND IOM.UserBankId = IB.UserBankId " +
                    " JOIN InvestorAdditionalInfo IAI ON IAI.PrimaryInvestorId = IOM.InvestorId AND IAI.RelationshipId = 'F' " +
                    " JOIN InvestorBasicDetail IBD ON IBD.InvestorID = IOM.InvestorID " +
                    " LEFT JOIN SI_ALLOWED_BANKS SI ON SI.BankLookUpId = IB.BankLookUpId AND SI.Active='A' AND VENDOR = 'TPSL_ENACH' " +
                    " WHERE IBD.UserId = ? AND IOM.investorid = ? AND IOM.ConsumerCode = ? ";

            mandateBO = jdbcTemplate.queryForObject(sqlQuery, new Object[]{userId, investorId, consumerCode}, (rs, rowNumber) -> {

                MandateInfoBO mandateInfoBO = new MandateInfoBO();

                mandateInfoBO.setConsumerCode(rs.getString("ConsumerCode"));
                mandateInfoBO.setCustomerId(rs.getString("cID"));
                mandateInfoBO.setHoldingProfileId(rs.getString("HoldingProfileId"));
                mandateInfoBO.setMaxAmount(rs.getInt("UpperLimit"));

                BankInfoBO bankInfoBO = investorInfoDAO.setBankInfoBO(rs);
               // bankInfoBO.setTpslSIBankId(rs.getString("SI_TPSLBankId"));

                mandateInfoBO.setBank(bankInfoBO);

                return mandateInfoBO;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        return mandateBO;

    }

    public String insertInvestorOpenMandate(CreateMandataBO mandataBO, String startDate, String endDate, String mandateType, String createdFrom, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert INVESTOR_OPEN_MANDATE_DETAILS");

        String consumerCode = "";

        try {
            String tempConsCode = generateConsumerCode(null);

            String sqlQuery = " INSERT INTO INVESTOR_OPEN_MANDATE_DETAILS (INVESTORID, CONSUMERCODE, FORMTYPE, NOOFYEARS, UPPERLIMIT, " +
                    " STARTDATE, ENDDATE, ACTIVE, USERID, CREATEDFROM, PDFFLAG,CREATEDUSER, CREATEDDATE, MODIFIEDUSER, MODIFIEDDATE, USERBANKID, MandateType ) " +
                    " VALUES(?, ?, ?, ?, ?, ?, ?, 'A', ?, ?, ?, ?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()), ?, ?)  ";

            int status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {

                int noOfYears = generalUtil.getYearDifference(startDate, endDate, true);

                ps.setInt(1, Integer.parseInt(mandataBO.getCustomerId()));
                ps.setString(2, tempConsCode);
                ps.setString(3, mandateType);
                ps.setInt(4, noOfYears);
                ps.setInt(5, mandataBO.getMaximumAmount());
                ps.setString(6, startDate);
                ps.setString(7, endDate);
                ps.setInt(8, userId);
                ps.setString(9, createdFrom);
                ps.setString(10, "T");
                ps.setInt(11, userId);
                ps.setInt(12, userId);
                ps.setInt(13, Integer.parseInt(mandataBO.getBankId()));
                ps.setString(14, mandateType);

            });

            if(status > 0)
                consumerCode = tempConsCode;

        } catch (Exception e) {
            log.error(logPrefix + "Exception in INVESTOR_OPEN_MANDATE_DETAILS insertion: ", e);
        }

        log.debug(logPrefix + "ConsumerCode : " + consumerCode);
        return consumerCode;
    }

    public int insertInvestorDocumentStatus(CreateMandataBO mandataBO, String consumerCode, int holdingProfileId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #ConsumerCode : " + consumerCode + " | ";
        log.debug(logPrefix + "Insert INVESTOR_DOCUMENT_STATUS_DETAILS");

        int status = 0;

        try {

            String sqlQuery = " INSERT INTO INVESTOR_DOCUMENT_STATUS_DETAILS ( INVESTORID, PROCID, STATUS, CREATEDDATE, CREATEDUSER, " +
                              " MODIFIEDDATE, MODIFIEDUSER, CONSUMERCODE, COMMENTS, HOLDINGPROFILEID, ACTIVE) " +
                              " VALUES(?, ?, ?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()), ?, ?, '', ?, 'A') ";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {

                String mandateType = mandataBO.getType().equalsIgnoreCase(BPConstants.MANDATE_TYPE_ENACH) ? "ENACH" : (mandataBO.getType().equalsIgnoreCase(BPConstants.MANDATE_TYPE_NACH) ? "NACH" : "");

                ps.setInt(1, Integer.parseInt(mandataBO.getCustomerId()));
                ps.setString(2, mandateType);
                ps.setString(3, "G");
                ps.setInt(4, userId);
                ps.setInt(5, userId);
                ps.setString(6, consumerCode);
                ps.setInt(7, holdingProfileId);

            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in INVESTOR_DOCUMENT_STATUS_DETAILS insertion: ", e);
        }

        return status;
    }

    public int insertConsumerCode(String consumerCode, int userId) {

        String logPrefix = "#UserId : " + userId + " | #ConsumerCode : " + consumerCode + " | ";
        log.debug(logPrefix + "Insert SIP_CONSUMER_DETAILS");

        int status = 0;

        try {

            String sqlQuery = " INSERT INTO SIP_CONSUMER_DETAILS (COSUMERCODE, CREATED_USER, MODIFIED_USER, MODIFIED_DATE ) VALUES (?, ?, ?,DATEADD(MI, 330, GETDATE())) ";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {

                ps.setString(1, consumerCode);
                ps.setInt(2, userId);
                ps.setInt(3, userId);

            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in SIP_CONSUMER_DETAILS insertion: ", e);
        }

        return status;
    }

    public int updateSIPConsumerDetails(String consumerCode, String paidStatus, String mandateRegNo, int userId) {

        String logPrefix = "#UserId : " + userId + " | #ConsumerCode : " + consumerCode + " | #PaidStatus : " + paidStatus + " | ";
        log.debug(logPrefix + "Update SIP_CONSUMER_DETAILS");

        int status = 0;

        try {

            String ffEligibleDay = generalConfig.tpslEnachFFEligibleDays();

            String sqlQuery = " UPDATE SIP_CONSUMER_DETAILS SET STATUS=?, FF_ELIGIBLE_DATE = ?, " +
                              " MTS_SERIEL_NO=?, MTS_NO_RECD_DATE = DATEADD(MI, 330, GETDATE()), MODIFIED_USER = ?, MODIFIED_DATE = DATEADD(MI, 330, GETDATE()), " +
                              " TECH_PROCESS_RCD_FLAG = 'Y', TECH_PROCESS_SENT_FLAG = 'Y', TECH_PROCESS_SENT_DATE = DATEADD(MI, 330, GETDATE()) " +
                              " WHERE COSUMERCODE = ? AND MTS_SERIEL_NO is null ";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {

                ps.setString(1, paidStatus);

                if(paidStatus.equalsIgnoreCase("APPROVED") && StringUtils.isNotEmpty(ffEligibleDay)) {

                    MFBusinessDayBO mfBusinessDayBO = generalInfoDAO.getNthMFBusinessDay(Integer.parseInt(ffEligibleDay));
                    ps.setString(2, mfBusinessDayBO.getDbNthBusinessDay());
                }
                else
                    ps.setString(2, null);

                ps.setString(3, mandateRegNo);
                ps.setInt(4, userId);
                ps.setString(5, consumerCode);

            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in SIP_CONSUMER_DETAILS update: ", e);
        }

        return status;
    }

    public int insertSIPBankDetails(String consumerCode, BankInfoBO bankInfoBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | #ConsumerCode : " + consumerCode + " | ";
        log.debug(logPrefix + "Insert SIP_CONSUMER_DETAILS");

        int status = 0;

        try {

            String sqlQuery = " INSERT INTO SIP_BANK_DETAILS (CONSUMERCODE, BANKLOOKUPID, ACCOUNTNUMBER, MICRCODE, CREATEDDATE, MODIFIEDDATE) "
                            + " VALUES (?, ?, ?, ?, DATEADD(MI, 330, GETDATE()),DATEADD(MI, 330, GETDATE())) ";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {

                ps.setString(1, consumerCode);
                ps.setString(2, bankInfoBO.getBankLookUpId());
                ps.setString(3, bankInfoBO.getAccountNo());
                ps.setString(4, bankInfoBO.getMicr());

            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in SIP_CONSUMER_DETAILS insertion: ", e);
        }

        return status;
    }

    public double getExistingAmountInMandate(String consumerCode, String sipDate, String mandateType, int userId) {

        String logPrefix = "#UserId : " + userId + " | #ConsumerCode : " + consumerCode + " | #SipDate : " + sipDate + " | #MandateType : " + mandateType + " | ";

        log.debug(logPrefix + "Existing Mandate Amount fetch");

        List<Integer> status = new ArrayList<>();
        AtomicReference<Double> existingSIPTotal = new AtomicReference<>(0d);
        AtomicReference<Double> existingVIPTotal = new AtomicReference<>(0d);
        AtomicReference<Double> rmgpAmt = new AtomicReference<>(0d);
        AtomicReference<Double> lumpsumAmt = new AtomicReference<>(0d);
        double existingTotalAmnt = 0d;
        String addCondition = "";

        try {

            String sqlQuery = "SELECT S.SIPID,S.SIPTYPE,S.ECSAMOUNT,S.MAXAMOUNT " +
                    "  FROM INVESTOR_OPEN_MANDATE_DETAILS O " +
                    "  INNER JOIN SIPDETAILS S ON O.CONSUMERCODE = S.CONSUMERCODE AND O.ACTIVE='A' " +
                    "  WHERE S.ACTIVE in(1,2,3) AND S.SIPTYPE <> 'ALERT' " +
                    "  AND S.CONSUMERCODE = ? AND S.ECSDATE = ? AND O.USERID = ? ";
            status = jdbcTemplate.query(sqlQuery, new Object[]{consumerCode, sipDate, userId}, (rs, rowNumber) -> {

                String sipType = rs.getString("SIPTYPE") == null ? "" : rs.getString("SIPTYPE").trim();
                if (sipType.equalsIgnoreCase(BPConstants.FLEXI_SIP_TYPE) || sipType.equalsIgnoreCase(BPConstants.STEPUP_SIP_TYPE)) {
                    existingSIPTotal.set(existingSIPTotal.get() + rs.getDouble("MAXAMOUNT"));
                } else {
                    existingSIPTotal.set(existingSIPTotal.get() + rs.getDouble("ECSAMOUNT"));
                }

                return 1;
            });

            sqlQuery = "SELECT sum(V.MAXIMUMAMOUNT) as MAXIMUMAMOUNT " +
                    "  FROM VIPSETUP V " +
                    "  JOIN INVESTOR_OPEN_MANDATE_DETAILS O ON V.CONSUMERCODE = O.CONSUMERCODE " +
                    "  JOIN INVESTORTRANSACTION T ON T.USERTRANSREFID = V.USERTRANSREFID  AND T.ACTIVE = 'A' AND ((T.TRANSACTIONSTATUS = 'TS' AND V.ACTIVEFLAG IN (1,2,3) ) " +
                    "  or  (T.TRANSACTIONSTATUS IN ('PW','TM','PS') )) " +
                    "  WHERE O.ACTIVE='A'  AND V.CONSUMERCODE = ? AND V.VIPDATE = ? AND O.USERID = ? ";

            status = jdbcTemplate.query(sqlQuery, new Object[]{consumerCode, sipDate, userId}, (rs, rowNumber) -> {

                existingVIPTotal.set(existingVIPTotal.get() + rs.getDouble("MAXIMUMAMOUNT"));

                return 1;
            });

            if("SI".equalsIgnoreCase(mandateType)){
                addCondition = " CONVERT(VARCHAR(10),Actual_Debit_Date,23) > CONVERT(VARCHAR(10),DATEADD(MI,330,GETDATE()),23) AND ";
            }else {
                addCondition = " TPSL_Forward_Feed IS NULL AND ";
            }

            sqlQuery = "Select sum(Amount) as LumpsumAmount from InvestorTransactionLumpsum where " + addCondition +
                    " Active = 1 and DAY(Actual_Debit_Date)  = ? and ConsumerCode = ?  ";

            status = jdbcTemplate.query(sqlQuery, new Object[]{sipDate, consumerCode}, (rs, rowNumber) -> {

                lumpsumAmt.set(rs.getDouble("LumpsumAmount"));

                return 1;
            });


        } catch (Exception e){
            log.error(logPrefix + "Exception : ", e);
        }

        existingTotalAmnt = existingSIPTotal.get() + existingVIPTotal.get() + rmgpAmt.get() + lumpsumAmt.get();
        log.debug(logPrefix + "Existing Mandate Amount : " + existingTotalAmnt);

        return existingTotalAmnt;
    }

    public String generateConsumerCode(String createdfrom){

        String fullConsumerCode = "";
        try{
            int consumer_code = autoNumberGenerator.getAutoNumberGenerated("NACHMandRefId");
            fullConsumerCode = "10000000" + consumer_code;
            if (!StringUtils.isEmpty(createdfrom) && "MA".equalsIgnoreCase(createdfrom)) {
                fullConsumerCode = "80000000" + consumer_code;
            }

        }catch(Exception e){
            log.error("Exception: ", e);
        }
        return fullConsumerCode;
    }

    public int updateConsumerCodeInCart(String consumerCode, int cartId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #ConsumerCode : " + consumerCode + " | #CartId : " + cartId + " | ";
        log.debug(logPrefix + "Update InvestorCartDetails");

        int status = 0;

        try {

            String sqlQuery = " UPDATE InvestorCartDetails SET ConsumerCode = ?, ModifiedUser = ?, ModifiedDate = DATEADD(MI,330,GETDATE()) WHERE CartId = ? AND Active=1 ";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {

                ps.setString(1, consumerCode);
                ps.setInt(2, userId);
                ps.setInt(3, cartId);

            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorCartDetails update: ", e);
        }

        return status;
    }
    public double getAvalbleDayLimitAmt(String cons_code, String sip_date, int upper_limit, String mandateType, int userId) {
        double avlableAmt = 0;
        try {


            if (cons_code != null && cons_code.length() > 0 && sip_date != null && sip_date.length() > 0) {

                if(StringUtils.isEmpty(mandateType) || upper_limit==0) {
                    HashMap<String, String> mandateDetails = getMandateDetails(cons_code);
                    if(upper_limit == 0)
                        upper_limit = StringUtils.isNotEmpty(mandateDetails.get("upperLimit")) ? Integer.parseInt(mandateDetails.get("upperLimit")) : 0;
                    if(StringUtils.isEmpty(mandateType))
                        mandateType = mandateDetails.get("mandateType");
                }

                double existingAmt = getExistingAmountInMandate(cons_code, sip_date, mandateType, userId);

                BigDecimal totAddedAmt = new BigDecimal(0);
                /*if (from.equalsIgnoreCase(FIConstants.DESIGN_SIP_TYPE) || from.equalsIgnoreCase(FIConstants.PORT_SIP_TYPE)
                        || from.equalsIgnoreCase(FIConstants.SMART_SIP_TYPE)) {
                    totAddedAmt = new BigDecimal(0);
                } else {
                    totAddedAmt = getSIPAddedAmount(request, cons_code, sip_date, from); :todo sip added available amount concept
                }*/

                avlableAmt = (double) upper_limit - (existingAmt + totAddedAmt.doubleValue());

                log.debug("cons: " + cons_code + ", upper lmt: " + upper_limit + ", totAddedAmt: " + totAddedAmt + ", avlbl Amt: " + avlableAmt);

            }

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return avlableAmt;
    }

    public double getMandateLimitForSIPEdit(int sipRefId, int userId, boolean isFlexi) {

        String logPrefix = "#SipRefId : " + sipRefId + " | #UserId : " + userId + " | ";
        double avlableAmt = 0;

        try {

            HashMap<String, String> details = getECSDetails(sipRefId, isFlexi);

            avlableAmt = getAvalbleDayLimitAmt(
                    details.get("consumerCode"),
                    details.get("ecsDate"),
                    Integer.parseInt(details.get("upperLimit")),
                    details.get("mandateType"),
                    userId);

            double sipAmount = Double.parseDouble(details.get("amount"));

            avlableAmt = avlableAmt + sipAmount;

        } catch (Exception e) {
            log.error(logPrefix + "Mandate Limit Fetch for SIP edit - Exception :", e);
        }
        log.debug(logPrefix + "Available amount  : " + avlableAmt);
        return avlableAmt;
    }

    public String getNextWorkingDayForTPSL() { //:todo need to check the logic

        String currentTime =   "";
        String queryReturn  =   "";
        String nextDate = "";
        String dateOfDay = "";
        DateUtil dateUtil = new DateUtil();
        try {
            currentTime =   getCurrentTime();
            String batchTime[] = currentTime.split(":");
            if (Integer.parseInt(batchTime[0]) >= 14)
                nextDate = dateUtil.addNoofDays(1);
            else
                nextDate = dateUtil.getTodayDateXLformat();

            String sqlQuery = "SELECT CONVERT(VARCHAR(10), DateOfHoliday, 120) AS DateOfHoliday " +
                    "FROM TPSL_OR_BANK_HOLIDAY WHERE CONVERT(VARCHAR(10), DateOfHoliday, 120) = ? ";
            int i = 0;
            while (i < 2) {
                dateOfDay = dateUtil.getDateofDay(nextDate);
                if (dateOfDay.equalsIgnoreCase("Sat")) {
                    nextDate = dateUtil.addNoofDays(nextDate, 1);
                } else if (dateOfDay.equalsIgnoreCase("Sun")) {
                    nextDate = dateUtil.addNoofDays(nextDate, 1);
                } else {
                    queryReturn = jdbcTemplate.queryForObject(sqlQuery, new Object[]{nextDate}, String.class);
                    if( (!queryReturn.equals("")) &&  (i == 0) ) {
                        nextDate = dateUtil.addNoofDays(nextDate, 1);
                        i++;
                    }
                }
            }
            log.info("CurrentTime:" + currentTime + " nextWorkingDay:" + nextDate);
        } catch (Exception daoEx) {
            log.error("DAO - NXT WRK'G DAY # I/P : " + nextDate + " Exception : " + daoEx);
        }

        return nextDate;
    }

    public String getCurrentTime() {
        String queryReturn = "";
        try {
            String sqlQuery = "SELECT CONVERT(char(8), DATEADD(MI, 330, GETDATE()), 114) AS HHMMSS";
            queryReturn = jdbcTemplate.queryForObject(sqlQuery, new Object[]{}, String.class);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return queryReturn;
    }

    /**
     *
     * @param
     */
    public List<MandateInfoBO> getOpenMandListForPurchase(int userId, int holdingProfileId, int investorId, String sip_date, String userBankId, String consumerCode, boolean activatedInvestor){

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | #InvestorId : " + investorId;

        List<MandateInfoBO> mandateInfoBOArrayList = new ArrayList<>();
        String investmentBank = "", appendCond = "";

        try {
//            String investorIds = "175288,176119,176120,176121,176122,176123,176124,176215,177535,177538,177539,177540,177541,177542,177543,177544,177545,177546,177547,177548,177549,177550,177551,177552,177554,177555,177556,177557,177558,177559,177560,177561,177562,178005,178017,178018,178019,178021,178024";
            String investorIds = String.valueOf(investorId);
            if(investorId==0)
                investorIds = getInvestorListForMandateLimit(holdingProfileId, userBankId);
            if (StringUtils.isNotEmpty(userBankId))
                investmentBank = " AND IB.userBankId = " + userBankId;
            if(StringUtils.isNotEmpty(consumerCode))
                appendCond += " AND O.ConsumerCode = '" + consumerCode + "' ";
            if(activatedInvestor)
                appendCond += " AND IBD.InvestmentLimit=-1 AND IBD.AllowTransact='A' ";

            String sqlQuery = "SELECT DISTINCT O.CONSUMERCODE, O.FORMTYPE, O.MandateType, O.ENDDATE, O.UPPERLIMIT, O.INVESTORID,IBD.IRTYPE,IBD.OEIHN,O.dayOfMonth,O.amcCode,A.FUND amcName, " +
                    " B.BANKLOOKUPID,B.ACCOUNTNUMBER,B.MICRCODE, D.STATUS DSTATUS,C.FF_ELIGIBLE_DATE, O.CREATEDFROM, IB.BankCategory, IB.BankAccountType, IB.ModeOfHolding, IB.UserBankId AS BankId, " +
                    " IB.IfscCode, IBD.InvestorName, SI.SI_BankCode AS SI_TPSLBankId, O.Active AS MandateActive, IBD.InvestmentLimit, IBD.AllowTransact, IBD.isBankVerified, IBD.IrType, " +
                    " CASE " +
                    "       WHEN C.STATUS = 'APPROVED' THEN 'Approved' " +
                    "       WHEN D.STATUS = 'R' THEN 'MR' " +
                    "		ELSE 'MNR' END STATUSDESC, " +
                    " CASE " +
                    "       WHEN IB.banklookupid = '999' THEN IB.bankname " +
                    "       ELSE (SELECT lookupdescription FROM lookup WHERE lookupid = IB.banklookupid AND lookuptype = 'BANKLIST') " +
                    "       END AS Bankname " +
                    " FROM INVESTOR_OPEN_MANDATE_DETAILS O" +
                    " INNER JOIN SIP_BANK_DETAILS B ON O.CONSUMERCODE = B.CONSUMERCODE " +
                    " INNER JOIN SIP_CONSUMER_DETAILS C ON O.CONSUMERCODE = C.COSUMERCODE AND ( C.STATUS IS NULL OR C.STATUS = 'APPROVED' )" +
                    " INNER JOIN INVESTORBASICDETAIL IBD ON  IBD.INVESTORID=O.INVESTORID " +
                    " INNER JOIN InvestorBankInfo IB on IB.accountNumber = B.AccountNumber AND IB.BANKLOOKUPID = B.BankLookupId " +
                    "   AND IB.Investor_ID = O.INVESTORID " + investmentBank +
                    " LEFT JOIN SI_ALLOWED_BANKS SI ON SI.BankLookUpId = IB.BankLookUpId AND SI.Active='A' AND SI.VENDOR = 'TPSL_ENACH' " +
                    " LEFT JOIN dbo.AFT_AMC_MST A ON O.AMCCode=A.AMC_CODE " +
                    " LEFT JOIN INVESTOR_DOCUMENT_STATUS_DETAILS D ON O.INVESTORID = D.InvestorID AND O.CONSUMERCODE = D.ConsumerCode AND D.ACTIVE = 'A' " +
                    " LEFT JOIN LOOKUP L ON D.STATUS = L.LOOKUPID AND L.LOOKUPTYPE IN('ECS_FORM','DD_FORM') AND L.ACTIVE = 1 " +
                    " WHERE O.FormType IN('NACH','SI','ENACH') and  O.ACTIVE='A' AND O.ENDDATE > DATEADD(MI, 330, GETDATE()) AND " + //Yes bank allowed for Lumpsum  and O.MandateType = 'NACH'
                    " O.INVESTORID IN( " + investorIds + ") AND O.USERID = ?  " + appendCond +
                    " AND ( (O.FORMTYPE ='SI' AND C.STATUS = 'APPROVED' AND SI_INSTRUCTION_ID IS NOT NULL) OR  O.FORMTYPE <>'SI') "+
                    " ORDER BY STATUSDESC, O.CONSUMERCODE";

            mandateInfoBOArrayList = jdbcTemplate.query(sqlQuery, new Object[]{userId}, (rs, rowNumber) -> {

                MandateInfoBO mandateBO = new MandateInfoBO();

                //OE investor, Open mandate created from APPForm. so mandate should not display in SIP/VIP
                if ((BPConstants.ONLINE_ENROLLMENT.equalsIgnoreCase(rs.getString("irtype"))) && (rs.getString("MandateType").equals("OPEN")) && (rs.getString("CREATEDFROM").equals("APPFORM"))) {
                    log.info("OE investor, Open mandate created from APPForm. investorid:" + rs.getInt("INVESTORID") + " consumercode:" + rs.getString("CONSUMERCODE"));
                }

                else {

                    //if (FIConstants.ONLINE_ENROLLMENT.equalsIgnoreCase(rs.getString("irtype")) && (rs.getString("MandateType").equals("OPEN"))) {
                        //if (sip_date.equals(rs.getString("dayOfMonth"))) {

                            String orgConsCode = (rs.getString("CONSUMERCODE") != null) ? rs.getString("CONSUMERCODE").trim() : "";
                            String accNo = (rs.getString("ACCOUNTNUMBER") != null) ? rs.getString("ACCOUNTNUMBER").trim() : "";
                            String micrCode = (rs.getString("MICRCODE") != null) ? rs.getString("MICRCODE").trim() : "";
                            String formType = (rs.getString("FORMTYPE") != null) ? rs.getString("FORMTYPE").trim() : "";
                            String status = rs.getString("STATUSDESC");
                            int upperLimit = rs.getInt("UPPERLIMIT");
                            DateBO endDate = new DateBO(rs.getString("ENDDATE"));

                            mandateBO.setConsumerCode(orgConsCode);
                            mandateBO.setHoldingProfileId(String.valueOf(holdingProfileId));
                            mandateBO.setMaxAmount(upperLimit);
                            mandateBO.setExpiresOn(endDate.getDateFirstView());
                            mandateBO.setCustomerId(rs.getString("INVESTORID"));
                            mandateBO.setName(rs.getString("InvestorName"));
                            mandateBO.setStatus(status);
                            mandateBO.setRemarks("");
                            mandateBO.setActivated(rs.getString("MandateActive").equalsIgnoreCase("A"));

                            String dbnextBusinessDay = "";
                            String tempSipDate;

                            if ("SI".endsWith(rs.getString("MandateType"))) {
                                MFBusinessDayBO businessDayBO = generalInfoDAO.getNthMFBusinessDay(2);
                                dbnextBusinessDay = businessDayBO.getDbNthBusinessDay();
                                tempSipDate = businessDayBO.getDays();
                            }
                            else
                                tempSipDate = sip_date;

                            double avlbleDayLimitAmt = rs.getDouble("UpperLimit");
                            if(StringUtils.isNotEmpty(sip_date))
                                avlbleDayLimitAmt = getAvalbleDayLimitAmt(orgConsCode, tempSipDate, upperLimit, rs.getString("MandateType"), userId);

                            mandateBO.setAvailableAmount(avlbleDayLimitAmt);

                            BankInfoBO bankInfoBO = investorInfoDAO.setBankInfoBO(rs);
                            bankInfoBO.setMicr(micrCode);
                            mandateBO.setBank(bankInfoBO);

                            //String maskdACNo = GeneralUtil.maskBankAccountNumbe(accNo);

                            //bankName = bankName.replace(" Ltd", "");
                            /*boolean isOE = (FIConstants.ONLINE_ENROLLMENT.equalsIgnoreCase(rs.getString("irtype")) && (rs.getString("MandateType").equals("OPEN"))); //:todo need to identify
                            if (isOE) {
                                IdDesc ecsFormType = new IdDesc();
                                ecsFormType.setId("ECS");
                                ecsFormType.setValue("2");
                                mandateBO.setSIPFormTypeTDays(ecsFormType);
                                //mandateBO.setOE(isOE + "");
                                mandateBO.setAmcCode(rs.getString("amcCode"));
                                mandateBO.setAmcName(rs.getString("amcName"));

                            } else {
                                mandateBO.setSIPFormTypeTDays(getDDorECSFormWithTDays(formType, micrCode));
                            }*/

                            DateBO ffEligibleDate = new DateBO(rs.getString("FF_ELIGIBLE_DATE"));
                            try {
                                if (StringUtils.isNotEmpty(dbnextBusinessDay)) {//SI
                                    Date siDebitDt = GeneralUtil.getDateFromStr(new DateBO(dbnextBusinessDay).getDefaultDBFormat());
                                    Date ffEligibleDt = GeneralUtil.getDateFromStr(ffEligibleDate.getDefaultDBFormat());
                                    if (siDebitDt.compareTo(ffEligibleDt) < 0) {
                                        mandateBO.setStatus("NotAllowed");
                                        log.info(mandateBO.getConsumerCode() + " Mandate not allowed due to ffeligible date " + ffEligibleDate.getDateFirstView() + " failed. debit date " + new DateBO(dbnextBusinessDay).getDateFirstView());
                                    }
                                }else if ("ENACH".equalsIgnoreCase(rs.getString("MandateType"))) {
                                    String strtoDayDate = GeneralUtil.gettodayDtyyyMMdd();
                                    Date toDayDate = GeneralUtil.getDateFromStr(strtoDayDate);
                                    Date ffEligibleDt = GeneralUtil.getDateFromStr(ffEligibleDate.getDefaultDBFormat());
                                    if (toDayDate.compareTo(ffEligibleDt) < 0) {
                                        mandateBO.setStatus("NotAllowed");
                                        log.info(mandateBO.getConsumerCode() + " Mandate not allowed due to ffeligible date " + ffEligibleDate.getDefaultDBFormat() + " failed. today date " + strtoDayDate);
                                    }
                                }
                            }catch(Exception e){
                                log.info("error in mandate disable"+e);
                            }

                            if (((avlbleDayLimitAmt - upperLimit) != 0) && StringUtils.isNotEmpty(bankInfoBO.getBankAccountType())
                                    && bankInfoBO.getBankAccountType().equalsIgnoreCase(BPConstants.ACC_TYPE_NRE)) {
                                mandateBO.setStatus("NotAllowed");
                            }

                            String maskdACNo = GeneralUtil.maskBankAccountNumber(accNo);
                            mandateBO.setBankNameAccNo(bankInfoBO.getBankName() + " - " + maskdACNo + " - Available Amount for Purchase: Rs " + GeneralUtil.getZeroBigDecimalRupees(new BigDecimal(avlbleDayLimitAmt)));
                        }
                    /*} else {
                        *//*if (StringUtils.isEmpty(rs.getString("amcCode")))
                            investorBankList.add(mandateBO);*//*
                    }*/
                //}
                return mandateBO;
            });

        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }
        return mandateInfoBOArrayList;
    }

    public IdDesc getDDorECSFormWithTDays(String form_type, String micr_code) {

        IdDesc formType = new IdDesc();
        formType.setKey("NOTSUPPORTED");
        try {

            if ("DD".equalsIgnoreCase(form_type)) {
                formType.setKey("DD");
                formType.setValue("1");
            } else if ("NACH".equalsIgnoreCase(form_type)) {
                formType.setKey("NACH");
                formType.setValue("1");
            }  else if ("ENACH".equalsIgnoreCase(form_type)) {
                formType.setKey("ENACH");
                formType.setValue("1");
            } else if ("SI".equalsIgnoreCase(form_type)) {
                formType.setKey("SI");
                formType.setValue("1");
            } else if ("ECS".equalsIgnoreCase(form_type)) {

                formType.setKey("ECS");
                formType.setValue("0");

                // resetting the values if ECS_MICR_CODE table values available
                String sqlQuery = " SELECT MICR,TDAYS FROM ECS_MICR_CODE WHERE MICR =? ";
                jdbcTemplate.query(sqlQuery, new Object[]{micr_code}, (rs, rowNumber) -> {
                    formType.setKey("ECS");
                    formType.setValue("" + rs.getInt("TDAYS"));
                    return formType;
                });

            }

        } catch (Exception e) {
            log.error("Exception while checking DD or ECS TDays : " + e.getMessage());
        }
        return formType;
    }

    /*public String getMandateType(String cons_code) throws Exception {
        String mandateType = "";
        try {
            String sqlQuery = "select MandateType from dbo.INVESTOR_OPEN_MANDATE_DETAILS where ConsumerCode = ? ";
            mandateType  =   jdbcTemplate.query(sqlQuery, new Object[]{cons_code}, (rs, rowNumber) -> {
                mandateType = rs.getString("MandateType")==null?"":rs.getString("MandateType").trim();
                return mandateType;
            });
            log.debug("cons_code: "+cons_code+", mandateType: "+mandateType);
        } catch (Exception e) {
            log.error(" Ex: " , e);
        }

        return mandateType;
    }*/


    public String getInvestorListForMandateLimit(int hp_id, String investmentBankNRI) throws Exception {

        String investors = "";

        try {
            IdDesc returnIdDesc = getBankAcType(hp_id, investmentBankNRI);
            String accType = "";
            if (returnIdDesc != null) {
                if ("1".equalsIgnoreCase(returnIdDesc.getKey()) && "4".equalsIgnoreCase(returnIdDesc.getValue())) {
                    accType = "NRE";
                } else if ("1".equalsIgnoreCase(returnIdDesc.getDesc())) {
                    accType = "MINOR";
                } else {
                    accType = "INDIVIDUAL";
                }
            }
            String sqlQuery;

            if(StringUtils.isEmpty(investmentBankNRI))
                sqlQuery = "SELECT ASSOCIATEDINVESTORID FROM INVESTORADDITIONALINFO WHERE HOLDINGPROFILEID = ? ";

            else if ("MINOR".equalsIgnoreCase(accType))
                sqlQuery = "SELECT ASSOCIATEDINVESTORID FROM INVESTORADDITIONALINFO WHERE HOLDINGPROFILEID = ? and RelationshipID <> 'G'";

            else
                sqlQuery = "SELECT ASSOCIATEDINVESTORID FROM INVESTORADDITIONALINFO WHERE HOLDINGPROFILEID = ? AND RELATIONSHIPID ='F'";


            List<String> investorIds = jdbcTemplate.query(sqlQuery, new Object[]{hp_id}, (rs, rowNumber) -> rs.getString("ASSOCIATEDINVESTORID"));

            if(investorIds!=null && investorIds.size()>0)
                investors = StringUtils.join(investorIds, ",");

        } catch (Exception daoEx) {
            log.error("HP : " + hp_id + ", Exception : " + daoEx);
            throw new Exception(daoEx);
        }
        return investors;
    }


    public IdDesc getBankAcType(int hpId, String investmentForNRI) {

        IdDesc idDesc = new IdDesc();
        try {

            String investmentBank = "";

            if (!StringUtils.isEmpty(investmentForNRI))
                investmentBank = " AND IBI.USERBANKID = " + investmentForNRI;
            else
                investmentBank = " AND IBI.USERBANKID = 1 ";

            String sqlQuery = "SELECT IBD.ISNRI,IBI.BANKACCOUNTTYPE,IBD.ISMINOR FROM INVESTORADDITIONALINFO IAI," +
                    " INVESTORBANKINFO IBI,INVESTORBASICDETAIL IBD,INVESTORHOLDINGPROFILE IHP WHERE" +
                    " IBI.INVESTOR_ID = IBD.INVESTORID AND IAI.HOLDINGPROFILEID = IHP.HOLDINGPROFILEID" +
                    " AND IBD.INVESTORID = IAI.ASSOCIATEDINVESTORID AND RELATIONSHIPID = 'F' " + investmentBank +
                    " AND IBI.INVESTORTYPE='MF' AND IHP.HOLDINGPROFILEID = ? ";

             jdbcTemplate.query(sqlQuery, new Object[]{hpId}, (rs, rowNumber) -> {

                IdDesc returnIdDesc = new IdDesc();
                returnIdDesc.setKey(rs.getString("ISNRI"));
                returnIdDesc.setValue(rs.getString("BANKACCOUNTTYPE"));
                returnIdDesc.setDesc(rs.getString("ISMINOR"));
                return returnIdDesc;

            });

        } catch (Exception daoEx) {
            log.error("Ex: " + daoEx.toString());
        }
        return idDesc;
    }

    public HashMap<String, String> getECSDetails(int sipRefId, boolean isFlexi) {

        HashMap<String, String> result = new HashMap<>();
        try {

            String sqlQuery = " SELECT S.ECSDate, O.UpperLimit, S.ConsumerCode, S.FlexiAmount, S.ECSAmount, S.FlexiAmount, O.MandateType " +
                    " FROM SIPDetails S " +
                    " INNER JOIN INVESTOR_OPEN_MANDATE_DETAILS O ON O.ConsumerCode = S.ConsumerCode " +
                    " INNER JOIN SIP_BANK_DETAILS B ON O.CONSUMERCODE = B.CONSUMERCODE " +
                    " INNER JOIN SIP_CONSUMER_DETAILS C ON O.CONSUMERCODE = C.COSUMERCODE AND ( C.STATUS IS NULL OR C.STATUS = 'APPROVED' )" +
                    " WHERE S.SIPId = ? AND S.Active = 1 ";

            result = jdbcTemplate.queryForObject(sqlQuery, new Object[]{sipRefId}, (rs, rowNumber) -> {

                HashMap<String,String> details = new HashMap<>();
                details.put("ecsDate", rs.getString("ECSDate"));
                details.put("consumerCode", rs.getString("ConsumerCode"));
                details.put("mandateType", rs.getString("MandateType"));
                details.put("upperLimit", rs.getString("UpperLimit"));
                details.put("amount", isFlexi ? rs.getString("FlexiAmount") : rs.getString("ECSAmount"));
                return details;

            });

        } catch (Exception e) {
            log.error("Exception while fetching ECSDate: ", e);
        }

        return result;
    }

    public HashMap<String, String> getMandateDetails(String consumerCode) {

        HashMap<String, String> result = new HashMap<>();
        try {

            String sqlQuery = " SELECT O.UpperLimit, O.MandateType " +
                    " FROM INVESTOR_OPEN_MANDATE_DETAILS O " +
                    " INNER JOIN SIP_CONSUMER_DETAILS C ON O.CONSUMERCODE = C.COSUMERCODE AND ( C.STATUS IS NULL OR C.STATUS = 'APPROVED' )" +
                    " WHERE O.CONSUMERCODE = ? AND O.Active = 'A' ";

            result = jdbcTemplate.queryForObject(sqlQuery, new Object[]{consumerCode}, (rs, rowNumber) -> {

                HashMap<String,String> details = new HashMap<>();
                details.put("mandateType", rs.getString("MandateType"));
                details.put("upperLimit", rs.getString("UpperLimit"));
                return details;

            });

        } catch (Exception e) {
            log.error("Exception while fetching MandateType & UpperLimit: ", e);
        }

        return result;
    }
}

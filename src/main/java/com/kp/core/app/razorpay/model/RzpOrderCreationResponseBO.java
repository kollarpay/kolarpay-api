package com.kp.core.app.razorpay.model;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Getter
@Setter
public class RzpOrderCreationResponseBO {

    private String id;
    private String entity;
    private BigInteger amount;
    @SerializedName("amount_paid")
    private BigInteger amountPaid;
    @SerializedName("amount_due")
    private BigInteger amountDue;
    private String currency;
    private String receipt;
    @SerializedName("offer_id")
    private String offerId;
    private String status;
    private int attempts;
    @SerializedName("created_at")
    private int createdAt;



}

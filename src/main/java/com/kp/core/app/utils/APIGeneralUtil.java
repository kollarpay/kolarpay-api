package com.kp.core.app.utils;

import com.kp.core.app.config.PaymentConfig;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import jakarta.servlet.http.HttpServletRequest;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

@Slf4j
@Service
public class APIGeneralUtil {

    private static PaymentConfig paymentConfig = ConfigFactory.create(PaymentConfig.class);

    public static String getHeader(MultiValueMap<String, Object> httpHeaders, String headerName) throws Exception {

        String value = "";
        try {

            if (httpHeaders.get(headerName) != null) {
                int len = httpHeaders.get(headerName).size();
                value = (String) httpHeaders.get(headerName).get(len - 1);
            }
        } catch (Exception ex) {
            log.error(ex+headerName);
        }

        return value;

    }
    public static GlobalResponseBO getGlobalResponseBO(String stat, List<ErrorBO> errorBoList, Object reponseData, GlobalResponseBO globalResponseBO) {

        try {
            if(stat.equalsIgnoreCase(BPConstants.SUCCESS) || stat.equalsIgnoreCase("")) {
                globalResponseBO.setData(reponseData!=null ? reponseData : globalResponseBO.getData() );
                globalResponseBO.setSuccess(true);
                globalResponseBO.setDesc(BPConstants.SUCCESS);
                globalResponseBO.setCode(APIConstants.SUCCESS_INT);
            } else {
                if(errorBoList != null && errorBoList.size()>0)
                    globalResponseBO.setErrors(errorBoList);

                globalResponseBO.setSuccess(false);
                globalResponseBO.setData("");
                if(stat.equalsIgnoreCase(BPConstants.UNAUTHORIZED)) {
                    globalResponseBO.setDesc(APIConstants.UNAUTHORIZED_STRING);
                    globalResponseBO.setCode(APIConstants.UNAUTHORIZED);
                }
                else {
                    globalResponseBO.setDesc(APIConstants.BAD_REQUEST_STRING);
                    globalResponseBO.setCode(APIConstants.BAD_REQUEST);
                }
            }

        } catch( Exception e) {
            log.error("Exception: ", e);
        }
        return globalResponseBO;
    }
    public static int getUserIdFromHeader(MultiValueMap<String, Object> headers) throws Exception {
        int userId;
        try {
            userId = NumberUtils.toInt(getHeader(headers, "user-id"));
        } catch (Exception e) {
            log.error("Exception while reading header:", e);
            throw new Exception(e);
        }
        return userId;
    }
    public static String getIPaddress(HttpServletRequest request) {
        try {
            return request.getRemoteAddr();
        } catch (Exception e){
            log.error("IPAddress | Exception: {}", e, e);
            return "";
        }
    }
    public static String getBase64(String payload) {
        String base64 = "";
        try {
             base64=  Base64.getEncoder().encodeToString(payload.getBytes());
        } catch (Exception e){
            log.error("getBase64 | Exception: {}", e, e);
            return "";
        }
        return base64;
    }

    public static String getCheckSum(String base64, String url) {
       String phonePeApiKey= paymentConfig.getPhonePeApiKey();
       String phonePeApiKeyIndex= paymentConfig.getPhonePeApiKeyIndex();
       String  checksum=base64+url+phonePeApiKey;
        System.out.println("sha256hex1----"+checksum);
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance( "SHA-256" );
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        byte[] sha256hexBytes =md.digest(checksum.getBytes( StandardCharsets.UTF_8 ) ) ;
        String sha256hex=toHexString(sha256hexBytes)+"###"+phonePeApiKeyIndex;
        return sha256hex;

    }
    public static String toHexString( byte[ ] hash )
    {
        // For converting byte array into signum representation
        BigInteger number = new BigInteger( 1, hash ) ;
        // For converting message digest into hex value
        StringBuilder hexString = new StringBuilder( number.toString( 16 ) ) ;

        // Pad with leading zeros
        while ( hexString.length( ) < 32 )
        {
            hexString.insert( 0,  " 0 " ) ;
        }
        return hexString.toString( ) ;
    }

    public static GlobalResponseBO buildErrorResponse(Integer statusCode, String message, GlobalResponseBO response) {
        response.setName(BPConstants.SIMPLE_ERROR_RESPONSE);
        response.setCode(statusCode);
        response.setDesc(message);
        return response;
    }

}

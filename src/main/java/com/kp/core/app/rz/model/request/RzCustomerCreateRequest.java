package com.kp.core.app.rz.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RzCustomerCreateRequest {
    private String name;
    private String email;
    private String contact;
    private String fail_existing;
    private String gstin;
    private RzCustomerCreateNote notes;
    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class RzCustomerCreateNote {
        private String user_mobile;
        private String pid;
    }


}

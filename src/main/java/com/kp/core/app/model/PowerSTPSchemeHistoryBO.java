package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PowerSTPSchemeHistoryBO {

    private double sourceSchemeValue;
    private String sourceSchemeValueFormatted;
    private double baseStpAmount;
    private String baseStpAmountFormatted;
    private String allocationMultiplier;
    private double actualStpAmount;
    private String actualStpAmountFormatted;
    private double nav;
    private String navFormatted;
    private String navAsOn;
    private double units;
    private String unitsFormatted;
    private String transactionDate;

}

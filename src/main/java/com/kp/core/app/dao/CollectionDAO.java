package com.kp.core.app.dao;


import com.google.gson.Gson;
import com.kp.core.app.model.*;


import com.kp.core.app.model.Products.ProductCategory;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Repository

public class CollectionDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;
    @Autowired
    Gson gson;

    @Autowired
    private ProductDAO productDAO;

    public CollectionBO insertCollection(CollectionBO collectionBO,int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert collection");


        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery = " INSERT INTO collections (handle, sort_order, template_suffix, product_count, " +
                    " collection_type, published_scope, title, body_html, created_at,updated_at, created_user, updated_user,status,alt,width,height,src) " +
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?,NOW(), NOW(), ?, ?, ?,?, ?, ?, ?)  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[] { "collectionid" });

                ps.setString(1, collectionBO.getHandle());
                ps.setString(2, collectionBO.getSortOrder());
                ps.setString(3, collectionBO.getTemplateSuffix());
                ps.setInt(4, collectionBO.getProductsCount());
                ps.setInt(5, collectionBO.getCollectionType().isEmpty()?1:Integer.parseInt(collectionBO.getCollectionType()));
                ps.setString(6, collectionBO.getPublishedScope());
                ps.setString(7, collectionBO.getTitle());
                ps.setString(8, collectionBO.getBodyHtml());
                ps.setInt(9, userId);
                ps.setInt(10, userId);
                ps.setInt(11, 1);
                ps.setString(12, collectionBO.getAlt());
                ps.setInt(13, collectionBO.getWidth());
                ps.setInt(14, collectionBO.getHeight());
                ps.setString(15, collectionBO.getSrc());
                return ps;

            }, keyHolder);
            long collectionId=keyHolder.getKey().longValue();

            if(collectionId > 0) {
                collectionBO.setCollection_id(collectionId);
            }
        } catch (Exception e) {
            log.error(logPrefix + "Exception in collections insertion: ", e);
        }

        log.debug(logPrefix + "collectionId : " + collectionBO.getCollection_id());
        return collectionBO;
    }

    public List<CollectionBO> getCollectionList(int userId,String tags){

        String logPrefix = "#UserId : " + userId;
        List<CollectionBO> collectionBOList=new ArrayList<>();
        String tagsSql="";
//        if(!StringUtils.isEmpty(tags) && tags.equalsIgnoreCase("SHOP")) {
//            String strType = GeneralUtil.commaSqlString(tags);
//            tagsSql=" where title in("+strType+")";
//        }
//        else
            if(!StringUtils.isEmpty(tags)) {
            String strType = GeneralUtil.commaSqlString(tags);
            tagsSql=" where title in("+strType+")";
        }


        try {


            String sqlQuery = " select id,collectionid,handle,sort_order,height,product_count,title,body_html,published_scope,position,alt,src,bpid,tags from collections "+tagsSql+" order by position asc";
            log.info("sqlQuery--"+sqlQuery);
            collectionBOList = jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {

                    CollectionBO collectionbo = new CollectionBO();
                    List<ProductCategory> categories;
                    long id = (rs.getLong("id")>0) ? rs.getLong("id"): 0;
                    long collection_id = (rs.getLong("collectionid")>0) ? rs.getLong("collectionid") : 0;
                    String handle = (rs.getString("handle") != null) ? rs.getString("handle").trim() : "";
                    String tag = (rs.getString("tags") != null) ? rs.getString("tags").trim() : "";
                    String sort_order = (rs.getString("sort_order") != null) ? rs.getString("sort_order").trim() : "";
                    int products_count = (rs.getInt("product_count")>0) ? rs.getInt("product_count"): 0;
                    int height = (rs.getInt("height")>0) ? rs.getInt("height"): 0;
                    int width = (rs.getInt("product_count")>0) ? rs.getInt("product_count"): 0;
                    String title = (rs.getString("title") != null) ? rs.getString("title").trim() : "";
                    String body_html = (rs.getString("body_html") != null) ? rs.getString("body_html").trim() : "";
                    String published_scope = (rs.getString("published_scope") != null) ? rs.getString("published_scope").trim() : "";
                    int position = (rs.getInt("position")>0) ? rs.getInt("position"): 0;
                    String alt = (rs.getString("alt") != null) ? rs.getString("alt").trim() : "";
                    String src = (rs.getString("src") != null) ? rs.getString("src").trim() : "";
                   int bpid = (rs.getInt("bpid")>0) ? rs.getInt("bpid"): 0;

                    collectionbo.setId(id);
                    collectionbo.setCollection_id(collection_id);
                    collectionbo.setHandle(handle);
                    collectionbo.setSortOrder(sort_order);
                    collectionbo.setProductsCount(products_count);
                    collectionbo.setHeight(height);
                    collectionbo.setWidth(width);
                    collectionbo.setTitle(title);
                    collectionbo.setBodyHtml(body_html);
                    collectionbo.setPublishedScope(published_scope);
                    collectionbo.setPosition(position);
                    collectionbo.setAlt(alt);
                    collectionbo.setSrc(src);
                    collectionbo.setTags(tag);
//                    categories=productDAO.getCollectionCategories(bpid,collection_id+"");
//                    if(categories!=null){
//                        collectionbo.setProductCategoryList(categories);
//                    }

                return collectionbo;
            });

        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }

        return collectionBOList;
    }

    public List<CollectionBO> getCollectionListByCategory(int userId,String collectionId){

        String logPrefix = "#UserId : " + userId;
        List<CollectionBO> collectionBOList=new ArrayList<>();


        try {


            String sqlQuery = " select id,collectionid,handle,sort_order,height,product_count,title,body_html,published_scope,position,alt,src,bpid from collections cl join categories ca on cl.collectionid::text  =ca.product_type where cl.collectionid='"+collectionId+"' order by ca.place asc";

            collectionBOList = jdbcTemplate.query(sqlQuery, (rs, rowNumber) -> {

                CollectionBO collectionbo = new CollectionBO();
                List<ProductCategory> categories;
                long id = (rs.getLong("id")>0) ? rs.getLong("id"): 0;
                long collection_id = (rs.getLong("collectionid")>0) ? rs.getLong("collectionid") : 0;
                String handle = (rs.getString("handle") != null) ? rs.getString("handle").trim() : "";
                String sort_order = (rs.getString("sort_order") != null) ? rs.getString("sort_order").trim() : "";
                int products_count = (rs.getInt("product_count")>0) ? rs.getInt("product_count"): 0;
                int height = (rs.getInt("height")>0) ? rs.getInt("height"): 0;
                int width = (rs.getInt("product_count")>0) ? rs.getInt("product_count"): 0;
                String title = (rs.getString("title") != null) ? rs.getString("title").trim() : "";
                String body_html = (rs.getString("body_html") != null) ? rs.getString("body_html").trim() : "";
                String published_scope = (rs.getString("published_scope") != null) ? rs.getString("published_scope").trim() : "";
                int position = (rs.getInt("position")>0) ? rs.getInt("position"): 0;
                String alt = (rs.getString("alt") != null) ? rs.getString("alt").trim() : "";
                String src = (rs.getString("src") != null) ? rs.getString("src").trim() : "";
                int bpid = (rs.getInt("bpid")>0) ? rs.getInt("bpid"): 0;

                collectionbo.setId(id);
                collectionbo.setCollection_id(collection_id);
                collectionbo.setHandle(handle);
                collectionbo.setSortOrder(sort_order);
                collectionbo.setProductsCount(products_count);
                collectionbo.setHeight(height);
                collectionbo.setWidth(width);
                collectionbo.setTitle(title);
                collectionbo.setBodyHtml(body_html);
                collectionbo.setPublishedScope(published_scope);
                collectionbo.setPosition(position);
                collectionbo.setAlt(alt);
                collectionbo.setSrc(src);
                categories=productDAO.getCollectionCategories(bpid,collection_id+"");
                if(categories!=null){
                    collectionbo.setProductCategoryList(categories);
                }

                return collectionbo;
            });

        } catch (Exception daoEx) {
            log.error(logPrefix + "Exception : " + daoEx);
        }

        return collectionBOList;
    }



}

package com.kp.core.app.utils;

import com.kp.core.app.config.DocConfig;
import com.kp.core.app.model.BankInfoBO;
import com.kp.core.app.model.DateBO;
import com.kp.core.app.model.MandateInfoBO;
import com.kp.core.app.model.UserBO;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.*;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;


@Slf4j
@Component
public class GenerateMandate {

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private NumberConverter numberConverter;

    public boolean generateNACHMandate(MandateInfoBO mandateInfoBO, DateBO startDate, DateBO endDate, String formType, String createdFrom, UserBO userBO, int userId) {

        boolean status = false;
        String templateName = "NACH_Mandate_New";
        String YES = "Yes";
        String appFormDir;
        String ecsFullPath;

        try {

            DocConfig configuration = ConfigFactory.create(DocConfig.class);

            log.debug("investorId: "+ mandateInfoBO.getCustomerId() + ", createdfrom: " + createdFrom);
            String startDt = startDate.getDateFirstView();
            startDt = startDt.replace("/", "");
            String endDt = endDate.getDateFirstView();
            endDt = endDt.replace("/", "");

            String templateDir = configuration.investorsTemplatePath();
            appFormDir = configuration.pdfPath();

            appFormDir = appFormDir + userId + File.separator;
            if (!(new File(appFormDir)).exists()) {
                new File(appFormDir).mkdirs();
            }

            String ecsFileName = mandateInfoBO.getCustomerId() + "-" + mandateInfoBO.getConsumerCode() + ".pdf";
            ecsFullPath = appFormDir + ecsFileName;
            log.debug("Generating NACH mandate: " + ecsFullPath);

            PdfReader reader = new PdfReader(templateDir + templateName + ".pdf");
            PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(ecsFullPath));
            AcroFields acroFields = stamper.getAcroFields();

            // ---------BarCode--------------------------------------------------------------------------------
            try {

                int consCodeLength = mandateInfoBO.getConsumerCode().length();
                PdfContentByte cb = stamper.getOverContent(1);
                PdfContentByte underContent = stamper.getOverContent(1);

                Barcode128 shipBarCode = new Barcode128();
                shipBarCode.setX(0.75f);
                shipBarCode.setN(1.5f);
                shipBarCode.setChecksumText(true);
                shipBarCode.setGenerateChecksum(true);
                shipBarCode.setSize(10f);
                shipBarCode.setTextAlignment(Element.ALIGN_CENTER);
                shipBarCode.setBaseline(0f);
                shipBarCode.setBarHeight(25f);
                shipBarCode.setCode(formType.substring(0, 2) + "_Form#" + mandateInfoBO.getCustomerId() + "#" + mandateInfoBO.getConsumerCode().substring(consCodeLength-7, consCodeLength));

                Image imageI25 = shipBarCode.createImageWithBarcode(cb, Color.black, Color.white);
                imageI25.setAbsolutePosition(250, 485);
                underContent.addImage(imageI25, true);

            } catch (Exception e) {
                log.error("Barcode generation Exception :" , e);
            }
            //-----------------------------------------------------------------------------------------

            BankInfoBO bankInfoBO = mandateInfoBO.getBank();
            String bankName = bankInfoBO.getBankName();
            if (bankName != null) bankName = bankName.trim();
            String ifscCode = bankInfoBO.getIfsc() != null ? bankInfoBO.getIfsc().trim() : "";

            String bankAccNo = bankInfoBO.getAccountNo() == null ? "" : bankInfoBO.getAccountNo().trim();
            String bankAccType = bankInfoBO.getAccountType() != null ? bankInfoBO.getAccountType().trim() : "";

            log.debug("bankName: " + bankName + ", bankAcNo: " + bankAccNo + ", bankaccType: " + bankAccType + ", ifsc:" + ifscCode);

            if (!"".equals(bankName) && !"".equals(bankAccNo) && !"".equals(ifscCode) && !"".equals(bankAccType)) {
                acroFields.setField("bank_name", bankName);
                acroFields.setField("branch_name", (bankInfoBO.getBranchName() != null) ? bankInfoBO.getBranchName().trim() : "");


                fillFields(ifscCode, "fc_", acroFields, 11);

                if (bankAccType.equals(BPConstants.ACC_TYPE_CURRENT))
                    acroFields.setField("curr_acc_type", YES);
                else if (bankAccType.equals(BPConstants.ACC_TYPE_SAVINGS))
                    acroFields.setField("saving_acc_type", YES);
                else if (bankAccType.equals(BPConstants.ACC_TYPE_NRO))
                    acroFields.setField("saving_acc_type", YES);
                else if (bankAccType.equals(BPConstants.ACC_TYPE_NRE))
                    acroFields.setField("saving_acc_type", YES);
                fillFields(bankAccNo, "ac_", acroFields, 30);
            }


            acroFields.setField("inv_name", "Investor Name: " + userBO.getName());
            acroFields.setField("user_email", userBO.getEmail());

            if (userBO.getMobile() != null && userBO.getMobile().length() == 10)
                acroFields.setField("m_", userBO.getMobile());

            acroFields.setField("first_acc_holder_name", (bankInfoBO.getAccountHolderName() != null ? bankInfoBO.getAccountHolderName().trim() : ""));

//            if (bankInfoBO.getAccOperatingMode() != null && bankInfoBO.getAccOperatingMode().equalsIgnoreCase("2"))
//                acroFields.setField("second_acc_holder_name", (bankInfoBO.getSecAccHolderName() != null ? bankInfoBO.getSecAccHolderName().trim() : ""));

            acroFields.setField("upper_limit", mandateInfoBO.getMaxAmount() + " /-");
            String strAmount = numberConverter.convertNumber(String.valueOf(mandateInfoBO.getMaxAmount()));
            acroFields.setField("amount_words", strAmount + " Only");

            fillFields(startDt, "sd_", acroFields, 8);
            fillFields(startDt, "dt", acroFields, 8);
            fillFields(endDt, "ed_", acroFields, 8);
            acroFields.setField("cc_", mandateInfoBO.getConsumerCode());

            stamper.close();

            status = true;
            log.debug("NACH form generated successfully");

        } catch (Exception e) {
            log.error("Exception: ", e);
        }
        return status;
    }

    public void fillFields(String value, String fieldType, AcroFields acroFields, int count) {
        try {
            int tempCount = 0;
            if (count > value.length()) {
                tempCount = value.length();
            } else {
                tempCount = count;
            }
            for (int i = 1; i <= tempCount; i++) {
                String filedName = fieldType + i;
                acroFields.setField(filedName, value.charAt(i - 1) + "");
            }
        } catch (Exception e) {
            log.error("Error while creating fields", e);
        }

    }
}

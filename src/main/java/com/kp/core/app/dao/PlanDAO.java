
package com.kp.core.app.dao;

        import com.google.gson.Gson;
        import com.kp.core.app.model.PlanBO;
        import io.netty.util.internal.StringUtil;
        import lombok.extern.slf4j.Slf4j;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.jdbc.core.BatchPreparedStatementSetter;
        import org.springframework.jdbc.core.JdbcTemplate;
        import org.springframework.jdbc.support.GeneratedKeyHolder;
        import org.springframework.jdbc.support.KeyHolder;
        import org.springframework.stereotype.Repository;
        import org.springframework.transaction.annotation.Propagation;

        import jakarta.transaction.Transactional;
        import java.sql.PreparedStatement;
        import java.sql.Statement;
        import java.util.List;

@Slf4j
@Repository

public class PlanDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;
    @Autowired
    Gson gson;

    public PlanBO createPlan(PlanBO planBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";

        log.debug(logPrefix + "Insert plan");


        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sqlQuery = " INSERT INTO Plan (planname,description,plantype,startdate,enddate," +
                    "maxcycle,amount,maxamount,intervaltype,intervals,position,remarks,expires_at, " +
                    " created_at,updated_at, created_user, updated_user,active) " +
                    " VALUES( ?, ?, ?, ?, ?, ?,?,?,?,?, ?, ?, ?,NOW(), NOW(), ?, ?, 1)  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, planBO.getPlanName());
                ps.setString(2, planBO.getPlanDescription());
                ps.setString(3, planBO.getPlanType());
                ps.setString(4, planBO.getStartDate());
                ps.setString(5, planBO.getEndDate());
                ps.setInt(6, planBO.getMaxCycle());
                ps.setDouble(7, planBO.getAmount());
                ps.setDouble(8, planBO.getMaxAmount());
                ps.setInt(9, planBO.getIntervalType());
                ps.setInt(10, planBO.getIntervals());
                ps.setInt(11, planBO.getPosition());
                ps.setString(12, planBO.getRemarks());
                ps.setString(13, planBO.getExpires_at());
                ps.setInt(14, userId);
                ps.setInt(15, userId);
                return ps;

            }, keyHolder);
            String id;
            String planID;
            if (keyHolder.getKeys().size() > 1) {
                id = keyHolder.getKeys().get("id").toString();
                planBO.setId(id + "");
                planID = keyHolder.getKeys().get("plan_id").toString();
                planBO.setPlanId(planID);
            }

        } catch (Exception e) {
            log.error(logPrefix + "Exception in plan insertion: ", e);
        }

        log.debug(logPrefix + "Plan id  generate: " + planBO.getPlanId());
        return planBO;
    }
}
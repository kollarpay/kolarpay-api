package com.kp.core.app.dao;

import com.kp.core.app.model.*;
import com.kp.core.app.model.request.OrderBO;
import com.kp.core.app.utils.DateUtil;
import com.kp.core.app.utils.BPConstants;
import com.kp.core.app.utils.GeneralUtil;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Repository
public class TransactionDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;

    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    Gson gson;

    public boolean insertBuyCartTransactions(List<SchemeCartBO.Schemes> schemes, int cartId, int holdingProfileId, String mediumThru, int userId, List<ErrorBO> errorList) {

        String logPrefix = "#CartId : " + cartId + " | #HoldingProfileId : " + holdingProfileId + " | #UserId : " + userId + " | ";
        int status;
        String dividendId;
        boolean success = false;

        try {

            for(SchemeCartBO.Schemes schemeBO : schemes) {

                int userTransRefId = schemeBO.getUserTransRefId();
                if (userTransRefId == 0)
                    userTransRefId = autoNumberGenerator.getAutoNumberGenerated("TransRefID");

                status = updateUserTransRefIdCart(cartId, schemeBO.getId(), userTransRefId, userId);
                log.debug(logPrefix + "UserTransRefId update status : " + status);

                if (StringUtils.isNotEmpty(schemeBO.getSipType())) {

                    dividendId = getDividendId(schemeBO.getOption(), schemeBO.getDividendOption());

                    int sipRefId = autoNumberGenerator.getAutoNumberGenerated("SIPREFID");

                    status = insertSIPDetails(schemeBO, holdingProfileId, userTransRefId, sipRefId, dividendId, mediumThru, userId);
                    if (status == 0)
                        errorList.add(new ErrorBO(schemeBO.getId(), "sip", "SIPDetails Insert failed"));
                    else
                        success = true;
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "Exception in transaction insert : ", e);
            errorList.add(new ErrorBO("exception", "Data Insertion failed"));
        }

        return success;
    }

    public String getDividendId(String option, String dividendOption) {

        String dividendId = "";
        try {

            String dividendDesc = BPConstants.GROWTH;

            if(option.equalsIgnoreCase(BPConstants.DIVIDEND) && StringUtils.isNotEmpty(dividendOption)) {
                if (dividendOption.equalsIgnoreCase(BPConstants.DIVIDEND_PAYOUT))
                    dividendDesc = "Pay Out";
                else
                    dividendDesc = BPConstants.DIVIDEND_REINVESTMENT;
            }

            dividendId = generalInfoDAO.getLookUpId(dividendDesc, "DividendType").trim();

        } catch (Exception e) {
            log.error("Exception in get dividendId : ", e);
        }
        return dividendId;
    }

    public int insertSIPDetails(SchemeCartBO.Schemes schemeBO, int holdingProfileId, int userTransRefID, int sipRefId, String dividendType, String mediumThru, int userId) {

        String logPrefix = "#HoldingProfileId : " + holdingProfileId + " | #UserId : " + userId + " | #UserTransRefId : " + userTransRefID + " | ";
        log.debug(logPrefix + "SIPDetails Insert");

        int status = 0;
        try {

            String insertQuery_SIP = "INSERT INTO SIPDetails "
                    + "( HOLDINGPROFILEID, FOLIONUMBER, SCHEMECODE, STARTDATE, ENDDATE, ECSDATE, AMOUNT,NOOFINSTALMENT, FREQUENCY, USERBANKID, FLAG, DIVIDENDTYPE, "
                    + " CREATEDUSER, CREATEDDATE, MODIFIEDUSER, MODIFIEDDATE, PORTFOLIOID, UserTransRefID, ECSAMOUNT, SIPTYPE, INITIALPAYMENTMADE,SIPREFID, "
                    + " CONSUMERCODE, Active, FolioBankId, Perpetual, MAXAMOUNT, PAUSEDORFLEXIDATE, FLEXIAMOUNT, STEPUPFREQ, STEPUPAMOUNT, MediumThru)"
                    + " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                    + " DATEADD(MI, 330, GETDATE()), ?,DATEADD(MI, 330, GETDATE()), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            status = jdbcTemplate.update(insertQuery_SIP, ps -> {

                boolean isFlexi = false;
                SchemeCartBO.SipDetail sipDetail = new SchemeCartBO.SipDetail();
                if(schemeBO.getSipType().equalsIgnoreCase(BPConstants.REGULAR_SIP_TYPE))
                    sipDetail = schemeBO.getRegular();
                else if(schemeBO.getSipType().equalsIgnoreCase(BPConstants.FLEXI_SIP_TYPE)) {
                    sipDetail = schemeBO.getFlexi();
                    isFlexi = true;
                }
                else if(schemeBO.getSipType().equalsIgnoreCase(BPConstants.STEPUP_SIP_TYPE))
                    sipDetail = schemeBO.getStepup();
                else if(schemeBO.getSipType().equalsIgnoreCase(BPConstants.ALERT_SIP_TYPE))
                    sipDetail = schemeBO.getAlert();

                String frequency, stepupFrequency;

                frequency = sipDetail.getFrequency();
                stepupFrequency = sipDetail.getStepupfrequeny();

                if(StringUtils.isNotEmpty(frequency) && frequency.equalsIgnoreCase(BPConstants.FREQUENCY_MONTHLY))
                    frequency = "M";
                if(StringUtils.isNotEmpty(stepupFrequency)) {
                    if (stepupFrequency.equalsIgnoreCase(BPConstants.FREQUENCY_HALF_YEARLY))
                        stepupFrequency = "HY";
                    if (stepupFrequency.equalsIgnoreCase(BPConstants.FREQUENCY_YEARLY))
                        stepupFrequency = "Y";
                }
                if(StringUtils.isEmpty(sipDetail.getStartDate()))
                    sipDetail.setStartDate(DateUtil.getTDate());
                if(StringUtils.isEmpty(sipDetail.getEndDate()))
                    sipDetail.setEndDate(DateUtil.addMonthsToDate(sipDetail.getTenure(), sipDetail.getStartDate(), "YYYY-MM-dd"));


                ps.setInt(1, holdingProfileId);
                ps.setString(2, schemeBO.getFolio());
                ps.setInt(3, Integer.parseInt(schemeBO.getSchemeCode()));
                ps.setString(4, StringUtils.isEmpty(sipDetail.getStartDate()) ? "" : sipDetail.getStartDate());
                ps.setString(5, StringUtils.isEmpty(sipDetail.getEndDate()) ? "" : sipDetail.getEndDate());
                ps.setString(6, String.valueOf(schemeBO.getSipDate()));
                ps.setInt(7, schemeBO.getAmount());
                ps.setInt(8, sipDetail.getTenure());
                ps.setString(9, StringUtils.isNotEmpty(frequency) ? frequency.toUpperCase() : null);
                ps.setInt(10, Integer.parseInt(schemeBO.getBankId()));
                ps.setString(11, "F");
                ps.setString(12, dividendType);
                ps.setInt(13, userId);
                ps.setInt(14, userId);
                ps.setInt(15, Integer.parseInt(schemeBO.getGoalId()));
                ps.setInt(16, userTransRefID);
                ps.setInt(17, schemeBO.getAmount());
                ps.setString(18, schemeBO.getSipType());
                ps.setString(19, "N");
                ps.setInt(20, sipRefId);
                ps.setString(21, sipDetail.getConsumerCode());
                ps.setInt(22, 1);
                ps.setString(23, schemeBO.getBankId());
                ps.setInt(24, sipDetail.getTenure() == 999 ? 1 : 0);
                ps.setInt(25, sipDetail.getMaximumAmount());
                ps.setDate(26, null); //todo: pausedOrFlexiDate
                ps.setInt(27, isFlexi ? schemeBO.getAmount() : 0);
                ps.setString(28, StringUtils.isNotEmpty(stepupFrequency) ? stepupFrequency.toUpperCase() : null);
                ps.setInt(29, sipDetail.getStepupAmount());
                ps.setString(30, mediumThru);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in sipDetail Insertion : ", e);
        }

        return status;
    }




    public int createOrder(OrderBO orderBO,int userid) {

        String logPrefix = "#customerID : " + orderBO.getCustomerID() + " | #UserId : " + orderBO.getCreated_user();
        log.debug(logPrefix + "InvestorTransaction Insert");

        int status = 0;
        try {

            String insertQuery_IT = "INSERT INTO orders " +
                    " (orderid, customerID,paymentID,paymentgatewayID,ordertype,ordermode,order_dt, active,medium,require_dt, " +
                    "  shipped_dt,order_time, created_user,updated_user,created_at,updated_at,ipaddress, " +
                    " remarks) " +
                    " VALUES ( ?, ?, ?, ?, ?, now(), ?, ?, ?, ?," +
                    " now(), now(), ?, ?,now(),now(),?)";

            status = jdbcTemplate.update(insertQuery_IT, ps -> {
                ps.setString(1, orderBO.getCustomerID());
                ps.setString(2, orderBO.getPaymentID()); //todo : entry load
                ps.setString(3, orderBO.getPaymentGatewayID()); //todo : exit load
                ps.setString(4, orderBO.getOrderType());
                ps.setInt(5, orderBO.getOrderMode());
                ps.setString(6, "A");
                ps.setString(7, orderBO.getMedium());
                ps.setInt(8,userid );
                ps.setInt(9,userid );
                ps.setString(10, orderBO.getIpAddress());
                ps.setString(11, orderBO.getRemarks());
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorTransaction Insertion : ", e);
        }

        return status;
    }

    public int insertInvestorTransactionTrack(SchemeCartBO.Schemes schemeBO, int holdingProfileId, int userTransRefID, String ipAddress, int userId) {

        String logPrefix = "#HoldingProfileId : " + holdingProfileId + " | #UserId : " + userId + " | #UserTransRefId : " + userTransRefID + " | ";
        log.debug(logPrefix + "InvestorTransactionTrack Insert");

        int status = 0;
        try {

            String insertQuery_ITT = "INSERT INTO InvestorTransactionTrack " +
                    " (UserTransRefID, WebTrans, CreatedUser, CreatedDate, ModifiedUser, ModifiedDate, IPAddress, PaidThru, SubTransType) " +
                    " VALUES (?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()), ?, ?, ?) ";

            status = jdbcTemplate.update(insertQuery_ITT, ps -> {

                ps.setInt(1, userTransRefID);
                ps.setInt(2, userId);
                ps.setInt(3, userId);
                ps.setString(4, ipAddress);
                ps.setString(5, ""); //todo: Paid thru
                ps.setString(6, StringUtils.isNotEmpty(schemeBO.getSipType()) ? "SIP" : null);

            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorTransactionTrack Insertion : ", e);
        }

        return status;
    }

    public int insertInvestorTransactByPortfolio(SchemeCartBO.Schemes schemeBO, int holdingProfileId, int userTransRefID, String transType, String transStatus, int userId) {

        String logPrefix = "#HoldingProfileId : " + holdingProfileId + " | #UserId : " + userId + " | #UserTransRefId : " + userTransRefID + " | ";
        log.debug(logPrefix + "InvestorTransactByPortfolio Insert");

        int status = 0;
        try {

            String insertQuery_ITPF = "INSERT INTO InvestorTransactByPortfolio (UserTrnsRefID, PortfolioID, Status , TransType) VALUES (?, ?, ? , ?)";

            status = jdbcTemplate.update(insertQuery_ITPF, ps -> {

                ps.setInt(1, userTransRefID);
                ps.setInt(2, StringUtils.isEmpty(schemeBO.getGoalId()) ? 0 : Integer.parseInt(schemeBO.getGoalId()));
                ps.setString(3, transStatus);
                ps.setString(4, transType);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorTransactByPortfolio Insertion : ", e);
        }

        return status;
    }

    public int insertTransactionBankInfo(SchemeCartBO.Schemes schemeBO, int holdingProfileId, int userTransRefID, int userId) {

        String logPrefix = "#HoldingProfileId : " + holdingProfileId + " | #UserId : " + userId + " | #UserTransRefId : " + userTransRefID + " | ";
        log.debug(logPrefix + "Transaction_Bank_Info Insert");

        int status = 0;
        try {

            String insertQuery_TBF = " INSERT INTO TRANSACTION_BANK_INFO " +
                    " (UserTransRefID,UserBankId, CreatedUser, CreatedDate ) " +
                    " VALUES(?,?,?,DATEADD(MI, 330, GETDATE())) ";

            status = jdbcTemplate.update(insertQuery_TBF, ps -> {

                ps.setInt(1, userTransRefID);
                ps.setString(2, schemeBO.getBankId());
                ps.setInt(3, userId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in Transaction_Bank_Info Insertion : ", e);
        }

        return status;
    }

    public int insertInvestorTransactionPaymentTrack(int paymentId, String paymentFrom, int userId, String initiated_PaymentGateway) {

        String logPrefix = "#PaymentId : " + paymentId + " | #UserId : " + userId + " | #PaymentFrom : " + paymentFrom + " | ";
        log.debug(logPrefix + "InvestorTransactionPaymentTrack Insert");

        int status = 0;
        try {

            String insertQuery_ITPT = "IF NOT EXISTS (Select distinct Paymentid from InvestorTransactionPaymentTrack where PaymentID = ? ) " +
                    " Begin insert into InvestorTransactionPaymentTrack (PaymentId, PaymentFrom, BankLookupId, AccountNumber, CreatedUser, CreatedDate, ModifiedUser, ModifiedDate, Initiated_PaymentGateway) " +
                    " values (?, ?, ?, ?, ?, DATEADD(MI, 330, GETDATE()), ?, DATEADD(MI, 330, GETDATE()), ?) End " +
                    " Else " +
                    " Begin Update InvestorTransactionPaymentTrack set PaymentFrom = ?, ModifiedUser = ?, ModifiedDate = DATEADD(MI, 330, GETDATE()), Remarks = 'Re-Pay', Initiated_PaymentGateway = ? where PaymentId = ? " +
                    " End ";

            status = jdbcTemplate.update(insertQuery_ITPT, ps -> {

                ps.setInt(1, paymentId);
                ps.setInt(2, paymentId);
                ps.setString(3, paymentFrom);
                ps.setString(4, "0");
                ps.setString(5, "");
                ps.setInt(6,userId);
                ps.setInt(7,userId);
                ps.setString(8, initiated_PaymentGateway);
                ps.setString(9, paymentFrom);
                ps.setInt(10,userId);
                ps.setString(11, initiated_PaymentGateway);
                ps.setInt(12, paymentId);

            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorTransactionTrack Insertion : ", e);
        }

        return status;
    }

    public int updatePaymentIdCart(int cartId, int paymentId, int userId) {

        String logPrefix = "#CartId : " + cartId + " | #PaymentId : " + paymentId + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "InvestorCartDetails PaymentId Update");

        int status = 0;
        try {

            String sqlQuery = " UPDATE InvestorCartDetails SET PaymentId = ?, ModifiedUser = ?, ModifiedDate = DATEADD(MI, 330, GETDATE()) WHERE CartId = ? AND Active=1 ";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setInt(1, paymentId);
                ps.setInt(2, userId);
                ps.setInt(3, cartId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorCartDetails update : ", e);
        }

        return status;
    }


    public int updateUserTransRefIdCart(int cartId, String referenceId, int userTransRefId, int userId) {

        String logPrefix = "#CartId : " + cartId + " | #ReferenceId : " + referenceId + " | #UserTransRefId : " + userTransRefId + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "InvestorCartDetails PaymentId Update");

        int status = 0;
        try {

            String sqlQuery = " UPDATE InvestorCartDetails SET UserTransRefId = ?, ModifiedUser = ?, ModifiedDate = DATEADD(MI, 330, GETDATE()) WHERE CartId = ? AND ReferenceId = ? AND Active=1 ";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setInt(1, userTransRefId);
                ps.setInt(2, userId);
                ps.setInt(3, cartId);
                ps.setString(4, referenceId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorCartDetails update : ", e);
        }

        return status;
    }

    public int deactivateInvestorTransaction(int cartId, int paymentId, int userId) {

        String logPrefix = "#CartId : " + cartId + " | #PaymentId : " + paymentId + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "InvestorTransaction status Update");

        int status = 0;
        try {

            String sqlQuery = " UPDATE InvestorTransaction SET Active = 'D', Comments = ?, ModifiedUser = ?, ModifiedDate = DATEADD(MI, 330, GETDATE()) " +
                              "  WHERE Active = 'A' and PaymentID = ?  ";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setString(1, "Payment Retry | cartId : " + cartId);
                ps.setInt(3, userId);
                ps.setInt(4, paymentId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorTransaction update : ", e);
        }

        return status;
    }

    public int updateInvestorTransaction(String transId, String transStatus, int paymentId, int userId) {

        String logPrefix = "#PaymentId : " + paymentId + " | #TransactionId : " + transId + " | #TransStatus " + transStatus + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "InvestorTransaction status Update");

        int status = 0;
        try {

            String sqlQuery = " UPDATE InvestorTransaction SET PaymentGatewayId = ? , TransactionStatus = ?, " +
                    "  TransactionDate = DATEADD(MI, 390, GETDATE()),  TransactionTime = (SELECT CONVERT(char(8), DATEADD(MI, 390, GETDATE()), 114)), " +
                    "  ModifiedUser = ?, ModifiedDate = DATEADD(MI, 330, GETDATE()) " +
                    "  WHERE Active = 'A' and PaymentID = ?  ";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setString(1, transId);
                ps.setString(2, transStatus);
                ps.setInt(3, userId);
                ps.setInt(4, paymentId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorTransaction update : ", e);
        }

        return status;
    }

    public boolean transactionOtherPaymentUpdate(int paymentId, int UserId) throws Exception {
        boolean UpdateFlag = false;
        try {
            String sqlQuery = "UPDATE InvestorTransaction " +
                    "SET TransactionStatus = ?, ITCValue = ?, TransactionTime = (SELECT CONVERT(char(8), DATEADD(MI, 330, GETDATE()), 114)), " +
                    " ModifiedUser = ?, ModifiedDate = DATEADD(MI, 330, GETDATE()) " +
                    "WHERE Active = 'A' and TransactionStatus <> 'PW' and PaymentID = ? and PaymentID > 0 and PaymentID <> 567220 and TRANSTYPE IN ('PUR','PURA') ";

            jdbcTemplate.update(sqlQuery, ps -> {
                ps.setString(1, "PW");
                ps.setString(2, "N_ALL_ALL");
                ps.setInt(3, UserId);
                ps.setInt(4, paymentId);
            });
            log.debug("Other payment status update - paymentid: " + paymentId + ", UserId: " + UserId);

        } catch (Exception daoEx) {
            log.error("DAO - User Investor Transaction Update # i/p : ?, Exception : " + daoEx);
        }

        return UpdateFlag;
    }
    public boolean LumpsumTransactions(String paymentId, String consumerCode, int amount, String actualDebitDate, int userId) throws Exception {
        Boolean InsertFlag = false;

        try {
            String SQLQuery = "INSERT INTO InvestorTransactionLumpsum (PaymentId, ConsumerCode, Amount, Actual_Debit_Date, CreatedDate, CreatedUser, ModifiedDate, ModifiedUser) " +
                    " VALUES(?, ?, ?, ?, DATEADD(MI, 330, GETDATE()), ?,DATEADD(MI, 330, GETDATE()), ?)";
            log.debug(" SQLQuery :: " + SQLQuery + "  paymentId:: " + paymentId + " consumerCode :: " + consumerCode + " amount :: " + amount + " actualDebitDate ::" + actualDebitDate);
            jdbcTemplate.update(SQLQuery, ps -> {
                ps.setString(1, paymentId);
                ps.setString(2, consumerCode);
                ps.setInt(3, amount);
                ps.setString(4, actualDebitDate);
                ps.setInt(5, userId);
                ps.setInt(6, userId);

            });
            log.info("paymentid:"+paymentId+" ,consumerCode:"+consumerCode+" ,amount:"+amount+" ,actualDebitDate:"+actualDebitDate);
            InsertFlag = true;
        } catch (Exception Ex) {
            InsertFlag = false;
            log.error("Insert LumpsumTransactions Request :: " + Ex.getMessage());
        }
        return InsertFlag;
    }

    public int updateInvestorTransactionTrack(int userTransRefId, String paidThru, int userId) {

        String logPrefix = "#UserTransRefId : " + userTransRefId + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "InvestorTransactionTrack Update");

        int status = 0;
        try {

            String sqlQuery = " UPDATE InvestorTransactionTrack SET PaidThru = ?, WebTrans = DATEADD(MI, 330, GETDATE()), ModifiedUser = ?, ModifiedDate = DATEADD(MI, 330, GETDATE()) WHERE UserTransRefID = ? ";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setString(1, paidThru);
                ps.setInt(2, userId);
                ps.setInt(3, userTransRefId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorTransactionTrack update : ", e);
        }

        return status;
    }

    public int updateInvestorTransactionPaymentTrack(String statusCode, int paymentId, int userId) {

        String logPrefix = "#PaymentId : " + paymentId + " | #StatusCode : " + statusCode + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "InvestorTransactionPaymentTrack Update");

        int status = 0;
        try {

            String sqlQuery = " UPDATE InvestorTransactionPaymentTrack SET PaidStatus = 'Y', StatusCode = ?, ModifiedDate = DATEADD(MI, 330, GETDATE()), ModifiedUser = ? WHERE PaymentID = ?  ";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setString(1, statusCode);
                ps.setInt(2, userId);
                ps.setInt(3, paymentId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorTransactionPaymentTrack update : ", e);
        }

        return status;
    }

    public int updateSipDetails(int userTransRefId, int userId) {

        String logPrefix = "#UserTransRefId : " + userTransRefId + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "SipDetails Update");

        int status = 0;
        try {

            String sqlQuery = " UPDATE SIPDetails SET Active = 1, InitialPaymentMade='Y', ModifiedUser = ?, ModifiedDate = DATEADD(MI,330,GETDATE())" +
                              " WHERE UserTransRefId = ? ";
            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setInt(1, userId);
                ps.setInt(2, userTransRefId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in SipDetails update : ", e);
        }

        return status;
    }

    public int updateSipDetailsUserTransRefId(int newUserTransRefId, int userTransRefId, int userId) {

        String logPrefix = "#NewUserTransRefId : " + newUserTransRefId + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "SipDetails Update");

        int status = 0;
        try {

            String sqlQuery = " UPDATE SIPDetails SET UserTransRefId = ?, ModifiedUser = ?, ModifiedDate = DATEADD(MI,330,GETDATE())" +
                    " WHERE UserTransRefId = ? ";
            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setInt(1, newUserTransRefId);
                ps.setInt(2, userId);
                ps.setInt(3, userTransRefId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in SipDetails update : ", e);
        }

        return status;
    }

    public int updateInvestorTransactByPortfolioUserTransRefId(int newUserTransRefId, int userTransRefId, int portfolioId, int userId) {

        String logPrefix = "#NewUserTransRefId : " + newUserTransRefId + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "InvestorTransactByPortfolio Update");

        int status = 0;
        try {

            String insertQuery_ITPF = "UPDATE InvestorTransactByPortfolio SET UserTrnsRefID = ? WHERE UserTrnsRefID = ? AND PortfolioID = ? ";

            status = jdbcTemplate.update(insertQuery_ITPF, ps -> {

                ps.setInt(1, newUserTransRefId);
                ps.setInt(2, userTransRefId);
                ps.setInt(3, portfolioId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in InvestorTransactByPortfolio update : ", e);
        }

        return status;
    }

    public int updateTransactionBankInfoUserTransRefId(int newUserTransRefId, int userTransRefID, String userBankId) {

        String logPrefix = "#UserTransRefId : " + userTransRefID + " | #UserBankId : " + userBankId + " | ";
        log.debug(logPrefix + "Transaction_Bank_Info Insert");

        int status = 0;
        try {

            String insertQuery_TBF = " UPDATE TRANSACTION_BANK_INFO SET UserTransRefID = ? WHERE UserTransRefID = ? AND UserBankId = ? ";

            status = jdbcTemplate.update(insertQuery_TBF, ps -> {

                ps.setInt(1, newUserTransRefId);
                ps.setInt(2, userTransRefID);
                ps.setString(3, userBankId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in Transaction_Bank_Info Insertion : ", e);
        }

        return status;
    }


    public int updateSIPAmount(int sipId, int amount, String schemeCode, String remarks, int userId, boolean isFlexi) {

        String logPrefix = "#UserId : " + userId + " | #SipId : " + sipId + " | #Amount : " + amount + " | ";

        log.debug(logPrefix + "Update SIPDetails");
        int status = 0;

        try {

            String appendColumn = "ECSAmount";
            if(isFlexi) {
                String ecsDate = getEcsDateFromSIPID(sipId);
                Date ecs = new SimpleDateFormat("yyyy-MM-dd").parse(ecsDate);
                if(new Date().after(ecs))
                    ecsDate = DateUtil.addMonthsToDate(1, ecsDate, "yyyy-MM-dd");
                appendColumn = "PausedOrFlexiDate = '" + ecsDate + "', FlexiAmount";
            }

            String sqlQuery = " UPDATE SIPDetails SET " + appendColumn + " = ?, Remarks = ?, ModifiedUser = ?, ModifiedDate = DATEADD(MI,330,GETDATE()) " +
                              " WHERE SIPID = ? AND SchemeCode = ? AND Active = 1 ";

            status = jdbcTemplate.update(sqlQuery, preparedStatement -> {

                preparedStatement.setInt(1, amount);
                preparedStatement.setString(2, remarks);
                preparedStatement.setInt(3, userId);
                preparedStatement.setInt(4, sipId);
                preparedStatement.setString(5, schemeCode);

            });


        } catch (Exception e) {
            log.error(logPrefix + "Exception in SIPDetails update: ", e);
        }

        log.debug(logPrefix + "Update status : " + status);
        return status;
    }

    public int deactivateSIP(int sipRefId, String remarks, int userId) {

        String logPrefix = "#UserId : " + userId + " | #SipRefId : " + sipRefId + " | ";

        log.debug(logPrefix + "Deactivate SIP");
        int status = 0;

        try {

            String sqlQuery = " UPDATE SIPDetails SET Active = 0, Remarks = ?, ModifiedUser = ?, ModifiedDate = DATEADD(MI,330,GETDATE()) " +
                    " WHERE SipRefId = ? AND Active = 1 ";

            status = jdbcTemplate.update(sqlQuery, preparedStatement -> {


                preparedStatement.setString(1, remarks);
                preparedStatement.setInt(2, userId);
                preparedStatement.setInt(3, sipRefId);

            });


        } catch (Exception e) {
            log.error(logPrefix + "Exception in SIP Deactivate: ", e);
        }

        log.debug(logPrefix + "Update status : " + status);
        return status;
    }

    public int deactivateSTP(int sipRefId, String remarks, int userId) {

        String logPrefix = "#UserId : " + userId + " | #SipRefId : " + sipRefId + " | ";

        log.debug(logPrefix + "Deactivate STP");
        int status = 0;

        try {

            String sqlQuery = " UPDATE STPDetails SET Active = 0, Comments = ?, ModifiedUser = ?, ModifiedDate = DATEADD(MI,330,GETDATE()) " +
                    " WHERE STPID = ? AND Active = 1 ";

            status = jdbcTemplate.update(sqlQuery, preparedStatement -> {


                preparedStatement.setString(1, remarks);
                preparedStatement.setInt(2, userId);
                preparedStatement.setInt(3, sipRefId);

            });


        } catch (Exception e) {
            log.error(logPrefix + "Exception in STP Deactivate: ", e);
        }

        log.debug(logPrefix + "Update status : " + status);
        return status;
    }

    public int insertOEInvestorTransaction(int holdingProfileId, String amcCode, int paymentId, int userId) {

        String logPrefix = "#PaymentId : " + paymentId + " | #HoldingProfileId : " + holdingProfileId +  " | #AmcCode " + amcCode + " | #UserId : " + userId + " | ";
        log.debug(logPrefix + "OE_AMC_Transaction Insert");

        int status = 0;
        try {

            String sqlQuery = " IF EXISTS (" +
                            " SELECT InvestorId FROM dbo.InvestorBasicDetail where IRType='OE' and InvestorId=(select distinct PrimaryInvestorID from dbo.InvestorAdditionalInfo where  RelationshipID='F' and HoldingProfileID = ? )) " +
                            " BEGIN " +
                            " IF NOT EXISTS (SELECT * FROM dbo.OE_AMC_Transaction WHERE Flag='A' and investorId =(select distinct PrimaryInvestorID from dbo.InvestorAdditionalInfo " +
                            " WHERE  RelationshipID='F' and HoldingProfileID=? ) and amcCode=?) " +
                            " BEGIN " +
                            " INSERT INTO OE_AMC_Transaction  (investorId, amcCode, createdUser,  modifiedUser,  paymentId)" +
                            " VALUES((select distinct PrimaryInvestorID from dbo.InvestorAdditionalInfo where  RelationshipID='F' and HoldingProfileID=?), ? , ?, ?, ?) " +
                            " END  END  ";

            status = jdbcTemplate.update(sqlQuery, ps -> {

                ps.setInt(1, holdingProfileId);
                ps.setInt(2, holdingProfileId);
                ps.setString(3, amcCode);
                ps.setInt(4, holdingProfileId);
                ps.setString(5, amcCode);
                ps.setInt(6, userId);
                ps.setInt(7, userId);
                ps.setInt(8, paymentId);
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception in OE_AMC_Transaction insert : ", e);
        }

        return status;
    }

    public JSONObject getTransDateAndTime(int paymentId) {

        String logPrefix = "#PaymentId : " + paymentId + " | ";

        log.debug(logPrefix + "Transaction Date & Time fetch");

        JSONObject details = new JSONObject();

        try {


            String sqlQuery = " SELECT CONVERT(VARCHAR(10), TransactionDate, 120) TransactionDate, TransactionTime FROM InvestorTransaction WHERE Active = 'A' and PaymentID =? ";

            details = jdbcTemplate.queryForObject(sqlQuery, new Object[]{paymentId}, (rs, rowNumber) -> {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("transDate", rs.getString("TransactionDate"));
                jsonObject.put("transTime", rs.getString("TransactionTime"));
                return jsonObject;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        return details;
    }

    public List<PaymentInfoBO.SchemeDetails> getTransactionSchemes(int paymentId) {

        String logPrefix = "#PaymentId : " + paymentId + " | ";

        log.debug(logPrefix + "Scheme details");

        List<PaymentInfoBO.SchemeDetails> schemes = new ArrayList<>();

        try {


            String sqlQuery = " SELECT IT.UserTransRefID, IT.SchemeCode, ASD.AMC_Code, IHP.Userid, IHP.HoldingProfileId " +
                              " FROM InvestorTransaction IT " +
                              " JOIN dbo.InvestorHoldingProfile IHP ON IHP.HoldingProfileID = IT.HoldingProfileID " +
                              " JOIN AFT_SCHEME_DETAILS ASD ON ASD.SchemeCode = IT.SchemeCode " +
                              " WHERE IT.PaymentId = ? AND IT.TransactionStatus = 'TC' AND IT.Active = 'A' ";

            schemes = jdbcTemplate.query(sqlQuery, new Object[]{paymentId}, (rs, rowNumber) -> {

                PaymentInfoBO.SchemeDetails schemeDetails = new PaymentInfoBO.SchemeDetails();

                schemeDetails.setSchemeCode(rs.getString("SchemeCode"));
                schemeDetails.setAmcCode(rs.getString("Amc_Code"));
                schemeDetails.setTransactionReferenceNo(rs.getString("UserTransRefId"));
                schemeDetails.setHoldingProfileId(rs.getString("HoldingProfileId"));

                return schemeDetails;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        return schemes;
    }

    public PaymentInfoBO getPaymentInfo(int paymentId) {

        String logPrefix = "#PaymentId : " + paymentId + " | ";

        log.debug(logPrefix + "Payment Info details");

        PaymentInfoBO paymentInfoBO = new PaymentInfoBO();
        List<PaymentInfoBO.SchemeDetails> schemes = new ArrayList<>();
        AtomicReference<String> name = new AtomicReference<>("");
        AtomicReference<String> mobile = new AtomicReference<>("");
        AtomicReference<String> holdingProfileId = new AtomicReference<>("");
        AtomicReference<String> goalId = new AtomicReference<>("");
        AtomicReference<Double> totalAmount = new AtomicReference<>(0d);
        DecimalFormat df = new DecimalFormat("###");

        try {


            String sqlQuery = " SELECT IT.UserTransRefID, IT.SchemeCode, IT.Amount, ASD.S_Name, ASD.AMC_Code, AVM.AmcSchemeCode, AVM.TpslSchemeCode, " +
                    " IT.HoldingProfileID, ICD.PortfolioId, ICD.BankId, IA.Mobile, IBD.InvestorName " +
                    " FROM InvestorTransaction IT " +
                    " JOIN InvestorAdditionalInfo IAI ON IAI.HoldingProfileId = IT.HoldingProfileId AND IAI.RelationshipID='F' " +
                    " JOIN InvestorBasicDetail IBD ON IAI.PrimaryInvestorID = IBD.InvestorID " +
                    " JOIN InvestorAddress IA on IA.InvestorID = IBD.InvestorID and IA.MailingAddress = IA.AddressType and IA.InvestorType = 'MF' " +
                    " JOIN InvestorCartDetails ICD ON ICD.SchemeCode = IT.SchemeCode and ICD.PaymentId = IT.PaymentID " +
                    " JOIN AFT_SCHEME_DETAILS ASD ON ASD.SchemeCode = IT.SchemeCode " +
                    " JOIN AFT_VRO_MAPPING AVM ON AVM.AFT_Scheme_Code = ASD.SchemeCode "+
                    " WHERE IT.PaymentId = ? AND IT.TransactionStatus = 'TC' AND IT.Active = 'A' ";

            schemes = jdbcTemplate.query(sqlQuery, new Object[]{paymentId}, (rs, rowNumber) -> {

                PaymentInfoBO.SchemeDetails schemeDetails = new PaymentInfoBO.SchemeDetails();

                schemeDetails.setSchemeCode(rs.getString("SchemeCode"));
                schemeDetails.setSchemeName(rs.getString("S_Name"));
                schemeDetails.setAmount(rs.getDouble("Amount"));
                schemeDetails.setAmcSchemeCode(rs.getString("AmcSchemeCode"));
                schemeDetails.setTpslSchemeCode(rs.getString("TpslSchemeCode"));
                schemeDetails.setTransactionReferenceNo(rs.getString("UserTransRefID"));
                schemeDetails.setBankId(rs.getString("BankId"));

                totalAmount.set(totalAmount.get() + schemeDetails.getAmount());
                holdingProfileId.set(rs.getString("HoldingProfileID"));
                goalId.set(rs.getString("PortfolioId"));
                name.set(rs.getString("InvestorName"));
                mobile.set(rs.getString("Mobile"));

                return schemeDetails;
            });

            PaymentInfoBO.MFDetails mf = new PaymentInfoBO.MFDetails();
            mf.setName(name.get());
            mf.setMobile(mobile.get());
            mf.setHoldingProfileId(holdingProfileId.get());
            mf.setGoalId(goalId.get());
            mf.setTotalAmount(totalAmount.get());
            mf.setTotalAmountFormatted(df.format(totalAmount.get()));
            mf.setSchemes(schemes);

            paymentInfoBO.setPaymentId(String.valueOf(paymentId));
            paymentInfoBO.setMf(mf);

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        return paymentInfoBO;
    }

    public HashMap<String, Object> getInvDetailsForPaymentUpdate(int paymentId) {

        String logPrefix = "#PaymentId : " + paymentId + " | ";

        log.debug(logPrefix + "Investor details");

        HashMap<String, Object> invDetails = new HashMap<>();

        try {
            String sqlQuery = " SELECT IT.HoldingProfileID, IBD.IrType, IBD.isBankVerified " +
                    " FROM InvestorTransaction IT " +
                    " JOIN InvestorAdditionalInfo IAI ON IAI.HoldingProfileId = IT.HoldingProfileId " +
                    " JOIN InvestorBasicDetail IBD ON IBD.InvestorId = IAI.PrimaryInvestorId AND IAI.RelationshipID = 'F' " +
                    " WHERE IT.PaymentId = ? ";

            invDetails = jdbcTemplate.queryForObject(sqlQuery, new Object[]{paymentId}, (rs, rowNumber) -> {

                HashMap<String, Object> investorDetails = new HashMap<>();

                String isBankVerified = rs.getString("isBankVerified");
                investorDetails.put("holdingProfileId", rs.getInt("HoldingProfileID"));
                investorDetails.put("irType", rs.getString("IrType"));
                investorDetails.put("isBankVerified", StringUtils.isNotEmpty(isBankVerified) && isBankVerified.equalsIgnoreCase("true"));
                return investorDetails;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching investor details: ", e);
        }

        return invDetails;
    }

    public List<InvestedSchemeBO> getInvestedSchemeList(int holdingProfileId, int userId, List<InvestmentCostBO> costList) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "Invested scheme list");

        List<InvestedSchemeBO> schemeList = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("#.##");

        try {
            String sqlQuery = " SELECT PS.SchemeCode, ASD.S_Name, PS.FolioNumber, PF.PortfolioName, PF.PortfolioID, PS.Units, (PS.UNITS * ACN.NAVRS) AS Amount, " +
                    " WS.ShowUp, PS.DividendType, FB.UserBankId " +
                    " FROM PositionsInfo PS " +
                    " INNER JOIN PortfolioInfo PF ON PF.PortfolioID = PS.PortfolioID " +
                    " INNER JOIN AFT_SCHEME_DETAILS ASD ON ASD.SchemeCode = PS.SchemeCode " +
                    " INNER JOIN AFT_CURRENTNAV ACN ON PS.SchemeCode = ACN.SchemeCode " +
                    " INNER JOIN WEALTHSCHEMEMASTER WS ON WS.SCHEMECODE=ASD.SCHEMECODE and WS.UPD_FLAG='A' " +
                    " INNER JOIN FOLIO_BANK_INFO FB ON FB.FolioNumber = PS.FolioNumber AND FB.Active=1 " +
                    " WHERE PS.TransMode = 1 AND PS.Active = 'A' AND PS.HoldingProfileID = ? ";

            schemeList = jdbcTemplate.query(sqlQuery, new Object[]{holdingProfileId}, (rs, rowNumber) -> {

                InvestedSchemeBO schemeDetails = new InvestedSchemeBO();
                schemeDetails.setSchemeCode(rs.getString("SchemeCode"));
                schemeDetails.setSchemeName(rs.getString("S_Name"));
                schemeDetails.setFolio(rs.getString("FolioNumber"));
                schemeDetails.setGoalId(rs.getString("PortfolioID"));
                schemeDetails.setGoalName(rs.getString("PortfolioName"));
                schemeDetails.setUnits(rs.getDouble("Units"));
                schemeDetails.setUnitsFormatted(rs.getString("Units"));
                schemeDetails.setCurrentAmount(new BigDecimal(df.format(rs.getDouble("Amount"))));
                schemeDetails.setCurrentAmountFormatted(GeneralUtil.getRupeeBigDecimal(schemeDetails.getCurrentAmount(), 0));

                int portfolioId = rs.getInt("PortfolioID");
                double investment = costList.stream().filter(investmentCostBO ->
                        (investmentCostBO.getHoldingProfileId()==holdingProfileId && investmentCostBO.getFolioNumber().equals(schemeDetails.getFolio())
                                && investmentCostBO.getSchemeCode().equals(schemeDetails.getSchemeCode()) && investmentCostBO.getPortfolioId()==portfolioId))
                        .mapToDouble(costBO -> costBO.getInvestmentAmount()).sum();

                schemeDetails.setInvestedAmount(new BigDecimal(df.format(investment)));
                schemeDetails.setInvestedAmountFormatted(GeneralUtil.getRupeeBigDecimal(schemeDetails.getInvestedAmount(), 0));

                if(StringUtils.isNotEmpty(rs.getString("ShowUp")) && rs.getString("ShowUp").equalsIgnoreCase("T"))
                    schemeDetails.setInvest(true);
                else
                    schemeDetails.setInvest(false);

                String divType = rs.getString("DividendType");
                if(StringUtils.isNotEmpty(divType)) {

                    if(divType.equalsIgnoreCase("Z"))
                        schemeDetails.setOption(BPConstants.GROWTH);
                    else {
                        schemeDetails.setOption(BPConstants.DIVIDEND);
                        if (divType.equalsIgnoreCase("Y"))
                            schemeDetails.setDividendOption(BPConstants.DIVIDEND_REINVESTMENT);
                        else if (divType.equalsIgnoreCase("N"))
                            schemeDetails.setDividendOption(BPConstants.DIVIDEND_PAYOUT);
                    }
                }

                schemeDetails.setBankId(rs.getString("UserBankId"));

                return  schemeDetails;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching investor details: ", e);
        }

        return schemeList;
    }

    public List<SIPDetailsBO> getCurrentSIPList(int holdingProfileId, int userId, List<InvestmentCostBO> costList) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "Current SIPs list");

        List<SIPDetailsBO> sipList = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("#.##");

        try {
            String sqlQuery = " SELECT DISTINCT P.HOLDINGPROFILEID, P.SIPID, P.FOLIONUMBER, F.PORTFOLIOID, F.PORTFOLIONAME, S.SCHEMECODE, P.DividendType, H.AllowTransact, (PS.Units * ACN.NAVRS) AS CurrentValue, " +
                    " S.S_NAME, S.AMC_CODE, P.ECSAMOUNT, P.FLEXIAMOUNT, P.MAXAMOUNT, CONVERT(varchar(10),P.STARTDATE, 103) STARTDATE, CONVERT(varchar(10),P.ENDDATE, 103) ENDDATE, " +
                    " P.StepUpFreq, P.StepUpAmount, P.MaxAmount, SCD.FF_ELIGIBLE_DATE, isnull(P.DateOfEffect,'-') DateOfEffect, isnull(SCD.STATUS,'-') status, PS.Units, CONVERT(VARCHAR(10), PAUSEDORFLEXIDATE, 103) AS FLEXIDATE, " +
                    " P.NOOFINSTALMENT, P.FolioBankId, P.SIPTYPE, P.CONSUMERCODE, P.EXTENSIONFLAG, S.MININVT, P.ECSDATE, OM.INVESTORID AS ISOPENMANDATE, OM.dayOfMonth, P.Frequency, " +
                    " CASE WHEN CONVERT(VARCHAR(20),P.CREATEDDATE,23) <= '" + BPConstants.SIP_STOP_RESTRICTION_DATE + "' THEN 'GREEN' ELSE 'RED' END AS IDENT, " +
                    " OM.ACTIVE, P.SIPREFID, P.INITIALPAYMENTMADE,P.Perpetual, S.CLASSCODE, CONVERT(varchar(10),IT.CreatedDate, 103) transactiondate,OM.FormType formtype, SB.AccountNumber, " +
                    " LU.lookUpDescription AS Bankname, L.lookUpDescription AS Category, P.StepupMaxLimitFlag " +
                    " FROM SIPDETAILS P " +
                    " INNER JOIN AFT_SCHEME_DETAILS S ON P.SCHEMECODE = S.SCHEMECODE AND P.SIPTYPE IN ('','SETUP','FLEXI','STEPUP','DESIGN','" + BPConstants.INSURE_SIP_TYPE + "')" +
                    " AND ISNULL(P.Is_SIPINS_PrePackage, '') <> 'Y'" +
                    " INNER JOIN AFT_SCLASS_MST SCM ON SCM.CLASSCODE = S.CLASSCODE " +
                    " INNER JOIN LOOKUP L ON SCM.CATEGORYNEW = L.LOOKUPID AND L.LOOKUPTYPE ='SCHEMECLASS' AND L.Active>0 " +
                    " INNER JOIN AFT_CURRENTNAV ACN ON S.SchemeCode = ACN.SchemeCode " +
                    " LEFT JOIN InvestorTransaction IT ON IT.UserTransRefID=P.UserTransRefID AND IT.TransactionStatus IN('PS','TS')" +
                    " INNER JOIN INVESTORHOLDINGPROFILE H ON H.HOLDINGPROFILEID = P.HOLDINGPROFILEID " +
                    " INNER JOIN PORTFOLIOINFO F ON P.PORTFOLIOID = F.PORTFOLIOID " +
                    " LEFT JOIN PositionsInfo PS ON P.PORTFOLIOID = PS.PORTFOLIOID AND PS.HoldingProfileId = P.HoldingProfileId AND PS.SchemeCode = P.SchemeCode " +
                    " AND P.FolioNumber = PS.FolioNumber AND (PS.ACTIVE = 'A' OR PS.ACTIVE IS NULL) " +
                    " INNER JOIN SIP_CONSUMER_DETAILS SCD ON P.CONSUMERCODE = SCD.COSUMERCODE " +
                    " LEFT JOIN INVESTOR_OPEN_MANDATE_DETAILS OM ON P.CONSUMERCODE = OM.CONSUMERCODE " +
                    " INNER JOIN Sip_bank_details SB ON SB.ConsumerCode = P.ConsumerCode " +
                    " LEFT JOIN LookUp LU on SB.BankLookupId = LU.lookupid and LU.lookuptype = 'BANKLIST' " +
                    " WHERE P.ACTIVE = 1 AND P.SIPSTATUS <> 'F' AND (OM.ACTIVE IS NULL OR OM.ACTIVE = 'A') AND H.USERID = ? AND H.HOLDINGPROFILEID = ? " +
                    " ORDER BY P.HOLDINGPROFILEID, P.ECSDATE ";

            sipList = jdbcTemplate.query(sqlQuery, new Object[]{userId, holdingProfileId}, (rs, rowNumber) -> {

                if(StringUtils.isNotEmpty(rs.getString("SipTYpe")) && rs.getString("SipType").equalsIgnoreCase("flexi"))
                    log.debug("flexi sip");

                int portfolioId = rs.getInt("PortfolioId");
                String schemeCode = rs.getString("SchemeCode");
                String allowTransact = rs.getString("Allowtransact").trim();
                String perpetual = rs.getString("Perpetual");
                String transactionDate = rs.getString("transactiondate");

                boolean isOESip = (rs.getInt("dayOfMonth")>0);
                boolean isApproved = (rs.getString("Status").trim().equalsIgnoreCase("APPROVED"));
                boolean isPerpetual = (StringUtils.isNotEmpty(perpetual) && Integer.parseInt(perpetual)==1);

                SIPDetailsBO sipDetailsBO = new SIPDetailsBO();
                sipDetailsBO.setPayment(rs.getString("INITIALPAYMENTMADE").equalsIgnoreCase("Y"));
                sipDetailsBO.setSchemeCode(schemeCode);
                sipDetailsBO.setSchemeName(rs.getString("S_Name"));
                sipDetailsBO.setAmcCode(rs.getString("AMC_CODE"));
                sipDetailsBO.setFolio(rs.getString("FolioNumber"));
                sipDetailsBO.setUnits(rs.getDouble("Units"));
                sipDetailsBO.setUnitsFormatted(String.valueOf(sipDetailsBO.getUnits()));
                sipDetailsBO.setGoalId(rs.getString("PortfolioID"));
                sipDetailsBO.setGoalName(rs.getString("PortfolioName"));
                sipDetailsBO.setSipDate(rs.getInt("ECSDATE"));
                sipDetailsBO.setReferenceId(rs.getString("SIPID"));
                sipDetailsBO.setBankId(rs.getString("FolioBankId"));
                sipDetailsBO.setConsumerCode(rs.getString("CONSUMERCODE"));

                String divType = rs.getString("DividendType");
                if(StringUtils.isNotEmpty(divType)) {

                    if(divType.equalsIgnoreCase("Z"))
                        sipDetailsBO.setOption(BPConstants.GROWTH);
                    else {
                        sipDetailsBO.setOption(BPConstants.DIVIDEND);
                        if (divType.equalsIgnoreCase("Y"))
                            sipDetailsBO.setDividendOption(BPConstants.DIVIDEND_REINVESTMENT);
                        else if (divType.equalsIgnoreCase("N"))
                            sipDetailsBO.setDividendOption(BPConstants.DIVIDEND_PAYOUT);
                    }
                }

                String sipType = rs.getString("SIPTYPE");
                if (StringUtils.isEmpty(sipType) || "SETUP".equalsIgnoreCase(sipType) || "DESIGN".equalsIgnoreCase(sipType)) {
                    sipType = "REGULAR";
                }

                sipDetailsBO.setSipType(sipType.toLowerCase());

                int noOfInstallmentsPaid = getNoOfInstallmentsPaidForSIP(rs.getString("ConsumerCode"), rs.getInt("SIPID")).size();
                String nextEcsDate = getNextInstallmentOn(rs.getString("FF_ELIGIBLE_DATE"),
                        rs.getString("ECSDate"),
                        rs.getString("FormType"),
                        rs.getString("TransactionDate"),
                        rs.getString("DateOfEffect"),
                        noOfInstallmentsPaid);

                if(sipType.equalsIgnoreCase(BPConstants.FLEXI_SIP_TYPE)) {

                    SIPDetailsBO.FlexiSIP flexiSIP = new SIPDetailsBO.FlexiSIP();

                    flexiSIP.setTenure(rs.getInt("NOOFINSTALMENT"));
                    flexiSIP.setConsumerCode(rs.getString("CONSUMERCODE"));
                    flexiSIP.setFrequency(BPConstants.FREQUENCY_MONTHLY);
                    flexiSIP.setMaximumAmount(rs.getInt("MaxAmount"));

                    String flexiDate = rs.getString("FLEXIDATE");
                    if (isApproved && flexiDate!=null && flexiDate.equals(nextEcsDate))
                        flexiSIP.setAmount(rs.getInt("FLEXIAMOUNT"));
                    else
                        flexiSIP.setAmount(rs.getInt("ECSAMOUNT"));

                    sipDetailsBO.setFlexi(flexiSIP);
                }
                else if(sipType.equalsIgnoreCase(BPConstants.STEPUP_SIP_TYPE)) {

                    SIPDetailsBO.StepupSIP stepupSIP = new SIPDetailsBO.StepupSIP();

                    stepupSIP.setAmount(rs.getInt("ECSAMOUNT"));
                    stepupSIP.setTenure(rs.getInt("NOOFINSTALMENT"));
                    stepupSIP.setStepupfrequeny(rs.getString("StepUpFreq"));
                    stepupSIP.setStepupAmount(rs.getInt("FLEXIAMOUNT"));
                    stepupSIP.setFinalAmount(rs.getInt("MaxAmount"));
                    stepupSIP.setConsumerCode(rs.getString("CONSUMERCODE"));
                    stepupSIP.setFrequency(BPConstants.FREQUENCY_MONTHLY);
                    sipDetailsBO.setStepup(stepupSIP);

                }
                else if(sipType.equalsIgnoreCase(BPConstants.ALERT_SIP_TYPE)) {

                    SIPDetailsBO.AlertSIP alertSIP = new SIPDetailsBO.AlertSIP();

                    alertSIP.setStartDate(rs.getString("STARTDATE"));
                    alertSIP.setEndDate(rs.getString("ENDDATE"));
                    alertSIP.setAmount(rs.getInt("ECSAMOUNT"));
                    alertSIP.setFrequency(BPConstants.FREQUENCY_MONTHLY);
                    sipDetailsBO.setAlert(alertSIP);
                }
                else{
                    SIPDetailsBO.RegularSIP regularSIP = new SIPDetailsBO.RegularSIP();
                    regularSIP.setConsumerCode(rs.getString("CONSUMERCODE"));
                    regularSIP.setTenure(rs.getInt("NOOFINSTALMENT"));
                    regularSIP.setAmount(rs.getInt("ECSAMOUNT"));
                    regularSIP.setFrequency(BPConstants.FREQUENCY_MONTHLY);

                    sipDetailsBO.setRegular(regularSIP);
                }


                sipDetailsBO.setTenure(rs.getInt("NOOFINSTALMENT"));

                int remainingInstallments = rs.getInt("NOOFINSTALMENT") - noOfInstallmentsPaid;

                sipDetailsBO.setCompletedInstallments(noOfInstallmentsPaid);
                sipDetailsBO.setNextInstallmentOn(nextEcsDate);
                sipDetailsBO.setNextInstallmentAmount(rs.getInt("ECSAMOUNT"));
                sipDetailsBO.setNextInstallmentAmountFormatted(GeneralUtil.getRupeeBigDecimal(new BigDecimal(rs.getInt("ECSAMOUNT")), 0));
                sipDetailsBO.setTag(rs.getString("SIPTYPE"));
                sipDetailsBO.setDescription(""); //todo:
                sipDetailsBO.setCategory(rs.getString("Category"));


                double totalInvestment = costList.stream().filter(investmentCostBO ->
                        (investmentCostBO.getHoldingProfileId()==holdingProfileId && investmentCostBO.getFolioNumber().equals(sipDetailsBO.getFolio())
                                && investmentCostBO.getSchemeCode().equals(schemeCode) && investmentCostBO.getPortfolioId()==portfolioId))
                        .mapToDouble(costBO -> costBO.getInvestmentAmount()).sum();

                double currentValue = rs.getDouble("CurrentValue");
                double gain = currentValue - totalInvestment;
                sipDetailsBO.setCurrentValue(new BigDecimal(df.format(currentValue)));
                sipDetailsBO.setCurrentValueFormatted(GeneralUtil.getRupeeBigDecimal(sipDetailsBO.getCurrentValue(), 0));
                sipDetailsBO.setTotalGain(new BigDecimal(df.format(gain)));
                sipDetailsBO.setTotalGainFormatted(GeneralUtil.getRupeeBigDecimal(sipDetailsBO.getTotalGain(), 0));

                if(GeneralUtil.NullCheckString(rs.getString("StepupMaxLimitFlag")).equals("Y"))
                    sipDetailsBO.setStepUpFlag(true);
                else
                    sipDetailsBO.setStepUpFlag(false);

                sipDetailsBO.setBankName(rs.getString("BankName"));
                sipDetailsBO.setBankAccountNumber(GeneralUtil.maskBankAccountNumber(rs.getString("AccountNumber")));
                sipDetailsBO.setHoldingProfileName(userDao.getHoldingprofileName(holdingProfileId));

                List<String> actions = new ArrayList<>();

                if(sipType.equalsIgnoreCase(BPConstants.INSURE_SIP_TYPE))
                    actions.add(BPConstants.ACTION_STOP_SIP);

                else {

                    if (sipType.equalsIgnoreCase(BPConstants.ALERT_SIP_TYPE) && getAlertSIPPaymentFlag(userId, rs.getInt("SIPID"))) {
                        actions.add(BPConstants.ACTION_MAKE_PAYMENT);
                    }

//                    if (!generalUtil.isDSP(schemeCode)) {
//
//                        if (isApproved)
//                            actions.add(FIConstants.ACTION_SKIP);
//
//                        String extensionFlag = rs.getString("EXTENSIONFLAG") == null ? "" : rs.getString("EXTENSIONFLAG").trim();
//
//                        if (!sipType.equalsIgnoreCase(FIConstants.STEPUP_SIP_TYPE) && !isPerpetual && "N".equalsIgnoreCase(extensionFlag) && remainingInstallments == 1 && rs.getInt("CLASSCODE") != 24)  //For Liquid scheme, will not allow Extension, change scheme
//                            actions.add(FIConstants.ACTION_EXTEND);
//                    }

                    if (("GREEN".equalsIgnoreCase(rs.getString("IDENT")) || noOfInstallmentsPaid >= BPConstants.MINIMUM_NUMBER_OF_INSTALLMENTS_TO_STOP) && isPerpetual)
                        actions.add(BPConstants.ACTION_STOP_SIP);

                    if (!generalUtil.isSchemeHasRestrictions(schemeCode) && !generalUtil.isChangeAmountRestricted(schemeCode)) {

//                        actions.add(FIConstants.ACTION_CHANGE_DATE);

                        if (sipType.equalsIgnoreCase(BPConstants.REGULAR_SIP_TYPE) && rs.getInt("ISOPENMANDATE") > 0 && !isOESip)
                            actions.add(BPConstants.ACTION_CHANGE_AMOUNT);

                        else if (sipType.equalsIgnoreCase(BPConstants.FLEXI_SIP_TYPE) && isApproved)
                            actions.add(BPConstants.ACTION_CHANGE_FLEXI_AMOUNT);
                    }

//                    if (!isOESip)
//                        actions.add(FIConstants.ACTION_CHANGE_SCHEME);
                    //todo : perpetual to regular / change to regular sip
                }

                sipDetailsBO.setActions(actions);

                return  sipDetailsBO;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching current SIP list: ", e);
        }

        return sipList;
    }

    public List<STPDetailsBO> getCurrentSTPList(int holdingProfileId, int userId) {

        String logPrefix = "#UserId : " + userId + " | #HoldingProfileId : " + holdingProfileId + " | ";
        log.debug(logPrefix + "Current STP list");

        List<STPDetailsBO> schemeList = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("#.##");

        try {
            String sqlQuery = " SELECT " +
                    "  DISTINCT(STPD.STPID), STPD.HOLDINGPROFILEID, STPD.PORTFOLIOID, PFI.PORTFOLIONAME, STPD.FromSchemeCode, STPD.ToSchemeCode, " +
                    "  ASD.S_NAME FromSchemeName, ASD1.S_NAME ToSchemeName, STPD.STARTDATE, STPD.STPDATE, STPD.FREQUENCY, STPD.DividendType, " +
                    "  STPD.InitialUnits, STPD.MonthlyUnits, STPD.INITIALAMOUNT, STPD.MONTHLYAMOUNT, PS.Units, ASD.AMC_CODE, " +
                    "  STPD.NOOFINSTALMENTS, STPD.ACTIVE, STPD.FOLIONUMBER, STPD.ACCOUNTCLOSURE, STPD.PROCEEDBY, " +
                    "  CASE " +
                    "     WHEN STPD.Frequency = 'D' THEN 'false' " +
                    "     WHEN (STPD.Frequency = 'W' AND DATEPART(DW, DATEADD(MI, 330, GETDATE())) = STPD.STPDate) THEN 'false' " +
                    "     WHEN (STPD.Frequency = 'M' AND DATEPART(D, DATEADD(MI, 330, GETDATE())) = STPD.STPDate) THEN 'false' " +
                    "     ELSE 'true' " +
                    "  END EDITENABLE, " +
                    " (SELECT lookUpDescription FROM Lookup WHERE lookUpId = SCM_FROM.CATEGORYNEW AND lookUpType = 'SchemeClass' AND Lookup.active > 0) AS FromSchemeClassDesc, " +
                    " (SELECT lookUpDescription FROM Lookup WHERE lookUpId = SCM_TO.CATEGORYNEW AND lookUpType = 'SchemeClass' AND Lookup.active > 0) AS ToSchemeClassDesc" +
                    "  FROM STPDETAILS STPD " +
                    "  INNER JOIN INVESTORHOLDINGPROFILE H ON H.HOLDINGPROFILEID = STPD.HOLDINGPROFILEID AND H.USERID = ? AND H.HOLDINGPROFILEID = ? " +
                    "  JOIN AFT_SCHEME_DETAILS ASD ON STPD.FROMSCHEMECODE = ASD.SCHEMECODE " +
                    "  JOIN PORTFOLIOINFO PFI ON STPD.PORTFOLIOID = PFI.PORTFOLIOID " +
                    "  LEFT JOIN PositionsInfo PS ON STPD.PORTFOLIOID = PS.PORTFOLIOID AND STPD.HoldingProfileId = PS.HoldingProfileId AND STPD.FromSchemeCode = PS.SchemeCode " +
                    "  AND STPD.FolioNumber = PS.FolioNumber AND PS.ACTIVE = 'A' " +
                    "  JOIN AFT_SCHEME_DETAILS ASD1 ON ASD1.SCHEMECODE=STPD.TOSCHEMECODE " +
                    "  JOIN AFT_SCLASS_MST SCM_FROM ON ASD.CLASSCODE = SCM_FROM.CLASSCODE " +
                    "  JOIN AFT_SCLASS_MST SCM_TO ON ASD1.CLASSCODE = SCM_TO.CLASSCODE" +
                    "  WHERE STPD.ACTIVE in (1,3)  ORDER BY STPDATE ";

            schemeList = jdbcTemplate.query(sqlQuery, new Object[]{userId, holdingProfileId}, (rs, rowNumber) -> {

                STPDetailsBO stpDetailsBO = new STPDetailsBO();
                stpDetailsBO.setReferenceId(rs.getString("STPID"));
                stpDetailsBO.setSchemeCode(rs.getString("FromSchemeCode"));
                stpDetailsBO.setSchemeName(rs.getString("FromSchemeName"));
                stpDetailsBO.setFolio(rs.getString("FolioNumber"));
                stpDetailsBO.setUnits(rs.getDouble("Units"));
                stpDetailsBO.setUnitsFormatted(String.valueOf(stpDetailsBO.getUnits()));
                stpDetailsBO.setAmount(new BigDecimal(df.format(rs.getDouble("MONTHLYAMOUNT"))));
                stpDetailsBO.setAmountFormatted(GeneralUtil.getRupeeBigDecimal(stpDetailsBO.getAmount(), 0));
                stpDetailsBO.setInitialInvestment(new BigDecimal(df.format(rs.getDouble("INITIALAMOUNT"))));
                stpDetailsBO.setInitialInvestmentFormatted(GeneralUtil.getRupeeBigDecimal(stpDetailsBO.getInitialInvestment(), 0));
                stpDetailsBO.setGoalId(rs.getString("PortfolioID"));
                stpDetailsBO.setGoalName(rs.getString("PortfolioName"));

                String freq = rs.getString("FREQUENCY") != null ? rs.getString(
                        "FREQUENCY").trim() : "";
                int stpDate = rs.getInt("STPDATE");
                if ("M".equalsIgnoreCase(freq)) {
                    stpDetailsBO.setFrequency("monthly");
                    stpDetailsBO.setDayOrDate(generalUtil.getEcsDay(String.valueOf(stpDate)));
                } else if ("W".equalsIgnoreCase(freq)) {
                    stpDetailsBO.setFrequency("weekly");
                    stpDetailsBO.setDayOrDate(generalUtil.getSTPWeekDay(stpDate));
                } else if ("D".equalsIgnoreCase(freq)) {
                    stpDetailsBO.setFrequency("daily");
                    stpDetailsBO.setDayOrDate("");
                }

                String divType = rs.getString("DividendType");
                if(StringUtils.isNotEmpty(divType)) {

                    if(divType.equalsIgnoreCase("Z"))
                        stpDetailsBO.setDividendOption(BPConstants.GROWTH);
                    else {
                        if (divType.equalsIgnoreCase("Y"))
                            stpDetailsBO.setDividendOption(BPConstants.DIVIDEND_REINVESTMENT);
                        else if (divType.equalsIgnoreCase("N"))
                            stpDetailsBO.setDividendOption(BPConstants.DIVIDEND_PAYOUT);
                    }
                }

                stpDetailsBO.setCompletedInstallments(getNoOfInstallmentsPaidForSTP(rs.getInt("STPID")).size());
                stpDetailsBO.setNoOfInstallments(rs.getInt("NOOFINSTALMENTS"));
                stpDetailsBO.setToSchemeCode(rs.getString("ToSchemeCode"));
                stpDetailsBO.setToSchemeName(rs.getString("ToSchemeName"));
                stpDetailsBO.setCategory(rs.getString("FromSchemeClassDesc"));
                stpDetailsBO.setAmcCode(rs.getString("AMC_CODE"));
                stpDetailsBO.setTotalStpAmount(new BigDecimal(df.format(getTotalSTPAmount(rs.getInt("STPID")))));
                stpDetailsBO.setTotallStpAmountFormatted(GeneralUtil.getRupeeBigDecimal(stpDetailsBO.getTotalStpAmount(), 0));

                List<String> actions = new ArrayList<>();
                boolean editEnable = true;
                String proceedBy = rs.getString("PROCEEDBY") == null ? "F" : rs.getString("PROCEEDBY").trim();
                if ("UNT".equalsIgnoreCase(proceedBy)) {
                    editEnable = false;
                } else {
                    editEnable = rs.getBoolean("EDITENABLE");
                }

                if(editEnable)
                    actions.add(BPConstants.ACTION_EDIT);

                if(rs.getInt("Active")==1)
                    actions.add("cancel");

                stpDetailsBO.setAction(actions);

                return  stpDetailsBO;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching Current STP list: ", e);
        }

        return schemeList;
    }

    public List<Integer> getNoOfInstallmentsPaidForSIP(String consumerCode, int sipId) {

        String logPrefix = "#SipId : " + sipId + " | #ConsumerCode : " + consumerCode + " | ";
        log.debug(logPrefix + "No of Installemnts Paid");

        List<Integer> installments = new ArrayList<>();

        try {
            String sqlQuery = " { call WF_SIP_NoOfInstallmentsPaid ( ?, ? ) } ";

            installments = jdbcTemplate.query(sqlQuery, new Object[]{consumerCode, sipId}, (rs, rowNumber) -> 1);

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching installments paid: ", e);
        }

        return installments;
    }

    public List<Integer> getNoOfInstallmentsPaidForSTP(int stpId) {

        String logPrefix = "#StpId : " + stpId + " | ";
        log.debug(logPrefix + "No of Installemnts Paid");

        List<Integer> installments = new ArrayList<>();

        try {
            String sqlQuery = " SELECT DISTINCT(USERTRANSREFID) FROM STP_TRANSACTION WHERE STPID=?  ";

            installments = jdbcTemplate.query(sqlQuery, new Object[]{stpId}, (rs, rowNumber) -> 1);

        } catch (Exception e) {
            log.error(logPrefix + "Exception while fetching installments paid: ", e);
        }

        return installments;
    }

    public int getTransactionDebitCount(String formtype, String actualDebitDate, String transtype) {
        
        int debitCount = 0;
        try {

            String sqlQuery = " SELECT count(*) transactionCount FROM INVESTOR_OPEN_MANDATE_DETAILS O " +
                    " INNER JOIN dbo.TPSL_FORWARD_FEED T ON O.ConsumerCode = T.ConsumerCode AND T.Active=1 AND O.Active='A' " +
                    " WHERE O.FormType = ? AND T.ActualDebitDate = ? AND T.TransType = ? ";

            debitCount = jdbcTemplate.queryForObject(sqlQuery, new Object[]{formtype, actualDebitDate, transtype}, Integer.class);


        } catch (Exception e) {
            log.error("Exception while fetching debitCount: ", e);
        }

        log.debug("Debit count : " + debitCount);
        return debitCount;
    }

    public String getNextEcsDateFromUserTransRefId(int userTransRefId, String schemeCode) {

        String nextEcsDate = "";
        try {

            if(userTransRefId==0 || StringUtils.isEmpty(schemeCode))
                return nextEcsDate;

            String sqlQuery = " SELECT DISTINCT SCD.FF_ELIGIBLE_DATE, OM.FormType formtype, S.ECSDate, isnull(S.DateOfEffect,'-') DateOfEffect, CONVERT(varchar(10),IT.CreatedDate, 103) transactiondate, " +
                              " S.ConsumerCode, S.SIPID " +
                              " FROM SIPDetails S " +
                              " INNER JOIN SIP_CONSUMER_DETAILS SCD ON S.CONSUMERCODE = SCD.COSUMERCODE " +
                              " LEFT JOIN INVESTOR_OPEN_MANDATE_DETAILS OM ON S.CONSUMERCODE = OM.CONSUMERCODE " +
                              " LEFT JOIN InvestorTransaction IT ON IT.UserTransRefID=S.UserTransRefID AND IT.TransactionStatus IN('PS','TS') " +
                              " WHERE S.UserTransRefId = ? AND S.SchemeCode = ? AND S.Active = 1";

            nextEcsDate = jdbcTemplate.queryForObject(sqlQuery, new Object[]{userTransRefId, Integer.parseInt(schemeCode)}, (rs, rowNumber) -> {

                int noOfInstallmentsPaid = getNoOfInstallmentsPaidForSIP(rs.getString("ConsumerCode"), rs.getInt("SIPID")).size();

                String nextDate = getNextInstallmentOn(rs.getString("FF_ELIGIBLE_DATE"),
                                                       rs.getString("ECSDate"),
                                                       rs.getString("FormType"),
                                                       rs.getString("TransactionDate"),
                                                       rs.getString("DateOfEffect"),
                                                       noOfInstallmentsPaid);
                if(nextDate==null)
                    nextDate = "";

                return nextDate;

            });


        } catch (Exception e) {
            log.error("Exception while fetching nextEcsDate: ", e);
        }

        log.debug("Next EcsDate : " + nextEcsDate);
        return nextEcsDate;
    }

    public String getNextInstallmentOn(String ffEligibleDate, String ecsDate, String formType, String transactionDate, String dateOfEffect, int noOfInstallmentsPaid) {

        String nextDate = "";
        try {

            if(StringUtils.isNotEmpty(ecsDate) && Integer.parseInt(ecsDate)!=0) {
                nextDate = generalUtil.getNextEcsDate(new DateBO(ffEligibleDate).getDateFirstView(), ecsDate);
                int transactionDebitCount = getTransactionDebitCount(formType, DateUtil.getDBDate(nextDate, "dd/MM/yyyy"), "SIP");

                if (StringUtils.isNotEmpty(transactionDate) && StringUtils.isNotEmpty(nextDate) &&
                        (transactionDate.equals(nextDate) || (transactionDebitCount > 0 && noOfInstallmentsPaid == 0 && StringUtils.isNotEmpty(transactionDate)))) {
                    nextDate = DateUtil.addMonthsToDate(1, nextDate, "dd/MM/yyyy");
                }
            }

            if (StringUtils.isEmpty(nextDate))
                nextDate = dateOfEffect;
        } catch (Exception e) {
            log.error("Exception:", e);
        }

        return nextDate;
    }

    public boolean getAlertSIPPaymentFlag(int userId, int sipId) {

        int count = 0;
        boolean makePayment = false;
        try {

            String sqlQuery = " SELECT  DISTINCT top 1 S.HOLDINGPROFILEID " +
                    " FROM  SIPDETAILS S " +
                    " Inner Join ALERTSIP A on S.SIPID = A.SIPID AND A.PAYMENT_MADE = 'N' AND A.ACTIVE = 1 AND S.ACTIVE = 1 " +
                    " Inner Join INVESTORHOLDINGPROFILE H on  H.HOLDINGPROFILEID = S.HOLDINGPROFILEID AND H.USERID = ? AND S.SIPID = ? " +
                    " WHERE  (DATEDIFF(day,PAYMENT_DUE_DATE,DATEADD(MI, 330, GETDATE())) BETWEEN -2 AND 2) " +
                    " AND ( (A.Transactiontype ='Alert') or  (A.Transactiontype <> 'Alert' AND A.PAYMENT_ID IS NULL ))" +
                    " AND (DAY(DATEADD(MI, 330, GETDATE())-2) = ECSDATE OR DAY(DATEADD(MI, 330, GETDATE())-1) = ECSDATE OR DAY(DATEADD(MI, 330, GETDATE())) = ECSDATE) ";

            count = jdbcTemplate.queryForObject(sqlQuery, new Object[]{userId, sipId}, Integer.class);

            makePayment = count > 0;

        } catch (Exception e) {
            log.error("Exception while fetching alert SIP payment flag: ", e);
        }

        log.debug("PaymentFlag : " + makePayment);
        return makePayment;
    }

    public double getTotalSTPAmount(int stpId) {

        double amount = 0;

        try {

            String sqlQuery = " SELECT SUM(IT.AMOUNT) TotalSTPAmount " +
                    " FROM dbo.InvestorTransaction IT\n" +
                    " INNER JOIN dbo.STP_TRANSACTION  ST ON ST.UserTransRefID = IT.UserTransRefID " +
                    " WHERE IT.TransactionStatus = 'TS' AND IT.Active ='A' AND ST.STPID = ? " +
                    " GROUP BY ST.STPID ";

            amount = jdbcTemplate.queryForObject(sqlQuery, new Object[]{stpId}, Double.class);

        } catch (Exception e) {
            log.error("Exception while fetching STP Total amount: ", e);
        }

        log.debug("TotalSTPAmount : " + amount);
        return amount;
    }

    public String getEcsDateFromSIPID(int sipId) {

        String ecsDate = "";

        try {

            String sqlQuery = " SELECT ECSDate, CONVERT(VARCHAR(10),DATEADD(MI, 330, GETDATE()), 23) CurrentDate FROM SIPDetails WHERE SIPID = ? AND Active=1 ";

            ecsDate = jdbcTemplate.queryForObject(sqlQuery, new Object[]{sipId}, (rs, rowNumber) -> {

                String today = rs.getString("CurrentDate");
                String ecsDayStr = rs.getString("ECSDate");
                if(ecsDayStr.length()==1)
                    ecsDayStr = "0"+ecsDayStr;

                ecsDayStr = today.substring(0,8) + ecsDayStr;

                return ecsDayStr;

            });

        } catch (Exception e) {
            log.error("Exception while fetching SIP EcsDate : ", e);
        }

        log.debug("EcsDate : " + ecsDate);
        return ecsDate;
    }

}


package com.kp.core.app.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class BankVerifyRequestBO {


    @Expose
    private String status;
    @Expose
    private String subCode;
    @Expose
    private String message;
    @Expose
    private String accountStatus;
    @Expose
    private String accountStatusCode;
    @Expose
    private Data data;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Data{
        private String nameAtBank;
        private String refId;
        private String bankName;
        private String utr;
        private String city;
        private String branch;
        private String micr;
        private String nameMatchScore;
        private String nameMatchResult;
        private String bvRefId;
        private String accountExists;
        private String amountDeposited;
        private String bankAccount;
        private String ifsc;
    }
}

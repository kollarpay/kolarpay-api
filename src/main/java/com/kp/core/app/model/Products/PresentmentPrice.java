package com.kp.core.app.model.Products;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public  class PresentmentPrice {

    @SerializedName("price")
    @Expose
    public Price price;
    @SerializedName("compare_at_price")
    @Expose
    public Double compareAtPrice;

}
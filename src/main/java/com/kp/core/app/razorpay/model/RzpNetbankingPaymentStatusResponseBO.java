package com.kp.core.app.razorpay.model;


import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RzpNetbankingPaymentStatusResponseBO {


    @SerializedName("razorpay_payment_id")
    private String razorpayPaymentId;
    private List<NextBO> next;

    @Getter
    @Setter
    public static class NextBO {
        private String action;
        private String url;
    }

}

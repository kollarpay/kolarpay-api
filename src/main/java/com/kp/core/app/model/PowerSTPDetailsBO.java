package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PowerSTPDetailsBO {

    private String holdingProfileId;
    private String portfolioId;
    private String fromSchemeCode;
    private String toSchemeCode;
    private String startDate;
    private String stpDate;
    private double initialAmount;
    private String monthlyAmount;
    private String noOfInstallments;
    private String frequency;
    private String dividendType;
    private String folioNumber;
    private String active;
    private String stpType;
    private double targetAmount;
    private String initialUnits;
    private String monthlyUnits;
    private String proceedBy;
    private String accountClosure;
    private String euinId;
    private String channelId;
    private String euinFlag;
    private String incentiveTransaction;
    private String mediumThru;
    private String oldStpId;
}

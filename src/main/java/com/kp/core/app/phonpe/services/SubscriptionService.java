package com.kp.core.app.phonpe.services;

import com.google.gson.Gson;
import com.kp.core.app.config.PaymentConfig;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.SubscriptionBO;
import com.kp.core.app.phonpe.dao.SubscriptionDAO;
import com.kp.core.app.phonpe.model.*;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.APIGeneralUtil;
import com.kp.core.app.utils.GeneralUtil;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
public class SubscriptionService {

    private final PaymentConfig paymentConfig = ConfigFactory.create(PaymentConfig.class);
    private final RestTemplate restTemplate;

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private SubscriptionDAO subscriptionDAO;
    private final Gson gson;



    public SubscriptionService(RestTemplate restTemplate, Gson gson) {
        this.restTemplate = restTemplate;
        this.gson = gson;
    }


    public GlobalResponseBO createSubscription(SubscriptionBO subscriptionBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create subscription Service");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createSubscription");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        SubscriptionCreate subscriptionCreate;
        SubscriptionCreateResponse subscriptionCreateResponse;
        try {

            isValidPlan(subscriptionBO, errorBoList);
            LocalDate start_Date = LocalDate.now();
            LocalDate endate_date = start_Date.plusMonths(subscriptionBO.getNoofinstallment());
            subscriptionBO.setStartDate(start_Date.toString());
            subscriptionBO.setEndDate(endate_date.toString());

            if(errorBoList.isEmpty()) {
                subscriptionDAO.createSubscription(subscriptionBO,userId);
                if(!StringUtil.isNullOrEmpty(subscriptionBO.getSubscriptionId())){
                 subscriptionCreate= createSubscriptionRequest(subscriptionBO,userId,paymentConfig);
                    subscriptionCreateResponse=  phonePeCreateSubscription(subscriptionCreate,errorBoList);
                    subscriptionBO.setSubscriptionCreateResponse(subscriptionCreateResponse);
                }
            }


        } catch (Exception e) {
            log.error(logPrefix + "Create Subscription API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, subscriptionBO, globalResponseBo);

        log.debug(logPrefix + "Create Subscription API Status : " + stat);
        return  globalResponseBo;

    }

    public SubscriptionCreate createSubscriptionRequest(SubscriptionBO subscriptionBO, int userId,PaymentConfig paymentConfig) {
      /*
      {
  "merchantId": "MID12345",
  "merchantSubscriptionId": "MSUB123456789012345",
  "merchantUserId": "MU123456789",
  "authWorkflowType": "PENNY_DROP",		  //PENNY_DROP or TRANSACTION
  "amountType": "FIXED",		          //FIXED or VARIABLE
  "amount": 39900,
  "frequency": "MONTHLY",
  "recurringCount": 12,
  "mobileNumber": "9xxxxxxxxx",
  "deviceContext": {
  "phonePeVersionCode": 400922		         //Only for ANDROID
  }
}
      */
        SubscriptionCreate subscriptionCreate = new SubscriptionCreate();

        subscriptionCreate.setMerchantId(paymentConfig.getPhonePeApiMerchantId());
        subscriptionCreate.setMerchantSubscriptionId(subscriptionBO.getSubscriptionId());
        subscriptionCreate.setMerchantUserId(subscriptionBO.getCustomerId());
        subscriptionCreate.setAuthWorkflowType("PENNY_DROP");
        subscriptionCreate.setAmount((long) (subscriptionBO.getEcsamount()*100));
        subscriptionCreate.setAmountType("VARIABLE");
        subscriptionCreate.setFrequency(subscriptionBO.getFrequency().equalsIgnoreCase("M")?"MONTHLY":subscriptionBO.getFrequency());
        subscriptionCreate.setRecurringCount(subscriptionBO.getNoofinstallment());
        subscriptionCreate.setMobileNumber(subscriptionBO.getMobileNumber());
        DeviceContext deviceContext=new DeviceContext();
        deviceContext.setPhonePEVersionCode(paymentConfig.getPhonePeApiAndroidVersionCode());
        subscriptionCreate.setDeviceContext(deviceContext);
       return subscriptionCreate;
    }
    public SubscriptionAuthRequest subscriptionAuthRequest(SubscriptionBO subscriptionBO, int userId, PaymentConfig paymentConfig) {
      /*
  {
    "merchantId": "MID12345",
    "merchantUserId": "U123456789",
    "subscriptionId": "OMS2006110139450123456789",
    "authRequestId": "TX123456789",
    "amount": 39900,
    "paymentInstrument": {
      "type": "UPI_INTENT",
      "targetApp": "com.phonepe.app"
    },
    "deviceContext" : {
    "deviceOS" : "ANDROID"
    }
}
      */
        SubscriptionAuthRequest subscriptionAuthRequest = new SubscriptionAuthRequest();
        subscriptionAuthRequest.setMerchantId(paymentConfig.getPhonePeApiMerchantId());
        subscriptionAuthRequest.setMerchantUserId(subscriptionBO.getCustomerId());
        subscriptionAuthRequest.setSubscriptionId(subscriptionBO.getSubscriptionId());
        subscriptionAuthRequest.setAuthRequestId(subscriptionBO.getAuthRequestId());
        subscriptionAuthRequest.setAmount((long) (subscriptionBO.getEcsamount()*100));
        subscriptionAuthRequest.setPaymentInstrument(subscriptionBO.getPaymentInstrument());
        subscriptionAuthRequest.setDeviceContext(subscriptionBO.getDeviceContext());
        return subscriptionAuthRequest;
    }
    public void isValidPlan(SubscriptionBO subscriptionBO, List<ErrorBO> errorBoList) {

        try {

            if(StringUtil.isNullOrEmpty(subscriptionBO.getPlanId()))
                errorBoList.add(new ErrorBO("Plan Id", "Invalid plan id"));

        } catch (Exception e) {
            log.error("Exception in subscription creation - validate : ", e);
        }
    }
    public SubscriptionCreateResponse phonePeCreateSubscription(SubscriptionCreate subscriptionCreate, List<ErrorBO> errorBoList) {

        SubscriptionCreateResponse responseBO = new SubscriptionCreateResponse();
        String SubscriptionCreatePath= paymentConfig.getSlash() + paymentConfig.getPhonePeVersion() + paymentConfig.getSlash() + paymentConfig.getPhonePeApiSubscriptionCreateUrl();
        String subscriptionCreateApiUrl = paymentConfig.getPhonePeBaseUrl() +SubscriptionCreatePath;
        log.info("subscriptionCreateApiUrl : " + subscriptionCreateApiUrl);
        try {

            HttpHeaders headers = new HttpHeaders();
           String payload= gson.toJson(subscriptionCreate).toString();
            log.info("payload : " + payload);
            String base64= APIGeneralUtil.getBase64(payload);
            String checksum= APIGeneralUtil.getCheckSum(base64,SubscriptionCreatePath);
            log.info("base64 : " + base64);
            subscriptionCreate.setRequest(base64);
            headers.set("X-Verify",checksum);
            headers.set("accept","application/json");
            headers.set("Content-Type","application/json");
            headers.set("X-CALLBACK-URL",paymentConfig.getPhonePeApiRedirectUrl());
            log.info ("checksum : " + checksum);
            HttpEntity<SubscriptionCreate> request = new HttpEntity<>(subscriptionCreate, headers);


            log.info("subscription creation | Request: {}", gson.toJson(subscriptionCreate));

            ResponseEntity response = restTemplate.postForEntity(subscriptionCreateApiUrl, request, String.class);

            log.info("subscription creation | Request: {} | Response: {} ", gson.toJson(response), response);

            if(response.getStatusCode().value() == 200 && response.getBody() != null) {

                responseBO = gson.fromJson(response.getBody().toString(), SubscriptionCreateResponse.class);
            }

            else if (response.getStatusCode().value() == 400 && response.getBody() != null) {

                log.debug("InitiatePhonePeSubscription Error Response : " + response.getBody().toString());

//                RzpErrorBO rzpErrorBO = gson.fromJson(response.getBody().toString(), RzpErrorBO.class);
//
//                if(rzpErrorBO!=null && rzpErrorBO.getError()!=null && StringUtils.isNotEmpty(rzpErrorBO.getError().getDescription())
//                        && rzpErrorBO.getError().getDescription().contains("Contact number contains invalid country code")) {
//
//                    if(isIndividual && retry) {
//                        requestBO.setContact(requestBO.getNotes().getMobile());
//
//                        responseBO = custmerCreation(referenceId, requestBO, product, paymentMode,false, true, errorBoList);
//                    }
//                    else {
//                        errorBoList.add(new ErrorBO(referenceId, product, "CustomerId", ErrorsConstants.CUSTOMERID_INVALID_CONTACT, errorConfig.customerIdInvalidContact()));
//                    }
//                }
            }

        } catch (Exception e) {
            log.error("subscription creation | Request: {} | Exception : {}", gson.toJson(subscriptionCreate), e);
        }

        return responseBO;
    }

    public GlobalResponseBO subscriptionAuth(SubscriptionBO subscriptionBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + " subscription Auth Service");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "subscriptionAuth");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        SubscriptionAuthResponse subscriptionAuthResponse=new SubscriptionAuthResponse();
        SubscriptionAuthRequest subscriptionAuthRequest=new SubscriptionAuthRequest();
        try {

            isValidSubscription(subscriptionBO, errorBoList);


            if(errorBoList.isEmpty()) {
                if(!StringUtil.isNullOrEmpty(subscriptionBO.getSubscriptionId())){
                    subscriptionAuthRequest= subscriptionAuthRequest(subscriptionBO,userId,paymentConfig);
                    subscriptionAuthResponse=  phonePeSubscriptionAuth(subscriptionAuthRequest,errorBoList);
                }
            }


        } catch (Exception e) {
            log.error(logPrefix + "Create Subscription API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, subscriptionAuthResponse, globalResponseBo);

        log.debug(logPrefix + "Subscription Auth API Status : " + stat);
        return  globalResponseBo;

    }
    public void isValidSubscription(SubscriptionBO subscriptionBO, List<ErrorBO> errorBoList) {

        try {

            if(StringUtil.isNullOrEmpty(subscriptionBO.getSubscriptionId()))
                errorBoList.add(new ErrorBO("subscription Id", "Invalid subscription id"));

        } catch (Exception e) {
            log.error("Exception in subscription auth - validate : ", e);
        }
    }
    public SubscriptionAuthResponse phonePeSubscriptionAuth(SubscriptionAuthRequest subscriptionAuthRequest, List<ErrorBO> errorBoList) {

        SubscriptionAuthResponse responseBO = new SubscriptionAuthResponse();
        String SubscriptionAuthPath= paymentConfig.getSlash() + paymentConfig.getPhonePeVersion() + paymentConfig.getSlash() + paymentConfig.getPhonePeApiSubscriptionAuthUrl();
        String subscriptionAuthApiUrl = paymentConfig.getPhonePeBaseUrl() +SubscriptionAuthPath;
        log.info("subscription Auth ApiUrl : " + subscriptionAuthApiUrl);
        try {

            HttpHeaders headers = new HttpHeaders();
            String payload= gson.toJson(subscriptionAuthRequest);
            log.info("payload : " + payload);
            String base64= APIGeneralUtil.getBase64(payload);
            String checksum= APIGeneralUtil.getCheckSum(base64,SubscriptionAuthPath);
            log.info("base64 : " + base64);
            subscriptionAuthRequest.setRequest(base64);
            headers.set("X-Verify",checksum);
            headers.set("accept","application/json");
            headers.set("Content-Type","application/json");
            headers.set("X-CALLBACK-URL",paymentConfig.getPhonePeApiRedirectUrl());
            log.info ("checksum : " + checksum);
            HttpEntity<SubscriptionAuthRequest> request = new HttpEntity<>(subscriptionAuthRequest, headers);


            log.info("subscription Auth req | Request: {}", gson.toJson(subscriptionAuthRequest));

            ResponseEntity  response = restTemplate.postForEntity(subscriptionAuthApiUrl, request, String.class);

            log.info("subscription Auth | Request: {} | Response: {} ", gson.toJson(response), response);

            if(response.getStatusCode().value() == 200 && response.getBody() != null) {

                responseBO = gson.fromJson(response.getBody().toString(), SubscriptionAuthResponse.class);
            }

            else if (response.getStatusCode().value() == 400 && response.getBody() != null) {

                log.debug("PhonePe Subscription Auth Error Response : " + response.getBody().toString());

//                RzpErrorBO rzpErrorBO = gson.fromJson(response.getBody().toString(), RzpErrorBO.class);
//
//                if(rzpErrorBO!=null && rzpErrorBO.getError()!=null && StringUtils.isNotEmpty(rzpErrorBO.getError().getDescription())
//                        && rzpErrorBO.getError().getDescription().contains("Contact number contains invalid country code")) {
//
//                    if(isIndividual && retry) {
//                        requestBO.setContact(requestBO.getNotes().getMobile());
//
//                        responseBO = custmerCreation(referenceId, requestBO, product, paymentMode,false, true, errorBoList);
//                    }
//                    else {
//                        errorBoList.add(new ErrorBO(referenceId, product, "CustomerId", ErrorsConstants.CUSTOMERID_INVALID_CONTACT, errorConfig.customerIdInvalidContact()));
//                    }
//                }
            }

        } catch (Exception e) {
            log.error("subscription Auth request | Request: {} | Exception : {}", gson.toJson(subscriptionAuthRequest), e);
        }

        return responseBO;
    }

}

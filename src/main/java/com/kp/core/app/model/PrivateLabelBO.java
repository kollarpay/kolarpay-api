package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PrivateLabelBO {

    private int PLId;
    private String plName;
    private String mailFrom;
    private String mailpassword;
    private String mailCc;
    private String mailBcc;
    private String mailSupport;
    private String mailRequestId;
    private String smsUserId;
    private String smsPassword;
    private String mailHost;
    private String mailPort;
    private String smsSender;
    private String mailRegards;
    private String mailContact;
    private String mailContactPhone;
    private String mailSign;
    private String webTitle;

    private String labelShortName;
    private String URLShortName;
    private String URL;

    private String mailTransaction;
    private String mailTransactionPaswd;
    private String plType = "P";
    private String logo;
    private String newLogo;
}

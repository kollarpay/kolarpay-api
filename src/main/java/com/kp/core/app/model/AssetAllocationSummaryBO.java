package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssetAllocationSummaryBO {

    private String currentPortfolioValue;
    private String proposedPortfolioValue;
    private String currentStpAllocation;
    private String proposedStpAllocation;
    private String comment;
    private List<Assets> assets;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Assets {

        @SerializedName("class")
        @JsonProperty("class")
        private String className;
        private double currentAllocation;
        private double proposedAllocation;
        private String netImpact;
    }

}

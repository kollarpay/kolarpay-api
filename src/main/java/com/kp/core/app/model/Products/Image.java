package com.kp.core.app.model.Products;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)


public class Image {

    @SerializedName("imageid")
    @Expose
    public Long imageid;
    @SerializedName("product_id")
    @Expose
    public Long productId;
    @SerializedName("position")
    @Expose
    public Integer position;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("alt")
    @Expose
    public String alt;
    @SerializedName("width")
    @Expose
    public Integer width;
    @SerializedName("height")
    @Expose
    public Integer height;
    @SerializedName("src")
    @Expose
    public String src;
    @SerializedName("variant_ids")
    @Expose
    public List<Integer> variantIds = null;


}



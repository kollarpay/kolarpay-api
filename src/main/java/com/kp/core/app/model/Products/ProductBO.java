package com.kp.core.app.model.Products;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBO {

    @SerializedName("id")
    @Expose
    public Long id;

    @SerializedName("productId")
    @Expose
    public Long productId;

    @SerializedName("collection_id")
    @Expose
    public Long collection_id;

    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("body_html")
    @Expose
    public String bodyHtml;
    @SerializedName("vendor")
    @Expose
    public String vendor;
    @SerializedName("product_type")
    @Expose
    public String productType;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("handle")
    @Expose
    public String handle;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("published_at")
    @Expose
    public String publishedAt;
    @SerializedName("template_suffix")
    @Expose
    public String templateSuffix;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("published_scope")
    @Expose
    public String publishedScope;
    @SerializedName("tags")
    @Expose
    public String tags;
    @SerializedName("shape")
    @Expose
    public String shape;

    @SerializedName("position")
    @Expose
    public Integer position;


    @SerializedName("stock_units")
    @Expose
    public Integer stockUnits;

    @SerializedName("reward_points_credit")
    @Expose
    public Integer rewardPointsCredit;

    @SerializedName("category_id")
    @Expose
    public Long categoryId;
    private ProductImage productImage;
    private ProductVariant productVariant;
    private List<ProductVariant> variants;
    private List<ProductImage> productImages;
    private ProductDiscount productDiscount;
    private ProductCategoryDiscount productCategoryDiscount;


    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ProductVariant {

        public Long id;
        public Long productId;
        public String color;
        public String title;
        public double price=0.0d;
        public double sale_price=0.0d;
        public String sku;
        public Integer position=0;
        public String inventoryPolicy;
        public Double compareAtPrice=0.0d;
        public String fulfillmentService;
        public String inventoryManagement;
        public String option1;
        public String option2;
        public String option3;
        public String createdAt;
        public String updatedAt;
        public Boolean taxable;
        public String barcode;
        public Double grams;
        public Integer imageId;
        public Double weight;
        public String weightUnit;
        public Integer inventoryItemId=0;
        public Integer inventoryQuantity=0;
        public Integer oldInventoryQuantity=0;
        public Boolean requiresShipping;
        public String expireAt;
        public String effectFrom;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ProductDiscount {

        public Long id;
        public Long productId;
        public Long discountValue=0L;
        public String discountUnit;
        public String validFrom;
        public String validUntil;
        public String couponCode;
        public int minimumOrder=0;
        public int maximumDiscountAmount=0;
        public String isredeemAllowed;
        public String createdAt;
    }
    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ProductCategoryDiscount {

        public Long id;
        public Long productId;
        public Long discountValue;
        public Long discountUnit;
        public String validFrom;
        public String validUntil;
        public String couponCode;
        public int minimumOrder;
        public int maximumDiscountAmount;
        public String isredeemAllowed;
        public String createdAt;
    }
    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ProductImage {

        public Long imageid;
        public Long productId;
        public Integer position;
        public String createdAt;
        public String updatedAt;
        public String alt;
        public double width=0.0d;
        public double height=0.0d;
        public String src;
    }
}


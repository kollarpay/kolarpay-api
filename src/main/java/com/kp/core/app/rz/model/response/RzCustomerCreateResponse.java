package com.kp.core.app.rz.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.kp.core.app.rz.model.request.RzCustomerCreateRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RzCustomerCreateResponse {
    private String id;
    private String email;
    private String contact;
    private String fail_existing;
    private String gstin;
    private String created_at;
    private RzCustomerCreateResponseNote notes;
    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class RzCustomerCreateResponseNote {
        private String user_mobile;
        private String pid;
    }

}

package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/bros/config/properties/sms.properties")
@Config.HotReload(type = Config.HotReloadType.ASYNC, unit = TimeUnit.MINUTES, value = 10)

public interface SmsConfig extends Reloadable {

    @Key("slash")
    @DefaultValue("/")
    String getSlash();
    @Key("KAL.SMS.EXTERNAL.BASEURL")
    @DefaultValue("https://api.kaleyra.io")
    String getBaseURL();

    @Key("KAL.SMS.EXTERNAL.VERSION")
    @DefaultValue("v1")
    String getVersion();

    @Key("KAL.SMS.EXTERNAL.SID")
    @DefaultValue("HXIN1731471518IN")
    String getSID();

    @Key("KAL.SMS.EXTERNAL.MESSAGES")
    @DefaultValue("messages")
    String getStrMessage();

    @Key("KAL.SMS.EXTERNAL.APIKEY")
    @DefaultValue("Afed711969f41b799d5bf46ed2245e146")
    String getSmsApi();

    @Key("KAL.SMS.EXTERNAL.CONTENTTYPE")
    @DefaultValue("application/x-www-form-urlencoded")
    String getContentType();

    @Key("KAL.SMS.EXTERNAL.OTP")
    @DefaultValue("OTP")
    String getMessageType();

    @Key("KAL.SMS.EXTERNAL.SENDER")
    @DefaultValue("BROSPY")
    String getSender();

    @Key("KAL.SMS.EXTERNAL.template")
    @DefaultValue("1207166247415332175")
    String getTemplate();

    @Key("KAL.SMS.EXTERNAL.callback")
    @DefaultValue("https://webhook.site/89b43b12-b889-4f66-8ebf-3379b4b3345c")
    String getCallBack();

    @Key("KAL.SMS.EXTERNAL.otpmessage")
    @DefaultValue("Your OTP verification code is <!code>. Please enter the verification code and submit your details to proceed further. Brospay Team.")
    String getMessage();

    // MSG 91 start https://control.msg91.com/api/v5/otp?mobile=&template_id=

    @Key("MSG91.SMS.EXTERNAL.BASEURL")
    @DefaultValue("https://control.msg91.com")
    String getMsgBaseURL();

    @Key("MSG91.SMS.EXTERNAL.API")
    @DefaultValue("api")
    String getMsgApi();

    @Key("MSG91.SMS.EXTERNAL.METHOD")
    @DefaultValue("otp")
    String getMsgMethod();

    @Key("MSG91.SMS.EXTERNAL.VERSION")
    @DefaultValue("v5")
    String getMsgVersion();

    @Key("MSG91.SMS.EXTERNAL.TEMPLATEID")
    @DefaultValue("648150b1d6fc055a8961e5a2")
    String getMsgTemplate();

    @Key("MSG91.SMS.EXTERNAL.CONTENTTYPE")
    @DefaultValue("application/json")
    String getMsgContentType();

    @Key("MSG91.SMS.EXTERNAL.AUTHKEY")
    @DefaultValue("397065AKRRKsJ92647789f1P1")
    String getMsgAuthKey();

    @Key("MSG91.SMS.EXTERNAL.OTPLENGTH")
    @DefaultValue("6")
    String getMSgOtpLength();
    // MSG 91 end
}

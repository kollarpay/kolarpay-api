package com.kp.core.app.service;

import com.kp.core.app.dao.CartDAO;
import com.kp.core.app.dao.GeneralInfoDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.CartBO;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.GeneralUtil;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class CartService {

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private CartDAO cartDAO;
    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    public GlobalResponseBO createCart(CartBO cartBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create Cart Service");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createCart");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

           // isValidCart(cartBO, errorBoList);


            if(errorBoList.size()==0 ) {
                cartDAO.createCart(cartBO,userId);
                if(!StringUtil.isNullOrEmpty(cartBO.getCartID())){
                 cartDAO.cartItesmInsert(cartBO);
                }else{

                }
            }


        } catch (Exception e) {
            log.error(logPrefix + "Create cart API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, cartBO, globalResponseBo);

        log.debug(logPrefix + "Create Collection API Status : " + stat);
        return  globalResponseBo;

    }
    public void isValidCart(CartBO cartBO, List<ErrorBO> errorBoList) {

        try {

            if(StringUtil.isNullOrEmpty(cartBO.getCartID()))
                errorBoList.add(new ErrorBO("Cart Id", "Invalid cart id"));


        } catch (Exception e) {
            log.error("Exception in cart creation - validate : ", e);
        }
    }
}

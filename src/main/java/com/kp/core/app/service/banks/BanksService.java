package com.kp.core.app.service.banks;

import com.kp.core.app.config.ErrorConfig;
import com.kp.core.app.dao.CustomerBankDAO;
import com.kp.core.app.dao.GeneralInfoDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.model.request.BankVerifyRequestBO;
import com.kp.core.app.model.request.PaymentRequestBO;
import com.kp.core.app.service.Payment.PaymentServices;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.ErrorsConstants;
import com.kp.core.app.utils.GeneralUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.*;
import org.bson.Document;
import com.mongodb.client.MongoDatabase;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;

import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;



import java.util.*;
import java.util.regex.Pattern;


@Service
@Slf4j
public class BanksService {

    @Autowired
    private GeneralUtil generalUtil;
    @Autowired
    private GeneralInfoDAO generalInfoDAO;
    @Autowired
    private CustomerBankDAO customerBankDAO;
    @Autowired
    MongoConverter mongoConverter;


    private final
    PaymentServices paymentServices;
    private final ErrorConfig errorConfig = ConfigFactory.create(ErrorConfig.class);



    @Autowired
    public BanksService(   PaymentServices paymentServices) {
        this.paymentServices = paymentServices;
    }


    public GlobalResponseBO verifyIfsc(CustomerBankInfo customerBankInfo, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create and verify bank Service");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createBank");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            isValidIFSC(customerBankInfo, errorBoList);


            if(errorBoList.isEmpty()) {
               if(customerBankInfo!=null && !StringUtil.isNullOrEmpty(customerBankInfo.getIfscCode())){
                   customerBankInfo= generalInfoDAO.verifyIFSC(customerBankInfo.getIfscCode(),customerBankInfo);
               }
            }


        } catch (Exception e) {
            log.error("{}Verify IfscCode API Exception : ", logPrefix, e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, customerBankInfo, globalResponseBo);

        log.debug(logPrefix + "Verify Ifsc code API Status : " + stat);
        return  globalResponseBo;

    }
    public void isValidIFSC(CustomerBankInfo customerBankInfo, List<ErrorBO> errorBoList) {

        try {

            if(StringUtil.isNullOrEmpty(customerBankInfo.getIfscCode()))
                errorBoList.add(new ErrorBO("IFSC CODE", "Invalid IFSC CODE"));
            if(!StringUtil.isNullOrEmpty(customerBankInfo.getIfscCode()) && customerBankInfo.getIfscCode().length()<11)
                errorBoList.add(new ErrorBO("IFSC CODE", "Invalid IFSC CODE"));

        } catch (Exception e) {
            log.error("Exception in Verify IFSC CODE - validate : ", e);
        }
    }
    public void isValidVPA(CustomerBankInfo customerBankInfo, List<ErrorBO> errorBoList) {

        try {

            if(StringUtil.isNullOrEmpty(customerBankInfo.getAccountName()))
                errorBoList.add(new ErrorBO("account name", "Invalid name"));
            if(StringUtil.isNullOrEmpty(customerBankInfo.getVpa()))
                errorBoList.add(new ErrorBO("VPA", "VPA is Empty"));
            if(!StringUtil.isNullOrEmpty(customerBankInfo.getVpa()) && !(customerBankInfo.getVpa().matches("^[\\w\\.\\-_]{3,}@[a-zA-Z]{3,}")))
                errorBoList.add(new ErrorBO("VPA", "Invalid VPA"));

        } catch (Exception e) {
            log.error("Exception in Verify IFSC CODE - validate : ", e);
        }
    }

    public GlobalResponseBO verifyBank(CustomerBankInfo customerBankInfo, int userId) {
        List<ErrorBO> errorBoList = new ArrayList<>();
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "verifyBank");
        BankVerifyRequestBO bankVerifyRequestBO=new BankVerifyRequestBO();
        String stat = "";
        String bvRefId="";

        try {
            HttpHeaders headers = new HttpHeaders();
            //headers= GeneralInfoService.LoadPayOutHeader(headers);
            PaymentAuthTokenBO paymentAuthTokenBO=new PaymentAuthTokenBO();
            paymentAuthTokenBO=paymentServices.GeneratePaymentAuthToken(paymentAuthTokenBO,errorBoList); //Get Authtoken for bank verification suite
            if(customerBankInfo!=null && !StringUtil.isNullOrEmpty(customerBankInfo.getIfscCode())){
                 generalInfoDAO.verifyIFSC(customerBankInfo.getIfscCode(),customerBankInfo);
            }            if(paymentAuthTokenBO.status.equalsIgnoreCase(APIConstants.SUCCESS_STRING)) {
                bankVerifyRequestBO=paymentServices.getBankVerification(paymentAuthTokenBO,customerBankInfo,headers,errorBoList); // GET bank verification ref id
            }
            if(bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.SUCCESS_STRING) && (Integer.parseInt(bankVerifyRequestBO.getSubCode())==APIConstants.SUCCESS_INT)){
                bvRefId=bankVerifyRequestBO.getData().getBvRefId();
            }
            if(!StringUtil.isNullOrEmpty(bvRefId)){
                log.info("bvRefId---"+bvRefId);
                bankVerifyRequestBO=paymentServices.getBankVerifyStatus(bankVerifyRequestBO,headers,errorBoList);
                customerBankInfo=new PaymentRequestBO().getCustomerBankInfo(customerBankInfo,bankVerifyRequestBO);
                customerBankDAO.createCustomerBank(customerBankInfo,userId);
            }


        } catch (Exception e) {
            log.error("Validate Plan - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
            stat = APIConstants.SERVER_ERROR_STRING;

        }
        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, bankVerifyRequestBO, globalResponseBo);

        return globalResponseBo;
    }

    public GlobalResponseBO verifyVPA(CustomerBankInfo customerBankInfo, int userId) {
        List<ErrorBO> errorBoList = new ArrayList<>();
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "verifyVPA");
        BankVerifyRequestBO bankVerifyRequestBO=new BankVerifyRequestBO();
        String stat = "";

        try {
            PaymentAuthTokenBO paymentAuthTokenBO=new PaymentAuthTokenBO();
            paymentAuthTokenBO=paymentServices.GeneratePaymentAuthToken(paymentAuthTokenBO,errorBoList); //Get Authtoken for bank verification suite
            isValidVPA(customerBankInfo,errorBoList);
            if(errorBoList.isEmpty()) {
                if(paymentAuthTokenBO.status.equalsIgnoreCase(APIConstants.SUCCESS_STRING)) {
                    customerBankDAO.createCustomerBank(customerBankInfo, userId);
                }
            }
//            if(customerBankInfo!=null && !StringUtil.isNullOrEmpty(customerBankInfo.getIfscCode())){
//                generalInfoDAO.verifyIFSC(customerBankInfo.getIfscCode(),customerBankInfo);
//            }            if(paymentAuthTokenBO.status.equalsIgnoreCase(APIConstants.SUCCESS_STRING)) {
//                bankVerifyRequestBO=paymentServices.getBankVerification(paymentAuthTokenBO,customerBankInfo,headers,errorBoList); // GET bank verification ref id
//            }
//            if(bankVerifyRequestBO.getStatus().equalsIgnoreCase(APIConstants.SUCCESS_STRING) && (Integer.parseInt(bankVerifyRequestBO.getSubCode())==APIConstants.SUCCESS_INT)){
//                bvRefId=bankVerifyRequestBO.getData().getBvRefId();
//            }
//            if(!StringUtil.isNullOrEmpty(bvRefId)){
//                log.info("bvRefId---"+bvRefId);
//                bankVerifyRequestBO=paymentServices.getBankVerifyStatus(bankVerifyRequestBO,headers,errorBoList);
//                customerBankInfo=new PaymentRequestBO().getCustomerBankInfo(customerBankInfo,bankVerifyRequestBO);
//                customerBankDAO.createCustomerBank(customerBankInfo,userId);
//            }


        } catch (Exception e) {
            log.error("Validate Plan - Exception : {}", e, e);
            errorBoList.add(new ErrorBO("", "", ErrorsConstants.SERVER_ERROR, errorConfig.serverError()));
            stat = APIConstants.SERVER_ERROR_STRING;

        }
        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, bankVerifyRequestBO, globalResponseBo);

        return globalResponseBo;
    }
    public GlobalResponseBO deactivateBank(CustomerBankInfo customerBankInfo, int userId) {

        String logPrefix = "#UserId : " + userId ;
        log.debug(logPrefix + "Deactivate customerBank");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "DeleteCustomerBankResponse");
        List<ErrorBO> errorBoList = new ArrayList<>();
        HashMap<String, Object> result = new HashMap<>();
        String stat = "";
        String bankId= "";
        String customerId= "";

        try {
            if(customerBankInfo!=null && !StringUtil.isNullOrEmpty(customerBankInfo.getId())){
                bankId=customerBankInfo.getId();
                customerId=customerBankInfo.getCustomerId();
            }
            if(StringUtils.isEmpty(customerId))
                errorBoList.add(new ErrorBO("customerId", "Invalid input"));
            if(StringUtils.isEmpty(bankId) || Integer.parseInt(bankId)==0)
                errorBoList.add(new ErrorBO("bankId", "Invalid input"));
            if(!customerBankDAO.isBankAvailable(customerBankInfo))
                errorBoList.add(new ErrorBO("bankId", "bankId does not exist"));

            else {
                int status = customerBankDAO.deactivateBank(customerBankInfo.getCustomerId(),Integer.parseInt(bankId), userId);
                if(status==0)
                    errorBoList.add(new ErrorBO("failed", "bankId delete failed"));
            }

        } catch (Exception e) {
            log.error(logPrefix + "Delete bankId API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;
        else {
            result.put("bankId",bankId);
            result.put("updatedOn", java.time.LocalDateTime.now().toString());
            result.put("result", true);
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, result, globalResponseBo);

        log.debug(logPrefix + "Fetch bankId API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO updatePrimaryAccount(CustomerBankInfo customerBankInfo, int userId) {

        String logPrefix = "#UserId : " + userId ;
        log.debug(logPrefix + "primary customerBank update");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "updatePrimaryCustomerBank");
        List<ErrorBO> errorBoList = new ArrayList<>();
        HashMap<String, Object> result = new HashMap<>();
        String stat = "";
        String bankId= "";
        String customerId= "";

        try {
            if(customerBankInfo!=null && !StringUtil.isNullOrEmpty(customerBankInfo.getId())){
                bankId=customerBankInfo.getId();
                customerId=customerBankInfo.getCustomerId();
            }
            if(StringUtils.isEmpty(customerId))
                errorBoList.add(new ErrorBO("customerId", "Invalid input"));
            if(!customerBankDAO.isPrimaryBankAvailable(customerBankInfo.getCustomerId()))
                errorBoList.add(new ErrorBO("primary bank", "primary bank does not exist"));

            else {
                int status = customerBankDAO.primaryBankUpdate(customerBankInfo.getCustomerId(),Integer.parseInt(bankId), userId);
                if(status==0)
                    errorBoList.add(new ErrorBO("failed", "Primary bank update failed"));
            }

        } catch (Exception e) {
            log.error(logPrefix + "Primary bank update API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;
        else {
            result.put("bankId", String.valueOf(bankId));
            result.put("updatedOn", java.time.LocalDateTime.now().toString());
            result.put("result", true);
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, result, globalResponseBo);

        log.debug(logPrefix + "Primary bank update API Status : " + stat);
        return  globalResponseBo;

    }
//    public List<Ifsccodes> partialTextSearch(String searchPhrase) {
//        TextCriteria criteria = TextCriteria
//                .forDefaultLanguage()
//                .matchingPhrase(searchPhrase);
//
//        Query query = (Query) TextQuery.queryText(criteria).sortByScore();
//
//        List<Ifsccodes> ifsccodesList =mongoTemplate.find(query, Ifsccodes.class);
//
//        return ifsccodesList;
//    }
    public GlobalResponseBO textSearch(String searchPhrase) {
        String logPrefix = "#ifsccodeSearch : " + searchPhrase;
        log.debug("{}searchPhrase", logPrefix);
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "IfscCodeSearch");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<IfscCode> ifsccodesList = new ArrayList<>();
        String stat = "";
        MongoDatabase database =null;
        MongoCollection<Document> collection = null;
        MongoCursor<Document> cursor=null;
        try {
            if(searchPhrase.length() > 3) {
                database=  generalUtil.getMongoDatabase();
//                ClassModel<IfscCode> ifscCodeModel = ClassModel.builder(IfscCode.class).enableDiscriminator(true).build();
//                CodecProvider pojoCodecProvider = PojoCodecProvider.builder().register(ifscCodeModel).build();
//                CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));
              String search="{ ifsccode: { $regex: /"+searchPhrase+"/i } }";
               if(database!=null){
                    collection=    database.getCollection("ifsccodes");
                    collection.createIndex(Indexes.text("ifsccode"));

                   DBObject ref = new BasicDBObject();
                   ref.put("ifsccode", Pattern.compile(".*"+searchPhrase+".*" , Pattern.CASE_INSENSITIVE));
                   ref.put("address", Pattern.compile(".*"+searchPhrase+".*" , Pattern.CASE_INSENSITIVE));

                    cursor= collection
                           .find((Bson) ref)
                           // .find(Filters.text(searchPhrase,options))
                           .projection(Projections.fields(Projections.excludeId()))
                            .limit(100).cursor();
                   while (cursor.hasNext()) {
                       Document document = cursor.next();
                      String bank= document.getString("bank");
                      String ifsccode= document.getString("ifsccode");
                      String micrcode= document.getString("micrcode");
                      String branchname= document.getString("branchname");
                      String address= document.getString("address");
                      String city= document.getString("city");
                      String state= document.getString("state");
                       ifsccodesList.add(new IfscCode(bank,ifsccode,micrcode,branchname,address,city,state));
                   }

                }
            }
            log.info("ifsc codes List size {}",ifsccodesList.size());
            if(ifsccodesList.isEmpty()) errorBoList.add(new ErrorBO("search text", "no data found"));
        } catch (Exception e) {
            log.error("{}Ifsc code search API Exception : ", logPrefix, e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }
        finally {
            assert cursor != null;
            cursor.close();
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, ifsccodesList, globalResponseBo);

        return globalResponseBo;
    }

}

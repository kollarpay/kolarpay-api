package com.kp.core.app.phonpe.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionAuthRequest {
    public String merchantId;
    public String merchantUserId;
    public String subscriptionId;
    public String authRequestId;
    public long amount;
    public String request;
    public PaymentInstrument paymentInstrument;
    public DeviceContext deviceContext;
}

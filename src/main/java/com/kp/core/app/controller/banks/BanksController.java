package com.kp.core.app.controller.banks;
        import com.google.gson.Gson;
        import com.kp.core.app.enums.ResponseDataObjectType;
        import com.kp.core.app.model.CustomerBankInfo;
        import com.kp.core.app.model.GlobalResponseBO;
        import com.kp.core.app.model.Ifsccodes;
        import com.kp.core.app.service.banks.BanksService;
        import com.kp.core.app.utils.APIGeneralUtil;
        import lombok.extern.slf4j.Slf4j;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.MediaType;
        import org.springframework.http.ResponseEntity;
        import org.springframework.util.MultiValueMap;
        import org.springframework.web.bind.annotation.*;

        import java.util.ArrayList;
        import java.util.List;

@RestController
@Slf4j
@RequestMapping("/banks")
public class BanksController {

    @Autowired
    BanksService banksService;
    @Autowired
    Gson gson;
    @PostMapping(path = "/ifsc", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> validateIfsc(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#verify ifsc POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "validateIfsc");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CustomerBankInfo customerBankInfo = gson.fromJson(payload, CustomerBankInfo.class);
                globalResponseBO = banksService.verifyIfsc(customerBankInfo, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#bannk verify POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @PostMapping(path = "/verify", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> verifyBank(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#verify bank POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "verify");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CustomerBankInfo customerBankInfo = gson.fromJson(payload, CustomerBankInfo.class);
                globalResponseBO = banksService.verifyBank(customerBankInfo, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#bannk verify POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @PostMapping(path = "/vpaverify", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> verifyVPA(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#verify VPA POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "verifyVPA");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CustomerBankInfo customerBankInfo = gson.fromJson(payload, CustomerBankInfo.class);
                globalResponseBO = banksService.verifyVPA(customerBankInfo, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#bannk verify VPI POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @DeleteMapping(path = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> deleteBank(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#DELETE BANK API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "DeleteBank");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CustomerBankInfo customerBankInfo = gson.fromJson(payload, CustomerBankInfo.class);
                globalResponseBO = banksService.deactivateBank(customerBankInfo, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#bank delete  API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @PostMapping(path = "/update-primary", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> updatePrimary(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("# PRIMARY BANK UPDATE API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "updatePrimaryAccount");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CustomerBankInfo customerBankInfo = gson.fromJson(payload, CustomerBankInfo.class);
                globalResponseBO = banksService.updatePrimaryAccount(customerBankInfo, userId);
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#primary bank update  API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @GetMapping(path = "/ifsccode/{name}")
    public ResponseEntity<GlobalResponseBO> searchIfscode( @PathVariable String name) throws Exception {

        log.debug("#search ifsc codes GET API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "searchIfsc");
        try {
            globalResponseBO= banksService.textSearch(name);
        } catch (Exception e) {
        }
        log.debug("#bank ifsc search get API Response : {}", globalResponseBO);
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

package com.kp.core.app.model.request;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kp.core.app.model.IdDesc;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionRequestBO {
    @Expose
    private String subscriptionId;
    @Expose
    private String planId;
    @Expose
    private String customerName;
    @Expose
    private String customerEmail;
    @Expose
    private String customerPhone;
    @Expose
    private String firstChargeDate;
    @Expose
    private double authAmount;
    @Expose
    private String expiresOn;
    @Expose
    private String returnUrl;
    @Expose
    private String subscriptionNote;
    @Expose
    private String notificationChannels;

}

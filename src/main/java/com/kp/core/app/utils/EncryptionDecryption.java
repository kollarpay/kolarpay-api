package com.kp.core.app.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Component
public class EncryptionDecryption {

    public String Encryption(String encryptionString) {

        StringBuffer hexString = new StringBuffer();

        byte[] defaultBytes = encryptionString.getBytes();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(defaultBytes);
            byte messageDigest[] = algorithm.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            }
        } catch (NoSuchAlgorithmException nsae) {
        }

        return hexString.toString();
    }

    /**
     * Encrypt the given input text by Apache digest with specified algorithm
     * @param algorithm MD5 / SHA-256 / SHA-512
     * @param input Text to encrypt
     * @return
     */
    public String encrypt(String algorithm, String input) {

        log.info("Input:" + algorithm + "|" + input);

        String hashtext = null;

        // Input validation
        if (StringUtils.isBlank(algorithm) || StringUtils.isBlank(input)) {
            log.info("Invalid input:" + algorithm + "|" + input);
            return hashtext;
        }

        // With Apache commons
        try {

            // SHA 512 Encryption
            if (algorithm.equalsIgnoreCase(BPConstants.ENCRYPTION_ALGORITHM))
                hashtext = org.apache.commons.codec.digest.DigestUtils.sha512Hex(input);
            /*else if // SHA 256
            hashtext = org.apache.commons.codec.digest.DigestUtils.sha256Hex(input);
            */
        }
        catch (Exception e) {
            log.error("Exception: " + e);
        }

        log.info(hashtext);
        return hashtext;
    }
}

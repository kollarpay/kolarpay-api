package com.kp.core.app.phonpe.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardPaymentInstrumentResponse {
    private String type;
    private String cardType;
    private String pgTransactionId;
    private String bankTransactionId;
    private String pgAuthorizationCode;
    private String arn;
    private String bankId;
    private String brn;
}

package com.kp.core.app.service.Customer;



import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.kp.core.app.dao.*;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.model.request.CashFreeOrderBO;
import com.kp.core.app.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class CustomerService {

    @Autowired
    private GeneralUtil generalUtil;


    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private UserDao userDao;


    public GlobalResponseBO saveCustomer(CustomerRequest customerRequest, List<ErrorBO> errorBoList, boolean insert, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        String customerId="";
        String stat = "";
        CustomerInfoBO customerInfoBO=new CustomerInfoBO();

        try {

            if (StringUtils.isEmpty(customerRequest.getName()))
                errorBoList.add(new ErrorBO("Name", "Name shouldn't be empty"));

            if (StringUtils.isEmpty(customerRequest.getEmailId()))
                errorBoList.add(new ErrorBO("emailId", "EmailId shouldn't be empty"));
            if(!StringUtils.isEmpty(customerRequest.getEmailId()) && !customerRequest.getEmailId().matches(BPConstants.EMAIL_REGEX))
                errorBoList.add(new ErrorBO("emailId", "Enter valid EmailId"));

            if (StringUtils.isEmpty(customerRequest.getMobileNumber()))
                errorBoList.add(new ErrorBO("mobileNumber", "MobileNumber shouldn't be empty"));
            if (!StringUtils.isEmpty(customerRequest.getMobileNumber()) && customerRequest.getMobileNumber().trim().length()<10)
                errorBoList.add(new ErrorBO("mobileNumber", "Enter valid MobileNumber"));
            if (StringUtils.isEmpty(customerRequest.getDob()))
                errorBoList.add(new ErrorBO("dateOfBirth", "dateOfBirth shouldn't be empty"));
            if (StringUtils.isEmpty(customerRequest.getPassword()))
                errorBoList.add(new ErrorBO("Password", "Password shouldn't be empty"));
            if (!StringUtils.isEmpty(customerRequest.getPassword()) && customerRequest.getPassword().trim().length()<8)
                errorBoList.add(new ErrorBO("Password", "Enter valid Password"));
            if(insert && errorBoList.isEmpty()) {
                customerInfoBO= customerDao.getCustomerInfo(customerRequest.getEmailId(),customerRequest.getMobileNumber());
                if(customerInfoBO==null) {
                    customerId = customerDao.saveCustomer(customerRequest);
                    if (StringUtils.isEmpty(customerId))
                        errorBoList.add(new ErrorBO("failed", "customer creation failed"));
                }

            }

            if(!StringUtils.isEmpty(customerId)) {
                customerInfoBO=new CustomerInfoBO();
                customerInfoBO.setUserId(customerRequest.getUserId());
                customerInfoBO.setCustomerId(customerId);
                customerInfoBO.setName(customerRequest.getName());
                customerInfoBO.setEmail(customerRequest.getEmailId());
                customerInfoBO.setMobile(customerRequest.getMobileNumber());
                customerInfoBO.setType(customerRequest.getType());
            }

        } catch (Exception e) {
            log.error("Exception in customer basic detail table insertions : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, customerInfoBO, globalResponseBo);

        return globalResponseBo;

    }

    public GlobalResponseBO saveUserCustomer(int userId,CustomerRequest customerRequest, List<ErrorBO> errorBoList, boolean insert, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        String customerId="";
        String stat = "";
        CustomerInfoBO customerInfoBO=new CustomerInfoBO();
        UserRequest userRequest=new UserRequest();
        UserInfoBO userInfoBO=new UserInfoBO();
        int userUpdateCount=0;
        try {
            if(userId>0) userRequest.setUserId(userId);
            if (StringUtils.isEmpty(customerRequest.getName()))
                errorBoList.add(new ErrorBO("Name", "Name shouldn't be empty"));


            if (StringUtils.isEmpty(customerRequest.getEmailId()))
                errorBoList.add(new ErrorBO("emailId", "EmailId shouldn't be empty"));
            if(!StringUtils.isEmpty(customerRequest.getEmailId()) && !customerRequest.getEmailId().matches(BPConstants.EMAIL_REGEX))
                errorBoList.add(new ErrorBO("emailId", "Enter valid EmailId"));

//            if (StringUtils.isEmpty(customerRequest.getMobileNumber()))
//                errorBoList.add(new ErrorBO("mobileNumber", "MobileNumber shouldn't be empty"));
//            if (!StringUtils.isEmpty(customerRequest.getMobileNumber()) && customerRequest.getMobileNumber().trim().length()<10)
//                errorBoList.add(new ErrorBO("mobileNumber", "Enter valid MobileNumber"));
            if (StringUtils.isEmpty(customerRequest.getDob()))
                errorBoList.add(new ErrorBO("dateOfBirth", "dateOfBirth shouldn't be empty"));
            if (StringUtils.isEmpty(customerRequest.getPassword()))
                errorBoList.add(new ErrorBO("Password", "Password shouldn't be empty"));
            if (!StringUtils.isEmpty(customerRequest.getPassword()) && customerRequest.getPassword().trim().length()<8)
                errorBoList.add(new ErrorBO("Password", "Enter valid Password"));
            if(insert && errorBoList.isEmpty()) {
                userInfoBO= userDao.getUserInfoBO(4,userRequest);
                if(userInfoBO!=null && StringUtils.isEmpty(userInfoBO.getEmailId()) && StringUtils.isNotEmpty(userInfoBO.getMobile())) {
                    customerRequest.setMobileNumber(userInfoBO.getMobile());
                    userUpdateCount=  customerDao.updateUserinfo(customerRequest,userId);
                }
                customerInfoBO= customerDao.getUserCustomerInfo(customerRequest.getEmailId(),customerRequest.getMobileNumber());
                if(StringUtils.isEmpty(customerInfoBO.getName()) && userUpdateCount>0) {
                    customerId = customerDao.saveCustomer(customerRequest);
                    if (StringUtils.isEmpty(customerId))
                        errorBoList.add(new ErrorBO("failed", "customer creation failed"));
                }
            }

            if(!StringUtils.isEmpty(customerId)) {
                customerInfoBO.setUserId(customerRequest.getUserId());
                customerInfoBO.setCustomerId(customerId);
                customerInfoBO.setName(customerRequest.getName());
                customerInfoBO.setEmail(customerRequest.getEmailId());
                customerInfoBO.setMobile(customerRequest.getMobileNumber());
                customerInfoBO.setType(customerRequest.getType());
            }

        } catch (Exception e) {
            log.error("Exception in customer basic detail table insertions : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, customerInfoBO, globalResponseBo);

        return globalResponseBo;

    }

    public GlobalResponseBO getCustomers(int userId, List<ErrorBO> errorBoList, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        String stat = "";
        CustomerInfoBO customerInfoBO=new CustomerInfoBO();
        List<CustomerInfoBO> customerInfoList=new ArrayList<>();
        log.info("userId--"+userId);

        try {
            if(errorBoList.isEmpty()) {
                customerInfoList= customerDao.getCustomerList(userId);
                if(customerInfoList==null || customerInfoList.isEmpty()) {
                    errorBoList.add(new ErrorBO("no customer data found", "noData found"));
                }else if(!customerInfoList.isEmpty()) {
                    for(CustomerInfoBO customerInfo:customerInfoList) {
                      String customerID=  customerInfo.getCustomerId();
                        customerDao.getCustomerAddress(customerID,customerInfo);
                        customerDao.getCustomerBankInfo(customerID,customerInfo);
                    }
                }

            }

        } catch (Exception e) {
            log.error("Exception in customer basic detail table search : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, customerInfoList, globalResponseBo);

        return globalResponseBo;

    }
    public GlobalResponseBO getCustomerBanks(int userId, List<ErrorBO> errorBoList, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        String stat = "";
        CustomerInfoBO customerInfoBO=new CustomerInfoBO();
        List<CustomerBankInfo> customerBankList=new ArrayList<>();
        log.info("userId--"+userId);

        try {
            if(errorBoList.isEmpty()) {
                customerBankList= customerDao.getCustomerBankList(userId);
                if(customerBankList==null || customerBankList.isEmpty()) {
                    errorBoList.add(new ErrorBO("no customer bank data found", "noData found"));
                }

            }

        } catch (Exception e) {
            log.error("Exception in customer bank info table search : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, customerBankList, globalResponseBo);

        return globalResponseBo;

    }
    public GlobalResponseBO getCustomerBankMap(int userId, List<ErrorBO> errorBoList, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        String stat = "";
        List<CustomerBankInfo> customerBankList=new ArrayList<>();
        log.info("userId--"+userId);
        Map<String, List<CustomerBankInfo>> customerBankMap=new HashMap<>();

        try {
            if(errorBoList.isEmpty()) {
                customerBankList= customerDao.getCustomerBankList(userId);
                customerBankMap = customerBankList.stream()
                        .collect(Collectors.groupingBy(CustomerBankInfo::getReferenceId,
                                Collectors.mapping(Function.identity(), Collectors.toList())));



                if(customerBankMap==null || customerBankMap.isEmpty()) {
                    errorBoList.add(new ErrorBO("no customer bank data found", "noData found"));
                }

            }

        } catch (Exception e) {
            log.error("Exception in customer bank info table search : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, customerBankMap, globalResponseBo);

        return globalResponseBo;

    }
    public GlobalResponseBO getCustomer(int type,String searchStr, List<ErrorBO> errorBoList, String createdFrom) {
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, createdFrom);
        String stat = "";
        List<CustomerInfoBO> customerInfoList=new ArrayList<>();
        log.info("getCustomer type--"+type+"--SearchStr---"+searchStr);

        try {

            if (StringUtils.isEmpty(searchStr))
                errorBoList.add(new ErrorBO("invalid data", "data shouldn't be empty"));

            if (!StringUtils.isEmpty(searchStr) && searchStr.trim().length()<4)
                errorBoList.add(new ErrorBO("invalid data", "Data should not be less than 4 char"));



            if(errorBoList.isEmpty()) {
                customerInfoList= customerDao.getCustomerInfoList(type,searchStr);
                if(customerInfoList!=null && customerInfoList.isEmpty()) {
                    errorBoList.add(new ErrorBO("no customer date found", "noDate found"));
                }

            }




        } catch (Exception e) {
            log.error("Exception in customer basic detail table search : ", e);
        }
        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, customerInfoList, globalResponseBo);

        return globalResponseBo;

    }

    public GlobalResponseBO createOrder(CashFreeOrderBO cashFreeOrderBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "order create");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createOrder");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            isValidOrder(cashFreeOrderBO, errorBoList);

            List<MandateInfoBO> mandateList = new ArrayList<>();
            if(errorBoList.size()==0) {
               // insertMandateDetails(createOrderRequest, userId, mandateList, errorBoList, true, BPConstants.MANDATE_CREATE_SIP);
            }

//            if(mandateList.size()>0)
//                mandateInfoBO = mandateList.get(0);

        } catch (Exception e) {
            log.error(logPrefix + "Create mandate API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

       // globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, mandateInfoBO, globalResponseBo);

        log.debug(logPrefix + "Create mandate list API Status : " + stat);
        return  globalResponseBo;

    }

    public void isValidOrder(CashFreeOrderBO cashFreeOrderBO, List<ErrorBO> errorBoList) {

        try {

            if(StringUtils.isEmpty(cashFreeOrderBO.getCustomerId()) || Integer.parseInt(cashFreeOrderBO.getCustomerId())==0)
                errorBoList.add(new ErrorBO("customer Id", "Invalid input"));
            if(StringUtils.isEmpty(cashFreeOrderBO.getCustomerName()))
                errorBoList.add(new ErrorBO("customer Name", "Invalid input"));
            if(StringUtils.isEmpty(cashFreeOrderBO.getCustomerEmail()))
                errorBoList.add(new ErrorBO("customer Email", "Invalid input"));
            if(StringUtils.isEmpty(cashFreeOrderBO.getCustomerPhone()))
                errorBoList.add(new ErrorBO("customer Phone", "Invalid input"));
            if(cashFreeOrderBO.getOrderAmount()>0)
                errorBoList.add(new ErrorBO("order Amount", "Invalid input"));


        } catch (Exception e) {
            log.error("Exception in isValidOrder  - validate : ", e);
        }
    }



}

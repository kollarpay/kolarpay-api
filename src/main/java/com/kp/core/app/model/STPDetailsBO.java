package com.kp.core.app.model;

import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class STPDetailsBO {

    private String referenceId;
    private String schemeName;
    private String schemeCode;
    private String folio;
    private String goalId;
    private String goalName;
    private double units;
    private String unitsFormatted;
    private BigDecimal amount;
    private String amountFormatted;
    private String frequency;
    private String initialInvestmentFormatted;
    private BigDecimal initialInvestment;
    private String dividendOption;
    private String dayOrDate;
    private int completedInstallments;
    private int noOfInstallments;
    private String toSchemeCode;
    private String toSchemeName;
    private String category;
    private String amcCode;
    private BigDecimal totalStpAmount;
    private String totallStpAmountFormatted;
    private List<String> action;

}

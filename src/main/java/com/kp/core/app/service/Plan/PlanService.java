package com.kp.core.app.service.Plan;

        import com.kp.core.app.dao.*;
        import com.kp.core.app.enums.ResponseDataObjectType;
        import com.kp.core.app.model.ErrorBO;
        import com.kp.core.app.model.GlobalResponseBO;
        import com.kp.core.app.model.PlanBO;
        import com.kp.core.app.service.Payment.PaymentServices;
        import com.kp.core.app.utils.APIConstants;
        import com.kp.core.app.utils.GeneralUtil;
        import io.netty.util.internal.StringUtil;
        import lombok.extern.slf4j.Slf4j;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;

        import java.util.ArrayList;
        import java.util.List;

@Service
@Slf4j
public class PlanService {

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private PlanDAO planDAO;
    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    private final
    PaymentServices paymentServices;

    public PlanService(PaymentServices paymentServices) {
        this.paymentServices = paymentServices;
    }


    public GlobalResponseBO createPlan(PlanBO planBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create Plan Service");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createPlan");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            //isValidPlan(planBO, errorBoList);


            if(errorBoList.size()==0 ) {
                planDAO.createPlan(planBO,userId);
               if(planBO!=null && !StringUtil.isNullOrEmpty(planBO.getPlanId())){
                   paymentServices.createCFPlan(planBO,errorBoList);
               }
            }


        } catch (Exception e) {
            log.error(logPrefix + "Create Plan API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, planBO, globalResponseBo);

        log.debug(logPrefix + "Create Plan API Status : " + stat);
        return  globalResponseBo;

    }
    public void isValidPlan(PlanBO planBO, List<ErrorBO> errorBoList) {

        try {

            if(StringUtil.isNullOrEmpty(planBO.getPlanId()))
                errorBoList.add(new ErrorBO("Plan Id", "Invalid plan id"));


        } catch (Exception e) {
            log.error("Exception in cart creation - validate : ", e);
        }
    }
}

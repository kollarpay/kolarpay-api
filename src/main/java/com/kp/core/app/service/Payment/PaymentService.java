package com.kp.core.app.service.Payment;

import com.kp.core.app.dao.*;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.service.Investment.InvestorInfoService;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.DateUtil;
import com.kp.core.app.utils.BPConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.*;

@Service
@Slf4j
public class PaymentService {

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private InvestorInfoDAO investorInfoDAO;

    @Autowired
    private TransactionDAO transactionDAO;

    @Autowired
    private MandateInfoDAO mandateInfoDAO;

    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    @Autowired
    private HolidayCalendarDAO holidayCalendarDAO;

    @Autowired
    private SchemeSearchDAO schemeSearchDAO;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;

    @Autowired
    private InvestorInfoService investorInfoService;


    /**
     * fetch paymentInfo for cartId & insert into transaction tables
     * @param cartIdStr
     * @param ipAddress
     * @return
     */
    public GlobalResponseBO getMFPaymentInfo(String cartIdStr, String paymentIdStr, String ipAddress, boolean retry) {

        String logPrefix = "#CartId : " + cartIdStr + " | ";
        log.debug(logPrefix + "MF Payment Info");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "PaymentInfoBo");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<SchemeCartBO.Schemes> schemes = new ArrayList<>();
        String stat = "";
        DecimalFormat df = new DecimalFormat("###");
        PaymentInfoBO paymentInfoBO = new PaymentInfoBO();
        int holdingProfileId = 0;

        try {

            if((StringUtils.isEmpty(cartIdStr) || Integer.parseInt(cartIdStr)<=0) && (StringUtils.isEmpty(paymentIdStr) || Integer.parseInt(paymentIdStr)<=0))
                errorBoList.add(new ErrorBO("cartId/paymentId", "Invalid input"));

            else {

                if (StringUtils.isNotEmpty(cartIdStr) && Integer.parseInt(cartIdStr) > 0) {
                    int cartId = Integer.parseInt(cartIdStr);

                    // fetch scheme details in cart
                    SchemeCartBO schemeCartBO = investorInfoDAO.fetchInvestorCart(cartId, 0, true);
                    if (StringUtils.isEmpty(schemeCartBO.getId()) || schemeCartBO.getId().equals("0"))
                        errorBoList.add(new ErrorBO("cartId", "No cart available"));
                    else {
                        int userId = schemeCartBO.getUserId();
                        if (StringUtils.isNotEmpty(schemeCartBO.getHoldingProfileId()))
                            holdingProfileId = Integer.parseInt(schemeCartBO.getHoldingProfileId());

                        int paymentId = 0;
                        if(StringUtils.isNotEmpty(schemeCartBO.getPaymentId()))
                            paymentId = Integer.parseInt(schemeCartBO.getPaymentId());

                        if (paymentId != 0 && !retry) //todo: restrict if paymentid already present
                            paymentInfoBO = transactionDAO.getPaymentInfo(paymentId);

                        else {
                            //MF Flow
                            if (schemeCartBO.getMf() != null && holdingProfileId != 0) {

                                if (schemeCartBO.getMf().getOti() != null && schemeCartBO.getMf().getOti().getSchemes().size() > 0)
                                    schemes.addAll(schemeCartBO.getMf().getOti().getSchemes());

                                if (schemeCartBO.getMf().getSip() != null && schemeCartBO.getMf().getSip().getSchemes().size() > 0)
                                    schemes.addAll(schemeCartBO.getMf().getSip().getSchemes());

                                String mediumThru = null;
                                if (schemeCartBO.getAppInfo() != null)
                                    mediumThru = schemeCartBO.getAppInfo().getOs();

                                //Insert into transaction
                                if (schemes.size() > 0)
                                    paymentInfoBO = insertTransactionTables(schemes, cartId, holdingProfileId, paymentId,"PUR", "TC", mediumThru, ipAddress, userId, errorBoList, retry);

                            } else if (schemeCartBO.getEmandate() != null) { //ENACH Flow

                                ENachInfoBO eNachInfoBO;

                                CreateMandataBO mandataBO = schemeCartBO.getEmandate();

                                eNachInfoBO = investorInfoService.insertMandateDetails(mandataBO, userId, null, errorBoList, StringUtils.isEmpty(mandataBO.getConsumerCode()), BPConstants.MANDATE_CREATE_SIP);

                                paymentInfoBO.setEnach(eNachInfoBO);

                                mandateInfoDAO.updateConsumerCodeInCart(eNachInfoBO.getConsumerCode(), cartId, userId);
                            }
                        }

                        if(paymentInfoBO != null)
                        {
                            //Mandate list todo:revist
                            String nextworkingDay = mandateInfoDAO.getNextWorkingDayForTPSL();
                            List<MandateInfoBO> mandateInfoBOArrayList = mandateInfoDAO.getOpenMandListForPurchase(userId, holdingProfileId, 0, DateUtil.getDateGivenFormat(nextworkingDay, "dd"), "", "", true); //:todo investmentBankForNRI = 0 , bankAccountType is NRE then not allowed
                            paymentInfoBO.setMandates(mandateInfoBOArrayList);
                        }
                    }
                } else if (StringUtils.isNotEmpty(paymentIdStr) && Integer.parseInt(paymentIdStr) > 0) {

                    int paymentId = Integer.parseInt(paymentIdStr);
                    paymentInfoBO = transactionDAO.getPaymentInfo(paymentId);
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "tax-saver API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, paymentInfoBO, globalResponseBo);

        log.debug(logPrefix + "payment-info API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO updateMFPaymentStatus(int userId, PaymentStatusBO paymentStatusBO) {

        String logPrefix = "#PaymentId : " + paymentStatusBO.getPaymentId() + " | #PaymentStatus : " + paymentStatusBO.getPaidStatus() + " | " ;
        log.debug(logPrefix + "MF Payment Status update");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "SimpleResponse");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            if(StringUtils.isEmpty(paymentStatusBO.getPaymentId()) && StringUtils.isEmpty(paymentStatusBO.getConsumerCode())) //:todo need to cross verify the consumer code
                errorBoList.add(new ErrorBO("paymentId/consumerCode", "Invalid input"));
            if(StringUtils.isEmpty(paymentStatusBO.getPaidStatus()))
                errorBoList.add(new ErrorBO("paidStatus", "Invalid input"));

            if(errorBoList.size()==0) {

                //UPDATE MF TRANSACTION STATUS
                if(StringUtils.isNotEmpty(paymentStatusBO.getPaymentId()) && Integer.parseInt(paymentStatusBO.getPaymentId())>1000) {

                    if(StringUtils.isEmpty(paymentStatusBO.getTransactionId()))
                        errorBoList.add(new ErrorBO("transactionId", "Invalid input"));

                    else {

                        List<PaymentInfoBO.SchemeDetails> schemes = transactionDAO.getTransactionSchemes(Integer.parseInt(paymentStatusBO.getPaymentId()));
                        if (schemes.size() > 0)
                            paymentStatusBO = updateTransactionTables(paymentStatusBO, schemes, userId);
                    }
                }

                //UPDATE ENACH STATUS
                if(StringUtils.isNotEmpty(paymentStatusBO.getConsumerCode())) {

                    String paidStatus = "REJECTED";
                    if (StringUtils.isNotEmpty(paymentStatusBO.getPaidStatus()) && paymentStatusBO.getPaidStatus().equalsIgnoreCase("Y"))
                        paidStatus = "APPROVED";

                    int status = mandateInfoDAO.updateSIPConsumerDetails(paymentStatusBO.getConsumerCode(), paidStatus, paymentStatusBO.getMandateRegNo(), userId);
                    if (status > 0)
                        paymentStatusBO.setStatus("true");
                    else
                        paymentStatusBO.setStatus("false");
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "tax-saver API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, paymentStatusBO, globalResponseBo);

        log.debug(logPrefix + "tax-saver API Status : " + stat);
        return  globalResponseBo;

    }
    public GlobalResponseBO updateNEFTMFPaymentStatus(int userId, PaymentStatusBO paymentStatusBO) {

        String logPrefix = "#PaymentId : " + paymentStatusBO.getPaymentId() + " | #PaymentStatus : " + paymentStatusBO.getPaidStatus() + " | " ;
        log.debug(logPrefix + "MF NEFT Payment Status update");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "SimpleResponse");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {
            if(StringUtils.isEmpty(paymentStatusBO.getPaymentId()) )
                errorBoList.add(new ErrorBO("paymentId/consumerCode", "Invalid input"));

            if(errorBoList.size()==0) {

                //UPDATE MF TRANSACTION STATUS
                if(Integer.parseInt(paymentStatusBO.getPaymentId())>1000) {
                    transactionDAO.transactionOtherPaymentUpdate(Integer.parseInt(paymentStatusBO.getPaymentId()),  userId);
                    paymentStatusBO.setPaidStatus("Y");
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "NEFT payment update API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, paymentStatusBO, globalResponseBo);

        log.debug(logPrefix + "tax-saver API Status : " + stat);
        return  globalResponseBo;

    }
    public GlobalResponseBO updateMandateMFPaymentStatus(int userId, PaymentStatusBO paymentStatusBO) {

        String logPrefix = "#PaymentId : " + paymentStatusBO.getPaymentId() + " | #PaymentStatus : " + paymentStatusBO.getPaidStatus() + " | " ;
        log.debug(logPrefix + "MF Payment Status update");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "SimpleResponse");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {
            if(StringUtils.isEmpty(paymentStatusBO.getPaymentId()) )
                errorBoList.add(new ErrorBO("paymentId/consumerCode", "Invalid input"));

            if(errorBoList.size()==0) {

                //UPDATE MF TRANSACTION STATUS
                if(Integer.parseInt(paymentStatusBO.getPaymentId())>1000) {
                    String actualDebitDate  =  mandateInfoDAO.getNextWorkingDayForTPSL();
                    transactionDAO.transactionOtherPaymentUpdate(Integer.parseInt(paymentStatusBO.getPaymentId()),  userId);
                    transactionDAO.LumpsumTransactions(paymentStatusBO.getPaymentId(), paymentStatusBO.getConsumerCode(), 10000, actualDebitDate, userId); //:todo amount need to dynamic
                    paymentStatusBO.setPaidStatus("Y");
                    paymentStatusBO.setStatus("true");
                    paymentStatusBO.setPaidThru("mandate");
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "NEFT payment update API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, paymentStatusBO, globalResponseBo);

        log.debug(logPrefix + "tax-saver API Status : " + stat);
        return  globalResponseBo;

    }

    public PaymentInfoBO insertTransactionTables(List<SchemeCartBO.Schemes> schemes, int cartId, int holdingProfileId, int prevPaymentId, String transType,
                                                               String transStatus, String mediumThru, String ipAddress, int userId, List<ErrorBO> errorList, boolean retry) {

        String logPrefix = "#CartId : " + cartId + " | #HoldingProfileId : " + holdingProfileId + " | #UserId : " + userId + " | ";

        PaymentInfoBO.MFDetails mfDetails = new PaymentInfoBO.MFDetails();
        List<PaymentInfoBO.SchemeDetails> schemesList = new ArrayList<>();
        PaymentInfoBO paymentInfoBO = new PaymentInfoBO();
        DecimalFormat df = new DecimalFormat("###");

        String dividendId, itcValue = "";
        int status;
        double totalAmount = 0d;

        try{

            int paymentId = autoNumberGenerator.getAutoNumberGenerated("PaymentID");

            String currentTime = generalInfoDAO.getCurrentTime();

            //If payment - retry, then deactivate the previous transaction
            if(retry)
                transactionDAO.deactivateInvestorTransaction(cartId, prevPaymentId, userId);

            for(SchemeCartBO.Schemes schemeBO : schemes) {

                int userTransRefId = schemeBO.getUserTransRefId();
                //for retry and initial payment - generate new userTransRefId
                if (userTransRefId == 0 || retry) {
                    userTransRefId = autoNumberGenerator.getAutoNumberGenerated("TransRefID");
                    status = transactionDAO.updateUserTransRefIdCart(cartId, schemeBO.getId(), userTransRefId, userId);
                    log.debug(logPrefix + "UserTransRefId update status : " + status);
                }

                //if retry, update new userTransRefIds for previous transaction
                if(retry) {
                    transactionDAO.updateSipDetailsUserTransRefId(userTransRefId, schemeBO.getUserTransRefId(), userId);
                    transactionDAO.updateInvestorTransactByPortfolioUserTransRefId(userTransRefId, schemeBO.getUserTransRefId(),
                            StringUtils.isEmpty(schemeBO.getGoalId()) ? 0 : Integer.parseInt(schemeBO.getGoalId()), userId);
                    transactionDAO.updateTransactionBankInfoUserTransRefId(userTransRefId, schemeBO.getUserTransRefId(), schemeBO.getBankId());
                }

                //get amcSchemeCode & tpslSchemeCode
                HashMap<String, String> schemeCodes = schemeSearchDAO.getSchemeDetailsForTransaction(Integer.parseInt(schemeBO.getSchemeCode()));

                //get ITC value
                itcValue = generalUtil.getITC(paymentId, currentTime, schemeCodes.get("amcSchemeCode"), schemeCodes.get("amcCode"), userId);

                dividendId = transactionDAO.getDividendId(schemeBO.getOption(), schemeBO.getDividendOption());
               status=10;
                //status = transactionDAO.insertInvestorTransaction(schemeBO, holdingProfileId, userTransRefId, paymentId, dividendId, currentTime, transType, transStatus, mediumThru, itcValue, userId);

                if (status > 0) {

                    status = transactionDAO.insertInvestorTransactionTrack(schemeBO, holdingProfileId, userTransRefId, ipAddress, userId);

                    if (status > 0) {

                        //insert new rows only for first transaction, for retry only update
                        if(!retry) {
                            status = transactionDAO.insertInvestorTransactByPortfolio(schemeBO, holdingProfileId, userTransRefId, transType, transStatus, userId);

                            if (status > 0) {

                                status = transactionDAO.insertTransactionBankInfo(schemeBO, holdingProfileId, userTransRefId, userId);
                                if (status == 0)
                                    errorList.add(new ErrorBO(schemeBO.getId(), "failed", "Transaction_Bank_Info insert failed"));
                            } else
                                errorList.add(new ErrorBO(schemeBO.getId(), "failed", "InvestorTransactByPortfolio insert failed"));
                        }
                    } else
                        errorList.add(new ErrorBO(schemeBO.getId(), "failed", "InvestorTransactionTrack Insert failed"));
                } else
                    errorList.add(new ErrorBO(schemeBO.getId(), "failed", "InvestorTransaction Insert failed"));

                //set schemeDetails for PaymentInfoBO
                PaymentInfoBO.SchemeDetails schemeDetails = new PaymentInfoBO.SchemeDetails();
                schemeDetails.setSchemeCode(schemeBO.getSchemeCode());
                schemeDetails.setSchemeName(schemeBO.getSchemeName());
                schemeDetails.setAmount(schemeBO.getAmount());
                schemeDetails.setAmcSchemeCode(schemeCodes.get("amcSchemeCode"));
                schemeDetails.setTpslSchemeCode(schemeCodes.get("tpslSchemeCode"));
                schemeDetails.setItc(itcValue);
                schemeDetails.setTransactionReferenceNo(String.valueOf(userTransRefId));

                schemesList.add(schemeDetails);
                totalAmount += schemeBO.getAmount();
            }

            status = transactionDAO.insertInvestorTransactionPaymentTrack(paymentId, "MF", userId, "NetBanking");
            if(status==0)
                errorList.add(new ErrorBO("paymentId", "InvestorTransactionPaymentTrack insert failed"));

            if(errorList.size()==0) {
                status = transactionDAO.updatePaymentIdCart(cartId, paymentId, userId);
                log.debug(logPrefix + "PaymentId Update Status : " + status);

                //set PaymentInfoBO for response
                UserBO userBO = investorInfoDAO.getInvestorDetail(investorInfoDAO.getPrimaryInvestorId(holdingProfileId), userId);

                mfDetails.setName(userBO.getName());
                mfDetails.setMobile(userBO.getMobile());
                mfDetails.setHoldingProfileId(String.valueOf(holdingProfileId));
                mfDetails.setGoalId(schemes.get(0).getGoalId());
                mfDetails.setTotalAmount(totalAmount);
                mfDetails.setTotalAmountFormatted(df.format(totalAmount));
                mfDetails.setSchemes(schemesList);

                paymentInfoBO.setPaymentId(String.valueOf(paymentId));
                paymentInfoBO.setMf(mfDetails);
                paymentInfoBO.setPaymentRetry(retry);

            }


        } catch (Exception e) {
            log.error(logPrefix + "Exception in transactions insert : ", e);
            errorList.add(new ErrorBO("cartId","exception", "Data Insertion failed"));
        }

        return paymentInfoBO;
    }

    /**
     * update the payment status in transaction tables
     * @param paymentStatusBO
     * @param schemes
     * @param userId
     * @return
     */
    public PaymentStatusBO updateTransactionTables(PaymentStatusBO paymentStatusBO, List<PaymentInfoBO.SchemeDetails> schemes, int userId) {

        int status, userTransRefId = 0;

        boolean isSuccess = true;
        try {

            String transactionStatus = "";
            if(StringUtils.isNotEmpty(paymentStatusBO.getPaidStatus())) {

                String paidStatus = paymentStatusBO.getPaidStatus().trim();

                if(paidStatus.equalsIgnoreCase("Y"))
                    transactionStatus = "PS";
                if(paidStatus.equalsIgnoreCase("N"))
                    transactionStatus = "PF";
                if(paidStatus.equalsIgnoreCase("MT08"))
                    transactionStatus = "TC";
                if(paidStatus.equalsIgnoreCase("NEFT"))
                    transactionStatus = "PW";
            }

            int weekDay = holidayCalendarDAO.weekDay();
            boolean isHoliday = holidayCalendarDAO.isHoliday();

            int paymentId = Integer.parseInt(paymentStatusBO.getPaymentId());

            if(paymentId>1000 && !transactionStatus.equals("")){

                status = transactionDAO.updateInvestorTransaction(paymentStatusBO.getTransactionId(), transactionStatus, paymentId, userId);
                if (status == 0)
                    isSuccess = false;

                if(isSuccess){
                    status = transactionDAO.updateInvestorTransactionPaymentTrack(paymentStatusBO.getStatusCode(), paymentId, userId);
                    if(status == 0)
                        isSuccess = false;

                    for(PaymentInfoBO.SchemeDetails scheme : schemes) {

                        int holdingProfileId = StringUtils.isNotEmpty(scheme.getHoldingProfileId()) ? Integer.parseInt(scheme.getHoldingProfileId()) : 0;

                        if(StringUtils.isNotEmpty(scheme.getTransactionReferenceNo())) {
                            userTransRefId = Integer.parseInt(scheme.getTransactionReferenceNo());

                            if (transactionStatus.equalsIgnoreCase("PS")) {

                                 transactionDAO.updateSipDetails(userTransRefId, userId);
                                 transactionDAO.insertOEInvestorTransaction(holdingProfileId, scheme.getAmcCode(), paymentId, userId);

                                 if(StringUtils.isNotEmpty(scheme.getSchemeClass()) && scheme.getSchemeClass().equalsIgnoreCase("LQ") && weekDay==6)
                                    paymentStatusBO.setLiquidSchemeMessage(BPConstants.LQ_SCHEMES_NAV_MSG);

                                 transactionDAO.updateInvestorTransactionTrack(userTransRefId, paymentStatusBO.getPaymentMode(), userId);
                            }
                        }
                    }
                }
            }
            JSONObject jsonObject = transactionDAO.getTransDateAndTime(paymentId);
            paymentStatusBO.setTransactionDate((String)jsonObject.get("transDate"));

            String transMsg = generalUtil.getTransactionMsg1(weekDay, isHoliday, (String)jsonObject.get("transTime"));
            paymentStatusBO.setMessage(transMsg);

            paymentStatusBO.setStatus(isSuccess ? "true" : "false");

        } catch (Exception e) {
            log.error("Exception in InvestorTransaction tables upadte : ", e);
        }

        log.debug("Transaction tables update status : " + isSuccess);
        return paymentStatusBO;
    }
}

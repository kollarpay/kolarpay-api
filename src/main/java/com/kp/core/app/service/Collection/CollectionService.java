package com.kp.core.app.service.Collection;

import com.kp.core.app.dao.CollectionDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
public class CollectionService {

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    private CollectionDAO collectionDAO;




    public GlobalResponseBO createCollection(CollectionBO collectionBO, int userId) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "Create Collection");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "createCollection");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";

        try {

            isValidCollection(collectionBO, errorBoList);


            if(errorBoList.isEmpty()) {
                collectionDAO.insertCollection(collectionBO,userId);
            }


        } catch (Exception e) {
            log.error(logPrefix + "Create mandate API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, collectionBO, globalResponseBo);

        log.debug(logPrefix + "Create Collection API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO collections(int userId,String tags) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "get all published Collection");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "collections");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        List<CollectionBO> collectionBOList=new ArrayList<>();
        try {


            if(errorBoList.size()==0 ) {
                collectionBOList=collectionDAO.getCollectionList(userId,tags);
            }


        } catch (Exception e) {
            log.error(logPrefix + "get Collections API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, collectionBOList, globalResponseBo);

        log.debug(logPrefix + "Collections API Status : " + stat);
        return  globalResponseBo;

    }
    public GlobalResponseBO collectionsCategory(int userId,String tags) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "get all published Collection");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "collections");
        List<ErrorBO> errorBoList = new ArrayList<>();
        String stat = "";
        List<CollectionBO> collectionBOList=new ArrayList<>();
        try {


            if(errorBoList.size()==0 ) {
                collectionBOList=collectionDAO.getCollectionList(userId,tags);
            }


        } catch (Exception e) {
            log.error(logPrefix + "get Collections API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, collectionBOList, globalResponseBo);

        log.debug(logPrefix + "Collections API Status : " + stat);
        return  globalResponseBo;

    }
    public void isValidCollection(CollectionBO collectionBO, List<ErrorBO> errorBoList) {

        try {

            if(StringUtils.isEmpty(collectionBO.getTitle()))
                errorBoList.add(new ErrorBO("title", "Invalid input"));

            if(collectionBO.getPosition()<1)
                errorBoList.add(new ErrorBO("position", "Invalid input"));

        } catch (Exception e) {
            log.error("Exception in Collection creation - validate : ", e);
        }
    }
}


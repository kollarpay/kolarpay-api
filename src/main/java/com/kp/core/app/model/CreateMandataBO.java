package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateMandataBO {

    private String customerId;
    private String bankId;
    private int maximumAmount;
    private String type;
    private String frequency;
    private String validTill;
    private Boolean validTillCancel;
    private String consumerCode;

    @Override
    public String toString() {

        return    "{\"bankId\" : \"" + bankId + "\""
                + ",\"maximumAmount\" : " + maximumAmount
                + ",\"type\" : \"" + type + "\""
                + ",\"frequency\" : \"" + frequency + "\""
                + ",\"validTill\" : \"" + validTill + "\""
                + ",\"validTillCancel\" : " + validTillCancel + "}";
    }

}

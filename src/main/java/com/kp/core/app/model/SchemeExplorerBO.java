package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class SchemeExplorerBO {

    private int sc = 0;         //scheme code
    private String sn = "";      //scheme name
    private String sw;         //swp
    private int si;            //sip 1 or 0
    private String mi;         //multiple investment
    private String mu;         //multiple units
    private String cc;         //class code
    private String cn;         //current nav
    private String cnf;         //current nav formatted
    private String cd;         //current nav date
    private String tc;         //type code
    private int vr;            //vro rating
    private int vc;            //vro code
    private double er;          //expense ratio
    private int wl;            //watchlist
    private int nr;            //nri 1 or 0
    private int acc;

    private String ai; //additional invest

    private String sm;
    private String sl;         //scheme load
    private String fm;
    private String fd;
    private String st;
    private String h52;        //52week high
    private String l52;        //52week low
    private String bm;         //benchmark
    private String bmi;         //benchmark id

    private SchemeReturnsBO r;     //returns
    private SchemeReturnsBO b;     //benchmark returns
    private SchemeReturnsBO cwr;     //categorywise return
    private ArrayList<SchemeHoldingsBO> h5;     //holdings
    private ArrayList<SchemeHoldingsBO> h10;     //holdings
    private ArrayList<SchemeHoldingsBO> seh;     //sector holdings
    private ArrayList<SchemeHoldingsBO> pf;     //performance
    private int vn;

    private String nc; //netchange nav
    private String navc;       //nav change
    private String incd;       //inception date
    private String amc;       //amc code
    private String amcn;       //amc name
    private String rnt;       //rnt code
    private int sg;
    private SchemeAumBO ad;
    private SchemeAumBO rntd;


    //inside look

    private String fo;          //fund objective
    private ArrayList<SchemeHoldingsBO> a;     //asset allocation
    private String am;          //avg maturity
    private String cr;          //credit rating
    private String av;          //avg yield to maturity
    private String cb;          //credit break
    private String mc;          //avg market cap
    private String mcp;         //market capitalization percent
    private String aums;        //aum

    private ArrayList<SchemeHoldingsBO> fman;  //fund managers
    private String rt;          //R&t address info
    private String fa;          //fund house address

    //how to invest
    private ArrayList<SchemeHoldingsBO> el;    //eligible list
    private ArrayList<SchemeHoldingsBO> ecl;    //eligible country list
    private String exl;         //exit load
    private ArrayList<SchemeHoldingsBO> dio;   //dividend options
    private ArrayList<SchemeQRBO> qr;

    private int color;
}

package com.kp.core.app.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {

    private int userId;
    private String firstname ="";
    private String lastname ="";
    private String username="";
    private String emailId ="";
    private String mobileno ="";
    private String dob ="";
    private String gender ="";
    private String userType ="";
    private String password="";
    private String encodePassword="";
    private String referenceId="";
    private int pid;
    private String verificationId="";
    private String smsOTP="";
    private String messageType="";
    private String productType="";
    private String ipAddress="";
    private int partnerId;

}

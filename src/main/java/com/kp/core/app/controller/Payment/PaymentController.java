package com.kp.core.app.controller.Payment;

import com.google.gson.Gson;
import com.kp.core.app.config.GeneralConfig;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.ErrorBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.request.PaymentInitiateRequestBO;
import com.kp.core.app.razorpay.config.ErrorConfig;
import com.kp.core.app.razorpay.utils.ErrorsConstants;
import com.kp.core.app.service.Payment.PaymentServices;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.APIGeneralUtil;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    private PaymentServices paymentServices;

    @Autowired
    private GeneralUtil generalUtil;
    @Autowired
    private Gson gson;

    private GeneralConfig config = ConfigFactory.create(GeneralConfig.class);

    private ErrorConfig errorConfig = ConfigFactory.create(ErrorConfig.class);


    @PostMapping(path = "/customer-account", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> paymentStatus(@RequestHeader MultiValueMap<String, Object> headers,
                                                          @RequestBody String payload) {

        log.debug("#customer-create POST API Request | #Payload : " + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "PaymentCustomerCreate");

        int userId = 0;
        String productType = "";

        try {
            PaymentInitiateRequestBO requestBO = gson.fromJson(payload, PaymentInitiateRequestBO.class);

            if(requestBO!=null) {
                if(StringUtils.isEmpty(requestBO.getProductType()))
                    requestBO.setProductType("GO");
                else {
                    requestBO.setProductType(requestBO.getProductType().toUpperCase());
                }


                if ("null".equalsIgnoreCase(requestBO.getSid())) {
                    requestBO.setSid("0");
                }
                String channelId = APIGeneralUtil.getHeader(headers, "channel-id");

                globalResponseBO = paymentServices.customerAccount(requestBO, channelId);
            }
            else {
                List<ErrorBO> errorBoList = new ArrayList<>();
                errorBoList.add(new ErrorBO("", productType,"payload", ErrorsConstants.REQUIRED_DATA_NOT_FOUND, errorConfig.requiredDataNotFound()));
                APIGeneralUtil.getGlobalResponseBO(APIConstants.SERVER_ERROR_STRING, errorBoList, "", globalResponseBO);
            }


        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        log.debug("#payment-info GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

package com.kp.core.app.service.Utilities;

import com.kp.core.app.config.PaymentConfig;
import com.kp.core.app.dao.GeneralInfoDAO;
import com.kp.core.app.dao.InvestorInfoDAO;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.model.request.CustomerRequest;
import com.kp.core.app.utils.APIConstants;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import java.util.Base64;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class GeneralInfoService {

    @Autowired
    private GeneralUtil generalUtil;
    @Autowired
    private GeneralInfoDAO generalInfoDAO;
    @Autowired
    private InvestorInfoDAO investorInfoDAO;

    private static PaymentConfig paymentConfig = ConfigFactory.create(PaymentConfig.class);


    public GlobalResponseBO lookUpList(String[] lookUpTypes) {

        String logPrefix = " | #LookUpTypes : " + Arrays.toString(lookUpTypes) + " | ";
        log.debug(logPrefix + "lookUp List  fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.array, "Lookup");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<LookUpResponseBO> lookUpResponse = new ArrayList<>();
        String stat = "";
        try {

            if(lookUpTypes.length==0)
                errorBoList.add(new ErrorBO("types", "Invalid input"));

            else {
                for (String lookUpType : lookUpTypes) {
                    List<IdDesc> lookUpList = generalInfoDAO.getLookUpList(lookUpType);

                    if(!lookUpList.isEmpty()) {
                        LookUpResponseBO responseBO = new LookUpResponseBO();
                        responseBO.setKey(lookUpType);
                        responseBO.setValues(lookUpList);
                        lookUpResponse.add(responseBO);
                    }
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "lookup API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, lookUpResponse, globalResponseBo);

        log.debug(logPrefix + "lookup API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO lookUpSvgList(String[] lookUpTypes) {

        String logPrefix = " | #LookUpTypes : " + Arrays.toString(lookUpTypes) + " | ";
        log.debug(logPrefix + "lookUpSvg List  fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.array, "lookUpSvg");
        List<ErrorBO> errorBoList = new ArrayList<>();
        List<LookUpResponseBO> lookUpResponse = new ArrayList<>();
        String stat = "";
        try {

            if(lookUpTypes.length==0)
                errorBoList.add(new ErrorBO("types", "Invalid input"));

            else {
                for (String lookUpType : lookUpTypes) {
                    List<IdDesc> lookUpList = generalInfoDAO.getLookUpSvgList(lookUpType);

                    if(!lookUpList.isEmpty()) {
                        LookUpResponseBO responseBO = new LookUpResponseBO();
                        responseBO.setKey(lookUpType);
                        responseBO.setValues(lookUpList);
                        lookUpResponse.add(responseBO);
                    }
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "lookup API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(!errorBoList.isEmpty())
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, lookUpResponse, globalResponseBo);

        log.debug(logPrefix + "lookup API Status : " + stat);
        return  globalResponseBo;

    }
    public GlobalResponseBO lookUpValue(String lookUpId,String lookUpType) {

        String logPrefix =" | #LookUpTypes : " + lookUpId + " | ";
        log.debug(logPrefix + "lookUp List  fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.array, "Lookup");
        List<ErrorBO> errorBoList = new ArrayList<>();
        IdDesc idDesc = new IdDesc();
        String stat = "";
        try {

            if(lookUpId.isEmpty())
                errorBoList.add(new ErrorBO("lookUpId", "Invalid input"));
            if(lookUpType.isEmpty())
                errorBoList.add(new ErrorBO("lookUpType", "Invalid input"));

            else {
                 idDesc= generalInfoDAO.getLookUp(lookUpId,lookUpType);

            }

        } catch (Exception e) {
            log.error(logPrefix + "lookup value API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, idDesc, globalResponseBo);

        log.debug(logPrefix + "lookup value API Status : " + stat);
        return  globalResponseBo;

    }

    public GlobalResponseBO userProfile(int userId, String token) {

        String logPrefix = "#UserId : " + userId + " | ";
        log.debug(logPrefix + "User profile  fetch");
        GlobalResponseBO globalResponseBo = new GlobalResponseBO(ResponseDataObjectType.object, "User");
        List<ErrorBO> errorBoList = new ArrayList<>();
        UserInfoBO userInfoBO = new UserInfoBO();
        String stat = "";
        try {

            userInfoBO = investorInfoDAO.getUserInfo(userId);

            if(userInfoBO==null || userInfoBO.getUserId()==0)
                errorBoList.add(new ErrorBO("userId", "No data found"));

        } catch (Exception e) {
            log.error(logPrefix + "lookup API Exception : ", e);
            stat = APIConstants.SERVER_ERROR_STRING;
        }

        globalResponseBo = generalUtil.getGlobalResponseBO(stat, errorBoList, userInfoBO, globalResponseBo);

        if(errorBoList.size()>0)
            stat = APIConstants.BAD_REQUEST_STRING;

        log.debug(logPrefix + "lookup API Status : " + stat);
        return  globalResponseBo;

    }
    public static HttpHeaders LoadPaymentHeader(HttpHeaders headers){

       String basicAuthEncodeStr= getBasicAuthString(paymentConfig.paymentExternalApiKey(),paymentConfig.paymentExternalApiSecret());
        headers.set("Authorization", "Basic " + basicAuthEncodeStr);
        headers.set("Content-Type",paymentConfig.getContentTypeJson());
        return headers;
    }
    public static HttpHeaders LoadPhonePeHeader(HttpHeaders headers){
        headers.set("Content-Type",paymentConfig.getContentTypeJson());
        return headers;
    }
    public static String getBasicAuthString(String username,String password){
        String authString = username + ":" + password;
        String authStringEnc =  Base64.getEncoder().encodeToString(authString.getBytes());
        return authStringEnc;
    }
    public static CustomerRequest getPaymentCustomerRequest(CustomerInfoBO customerInfoBO){
        CustomerRequest customerRequest=new CustomerRequest();
        customerRequest.setName(customerInfoBO.getName());
        customerRequest.setEmail(customerInfoBO.getEmail());
        customerRequest.setContact(customerInfoBO.getMobile());
        return customerRequest;
    }

}



package com.kp.core.app.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Slf4j
@Repository
public class AutoNumberGenerator {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public synchronized int getAutoNumberGenerated(String autoGenerateType) throws Exception {

        int intAutoNumber = 0;

        try {
            jdbcTemplate.setResultsMapCaseInsensitive(true);
            SimpleJdbcCall jdbcCall = new
                    SimpleJdbcCall(jdbcTemplate).withProcedureName("GETAUTOGENERATE_NUMBER");
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                                                    .addValue("COLUMNNAME", autoGenerateType);

            Map<String, Object> objectMap = jdbcCall.execute(sqlParameterSource);
            intAutoNumber = (Integer) objectMap.get("AUTOGENNUM");
            log.info("intAutoNumber=="+intAutoNumber);

        } catch (Exception e) {
            log.error("Exception while auto number generation:",e);
            throw new Exception("exception in getting auto number values" + e);
        }

        return intAutoNumber;
    }
}

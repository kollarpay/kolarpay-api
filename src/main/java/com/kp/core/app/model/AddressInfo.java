package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressInfo {

    private String addressId;
    private String customerId;
    private String addressType;
    private String addressLine1;
    private String addressLine2;
    private String landmark;
    private String location;
    private String city;
    private String pinCode;
    private String mobileNumber;
    private String workPhone;
    private String email;
    private String flag;
    private String remarks;
    private String state;

}

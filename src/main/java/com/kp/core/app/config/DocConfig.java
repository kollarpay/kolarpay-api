package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/brospay/config/properties/docConfig.properties")
@Config.HotReload(type = Config.HotReloadType.ASYNC, unit = TimeUnit.MINUTES, value = 10)

public interface DocConfig extends Reloadable {

    @Config.Key("SIP_PDF_PATH")
    @Config.DefaultValue("/data/logs/brospay/sipPdf/")
    String pdfPath();

    @Config.Key("INVESTORS_TEMPLATE_PATH")
    @Config.DefaultValue("/data/logs/brospay/config/Templates/Investor/")
    String investorsTemplatePath();
}

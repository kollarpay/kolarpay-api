package com.kp.core.app.phonpe.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionCreateResponse {
    private boolean success;
    private String code;
    private String message;
    private Data data;

    @Getter
    @Setter
    public static class Data {
        private String subscriptionId;
        private String state;
        private long validUpto;
        private boolean isSupportedApp;
        private boolean isSupportedUser;
    }

    }

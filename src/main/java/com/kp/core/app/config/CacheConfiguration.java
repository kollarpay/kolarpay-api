package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

/**
 * Created by subathira on 2020-09-22.
 */
@Config.Sources("file:/data/logs/brospay/config/properties/cache-config.properties")
@Config.HotReload(unit = TimeUnit.MINUTES, value = 3)
public interface CacheConfiguration extends Reloadable {

    @Config.Key("portfolio.dashboard.url")
    @Config.DefaultValue("fi-nse-test-ec.eshr2f.ng.0001.aps1.cache.amazonaws.com")
    String cacheUrl();

}

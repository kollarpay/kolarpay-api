package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppInfoBO {

    private String appVersion;
    private String os;
    private String brandModel;
    private String fcmId;
    private String packageName;
    private String osVersion;
    private String appVersionName;
    private String deviceId;
    private String processType;

    @Override
    public String toString() {

        if(appVersion==null) appVersion = "";

        String sipdetails = "{\"appVersion\" : \"" + appVersion + "\"";
        if(os!=null)
            sipdetails += ",\"os\" : \"" + os + "\"";
        if(brandModel!=null)
            sipdetails += ",\"brandModel\" : \"" + brandModel + "\"";
        if(fcmId!=null)
            sipdetails += ",\"fcmId\" : \"" + fcmId + "\"";
        if(packageName!=null)
            sipdetails += ",\"packageName\" : \"" + packageName + "\"";
        if(osVersion!=null)
            sipdetails += ",\"osVersion\" : \"" + osVersion + "\"";
        if(appVersionName!=null)
            sipdetails += ",\"appVersionName\" : \"" + appVersionName + "\"";
        if(deviceId!=null)
            sipdetails += ",\"deviceId\" : \"" + deviceId + "\"";
        if(processType!=null)
            sipdetails += ",\"endDate\" : \"" + processType + "\"";
        sipdetails += "}";

        return sipdetails;
    }
}

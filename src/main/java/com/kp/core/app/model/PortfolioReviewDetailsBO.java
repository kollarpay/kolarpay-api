package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortfolioReviewDetailsBO {

    private int reviewId;
    private int portfolioValue;
    private int cash;
    private double cashPercentage;
    private int transitCash;
    private double transitCashPercentage;
    private List<PortfolioSchemeBO> investments;
    private List<CategoryAllocationBO> categoryAllocation;
    private List<CategoryAllocationBO> subCategoryAllocation;
    private PortfolioAssetTotalBO totals;
    private PortfolioAssetPercentageBO changePercentage;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PortfolioAssetTotalBO {

        private double equity;
        private double debt;
        private double gold;
        private double grandTotal;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PortfolioAssetPercentageBO {

        private double equity;
        private double debt;
        private double gold;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CategoryAllocationBO {

        @SerializedName("class")
        @JsonProperty("class")
        private String className;
        private double currentAllocation;
        private double targetAllocation;
        private double proposedAllocation;
    }
}

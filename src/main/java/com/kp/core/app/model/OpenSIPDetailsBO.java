package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class OpenSIPDetailsBO {

    private int sipId = 0;
    private String consumerCode = "";
    private String tpslSchemeCode = "WIFS";
    private String DebitDate = "";
    private String transType = "";
    private Double amount = 0.0;
    private int ecsDate	=	0;
    private String siRefID;
    private String mandateDate;
    private int userid ;
    private int investorid;
    private String createdFrom;
    private String siInstructionID;
    private String email;
    private String investorname;
    private String dbnextBusinessDay;
    private int investorID;
    private int tpslFeedID =	0;
    private String allowTransact;

}

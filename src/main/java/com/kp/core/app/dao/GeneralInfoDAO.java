package com.kp.core.app.dao;

import com.kp.core.app.model.CustomerBankInfo;
import com.kp.core.app.model.IdDesc;
import com.kp.core.app.model.MFBusinessDayBO;
import com.kp.core.app.model.NameValueBO;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Repository
public class GeneralInfoDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    Gson gson;

    public String getLookUpId(String lookUpDesc, String lookUpType) {

        String lookUpId = "";

        try {

            String sqlQuery = " SELECT LookUpId FROM LookUp WHERE LookUpDescription = ? AND LookUpType = ? AND Active>0 ";

            lookUpId = jdbcTemplate.queryForObject(sqlQuery, new Object[]{lookUpDesc, lookUpType}, String.class);

        } catch (Exception e) {
            log.error("Exception in getting lookUpId : ", e);
        }
        return lookUpId;
    }

    public String getLookUpValue(String lookUpId, String lookUpType) {

        String lookupvalue= "";

        try {

            String sqlQuery = " SELECT lookupvalue FROM LookUp WHERE LookUpId = ? AND LookUpType = ? AND Active>0 ";

            lookupvalue = jdbcTemplate.queryForObject(sqlQuery, new Object[]{lookUpId, lookUpType}, String.class);

        } catch (Exception e) {
            log.error("Exception in getting lookUpId : ", e);
        }
        return lookupvalue;
    }
    public IdDesc getLookUp(String lookUpkey, String lookUpType) {

        String lookupvalue= "";
        IdDesc idDesc=new IdDesc();
        try {

            String sqlQuery = " SELECT lookupvalue FROM LookUp WHERE LookUpkey = ? AND LookUpType =? AND Active>0 ";

            lookupvalue = jdbcTemplate.queryForObject(sqlQuery, new Object[]{lookUpkey,lookUpType}, String.class);
            if(!lookupvalue.isEmpty()){
                idDesc.setKey(lookUpkey);
                idDesc.setValue(lookupvalue);
            }


        } catch (Exception e) {
            log.error("Exception in dao getting lookUpValue : ", e);
        }
        return idDesc;
    }

    public List<IdDesc> getLookUpList(String lookUpType) {

        List<IdDesc> lookUpList = new ArrayList<>();

        try {

            String sqlQuery = " SELECT Lookupkey, LookupValue FROM lookup WHERE LookUpType = ? AND Active>0 order by order_by asc ";

            lookUpList = jdbcTemplate.query(sqlQuery, new Object[]{lookUpType}, (rs, rowNumber) -> {

                IdDesc idDesc = new IdDesc();
                idDesc.setKey(rs.getString("Lookupkey"));
                idDesc.setValue(rs.getString("LookupValue"));
                return idDesc;
            });

        } catch (Exception e) {
            log.error("Exception in getting lookUpList : ", e);
        }
        return lookUpList;
    }

    public List<IdDesc> getLookUpSvgList(String lookUpType) {

        List<IdDesc> lookUpList = new ArrayList<>();

        try {

            String sqlQuery = " SELECT l.Lookupkey,l.LookupValue,s.lookupdescription FROM lookup l left join svg s on l.lookuptype=s.lookuptype and l.lookupkey=s.lookupkey  WHERE l.LookUpType = ? AND l.Active>0 order by l.lookupid asc ";

            lookUpList = jdbcTemplate.query(sqlQuery, new Object[]{lookUpType}, (rs, rowNumber) -> {

                IdDesc idDesc = new IdDesc();
                idDesc.setKey(rs.getString("Lookupkey"));
                idDesc.setValue(rs.getString("LookupValue"));
                idDesc.setDesc(rs.getString("lookupdescription"));
                return idDesc;
            });

        } catch (Exception e) {
            log.error("Exception in getting lookUpList : ", e);
        }
        return lookUpList;
    }

    public List<NameValueBO> getLookUpListInNameValue(String lookUpType, boolean sort) {

        List<NameValueBO> lookUpList = new ArrayList<>();

        try {

            String orderQuery = "";

            if(sort)
                orderQuery = " Order By LookUpId asc ";
            else
                orderQuery = " Order By LookUpId  desc";

            String sqlQuery = " SELECT LookUpkey, lookupvalue FROM LookUps WHERE LookUpType = ? AND Active>0 " + orderQuery;

            lookUpList = jdbcTemplate.query(sqlQuery, new Object[]{lookUpType}, (rs, rowNumber) -> {

                NameValueBO nameValueBO = new NameValueBO();
                nameValueBO.setName(rs.getString("LookUpkey"));
                nameValueBO.setValue(rs.getString("lookupvalue"));
                return nameValueBO;
            });

        } catch (Exception e) {
            log.error("Exception in getting lookUpList : ", e);
        }
        return lookUpList;
    }

    public String getCurrentTime() {

        String currentTime = "";

        try {

            String timeSql = "SELECT CONVERT(char(8), DATEADD(MI, 330, GETDATE()), 114) AS HHMMSS";
            currentTime = jdbcTemplate.queryForObject(timeSql, new Object[]{}, String.class);

        } catch (Exception e) {
            log.error("Exception in finding current time : ", e);
        }

        return currentTime;
    }

    public MFBusinessDayBO getNthMFBusinessDay(int nthDay)  {

        MFBusinessDayBO businessDayBO = new MFBusinessDayBO();

        try {

            String sqlQuery = "EXEC WF_NTH_TPSL_BUSINESS_DAY ? ";

            businessDayBO = jdbcTemplate.queryForObject(sqlQuery, new Object[]{nthDay}, (rs, rowNumber) -> {

                MFBusinessDayBO mfBusinessDayBO = new MFBusinessDayBO();

                mfBusinessDayBO.setNthBusinessDay(rs.getString("NTHBUSINESSDAY"));
                mfBusinessDayBO.setCurrentDay(rs.getString("CURRENTDAY"));
                mfBusinessDayBO.setDays(rs.getString("DAYS"));
                mfBusinessDayBO.setDbNthBusinessDay(rs.getString("DBNTHBUSINESSDAY"));
                return mfBusinessDayBO;
            });

        } catch (Exception e) {
            log.error("Exception in MF Nth BusinessDay : ", e);
        }

        return businessDayBO;
    }

    public List<String> getRestrictedSchemes() {


        log.debug("Restricted schemes");

        List<String> schemes = new ArrayList<>();

        try {
            String sqlQuery = " SELECT DISTINCT SCHEMEID FROM SIPDATEINFO ";

            schemes = jdbcTemplate.query(sqlQuery, new Object[]{}, (rs, rowNumber) -> rs.getString("SCHEMEID"));

        } catch (Exception e) {
            log.error("Exception while fetching restricted schemes: ", e);
        }

        return schemes;
    }

    public CustomerBankInfo verifyIFSC(String ifsccode,CustomerBankInfo customerBankInfo) {

        String ifsc=ifsccode.substring(0,3);
        try {

            String sqlQuery = " select bank,ifsccode,micrcode,branchname,address as branchaddress,city,state,phone,(select lookupvalue from lookups where lookupkey=substring(ifsccode,0,5)) as banklookupid\n" +
                    " from neftcode_rbi where ifsccode=? ";

             jdbcTemplate.query(sqlQuery, new Object[]{ifsccode}, (rs, rowNumber) -> {

                 customerBankInfo.setBankName(rs.getString("bank"));
                 customerBankInfo.setIfscCode(rs.getString("ifsccode"));
                 customerBankInfo.setMicrCode(StringUtils.isEmpty(rs.getString("micrcode"))?"":rs.getString("micrcode"));
                 customerBankInfo.setBranchName(rs.getString("branchname"));
                 customerBankInfo.setBranchAddress(rs.getString("branchaddress"));
                 customerBankInfo.setBankCity(rs.getString("city"));
                 customerBankInfo.setBankState(rs.getString("state"));
                 customerBankInfo.setPhone(rs.getString("phone"));
                 customerBankInfo.setBanklookupId(rs.getString("banklookupid"));
                 return customerBankInfo;
            });

        } catch (Exception e) {
            log.error("Exception in getting customerBankInfo : ", e);
        }
        return customerBankInfo;
    }


}

package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MandateInfoBO {

    private String consumerCode;
    private String customerId;
    private String holdingProfileId;
    private String name;
    private String expiresOn;
    private double availableAmount;
    private int maxAmount;
    private String status;
    private String bankNameAccNo;
    private String remarks;
    private Boolean activated;
    private BankInfoBO bank;

}

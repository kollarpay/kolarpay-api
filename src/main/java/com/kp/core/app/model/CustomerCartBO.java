package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerCartBO {

    private String customerId;
    private String productId;
    private String investorName;
    private String paymentId;
    private String paymentMode;
    private String paidStatus;
    private String transaction_at;
    private AppInfoBO appInfo;
    private int userId;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CartDataBO {

        private int totalAmount;
        private String investmentType;
        private List<CustomerCartBO.Schemes> schemes;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Schemes {

        private String id;
        private Boolean payment;
        private String schemeName;
        private String schemeCode;
        private String folio;
        private int amount;
        private int ecsDate;
        private String nextInstallmentOn;
        private String subscriptionType;
        private CustomerCartBO.SubscriptionDetail regular;
        private CustomerCartBO.SubscriptionDetail flexi;
        private CustomerCartBO.SubscriptionDetail stepup;
        private CustomerCartBO.SubscriptionDetail alert;
        private int userTransRefId;
    }
    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SubscriptionDetail {

        private String consumerCode;
        private int tenure;
        private int ecsDate;
        private int amount;
        private String frequency;
        private int maximumAmount;
        private String stepupfrequeny;
        private int stepupAmount;
        private int finalAmount;
        private String endDate;
        private String startDate;

        @Override
        public String toString() {

            String subscriptiondetails = "{\"amount\" : " + amount ;

            if(frequency!=null)
                subscriptiondetails += ",\"frequency\" : \"" + frequency + "\"";
            if(stepupfrequeny!=null)
                subscriptiondetails += ",\"stepupfrequeny\" : \"" + stepupfrequeny + "\"";
            if(stepupAmount!=0)
                subscriptiondetails += ",\"stepupAmount\" : " + stepupAmount;
            if(finalAmount!=0)
                subscriptiondetails += ",\"finalAmount\" : " + finalAmount;
            if(ecsDate!=0)
                subscriptiondetails += ",\"ecsDate\" : " + ecsDate;
            if(tenure!=0)
                subscriptiondetails += ",\"tenure\" : " + tenure;
            if(maximumAmount!=0)
                subscriptiondetails += ",\"maximumAmount\" : " + maximumAmount;
            if(consumerCode!=null)
                subscriptiondetails += ",\"consumerCode\" : \"" + consumerCode + "\"";
            if(endDate!=null)
                subscriptiondetails += ",\"endDate\" : \"" + endDate + "\"";
            if(startDate!=null)
                subscriptiondetails += ",\"startDate\" : \"" + startDate + "\"";

            subscriptiondetails += "}";

            return subscriptiondetails;
        }
    }

}

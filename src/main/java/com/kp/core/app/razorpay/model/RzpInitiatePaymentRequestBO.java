package com.kp.core.app.razorpay.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class RzpInitiatePaymentRequestBO {

    private BigInteger amount;
    private String currency;
    @SerializedName("order_id")
    @JsonProperty("order_id")
    private String orderId;
    private String email;
    private String contact;
    private String method;
    @SerializedName("customer_id")
    @JsonProperty("customer_id")
    private String customerId;
    private String save;
    private String ip;
    private String referer;
    private String user_agent;
    private String description;
    private InitiatePaymentNotesBO notes;
    private UpiBO upi;
    private String bank;
    private String bankCode;
    @SerializedName("callback_url")
    @JsonProperty("callback_url")
    private String callbackUrl;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)

    public static class UpiBO {

        private String flow;
        private String vpa;
        @SerializedName("expiry_time")
        @JsonProperty("expiry_time")
        private String expiryTime;

        public UpiBO(String flow) {
            this.flow = flow;
        }

        public UpiBO(String flow, String vpa, String expiryTime) {

            this.flow = flow;
            this.vpa = vpa;
            this.expiryTime = expiryTime;
        }
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)

    public static class InitiatePaymentNotesBO {

        private String fi_payment_id;
        private String fi_client_id;
        private String fi_product_type;

        public InitiatePaymentNotesBO(String fi_payment_id, String fi_client_id, String fi_product_type) {
            this.fi_payment_id = fi_payment_id;
            this.fi_client_id = fi_client_id;
            this.fi_product_type = fi_product_type;
        }
    }
}

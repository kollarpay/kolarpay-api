package com.kp.core.app.controller.portfolioreview;

import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.service.AdvisorPortfolioReview.SchemeSearchService;
import com.kp.core.app.utils.APIGeneralUtil;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;


@RestController
@Slf4j
@RequestMapping("/portfolio-review")
public class SchemeSearchController {

    @Autowired
    SchemeSearchService schemeSearchService;

    /**
     * get list of AMC's, Categories and SubCategories of schemes
     * @param headers
     * @return
     * @throws Exception
     */
    @GetMapping(path = "/scheme-search/form", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> schemeSearchForm(@RequestHeader MultiValueMap<String, Object> headers) throws Exception {

        log.debug("#scheme-search/form API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "SchemeSearchForm");
        int advUserId = 0;
        try {
            advUserId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#AdvUserId : " + advUserId);

            if(advUserId > 0) {
                globalResponseBO = schemeSearchService.getSchemeSearchForm(advUserId);
            }

        } catch (Exception e) {
            log.error("#AdvUserId : " + advUserId + "| Exception : ", e);
        }
        log.debug("#scheme-search/form API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    /**
     * get list of schemes satisfying provided conditions
     * save list in redis cache
     * @param headers
     * @param payload
     * @return
     * @throws Exception
     */
    @PostMapping(path = "/scheme-search", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> searchInvestor(@RequestHeader MultiValueMap<String, Object> headers,
                                                           @RequestBody String payload) throws Exception {

        log.debug("#scheme-search POST API Request :" + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "InvestorDetail");
        int advUserId = 0;
        try {
            advUserId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#AdvUserId : " + advUserId);

            if(advUserId > 0) {
                JSONObject jsonObject = GeneralUtil.parseJson(payload);
                String amc = (String) jsonObject.get("amc");
                String category = (String) jsonObject.get("category");
                String subCategory = (String) jsonObject.get("subCategory");
                String query = (String) jsonObject.get("query");
                String searchType = (String) jsonObject.get("filterBy");
                String order = (String) jsonObject.get("sortBy");
                String[] investors = GeneralUtil.convertJSONArray((JSONArray) jsonObject.get("investors"));

                globalResponseBO = schemeSearchService.getSchemeSearchList(amc, category, subCategory, query, searchType, investors, order, advUserId);
            }

        } catch (Exception e) {
            log.error("#AdvUserId : " + advUserId + "| Exception : ", e);
        }
        log.debug("#scheme-search POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);

    }

    /**
     * get list of schemes for given page number
     * fetch list from redis cache
     * @param headers
     * @param page
     * @param id
     * @return
     * @throws Exception
     */
    @GetMapping(path = "/scheme-search", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> schemeSearchForm(@RequestHeader MultiValueMap<String, Object> headers,
                                                             @RequestParam("page") int page,
                                                             @RequestParam("id") String id) throws Exception {

        log.debug("#scheme-search GET API Request | page : " + page + " | #id : " + id);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "SchemeSearchForm");
        int advUserId = 0;
        try {
            advUserId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#AdvUserId : " + advUserId);

            if(advUserId > 0) {
                globalResponseBO = schemeSearchService.getSchemeSearchListPagination(page, id, advUserId);
            }

        } catch (Exception e) {
            log.error("#AdvUserId : " + advUserId + "| Exception : ", e);
        }
        log.debug("#scheme-search GET API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

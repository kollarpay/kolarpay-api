package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerInfoBO {

    private String name;
    private String customerId;
    private int userId;
    private String pan;
    private String email;
    private String password;
    private String enPassword;
    private String mobile;
    private String type;
    private Boolean activated;
    private String dateOfBirth;
    private String gender;
    private String nationality;
    private Boolean nri;
    private String allowTransact;
    private boolean isBankVerified;
    private boolean verified_email;
    private boolean verified_mobile;
    private String last_order_id;
    private float total_spent;
    private String tags;
    private List<NameValueBO> notes;
    private List<BankInfoBO> banks;
    private List<AddressInfo> address;
    private int bpid=0;
}

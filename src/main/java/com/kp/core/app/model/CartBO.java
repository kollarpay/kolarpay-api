package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CartBO {

    @SerializedName("id")
    @Expose
    private String id;
    @Expose
    @SerializedName("cart_id")
    private String cartID;
    @SerializedName("userid")
    private String userId;
    @SerializedName("sessionId")
    @Expose
    public String sessionId;
    @SerializedName("firstname")
    @Expose
    public String firstname;
    @SerializedName("middlename")
    @Expose
    public String middlename;
    @SerializedName("lastname")
    @Expose
    public String lastname;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("line1")
    @Expose
    public String line1;
    @SerializedName("line2")
    @Expose
    public String line2;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("remarks")
    @Expose
    public String remarks;
    @SerializedName("token")
    @Expose
    public String token;
    @SerializedName("active")
    @Expose
    public String active;
    @SerializedName("paymentid")
    @Expose
    public String paymentid;
    @SerializedName("medium")
    @Expose
    public String medium;
    @SerializedName("appdata")
    @Expose
    public String appdata;
    @SerializedName("created_user")
    @Expose
    public String created_user;
    @SerializedName("updated_user")
    @Expose
    public String updated_user;
    @SerializedName("ipaddress")
    @Expose
    public String ipaddress;
    @SerializedName("expires_at")
    @Expose
    public String expires_at;
    @Expose
    private List<CartItemBO> items;
    @SerializedName("type")
    public String type;


}

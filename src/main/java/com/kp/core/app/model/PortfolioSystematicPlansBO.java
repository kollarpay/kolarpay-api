package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortfolioSystematicPlansBO {

    private SystematicInvestmentPlan sip;
    private SystematicTransferPlan stp;
    private SystematicInvestmentPlan swp;
    private SystematicInvestmentPlan vip;
    private SystematicTransferPlan vtp;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SystematicTransferPlan {

        private List<PortfolioSchemeSystematicPlanBO> schemes;
        private double allocation;
        private int allocationAmount;
        private int currentStpAmount;
        private int proposedStpAmount;
        private double percentageOfPortfolio;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SystematicInvestmentPlan {

        private List<PortfolioSchemeSystematicPlanBO> schemes;
        private int total;
        private int proposedTotal;
    }
}

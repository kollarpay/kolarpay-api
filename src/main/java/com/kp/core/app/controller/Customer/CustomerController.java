package com.kp.core.app.controller.Customer;
import com.google.gson.Gson;
        import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.*;
import com.kp.core.app.service.Customer.CustomerService;
import com.kp.core.app.utils.APIGeneralUtil;
import lombok.extern.slf4j.Slf4j;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.MediaType;
        import org.springframework.http.ResponseEntity;
        import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

        import java.util.ArrayList;
        import java.util.List;

@RestController
@Slf4j
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    Gson gson;

    @Autowired
    CustomerService customerService;


    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> saveCustomer(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#create customer POST API Request : " + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "createCustomer");
        List<ErrorBO> errorBoList = new ArrayList<>();
        int userId = 0;
        try {

            CustomerRequest customerRequest = gson.fromJson(payload, CustomerRequest.class);
            globalResponseBO = customerService.saveCustomer(customerRequest,errorBoList,true,"createCustomer");


        } catch (Exception e) {
            log.error("#AdvUserid : " + userId + "| Exception : ", e);
        }
        log.debug("#User create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }

    @PostMapping(path = "/signup", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> signupUser(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#saveuser customer POST API Request : " + payload);
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "signup");
        List<ErrorBO> errorBoList = new ArrayList<>();
        int userId = 0;

        try {
            userId = APIGeneralUtil.getUserIdFromHeader(headers);
            if(userId>0) {

                CustomerRequest customerRequest = gson.fromJson(payload, CustomerRequest.class);
                customerRequest.setUserId(userId);
                globalResponseBO = customerService.saveUserCustomer(userId,customerRequest, errorBoList, true, "saveUserCustomer");
            }

        } catch (Exception e) {
            log.error("#AdvUserid : " + userId + "| Exception : ", e);
        }
        log.debug("#User create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/info", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> ProductByTag(@RequestParam("type") String type,@RequestParam("str") String SearchStr,@RequestHeader MultiValueMap<String, Object> headers, @RequestBody(required = false) String payload) throws Exception {

        log.debug("# GET CustomerInfo by type API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "getCustomerInfoBySearchType");

        int userId = 0;
        int intType = 0;
        List<ErrorBO> errorBoList = new ArrayList<>();
        try {
            log.debug("#UserId : " + userId);
            if(!StringUtils.isEmpty(type)) intType=Integer.parseInt(type);

            if(intType > 0) {
                globalResponseBO = customerService.getCustomer(intType,SearchStr,errorBoList,"getCustomerInfoBySearchType");
            }

        } catch (Exception e) {
            log.error("#UserId : {}| Exception : ", userId, e);
        }
        log.debug("#getCustomerInfoBySearchType API Response : {}", globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/list", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> getCustomerList(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody(required = false) String payload) throws Exception {

        log.debug("# GET Customer list API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "getCustomerInfoBySearchType");

        int userId = 0;
        List<ErrorBO> errorBoList = new ArrayList<>();
        try {
            userId = APIGeneralUtil.getUserIdFromHeader(headers);

            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = customerService.getCustomers(userId,errorBoList,"getCustomerList");
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#getCustomerList API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/banklist", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> getCustomerBankList(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody(required = false) String payload) throws Exception {

        log.debug("# GET Customer bank list  by userid API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "getCustomerInfoBySearchType");

        int userId = 0;
        int intType = 0;
        List<ErrorBO> errorBoList = new ArrayList<>();
        try {
            userId = APIGeneralUtil.getUserIdFromHeader(headers);

            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = customerService.getCustomerBanks(userId,errorBoList,"getCustomerBankList");
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#getCustomerBankList API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
    @GetMapping(path = "/customer-bank-map", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> getCustomerBankMap(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody(required = false) String payload) throws Exception {

        log.debug("# GET Customer bank map  by userid API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "getCustomerInfoBySearchType");

        int userId = 0;
        int intType = 0;
        List<ErrorBO> errorBoList = new ArrayList<>();
        try {
            userId = APIGeneralUtil.getUserIdFromHeader(headers);

            log.debug("#UserId : " + userId);

            if(userId > 0) {
                globalResponseBO = customerService.getCustomerBankMap(userId,errorBoList,"getCustomerBankList");
            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#getCustomerBank map API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}


package com.kp.core.app.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Reloadable;

import java.util.concurrent.TimeUnit;

@Config.Sources("file:/data/logs/brospay/config/properties/sendmailconfig.properties")
//@Config.Sources("file:/usr/local/logs/bros/config/properties/sendmailconfig.properties")
@Config.HotReload(type = Config.HotReloadType.ASYNC, unit = TimeUnit.MINUTES, value = 10)

public interface SendMailConfiguration extends Reloadable {

    @Config.Key("info.brospay.sendGridKey")
    String sendgridApiKey();

    @Config.Key("partner.brospay.sendGridKey")
    String partnerSendgridApiKey();

}

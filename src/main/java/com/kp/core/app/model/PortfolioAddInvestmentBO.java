package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortfolioAddInvestmentBO {

    private String reviewId;
    private int schemeCode;
    private int amount;
    private int proposedAmount;
    private double percentage;
    private double proposedAllocation;
    private PortfolioSchemeSystematicPlanBO sip;
    private List<PortfolioSchemeSystematicPlanBO> stp;
    private List<PortfolioSchemeSystematicPlanBO> vtp;
    private PortfolioSchemeSystematicPlanBO vip;
    private PortfolioSchemeSystematicPlanBO swp;

}

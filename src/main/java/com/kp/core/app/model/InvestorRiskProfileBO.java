package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvestorRiskProfileBO {

    private int riskProfileId;
    private String riskProfileType;
    private int goalId;
    private int userId;
    private double equity;
    private double debt;
    private double gold;
    private Boolean questionnaire;
}

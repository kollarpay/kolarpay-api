package com.kp.core.app.dao;

import com.google.gson.Gson;
import com.kp.core.app.model.request.OrderBO;
import com.kp.core.app.utils.GeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;

@Slf4j
@Repository
public class OrderDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AutoNumberGenerator autoNumberGenerator;

    @Autowired
    private GeneralInfoDAO generalInfoDAO;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GeneralUtil generalUtil;

    @Autowired
    Gson gson;


    public String createOrder(OrderBO orderBO, int userid) {

        String logPrefix = "#customerID : " + orderBO.getCustomerID() + " | #UserId : " + userid;
        log.debug(logPrefix + "InvestorTransaction Insert");

        String orderID="";

        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();

            String insertQuery_ORDER = "INSERT INTO orders " +
                    " (customerID,paymentgatewayID,ordertype,ordermode,order_dt, active,medium, " +
                    "  shipped_dt,order_time, created_user,updated_user,created_at,updated_at,ipaddress, " +
                    " remarks) " +
                    " VALUES (?, ?, ?, ?, now(), ?, ?," +
                    " now(), now(), ?, ?,now(),now(),?,?)";

            int finalUserid = userid;
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(insertQuery_ORDER, new String[] { "orderid" });
                ps.setInt(1, Integer.parseInt(orderBO.getCustomerID()));
                ps.setString(2, orderBO.getPaymentGatewayID());
                ps.setString(3, orderBO.getOrderType());
                ps.setInt(4, orderBO.getOrderMode());
                ps.setString(5, "A");
                ps.setString(6, orderBO.getMedium());
                ps.setInt(7, finalUserid);
                ps.setInt(8, finalUserid);
                ps.setString(9, orderBO.getIpAddress());
                ps.setString(10, orderBO.getRemarks());
                return ps;

            }, keyHolder);
            log.info("keyHolder size()-----"+keyHolder.getKeys().size());
            if (keyHolder.getKeys().size() > 0) {
                orderID = String.valueOf(keyHolder.getKeys().get("orderid"));
                if(!StringUtils.isEmpty(orderID)){
                    log.info("orderID--"+orderID);
                    orderBO.setOrderID(orderID);
                }
            }

        } catch (Exception e) {
            log.error(logPrefix + "Exception in orders insert : ", e);
        }

        return orderID;
    }
}

package com.kp.core.app.utils;

public class MFErrorsConstants extends BusinessErrorsConstants {


    //GENERAL ERROR CODES
    public static final String MANDATORY_FIELD_NOT_EMPTY = "11001";
    public static final String FIELD_LENGTH_MISMATCH = "11002";
    public static final String DATATYPE_MISMATCH = "11003";
    public static final String INVALID_DATA = "11004";
    public static final String NO_DATA_FOUND = "11005";
    public static final String MINIMUM_MAXIMUM = "11006";
    public static final String PISITIVE_NUMBERS = "11007";
    public static final String INVALID_DATE_FORMAT = "11008";
    public static final String MANDATORY_FIELD_TAG_NOT_FOUND = "11009";
    public static final String INVALID_JSON_TAG = "11010";


    //GOAL CREATION
    public static final String GOAL_UNIQUE = "11031";
    public static final String GOAL_TYPE_VALUE = "11032";
    public static final String GOAL_AMOUNT_MINMAX = "11033";
    public static final String GOAL_TARGET_VAL = "11034";
    public static final String GOAL_INFLATION_MINMAX = "11035";
    public static final String GOAL_CUSTOM_ALLOCATION = "11036";

    //CART CREATION - General
    public static final String GOAL_TARGET_DATE_FORMAT_INVALID = "11037";
    public static final String CART_PRODUCT_TYPE_VALIDATION = "11038";
    public static final String CART_INVESTMENT_TYPE_VALIDATION = "11039";
    public static final String GOAL_OTI_TOTAL_MISMATCH = "11040";
    public static final String SCHEME_MIN_MISMATCH = "11041";
    public static final String INVESTMENT_OPTION_VALIDATE = "11042";
    public static final String DIVIDEND_OPTION_VALIDATE = "11043";
    public static final String GOAL_SIP_TOTAL_MISMATCH = "11044";
    public static final String GOAL_RISK_PROFILE_MISSING = "11045";
    public static final String FREQUENCY_VALIDATE = "11046";
    //CART CREATION - Business
    public static final String CART_NRI_RESTRICTION = "13001";
    public static final String CART_NRI_MULTIPLE_AMC_RESTRICTION = "13002";
    public static final String CART_NRI_NOT_ALLOWED_SCHEME = "13003";
    public static final String CART_MINOR_SCHEME = "13004";
    public static final String CART_LQ_SCHEME_NOT_ALLOWED = "13005";
    public static final String CART_NOT_AVAILABLE_PUR = "13006";
    public static final String CART_OE_RESTRICTED_AMC = "13007";
    public static final String CART_LUMPSUM_NOT_SUPPORT = "13008";
    public static final String CART_MIN_EXCEED_EKYC = "13009";
    public static final String CART_SCHEME_MIN_EXCEED_PERDAY = "13010";
    public static final String CART_AADHAR_INVESTOR_LIMIT = "13011";
    public static final String CART_MANDATE_AVAILABLE_LIMIT = "13012";

    public static final String DB_ERROR = "15001";
    public static final String SYSTEM_ERROR = "11999";



}

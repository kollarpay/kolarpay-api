package com.kp.core.app.dao;

import com.kp.core.app.mapper.CustomerMapper;
import com.kp.core.app.model.*;
import com.kp.core.app.utils.DateUtil;
import com.kp.core.app.utils.EncryptionDecryption;
import com.kp.core.app.utils.BPConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.sql.Date;
import java.util.*;


/**
 * Created by rsuresh on 25-09-2021.
 */
@Slf4j
@Repository
public class CustomerDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private EncryptionDecryption encryptionDecryption;




    public String saveCustomer(CustomerRequest customerRequest) {

        String logPrefix = "#customerId :  ";

        log.debug(logPrefix + "Insert customer basic detail");
        String customerId="";

        try {
           log.info("dob--"+customerRequest.getDob()+"--getName"+customerRequest.getName()+"---dob"+customerRequest.getDob()+"password--"+customerRequest.getPassword()+"---email"+customerRequest.getEmailId()+"--mobilenumber--"+customerRequest.getMobileNumber());
            KeyHolder keyHolder = new GeneratedKeyHolder();

            String sqlQuery = " INSERT INTO customerbasicdetail\n" +
                    "(name,dob,userid,created_user,updated_user, allow_trans, flag, customerlimit)  " +
                    " VALUES( ?, ?, ?, ?, ?, ?, ?, ?)  ";

               jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, customerRequest.getName());
                ps.setDate(2, Date.valueOf(DateUtil.getDBDate(customerRequest.getDob(),"dd-MM-YYYY")));
                ps.setInt(3, customerRequest.getUserId());
                ps.setInt(4, customerRequest.getUserId());
                ps.setInt(5, customerRequest.getUserId());
                ps.setString(6, "D");
                ps.setString(7, "A");
                ps.setInt(8, 49999);
                return ps;

            }, keyHolder);

            if (Objects.requireNonNull(keyHolder.getKeys()).size() > 1) {
                customerId = String.valueOf(keyHolder.getKeys().get("customerid"));
                if(!StringUtils.isEmpty(customerId)){
                    customerRequest.setCustomerId(customerId);
                }
            }


        } catch (Exception e) {
            log.error(logPrefix + "Exception in customer basic detail create insertion: ", e);
        }

        log.debug(logPrefix + "customerId : " + customerId);
        return customerId;
    }
    public int userMobileRegistrationInsert(UserRequest userRequest) {

        String logPrefix = "#UserId :  ";

        log.debug(logPrefix + "Insert Userinfo using mobile no");
        int userid=0;
        String encryptUserPassword;

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();

            String sqlQuery = " INSERT INTO userinfo (password_hash,first_name,last_name, email, mobileNumber,dob,gender,profile_picture, region,userType, verified, partnerid, " +
                    " flag, pid, smsflag,emailflag, referredby,usercomment,ipaddress, lockFlag,lockcount,superid,created_by,updated_by)  " +
                    " VALUES(?,?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,currval('userinfo_userid_seq'),currval('userinfo_userid_seq'))  ";

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery, new String[] { "userid" });
                ps.setString(1, "");
                ps.setString(2, "");
                ps.setString(3, "");
                ps.setString(4, "");
                ps.setString(5, userRequest.getMobileno());
                ps.setString(6, "IN");
                ps.setString(7, "RU");
                ps.setString(8, "A");
                ps.setInt(9, 3001);//partnerid
                ps.setString(10, "");
                ps.setString(11, "");
                ps.setString(12, "A");
                ps.setInt(13, userRequest.getPid());
                ps.setString(14, "A");
                ps.setString(15, "");
                ps.setString(16, "");
                ps.setString(17, "");
                ps.setString(18, "");
                ps.setString(19, "");
                ps.setString(20, "");
                ps.setString(21, "A");
                ps.setInt(22, 0);
                ps.setInt(23, 0);
                return ps;

            }, keyHolder);
            userid=keyHolder.getKey().intValue();

        } catch (Exception e) {
            log.error(logPrefix + "Exception in user create insertion: ", e);
        }

        log.debug(logPrefix + "userid : " + userid);
        return userid;
    }
    public boolean otpGenInsert(UserRequest userRequest,int userid) {
        String logPrefix = "#UserId :  ";
        log.debug(logPrefix + "Insert OTP_GEN_VERIFICATION ");
        boolean isInserted=false;
        try {
            String sqlQuery = " INSERT INTO OTP_GEN_VERIFICATION (userId,msgType,productType,referenceId,otp,created_at,createdUser,modified_at,modifiedUser,flag,expires_at)  " +
                    " VALUES(?, ?, ?, ?, ?,NOW(),?,NOW(),?,'A',NOW()+ interval '5 minute') ";

         int count= jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sqlQuery);
                ps.setInt(1, userid);
                ps.setString(2, userRequest.getMessageType());
                ps.setString(3, userRequest.getProductType());
                ps.setString(4, userRequest.getReferenceId());
                ps.setString(5, userRequest.getSmsOTP());
                ps.setInt(6, userid);
                ps.setInt(7, userid);
                return ps;
            });
         if(count>0){
          isInserted=true;
         }

        } catch (Exception e) {
            log.error(logPrefix + "Exception in user create insertion: ", e);
        }

        log.debug(logPrefix + "isInserted : " + isInserted);
        return isInserted;
    }

    public UserInfoBO getUserInfoBO(int typeCode,String name) {

        String logPrefix = " | UserID: " + typeCode + " | ";
        log.debug("{} get user mail id. ", logPrefix);
        UserInfoBO userInfoBO=new UserInfoBO();
        String appendSql="";
        if(typeCode==1){
            appendSql=" UI.EMAIL=? ";
        }else if(typeCode==2) {
            appendSql = " UI.MOBILENUMBER=? ";
        }

        try {

            String sql = " SELECT UI.NAME, UI.EMAIL, UI.MOBILENUMBER, UI.PID FROM USERINFO UI WHERE "+appendSql;
                    jdbcTemplate.query(sql, new Object[]{name}, (rs, rowNumber) -> {
                    userInfoBO.setName(rs.getString("NAME"));
                    userInfoBO.setEmailId(rs.getString("EMAIL"));
                    userInfoBO.setMobile(rs.getString("MOBILENUMBER"));
                    userInfoBO.setPId(rs.getInt("PID"));
                return userInfoBO;
            });

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        log.debug(logPrefix + "User Info : " );
        return userInfoBO;
    }
    public OTPBO getOTPDetailsToVerify(int userId,String referenceId) throws Exception {
        String logPrefix = "Fetch OTP details from db | UserId: " + userId + " ";

        log.debug(logPrefix + " getOTPDetailsToVerify ");
        OTPBO otpbo=new OTPBO();
        String appendSql="";
        if(referenceId!=""){
          appendSql=" REFERENCEID ='"+referenceId+"' ";
        }
        try {
            String sql = " SELECT OTP, PRODUCTTYPE, MESGTYPE FROM OTP_GEN_VERIFICATION WHERE USERID = ? "+appendSql+" AND FLAG = 'A'";
            jdbcTemplate.query(sql, new Object[]{userId}, (rs, rowNumber) -> {
                otpbo.setSmsOTP(rs.getString("OTP"));
                otpbo.setProductType(rs.getString("PRODUCTTYPE"));
                otpbo.setMsgType(rs.getString("MESGTYPE"));
                return otpbo;
            });

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        log.debug(logPrefix + "OTP_GEN_VERIFICATION : " );
        return otpbo;
    }

    public boolean isOTPExpired(OTPBO otpbo) throws Exception {
        String logPrefix = "Fetch OTP details from db | UserId: " + otpbo.getUserId() + " ";
        boolean isExpried=false;
        log.debug(logPrefix + " getOTPDetailsToVerify ");
        int userId=otpbo.getUserId();
         String referenceId=otpbo.getReferenceId();
        log.info(referenceId+"userId---"+userId);
        try {
            //String sqlQuery = " SELECT otp,REFERENCEID FROM OTP_GEN_VERIFICATION WHERE USERID = ? AND REFERENCEID=?  AND FLAG = 'A'";
             String sqlQuery = " SELECT otp FROM OTP_GEN_VERIFICATION WHERE USERID = ? AND REFERENCEID=? AND expires_at>now() AND FLAG = 'A'";
            log.info("userId---"+sqlQuery);

            jdbcTemplate.query(sqlQuery, new Object[]{userId, referenceId}, (rs, rowNumber) -> {
                otpbo.setSmsOTP(rs.getString("OTP"));
                return otpbo;
            });
            if(!StringUtils.isEmpty(otpbo.getSmsOTP())){
                isExpried=true;
            }

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        log.debug(logPrefix + "OTP_GEN_VERIFICATION : " );
        return isExpried;
    }
    public List<CustomerInfoBO> getCustomerList(int userID) {

        String logPrefix = "#userID : " + userID + " | ";

        log.debug(logPrefix + "customerBasic details fetch");

        List<CustomerInfoBO> customerInfoBOList=new ArrayList<>();

        try {

            String sqlQuery = " select u.userid,c.customerid,c.name,c.allow_trans,c.customerlimit,c.dob from userinfo u join customerbasicdetail c on u.userid=c.userid " +
                    " where c.userid=? ";

            customerInfoBOList=  jdbcTemplate.query(sqlQuery, new Object[]{userID}, (rs, rowNumber) -> {
                CustomerInfoBO customerInfoBO = new CustomerInfoBO();
                String allowTransact = StringUtils.isNotEmpty(rs.getString("allow_trans"))? rs.getString("allow_trans").trim() : "";
                int customerLimit = rs.getInt("customerlimit");
                customerInfoBO.setName(rs.getString("name"));
                customerInfoBO.setUserId(rs.getInt("userid"));
                customerInfoBO.setCustomerId(rs.getString("customerid"));
                customerInfoBO.setActivated(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && customerLimit!=49999);
                customerInfoBO.setDateOfBirth(rs.getString("dob"));
                return customerInfoBO;
            });

        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        log.debug("getCustomerList::userId:::"+userID+"customerInfo size : " + customerInfoBOList.size());

        return customerInfoBOList;
    }
    public List<CustomerBankInfo> getCustomerBankList(int userID) {

        String logPrefix = "#userID : " + userID + " | ";

        log.debug("{}customerBank details fetch", logPrefix);

        List<CustomerBankInfo> bankInfoList=new ArrayList<>();

        try {

            String sqlQuery = "select cbi.id,u.userid,c.customerid,cbi.accountname,u.mobilenumber,u.email,c.allow_trans,c.customerlimit,\n" +
                    " cbi.accountnumber,cbi.ifsccode,cbi.banklookupid,l.lookupvalue as bankname,s.lookupdescription as svg,cbi.userbankid,cbi.verified,cbi.bankcategory \n" +
                    " from userinfo u join customerbasicdetail c on u.userid=c.userid \n" +
                    " join customerbankinfo cbi on cbi.customerid=c.customerid\n" +
                    " join lookups l on l.lookupkey=cbi.banklookupid and l.lookuptype='BANKLIST'\n" +
                    " join svg s on s.lookupkey=l.lookupkey and s.lookuptype='BANKLIST'\n" +
                    " where c.userid=? and cbi.active=1 order by cbi.customerid,cbi.userbankid asc";

            bankInfoList=  jdbcTemplate.query(sqlQuery, new Object[]{userID}, (rs, rowNumber) -> {
                CustomerBankInfo bankInfoBO = new CustomerBankInfo();
                bankInfoBO.setId(rs.getString("id"));
                bankInfoBO.setReferenceId(rs.getString("customerid")+"@"+rs.getString("accountname"));
                bankInfoBO.setAccountName(rs.getString("accountname"));
                bankInfoBO.setCustomerId(rs.getString("customerid"));
                bankInfoBO.setCustomerEmailId(rs.getString("email"));
                bankInfoBO.setCustomerMobileNumber(rs.getString("mobilenumber"));
                bankInfoBO.setAccountNumber(rs.getString("accountnumber"));
                bankInfoBO.setIfscCode(rs.getString("ifsccode"));
                bankInfoBO.setBanklookupId(rs.getString("banklookupid"));
                bankInfoBO.setBankName(rs.getString("bankname"));
                bankInfoBO.setSvg(rs.getString("svg"));
                bankInfoBO.setUserBankId(rs.getString("userbankid"));
                bankInfoBO.setBankCategory(rs.getString("bankcategory"));
                bankInfoBO.setVerified(rs.getInt("verified"));
                return bankInfoBO;
            });

        } catch (Exception e) {
            log.error("{}Exception : ", logPrefix, e);
        }

        log.debug("getCustomer Bank List::userId:::{}customer bank size : {}", userID, bankInfoList.size());

        return bankInfoList;
    }
    public void getCustomerAddress(String customerId,CustomerInfoBO customerInfoBO) {

        String logPrefix = "#customerId : " + customerId + " | ";

        log.debug(logPrefix + "customerAddress details fetch");

        List<AddressInfo> addressInfoList = new ArrayList<>();
        int customerID=Integer.parseInt(customerId);

        try {
            String sqlQuery = "select addressid,customerid,addresstype,addressline1,addressline2,landmark,\"location\",city,state,pincode,mobileno,workphone,email,userid,flag from customeraddress where customerid=? ";

            addressInfoList=  jdbcTemplate.query(sqlQuery, new Object[]{customerID}, (rs, rowNumber) -> {
                AddressInfo addressInfo = new AddressInfo();
                addressInfo.setAddressId(rs.getString("addressid"));
                addressInfo.setCustomerId(rs.getString("customerid"));
                addressInfo.setAddressType(rs.getString("addresstype"));
                addressInfo.setAddressLine1(rs.getString("addressline1"));
                addressInfo.setAddressLine2(rs.getString("addressline2"));
                addressInfo.setLandmark(rs.getString("landmark"));
                addressInfo.setLocation(rs.getString("location"));
                addressInfo.setCity(rs.getString("city"));
                addressInfo.setState(rs.getString("state"));
                addressInfo.setPinCode(rs.getString("pincode"));
                addressInfo.setEmail(rs.getString("email"));
                addressInfo.setMobileNumber(rs.getString("mobileno"));
                addressInfo.setWorkPhone(rs.getString("workphone"));
                addressInfo.setFlag(rs.getString("flag"));
                return addressInfo;
            });
            if(!addressInfoList.isEmpty()) customerInfoBO.setAddress(addressInfoList);
        } catch (Exception e) {
            log.error(logPrefix + "customerAddress Exception : ", e);
        }


        log.debug("customer Address List : " + addressInfoList.size());

    }
    public void getCustomerBankInfo(String customerId,CustomerInfoBO customerInfoBO) {

        String logPrefix = "#customerId : " + customerId + " | ";

        log.debug("{}customerbankinfo details fetch", logPrefix);

        List<BankInfoBO> bankInfoBOList = new ArrayList<>();
        int customerID=Integer.parseInt(customerId);

        try {

            String sqlQuery = " select cbi.customerid,l.lookupkey as banklookupid,l.lookupvalue as bankName,cbi.accountnumber,nr.ifsccode,nr.micrcode as micrcode,nr.ifsccode,nr.address as branchAddress,nr.city as branchCity,nr.state,nr.phone,cbi.userbankid,cbi.verified,cbi.bankaccounttype,cbi.active from \n" +
                    "\tcustomerbankinfo cbi \n" +
                    "\tjoin neftcode_rbi nr on cbi.ifsccode=nr.ifsccode\n" +
                    "\tjoin lookups l on l.lookupkey =cbi.banklookupid and l.lookuptype='BANKLIST'\n" +
                    "\twhere cbi.customerid=? and cbi.active>0 ";

            bankInfoBOList=  jdbcTemplate.query(sqlQuery, new Object[]{customerID}, (rs, rowNumber) -> {
                BankInfoBO bankInfoBO = new BankInfoBO();
                boolean activated=false;
                bankInfoBO.setCustomerId(rs.getString("customerid"));
                bankInfoBO.setBankLookUpId(rs.getString("banklookupid"));
                bankInfoBO.setBankId(rs.getString("userbankid"));
                bankInfoBO.setBankName(rs.getString("bankName"));
                bankInfoBO.setAccountNo(rs.getString("accountnumber"));
                bankInfoBO.setIfsc(rs.getString("ifsccode"));
                bankInfoBO.setMicr(rs.getString("micrcode"));
                bankInfoBO.setBranchAddress(rs.getString("branchAddress"));
                bankInfoBO.setBranchCity(rs.getString("branchCity"));
                bankInfoBO.setAccountType(rs.getString("bankaccounttype"));
                bankInfoBO.setVerified(rs.getString("verified"));
                if(rs.getInt("active")>0) activated=true;
                bankInfoBO.setActivated(activated);
                return bankInfoBO;
            });
            if(!bankInfoBOList.isEmpty()) customerInfoBO.setBanks(bankInfoBOList);
        } catch (Exception e) {
            log.error("{}customerBankInfo Exception : ", logPrefix, e);
        }


        log.debug("customer Address List : " + bankInfoBOList.size());

    }
    public CustomerInfoBO getCustomerInfo(String customerid) {

        String logPrefix = "#customerId : " + customerid + " | ";

        log.debug(logPrefix + "customerBasic details fetch");

        CustomerInfoBO customerInfoBO = new CustomerInfoBO();
        int customerID=Integer.parseInt(customerid);

        try {

            String sqlQuery = " select u.userid,c.customerid,c.name,c.mobilenumber,c.email,c.allow_trans,c.customerlimit,c.dob from userinfo u join customerbasicdetail c on u.userid=c.userid " +
                    " where c.customerid=? ";

            jdbcTemplate.query(sqlQuery, new Object[]{customerID}, (rs, rowNumber) -> {
                String allowTransact = StringUtils.isNotEmpty(rs.getString("allow_trans"))? rs.getString("allow_trans").trim() : "";
                int customerLimit = rs.getInt("customerlimit");
                customerInfoBO.setName(rs.getString("name"));
                customerInfoBO.setCustomerId(rs.getString("customerid"));
                customerInfoBO.setEmail(rs.getString("email"));
                customerInfoBO.setMobile(rs.getString("mobilenumber"));
                customerInfoBO.setActivated(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && customerLimit!=49999);
                customerInfoBO.setDateOfBirth(rs.getString("dob"));
                return customerInfoBO;
            });



        } catch (Exception e) {
            log.error("{}Exception : ", logPrefix, e);
        }

        log.debug("customerInfo email : " + customerInfoBO.getEmail()+"::name"+customerInfoBO.getName());

        return customerInfoBO;
    }
    public CustomerInfoBO getUserCustomerInfo(String email,String mobilenumber) {

        String logPrefix = "#customer email : " + email + " | mobilenumber"+mobilenumber;

        log.debug(logPrefix + "customerBasic details fetch");

        CustomerInfoBO customerInfoBO = new CustomerInfoBO();

        try {

            String sqlQuery = " select u.userid,u.password,c.customerid,c.name,c.allow_trans,c.customerlimit,c.dob from userinfo u left join customerbasicdetail c on u.userid=c.userid " +
                    " where  u.mobilenumber=? ";

            jdbcTemplate.query(sqlQuery, new Object[]{mobilenumber}, (rs, rowNumber) -> {
                String allowTransact = StringUtils.isNotEmpty(rs.getString("allow_trans"))? rs.getString("allow_trans").trim() : "";
                int customerLimit = rs.getInt("customerlimit");
                customerInfoBO.setName(rs.getString("name"));
                customerInfoBO.setCustomerId(rs.getString("customerid"));
                customerInfoBO.setPassword(rs.getString("password"));
                customerInfoBO.setActivated(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && customerLimit!=49999);
                customerInfoBO.setDateOfBirth(rs.getString("dob"));
                return customerInfoBO;
            });



        } catch (Exception e) {
            log.error("{}getUserCustomerInfo Exception : ", logPrefix, e);
        }

        //log.debug("customerInfo email : " + customerInfoBO.getEmail()+"::name"+customerInfoBO.getName());

        return customerInfoBO;
    }
    public CustomerInfoBO getCustomerInfo(String email,String mobilenumber) {

        String logPrefix = "#customer email : " + email + " | mobilenumber"+mobilenumber;

        log.debug(logPrefix + "customerBasic details fetch");

        CustomerInfoBO customerInfoBO = null;

        try {

            String sqlQuery = " select u.userid,u.password,c.customerid,c.name,c.mobilenumber,c.email,c.allow_trans,c.customerlimit,c.dob from userinfo u join customerbasicdetail c on u.userid=c.userid " +
                    " where c.email=? and c.mobilenumber=? ";

            jdbcTemplate.query(sqlQuery, new Object[]{email,mobilenumber}, (rs, rowNumber) -> {
                String allowTransact = StringUtils.isNotEmpty(rs.getString("allow_trans"))? rs.getString("allow_trans").trim() : "";
                int customerLimit = rs.getInt("customerlimit");
                customerInfoBO.setName(rs.getString("name").trim());
                customerInfoBO.setCustomerId(rs.getString("customerid"));
                customerInfoBO.setEmail(rs.getString("email").trim());
                customerInfoBO.setMobile(rs.getString("mobilenumber"));
                customerInfoBO.setPassword(rs.getString("password"));
                customerInfoBO.setActivated(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && customerLimit!=49999);
                customerInfoBO.setDateOfBirth(rs.getString("dob"));
                return customerInfoBO;
            });



        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        //log.debug("customerInfo email : " + customerInfoBO.getEmail()+"::name"+customerInfoBO.getName());

        return customerInfoBO;
    }
    public CustomerInfoBO getCustomerInfoById(String customerId) {

        String logPrefix = "#customer customerId : " + customerId;

        log.debug(logPrefix + "customerBasic details fetch");

        CustomerInfoBO customerInfoBO = null;

        try {

            String sqlQuery = " select u.userid,u.password,c.customerid,c.name,c.mobilenumber,c.email,c.allow_trans,c.customerlimit,c.dob from userinfo u join customerbasicdetail c on u.userid=c.userid " +
                    " where c.customerid=? ";

            jdbcTemplate.query(sqlQuery, new Object[]{customerId}, (rs, rowNumber) -> {
                String allowTransact = StringUtils.isNotEmpty(rs.getString("allow_trans"))? rs.getString("allow_trans").trim() : "";
                int customerLimit = rs.getInt("customerlimit");
                customerInfoBO.setName(rs.getString("name").trim());
                customerInfoBO.setCustomerId(rs.getString("customerid"));
                customerInfoBO.setEmail(rs.getString("email"));
                customerInfoBO.setMobile(rs.getString("mobilenumber"));
                customerInfoBO.setPassword(rs.getString("password"));
                customerInfoBO.setActivated(allowTransact.equalsIgnoreCase(BPConstants.ALLOW_TRANSACT_ACTIVE) && customerLimit!=49999);
                customerInfoBO.setDateOfBirth(rs.getString("dob"));
                return customerInfoBO;
            });



        } catch (Exception e) {
            log.error(logPrefix + "Exception : ", e);
        }

        //log.debug("customerInfo email : " + customerInfoBO.getEmail()+"::name"+customerInfoBO.getName());

        return customerInfoBO;
    }
    public List<CustomerInfoBO> getCustomerInfoList(int type, String sqlTypeString) {

        String logPrefix = "#customer type : " + type + " | sqlString"+sqlTypeString;

        log.debug(logPrefix + "customerBasic details fetch");

        List<CustomerInfoBO> customerInfoBOList = new ArrayList<>();
        String typeValue="";
        if(type==0)
         typeValue= sqlTypeString;
        else typeValue=  "'" + sqlTypeString + "%'";
        String appendSql="";
        switch (type) {
            case 0:
                appendSql = "u.userid =?";
                break;
            case 1:
                appendSql = "u.name like ?  ";
                break;
            case 2:
                appendSql = "u.email like ?";
                break;
            case 3:
                appendSql = "u.mobilenumber like ?";
                break;
            case 4:
                appendSql = "c.customerid like ?";
                break;
            case 5:
                appendSql = "c.name like ?";
                typeValue=typeValue+","+typeValue;
                break;
//            case 6:
//                appendSql = "c.email like " + typeValue;
//                break;
//            case 7:
//                appendSql = "c.mobilenumber like " + typeValue;
//                break;
            default:
                appendSql = "c.email like " + typeValue;
                break;
        }



        try {

            String sqlQuery = " select u.userid,u.email,u.mobilenumber,c.customerid,c.name,c.allow_trans,c.customerlimit,c.dob from userinfo u join customerbasicdetail c on u.userid=c.userid where "+appendSql;
        if(type==0)
            customerInfoBOList=jdbcTemplate.query(sqlQuery,new CustomerMapper(),Integer.parseInt(typeValue));
        else
            customerInfoBOList=jdbcTemplate.query(sqlQuery,new CustomerMapper(),typeValue);



        } catch (Exception e) {
            log.error("{}Exception : ", logPrefix, e);
        }

        log.debug("customerInfo detail sixe : " + customerInfoBOList.size());

        return customerInfoBOList;
    }

    public int updateUserinfo(CustomerRequest customerRequest,int userid) {

        String logPrefix ="getName : " + customerRequest.getName() + "#UserId : " + customerRequest.getUserId() + " | #email : " + customerRequest.getEmailId() + " | ";

        int status = 0;
        String enPassword=encryptionDecryption.Encryption(customerRequest.getPassword());

        try {

            String sqlQuery = " update userinfo set \"name\"=?,email=?,\"password\"=?,updated_user = ?, updated_at = now(),email_marketing=?,mobile_marketing=? where userid=? and mobilenumber=? and verified='TRUE' and lockflag='A'";

            status = jdbcTemplate.update(sqlQuery, (PreparedStatementSetter) ps -> {
                ps.setString(1, customerRequest.getName());
                ps.setString(2, customerRequest.getEmailId());
                ps.setString(3, enPassword);
                ps.setInt(4, userid);
                ps.setInt(5, customerRequest.getMarketing());
                ps.setInt(6, customerRequest.getMarketing());
                ps.setInt(7, userid);
                ps.setString(8, customerRequest.getMobileNumber());
            });

        } catch (Exception e) {
            log.error("{}Exception in updateUserinfo: ", logPrefix, e);
        }
        return status;
    }


}

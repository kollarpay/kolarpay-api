package com.kp.core.app.model.Products;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPrice {

    @SerializedName("id")
    @Expose
    public Long id;
    @SerializedName("product_id")
    @Expose
    public Long productId;
    @SerializedName("base_price")
    @Expose
    public Long base_price;
    @SerializedName("created_at")
    @Expose
    public String createdAt;

    @SerializedName("expire_at")
    @Expose
    public String expireAt;
    @SerializedName("active")
    @Expose
    public String active;

}

package com.kp.core.app.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetailsBO {
    private String orderID;
    private String productID;
    private float price;
    private int quantity;
    private float discount;
    private float total;
    private int sizeID;
    private int colorID;
    private int shipperID;
    private String shipperDate;
    private float fright;
    private float salesTax;
    private int createdUser;
    private String createdDate;
    private int updatedUser;
    private String updatedDate;

}

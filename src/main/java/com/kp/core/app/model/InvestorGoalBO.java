package com.kp.core.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class InvestorGoalBO {

    private int goalId;
    private String goalName;
    private String goalType;
    private double amount;
    private String target;
    private AdjustedBO adjusted;
    private String riskType;
    private Boolean questionnaire;
    private CustomAssetBO customAssetAllocation;
    private boolean additionalInvestment;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AdjustedBO {

        private String target;
        private double amount;
        private double inflation;
    }

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CustomAssetBO {

        private double equity;
        private double debt;
        private double gold;
    }
}

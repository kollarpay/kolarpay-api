package com.kp.core.app.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
@Component
public class DateUtil {

    public static String getTDate() {
        Date d1 = new Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        return sdf.format(c1.getTime());
    }
    public String addNoofDays(int noOfDays) {
        String finalDate = "";
        try {
            Date todayDate = new Date();
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Calendar now = Calendar.getInstance();
            now.setTime(todayDate);
            now.add(Calendar.DATE, noOfDays);
            finalDate = sdf.format(now.getTime());
        } catch (Exception e) {
            log.error("Ex : " + e.getMessage());
        }
        return finalDate;
    }
    public String addNoofDays(String givenDate, int noOfDays) {
        String finalDate = "";
        try {
            Date todayDate = new Date();
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar now = Calendar.getInstance();

            todayDate = sdf.parse(givenDate);
            now.setTime(todayDate);
            now.add(Calendar.DATE, noOfDays);
            finalDate = sdf.format(now.getTime());
        } catch (Exception e) {
            log.error("Ex : " + e.getMessage());
        }
        return finalDate;
    }
    public String getTodayDateXLformat() {
        java.util.Date d1 = new Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        return sdf.format(c1.getTime());
    }
    public String getDateofDay(String givenDate) {
        String finalDate = "";
        try {
            Date todayDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("EEE");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            todayDate = dateFormat.parse(givenDate);
            sdf.applyPattern("EEE");
            finalDate = sdf.format(todayDate);
        } catch (Exception e) {
            log.error("Ex : " + e.getMessage());
        }
        return finalDate;
    }

    public static String addMonthsToDate(int noOfMonths, String dateStr, String pattern) {

        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(pattern);
        Calendar calendar = Calendar.getInstance();

        try {
            if(StringUtils.isEmpty(dateStr))
                dateStr = getTDate();

            Date d1 = sdf.parse(dateStr);
            calendar.setTime(d1);
            calendar.add(Calendar.MONTH, noOfMonths);

        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        return sdf.format(calendar.getTime());
    }

    public static String getDateGivenFormat(String dateValue, String pattern) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateReturnValue = "";
        try {
            Date date = formatter.parse(String.valueOf(dateValue));
            DateFormat Tempformatter = new SimpleDateFormat(pattern);
            dateReturnValue = Tempformatter.format(date);
        } catch (Exception e) {
            log.error(" Exception in getDisplayDate : " + e.getMessage());
        }
        return dateReturnValue;
    }

    public static String getDBDate(String dateString, String pattern) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        try {
            java.util.Date d = dateFormat.parse(dateString);
            dateFormat.applyPattern("yyyy-MM-dd");
            dateString = dateFormat.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

}

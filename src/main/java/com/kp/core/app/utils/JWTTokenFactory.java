package com.kp.core.app.utils;

import com.kp.core.app.config.JWTSettings;
import com.kp.core.app.enums.Scopes;
import com.kp.core.app.model.AccessJwtToken;
import com.kp.core.app.model.JwtToken;
import com.kp.core.app.model.UserDetailsBO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

@Component
@Slf4j
public class JWTTokenFactory {

    private final JWTSettings settings = ConfigFactory.create(JWTSettings.class);

    private Key getSigningKey() {
        return Keys.hmacShaKeyFor(settings.tokenSigningKey().getBytes());
    }


    public AccessJwtToken createAccessJwtToken(UserDetailsBO userBo) {
        if (StringUtils.isBlank(userBo.getUserName()))
            throw new IllegalArgumentException("Cannot create JWT Token without username");

        Claims claims = Jwts.claims().setSubject(String.valueOf(userBo.getUserId()));
        claims.put("scopes", "read,write");
        if(userBo.getUserId() >0)
        {
            claims.put("name", userBo.getUserName());
            claims.put("email", userBo.getEmail());
            claims.put("mobile", userBo.getMobileNumber());
            claims.put("role",userBo.getRole());
            claims.put("userid",userBo.getUserId());
            if(userBo.getRole()!=null && !BPConstants.NORMAL_USER_TYPE.equalsIgnoreCase(userBo.getRole())){
                claims.put("impersonated_user_id", userBo.getRole());
                claims.put("impersonated_role", userBo.getRole());
            }
        }


        LocalDateTime currentTime = LocalDateTime.now();
     Date expirationDt=   Date.from(
                currentTime
                        .plusMinutes(settings.tokenExpirationTime())
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
        Date issuedDt=Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant());
        String token =
                Jwts.builder()
                        .setClaims(claims)
                        .setIssuer(settings.tokenIssuer())
                        .setId(UUID.randomUUID().toString())
                        .setIssuedAt(issuedDt)
                        .setExpiration(expirationDt)
                        .signWith(getSigningKey())
                        .compact();

        return new AccessJwtToken(token, claims);
    }
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }
    private Claims extractAllClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }
    public JwtToken createRefreshToken(UserDetailsBO userBo) {
        if (StringUtils.isBlank(userBo.getUserName())) {
            throw new IllegalArgumentException("Cannot create JWT Token without username");
        }

        LocalDateTime currentTime = LocalDateTime.now();

        Claims claims = Jwts.claims().setSubject(String.valueOf(userBo.getUserId()));
        claims.put("scopes", List.of(Scopes.REFRESH_TOKEN.authority()));

        String token =
                Jwts.builder()
                        .setClaims(claims)
                        .setIssuer(settings.tokenIssuer())
                        .setId(UUID.randomUUID().toString())
                        .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
                        .setExpiration(
                                Date.from(
                                        currentTime
                                                .plusMinutes(settings.refreshTokenExpTime())
                                                .atZone(ZoneId.systemDefault())
                                                .toInstant()))
                        .signWith(getSigningKey())
                        .compact();

        return new AccessJwtToken(token, claims);
    }
}

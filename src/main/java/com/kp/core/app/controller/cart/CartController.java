package com.kp.core.app.controller.cart;

import com.google.gson.Gson;
import com.kp.core.app.enums.ResponseDataObjectType;
import com.kp.core.app.model.CartBO;
import com.kp.core.app.model.GlobalResponseBO;
import com.kp.core.app.model.Products.ProductBO;
import com.kp.core.app.service.CartService;
import com.kp.core.app.service.Collection.CollectionService;
import com.kp.core.app.utils.APIGeneralUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/cart")
public class CartController {

    @Autowired
    CartService cartService;
    @Autowired
    Gson gson;
    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GlobalResponseBO> createProduct(@RequestHeader MultiValueMap<String, Object> headers, @RequestBody String payload) throws Exception {

        log.debug("#cart create POST API Request");
        GlobalResponseBO globalResponseBO = new GlobalResponseBO(ResponseDataObjectType.object, "cartCreate");

        int userId = 0;
        try {
            userId = Integer.parseInt(APIGeneralUtil.getHeader(headers, "user-id"));
            log.debug("#UserId : " + userId);

            if(userId > 0) {

                CartBO cartBO = gson.fromJson(payload, CartBO.class);
                globalResponseBO = cartService.createCart(cartBO, userId);

            }

        } catch (Exception e) {
            log.error("#UserId : " + userId + "| Exception : ", e);
        }
        log.debug("#cart create POST API Response : " + globalResponseBO.toString());
        return ResponseEntity.status(globalResponseBO.getCode()).body(globalResponseBO);
    }
}

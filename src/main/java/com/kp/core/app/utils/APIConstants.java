package com.kp.core.app.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class APIConstants {
    public static final String JSON_RESPONSE = "application/json";
    public static final int SERVER_ERROR_INT = 500;
    public static final int BAD_REQUEST = 400;
    public static final int UNAUTHORIZED = 401;
    public static final int NOT_FOUND = 404;
    public static final int SUCCESS_INT = 200;
    public static final String BAD_REQUEST_STRING = "Bad request";
    public static final String NOT_FOUND_STRING = "Request not found";
    public static final String UNAUTHORIZED_STRING = "UnAuthorized";
    public static final String SUCCESS_STRING = "Success";
    public static final String VALID_STRING = "VALID";
    public static final String INVALID_STRING = "INVALID";
    public static final String INVALID_INPUT_STRING = "Invalid input";
    public static final String ERROR_STRING = "ERROR";
    public static final String SERVER_ERROR_STRING = "INTERNAL SERVER ERROR";
    public static final int RESPONSE_ERROR_CODE = 500;
    public static final String ERROR_LIST = "ErrorList";
    public static final String IST = " now() AT TIME ZONE 'Asia/Kolkata'";
    public static final String EMAIL_PATTERN_FORMAT = "^(.+)@(\\S+)$";
}

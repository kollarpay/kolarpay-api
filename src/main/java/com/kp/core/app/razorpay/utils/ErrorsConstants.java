package com.kp.core.app.razorpay.utils;

public class ErrorsConstants {

    public static final String RAZORPAY_PRODUCT_CODE = "17";


    //I/O VALIDATION
    public static final String REQUIRED_DATA_NOT_FOUND = "1001";
    public static final String INVALID_VPA = "1103";
    public static final String INVALID_PAYMENTID = "1106";
    public static final String INVALID_BANK_INFO = "1108";
    public static final String INELIGIBLE_USER = "1101";
    public static final String INVALID_PAYMENT_MODE = "1111";
    public static final String INVALID_CONTACT_NO = "1112";

    //BUSINESS ERRORS - custom-account
    public static final String VIRTUAL_ACCOUNT_EXIST = "2111";

    //INTERNAL API INTEGRATION
    public static final String INTERNAL_API_FAIL = "9001";

    //EXTERNAL-THIRD PARTY API INTEGRATION
    public static final String CUSTOMER_ACCOUNT_API_FAIL = "9102";
    public static final String VALIDATE_VPA_API_FAIL = "9104";
    public static final String PAYMENT_INITIATE_API_FAIL = "9105";
    public static final String ORDER_CREATION_API_FAIL = "9106";
    public static final String CUSTOMER_EDIT_API_FAIL = "9109";

    public static final String INVALID_CUSTOMERID = "9110";
    public static final String INVALID_ORDER_ID = "9111";

    //Custom Account
    public static final String BANK_NOT_SUPPORTED   =   "1104";
    public static final String INPUT_ERROR_VALIDATION   =   "1105";
    public static final String CUSTOM_ACCOUNT_SERVICE   =   "9102";
    public static final String CUSTOM_ACCOUNT_DOES_NOT_EXIST   =   "1107";
    public static final String VIRTUAL_ACCOUNT_SERVICE   =   "9103";
    public static final String VIRTUAL_ACCOUNT_DOES_NOT_EXIST  =   "2113";
    public static final String CUSTOMERID_INVALID_CONTACT = "9112";
    public static final String CUSTOMER_EMAIL_UPDATE_FAILED = "9113";

    //DB
    public static final String DB_INSERT_FAIL = "8201";
    public static final String DB_UPDATE_FAIL = "8202";

    //SYSTEM
    public static final String SERVER_ERROR = "8000";

    public static final String ROUTE_TRANSFER_EXIST = "2107";
    public static final String ROUTE_TRANSFER_ACCOUNT_MAPPING_NOT_AVAILABLE = "2105";
    public static final String ROUTE_TRANSFER_AMOUNT_MISMATCH = "2104";
    public static final String ROUTE_TRANSFER_SERVICE_NOT_AVAILABLE = "9107";
    public static final String VIRTUAL_ACCOUNT_EDIT_SERVICE_NOT_AVAILABLE = "9108";
    public static final String WEBHOOK_PAYLOAD_PARSE_ERROR = "2108";
    public static final String VIRTUAL_ACCOUNT_EDIT_MODIFY_ERROR = "2115";
    public static final String VIRTUAL_ACCOUNT_EDIT_BANK_ADDITION_ERROR = "2116";
    public static final String VIRTUAL_ACCOUNT_EDIT_BANK_DELETION_ERROR = "2117";

    public static final String PAYMENT_HEALTH_STATUS_METHOD_NOT_AVAILABLE = "1100";
    public static final String PAYMENT_HEALTH_STATUS_GATEWAY_NOT_AVAILABLE = "9100";
    public static final String PAYMENT_HEALTH_STATUS_GATEWAY_SERVICE_UNAVAILABLE = "9101";

    public static final String PAYMENT_UPDATE_STATUS_ERROR = "2118";
    public static final String PAYMENT_INFO_FETCH_ERROR = "2119";


}
